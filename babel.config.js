module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module:react-native-dotenv',
      {
        moduleName: '@env',
        path: '.env',
        blocklist: null,
        allowlist: ['BASE_URL', 'MAPS_API_KEY'],
        blacklist: null, // DEPRECATED
        whitelist: null, // DEPRECATED
        safe: true,
        allowUndefined: false,
        verbose: false,
      },
    ],
  ],
};
