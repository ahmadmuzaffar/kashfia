import React, {Component} from 'react';
import {
  Image,
  FlatList,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Colors from '../../utils/Colors';
import CardView from 'react-native-cardview';
import Globals from '../../utils/Globals';

export default class DocTags extends Component {
  constructor(props) {
    super(props);

    this.window = Dimensions.get('window');
  }

  Item({tag, index, cancelListener}) {
    const {height, width} = this.window;

    return (
      <View
        key={`${tag}-${index}`}
        style={{
          flexDirection: 'row',
          backgroundColor: Colors.tagBackground,
          padding: '2%',
          marginRight: '2%',
          marginTop: '3%',
          borderRadius: 5,
        }}>
        <Text
          style={{
            fontFamily: 'urbanist_regular',
            color: Colors.text_dark,
            fontSize: height * 0.015,
          }}>
          {tag}
        </Text>
        <TouchableOpacity
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginLeft: '6%',
          }}
          onPress={cancelListener}>
          <Image
            style={{
              height: height * 0.012,
              width: height * 0.012,
              resizeMode: 'contain',
            }}
            source={require('../../assets/delete.png')}
          />
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    return (
      <FlatList
        style={{
          marginTop: Globals.pHorizontal,
        }}
        showsHorizontalScrollIndicator={false}
        horizontal
        data={this.props.data}
        renderItem={({item}) => (
          <this.Item
            tag={item.tag}
            index={item.index}
            cancelListener={item.cancelListener}
          />
        )}
        keyExtractor={item => item.index}
      />
    );
  }
}
