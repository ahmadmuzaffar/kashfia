import React, {Component} from 'react';
import {
  Image,
  FlatList,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
} from 'react-native';
import Colors from '../../utils/Colors';
import CardView from 'react-native-cardview';

export default class SpecializationList extends Component {
  constructor(props) {
    super(props);
  }

  Item({logo, text, clickListener, height, width}) {
    return (
      <CardView
        style={{flex: 0.5, margin: '2%'}}
        cardElevation={5}
        cardMaxElevation={5}
        cornerRadius={10}>
        <TouchableOpacity
          onPress={clickListener}
          style={{
            paddingVertical: 10,
            paddingHorizontal: 5,
            backgroundColor: Colors.white,
          }}>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 10,
            }}>
            <Image
              style={{
                width: 70,
                height: 70,
                resizeMode: 'contain',
              }}
              source={logo}
            />
            <Text
              style={{
                fontSize: height * 0.021,
                marginTop: height * 0.03,
                fontFamily: 'urbanist_bold',
              }}>
              {text}
            </Text>
          </View>
        </TouchableOpacity>
      </CardView>
    );
  }

  render() {
    const {height, width} = this.props;
    return (
      <FlatList
        style={{
          marginVertical: 15,
        }}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        numColumns={2}
        data={this.props.data}
        renderItem={({item}) => (
          <this.Item
            logo={item.logo}
            text={item.text}
            clickListener={item.clickListener}
            height={height}
            width={width}
          />
        )}
        keyExtractor={item => item.id}
      />
    );
  }
}
