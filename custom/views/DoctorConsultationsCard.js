import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import StarRating from 'react-native-star-rating';
import Colors from '../../utils/Colors';
import CardView from 'react-native-cardview';

export default class DoctorConsultationsCard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <CardView
        style={this.props.style}
        cardElevation={15}
        cardMaxElevation={15}
        cornerRadius={10}>
        <View
          style={{
            borderRadius: 10,
            backgroundColor: Colors.white,
          }}>
          <View style={{flexDirection: 'row'}}>
            {this.props.cardStatusVisibility && (
              <View
                style={{
                  backgroundColor: this.props.cardStatusColor,
                  width: 10,
                  borderBottomLeftRadius: 10,
                  borderTopLeftRadius: 10,
                }}
              />
            )}
            <View>
              <View
                style={{
                  padding: 10,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text style={this.props.meetingTimeStyle}>
                    {this.props.meetingTime}
                  </Text>
                  {this.props.buttonVisibility && (
                    <TouchableOpacity
                      onPress={this.props.mainButtonListener}
                      style={
                        this.props.mainButtonStyle !== undefined
                          ? this.props.mainButtonStyle
                          : {
                              backgroundColor: Colors.light_secondary,
                              padding: 10,
                              alignItems: 'center',
                              borderRadius: 5,
                            }
                      }>
                      <Text
                        style={
                          this.props.mainButtonTextStyle !== undefined
                            ? this.props.mainButtonTextStyle
                            : {
                                color: Colors.secondary,
                                fontSize: 12,
                                fontFamily: 'urbanist_semi_bold',
                              }
                        }>
                        {this.props.mainButtonText !== undefined
                          ? this.props.mainButtonText
                          : 'Online Consultation'}
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    marginTop: 10,
                  }}>
                  <Image
                    style={{height: 40, width: 40, borderRadius: 20}}
                    source={this.props.avatar}
                  />
                  <View
                    style={{
                      marginHorizontal: 5,
                      marginVertical: 5,
                      flex: 1,
                    }}>
                    <Text
                      style={{
                        fontSize: 14,
                        color: Colors.text_dark,
                        fontFamily: 'urbanist_semi_bold',
                      }}>
                      {this.props.patientName}
                    </Text>

                    <View style={{flexDirection: 'row', marginTop: 5}}>
                      <StarRating
                        disabled={false}
                        maxStars={5}
                        starSize={15}
                        halfStarColor={Colors.star}
                        fullStarColor={Colors.star}
                        emptyStarColor={'#E7E7E7'}
                        rating={this.props.rating}
                        starStyle={{marginHorizontal: '1%'}}
                        selectedStar={rating => this.onStarRatingPress(rating)}
                      />
                      <Text
                        style={{
                          fontSize: 14,
                          fontFamily: 'urbanist_semi_bold',
                          marginLeft: 10,
                          color: Colors.text_light,
                        }}>
                        {this.props.totalReviews} Reviews
                      </Text>
                    </View>
                  </View>
                  {this.props.locationIconVisibility && (
                    <TouchableOpacity
                      onPress={this.props.onLocationClick}
                      style={{
                        height: 40,
                        width: 40,
                        marginRight: '1%',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: Colors.green,
                        borderRadius: 20,
                      }}>
                      <Image
                        style={{
                          tintColor: Colors.white,
                          resizeMode: 'contain',
                          height: 22,
                          width: 22,
                        }}
                        source={require('../../assets/direction.png')}
                      />
                    </TouchableOpacity>
                  )}
                  {this.props.messageIconVisibility && (
                    <TouchableOpacity
                      onPress={this.props.onMessageClick}
                      style={{
                        height: 40,
                        width: 40,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: Colors.secondary,
                        borderRadius: 20,
                      }}>
                      <Image
                        style={{
                          tintColor: Colors.white,
                          resizeMode: 'contain',
                          height: 18,
                          width: 18,
                        }}
                        source={require('../../assets/messages.png')}
                      />
                    </TouchableOpacity>
                  )}
                </View>

                {this.props.locVisibility && (
                  <View style={{flexDirection: 'row', marginTop: 10}}>
                    <Image
                      style={{
                        height: 20,
                        width: 20,
                        marginRight: 7,
                        resizeMode: 'contain',
                      }}
                      source={require('../../assets/location.png')}
                    />
                    <Text style={{fontFamily: 'urbanist_regular'}}>
                      Al Mansour, Baghdad, Iraq
                    </Text>
                  </View>
                )}
              </View>

              <View
                style={{
                  borderColor: '#F3F3F3',
                  borderWidth: 1,
                }}
              />

              <View style={{padding: 10}}>
                <Text
                  style={{
                    fontSize: 14,
                    marginRight: '3%',
                    fontFamily: 'urbanist_regular',
                    color: Colors.text_light,
                  }}>
                  {this.props.description !== undefined
                    ? this.props.description
                    : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nondiam pellentesque volutpat id aliquam tortor ipsum necporttitor.'}
                </Text>

                {this.props.reportLayout && (
                  <View
                    style={{
                      flexDirection: 'row',
                      marginTop: 15,
                      alignItems: 'center',
                    }}>
                    <TouchableOpacity style={{flex: 1}}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        <Image
                          style={{
                            height: 20,
                            width: 20,
                            resizeMode: 'contain',
                          }}
                          source={require('../../assets/danger.png')}
                        />
                        <Text
                          style={{
                            fontFamily: 'urbanist_regular',
                            color: '#FF7474',
                            marginLeft: 7,
                          }}>
                          Report User
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <Text
                      style={{
                        color: Colors.text_light,
                        fontSize: 12,
                        fontFamily: 'urbanist_regular',
                      }}>
                      2 minutes ago
                    </Text>
                  </View>
                )}

                {this.props.numOfApplicatons && (
                  <View
                    style={{
                      flexDirection: 'row',
                      marginTop: '5%',
                      justifyContent: 'space-evenly',
                    }}>
                    <Text
                      style={{
                        fontFamily: 'urbanist_regular',
                        color: Colors.gray,
                      }}>
                      5 Nurses applied
                    </Text>
                    <Text
                      style={{
                        fontFamily: 'urbanist_regular',
                        color: Colors.gray,
                      }}>
                      Lowest Offer: 25 IQD
                    </Text>
                  </View>
                )}

                {this.props.jobBtn && (
                  <View
                    style={{
                      width: '100%',
                      marginTop: 15,
                    }}>
                    <TouchableOpacity style={styles.button1}>
                      <Text
                        style={{
                          color: Colors.white,
                          alignSelf: 'center',
                          fontSize: 16,
                          fontFamily: 'urbanist_bold',
                        }}>
                        Apply to this job
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
              </View>
            </View>
          </View>
        </View>
      </CardView>
    );
  }
}

const styles = StyleSheet.create({
  button1: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    borderRadius: 10,
  },
});
