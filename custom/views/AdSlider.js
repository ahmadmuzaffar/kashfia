import React, {Component} from 'react';
import {Image, FlatList, View, Text, TouchableOpacity} from 'react-native';
import Colors from '../../utils/Colors';
import CardView from 'react-native-cardview';
import Globals from '../../utils/Globals';

export default class AdSlider extends Component {
  constructor(props) {
    super(props);
  }

  Item({logo, title, description, buttonText, buttonListener, ad_pic}) {
    return (
      <View style={{paddingHorizontal: Globals.pHorizontal}}>
        <CardView
          cardElevation={0}
          cardMaxElevation={0}
          cornerRadius={Globals.sliderBorderRadius}
          style={{
            flexDirection: 'row',
            width: Globals.sliderCardWidth,
            height: Globals.sliderCardHeight,
            backgroundColor: Colors.secondary,
          }}>
          <View style={{flex: 1, margin: '4%'}}>
            <Image
              style={{
                height: 30,
                width: 80,
                resizeMode: 'contain',
                tintColor: Colors.white,
              }}
              source={logo}
            />
            <Text
              style={{
                fontFamily: 'urbanist_bold',
                color: Colors.white,
                marginTop: 10,
                fontSize: 16,
              }}>
              {title}
            </Text>
            <Text
              style={{
                fontFamily: 'urbanist_regular',
                color: '#DAE7FF',
                fontSize: 12,
              }}>
              {description}
            </Text>
            <TouchableOpacity
              onPress={buttonListener}
              style={{
                borderRadius: 10,
                borderColor: Colors.white,
                borderWidth: 1,
                marginTop: 10,
                alignItems: 'center',
                justifyContent: 'center',
                width: '60%',
                padding: '4%',
              }}>
              <Text
                style={{
                  fontFamily: 'urbanist_regular',
                  color: Colors.white,
                  fontSize: 12,
                }}>
                {buttonText}
              </Text>
            </TouchableOpacity>
          </View>
          <Image
            style={{
              marginTop: '3%',
              height: Globals.sliderImageHeight,
              width: Globals.sliderImageWidth,
              resizeMode: 'cover',
            }}
            source={ad_pic}
          />
        </CardView>
      </View>
    );
  }

  render() {
    return (
      <FlatList
        pagingEnabled
        disableintervalmomentum
        removeClippedSubviews
        bounces
        style={{
          marginTop: Globals.pHorizontal,
        }}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        horizontal
        data={this.props.data}
        renderItem={({item}) => (
          <this.Item
            logo={item.logo}
            title={item.title}
            description={item.description}
            buttonText={item.buttonText}
            buttonListener={item.buttonListener}
            ad_pic={item.ad_pic}
          />
        )}
        keyExtractor={item => item.id}
      />
    );
  }
}
