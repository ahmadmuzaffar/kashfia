import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  Alert,
  Dimensions,
  View,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
  Image,
  BackHandler,
} from 'react-native';
import Colors from '../utils/Colors';
import SharedPreferences from 'react-native-shared-preferences';
import {CommonActions} from '@react-navigation/native';

export default class StartScreen extends Component {
  constructor(props) {
    super(props);

    this.screenHeight = Dimensions.get('screen').height;
    this.windowHeight = Dimensions.get('window').height;
    this.navbarHeight =
      this.screenHeight - this.windowHeight + StatusBar.currentHeight;
    this.backButtonPress = this.backButtonPress.bind(this);
  }

  backButtonPress = () => {
    BackHandler.exitApp();
    return true;
  };

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.backButtonPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backButtonPress);
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="dark-content" backgroundColor={Colors.white} />

        <Image
          style={styles.image}
          source={require('../assets/splash_2.png')}
        />

        <Text style={styles.discalimerOne}>Complete Health Solutions</Text>

        <Text style={styles.discalimerTwo}>
          Early Protection for family Health
        </Text>

        <View
          style={{
            bottom: 0,
            position: 'absolute',
            width: '100%',
            marginBottom: this.navbarHeight / 2,
          }}>
          <TouchableOpacity
            style={styles.button1}
            onPress={() => {
              this.props.navigation.navigate('sign_in', {
                auth_type: 'sign_up',
              });
            }}>
            <Text
              style={{
                color: Colors.white,
                alignSelf: 'center',
                fontSize: 16,
                fontFamily: 'urbanist_bold',
              }}>
              Join Now
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.button2}
            onPress={() => {
              this.props.navigation.navigate('sign_in', {
                auth_type: 'sign_in',
              });
            }}>
            <Text
              style={{
                color: Colors.secondary,
                alignSelf: 'center',
                fontSize: 16,
                fontFamily: 'urbanist_bold',
              }}>
              Sign In
            </Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.white,
  },
  image: {
    width: 330,
    height: 250,
    marginTop: 20,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  discalimerOne: {
    marginLeft: 30,
    marginTop: 10,
    fontSize: 32,
    fontFamily: 'urbanist_bold',
    color: Colors.text_dark,
  },
  discalimerTwo: {
    marginLeft: 30,
    fontSize: 18,
    fontFamily: 'urbanist_regular',
    color: Colors.text_light,
  },
  button1: {
    width: '90%',
    alignSelf: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    marginVertical: 5,
    fontSize: 16,
    fontFamily: 'urbanist_bold',
    borderRadius: 10,
  },
  button2: {
    width: '90%',
    alignSelf: 'center',
    backgroundColor: Colors.white,
    paddingVertical: 15,
    marginVertical: 5,
    fontSize: 16,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: Colors.secondary,
  },
});
