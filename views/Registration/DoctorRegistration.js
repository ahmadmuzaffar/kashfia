import React, {Component} from 'react';
import DocumentPicker from 'react-native-document-picker';
import {
  BackHandler,
  Platform,
  Text,
  Dimensions,
  StyleSheet,
  SafeAreaView,
  StatusBar,
  ToastAndroid,
  TouchableOpacity,
  PermissionsAndroid,
  ScrollView,
  Image,
  TextInput,
  View,
} from 'react-native';
import Colors from '../../utils/Colors';
import {CommonActions} from '@react-navigation/native';
import axios from 'react-native-axios';
import Tags from 'react-native-tags';
import {ProgressBar} from 'react-native-material-indicators';
import {YellowBox} from 'react-native';
import SharedPreferences from 'react-native-shared-preferences';
import {BASE_URL} from '@env';
import storage from '@react-native-firebase/storage';
import ImageResizer from 'react-native-image-resizer';
import Globals from '../../utils/Globals';

export default class DoctorRegistration extends Component {
  state = {
    buttonStyle: styles.button2,
    totalGraduationCertificates: '',
    graduationCertificates: [],
    syndicateId: '',
    totalSyndicateIdCertificates: '',
    syndicateIdCertificates: [],
    totalProfessionalPracticeCertificates: '',
    professionalPracticeCertificates: [],
    totalSpecializationCertificates: '',
    specializationCertificates: [],
    totalClinicCertificates: '',
    clinicCertificates: [],
    visible: false,
    buttonDisability: false,
    counter: 0,
    rejection1: '',
    rejection2: '',
    rejection3: '',
    rejection4: '',
    rejection5: '',
    issues: Globals.issues,
  };

  constructor(props) {
    super(props);
    this.body = {};
    YellowBox.ignoreWarnings(['Warning: ...']);
    console.disableYellowBox = true;
    this.user = Globals.user;
    this.imageName = '';
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.requestCameraPermission = this.requestCameraPermission.bind(this);
    this.imageResizer = this.imageResizer.bind(this);
    this.uploadToFirebase = this.uploadToFirebase.bind(this);

    this.window = Dimensions.get('window');
    this.screenHeight = Dimensions.get('screen').height;
    this.windowHeight = Dimensions.get('window').height;
    this.navbarHeight =
      this.screenHeight - this.windowHeight + StatusBar.currentHeight;
  }

  async requestCameraPermission(key) {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Kashfia App Camera Permission',
          message:
            'Kashfia App needs access to your camera ' +
            'so you can take pictures.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        try {
          const response = await DocumentPicker.pickMultiple({
            type: [DocumentPicker.types.images, DocumentPicker.types.pdf],
            copyTo: 'cachesDirectory',
          });
          if (key === 'graduation_certificates') {
            this.populateGraduatesCertificate(response);
          } else if (key === 'syndicate_id_certificates') {
            this.populateSyndicateIdCertificate(response);
          } else if (key === 'professional_practice_certificates') {
            this.populateProfessionalPractice(response);
          } else if (key === 'specialization_certificates') {
            this.populateSpecializationCertificates(response);
          } else if (key === 'clinic_certificates') {
            this.populateClinicCertificates(response);
          }
        } catch (err) {
          if (DocumentPicker.isCancel(err)) {
          } else {
            throw err;
          }
        }
      } else {
        console.log('Permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );

    const user = this.user;
    if (
      this.props.route !== undefined &&
      this.props.route.params !== undefined
    ) {
      this.body = this.props.route.params.body;
    } else {
      this.body = {
        name: user.fullName,
        gender: user.gender,
        dob: user.dateObj.toISOString(),
        email: user.email,
        district: user.district,
        city: user.city,
        specialization: user.specialization,
        country: user.country,
        role: user.role,
      };
    }
    this.setState(
      {
        totalGraduationCertificates:
          user.graduationCertificates.tags.length > 0
            ? user.graduationCertificates.tags.length + ' file(s) selected'
            : '',
        graduationCertificates:
          user.graduationCertificates.tags.length > 0
            ? user.graduationCertificates.tags
            : this.state.graduationCertificates,
        syndicateId: user.syndicateId,
        totalSyndicateIdCertificates:
          user.syndicateIdDocuments.tags.length > 0
            ? user.syndicateIdDocuments.tags.length + ' file(s) selected'
            : '',
        syndicateIdCertificates:
          user.syndicateIdDocuments.tags.length > 0
            ? user.syndicateIdDocuments.tags
            : this.state.syndicateIdCertificates,
        totalProfessionalPracticeCertificates:
          user.professionalPracticeLetters.tags.length > 0
            ? user.professionalPracticeLetters.tags.length + ' file(s) selected'
            : '',
        professionalPracticeCertificates:
          user.syndicateIdDocuments.tags.length > 0
            ? user.professionalPracticeLetters.tags
            : this.state.professionalPracticeCertificates,
        totalSpecializationCertificates:
          user.specializationCertificates.tags.length > 0
            ? user.specializationCertificates.tags.length + ' file(s) selected'
            : '',
        specializationCertificates:
          user.specializationCertificates.tags.length > 0
            ? user.specializationCertificates.tags
            : this.state.specializationCertificates,
        totalClinicCertificates:
          user.clinicalCertificate.tags.length > 0
            ? user.clinicalCertificate.tags.length + ' file(s) selected'
            : '',
        clinicCertificates:
          user.clinicalCertificate.tags.length > 0
            ? user.clinicalCertificate.tags
            : this.state.clinicCertificates,
      },
      this.handleChange,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.navigate('basic_registration', {
      go_back_key: this.props.navigation.key,
    });
    return true;
  }

  populateGraduatesCertificate(response) {
    let i = 0;
    if (this.state.graduationCertificates.length + response.length > 3) {
      ToastAndroid.showWithGravityAndOffset(
        'Max 3 files can be uploaded',
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        25,
        50,
      );
      return;
    }
    for (const res of response) {
      let mb = res.size / 1024.0 / 1024.0;
      mb = mb.toFixed(2);
      let entry = {
        tag: res.name + '  (' + mb + ' MB)',
        filename: res.name,
        type: res.type,
        uri: res.uri,
        path: res.fileCopyUri,
      };
      this.state.graduationCertificates.push(entry);
    }
    this.setState(
      {
        graduationCertificates: this.state.graduationCertificates,
        totalGraduationCertificates:
          this.state.graduationCertificates.length + ' file(s) selected',
      },
      this.handleChange,
    );
  }

  populateSyndicateIdCertificate(response) {
    let i = 0;
    if (this.state.syndicateIdCertificates.length + response.length > 3) {
      ToastAndroid.showWithGravityAndOffset(
        'Max 3 files can be uploaded',
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        25,
        50,
      );
      return;
    }
    for (const res of response) {
      let mb = res.size / 1024.0 / 1024.0;
      mb = mb.toFixed(2);
      let entry = {
        tag: res.name + '  (' + mb + ' MB)',
        filename: res.name,
        type: res.type,
        uri: res.uri,
        path: res.fileCopyUri,
      };
      this.state.syndicateIdCertificates.push(entry);
    }
    this.setState(
      {
        syndicateIdCertificates: this.state.syndicateIdCertificates,
        totalSyndicateIdCertificates:
          this.state.syndicateIdCertificates.length + ' file(s) selected',
      },
      this.handleChange,
    );
  }

  populateProfessionalPractice(response) {
    let i = 0;
    if (
      this.state.professionalPracticeCertificates.length + response.length >
      3
    ) {
      ToastAndroid.showWithGravityAndOffset(
        'Max 3 files can be uploaded',
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        25,
        50,
      );
      return;
    }
    for (const res of response) {
      let mb = res.size / 1024.0 / 1024.0;
      mb = mb.toFixed(2);
      let entry = {
        tag: res.name + '  (' + mb + ' MB)',
        filename: res.name,
        type: res.type,
        uri: res.uri,
        path: res.fileCopyUri,
      };
      this.state.professionalPracticeCertificates.push(entry);
    }
    this.setState(
      {
        professionalPracticeCertificates:
          this.state.professionalPracticeCertificates,
        totalProfessionalPracticeCertificates:
          this.state.professionalPracticeCertificates.length +
          ' file(s) selected',
      },
      this.handleChange,
    );
  }

  populateSpecializationCertificates(response) {
    let i = 0;
    if (this.state.specializationCertificates.length + response.length > 3) {
      ToastAndroid.showWithGravityAndOffset(
        'Max 3 files can be uploaded',
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        25,
        50,
      );
      return;
    }
    for (const res of response) {
      let mb = res.size / 1024.0 / 1024.0;
      mb = mb.toFixed(2);
      let entry = {
        tag: res.name + '  (' + mb + ' MB)',
        filename: res.name,
        type: res.type,
        uri: res.uri,
        path: res.fileCopyUri,
      };
      this.state.specializationCertificates.push(entry);
    }
    this.setState(
      {
        specializationCertificates: this.state.specializationCertificates,
        totalSpecializationCertificates:
          this.state.specializationCertificates.length + ' file(s) selected',
      },
      this.handleChange,
    );
  }

  populateClinicCertificates(response) {
    let i = 0;
    if (this.state.clinicCertificates.length + response.length > 3) {
      ToastAndroid.showWithGravityAndOffset(
        'Max 3 files can be uploaded',
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        25,
        50,
      );
      return;
    }
    for (const res of response) {
      let mb = res.size / 1024.0 / 1024.0;
      mb = mb.toFixed(2);
      let entry = {
        tag: res.name + '  (' + mb + ' MB)',
        filename: res.name,
        type: res.type,
        uri: res.uri,
        path: res.fileCopyUri,
      };
      this.state.clinicCertificates.push(entry);
    }
    this.setState(
      {
        clinicCertificates: this.state.clinicCertificates,
        totalClinicCertificates:
          this.state.clinicCertificates.length + ' file(s) selected',
      },
      this.handleChange,
    );
  }

  handleChange() {
    if (
      this.state.syndicateId.trim().length > 0 &&
      this.state.graduationCertificates.length > 0 &&
      this.state.syndicateIdCertificates.length > 0 &&
      this.state.professionalPracticeCertificates.length > 0 &&
      this.state.specializationCertificates.length > 0
    ) {
      this.setState({
        buttonStyle: styles.button1,
        buttonDisability: true,
      });
    } else {
      this.setState({
        buttonStyle: styles.button2,
        buttonDisability: false,
      });
    }
  }

  refresh() {
    this.setState({
      issues: Globals.issues,
    });
  }

  navigateToDashboard() {
    SharedPreferences.setItem('isLoggedIn', JSON.stringify(true));
    SharedPreferences.setItem('user', JSON.stringify(this.state.user));
    Globals.user = this.state.user;
    this.props.navigation.navigate('under_review', {
      onGoBack: function (params) {
        this.refresh();
      }.bind(this),
    });
  }

  processUploads() {
    this.body['syndicateId'] = this.state.syndicateId;
    this.body['role'] = 'doctor';
    this.uploadGraduationCertificates();
  }

  imageResizer(uri) {
    return new Promise((resolve, reject) => {
      ImageResizer.createResizedImage(uri, 600, 600, 'JPEG', 100, 0)
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  uploadToFirebase(uri, filename) {
    return new Promise((resolve, reject) => {
      storage()
        .ref('Docs/' + filename)
        .putFile(uri)
        .then(snapshot => {
          resolve(snapshot);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  async uploadGraduationCertificates() {
    var i = 0;
    var graduationUrls = [];

    while (i < this.state.graduationCertificates.length) {
      let {path, type, filename} = this.state.graduationCertificates[i];

      if (type === DocumentPicker.types.images) {
        await this.imageResizer(path)
          .then(async response => {
            uri =
              Platform.OS === 'ios'
                ? response.uri.replace('file://', '')
                : response.uri;

            await this.uploadToFirebase(uri, filename)
              .then(async snapshot => {
                if (snapshot.state === 'success') {
                  const url = await storage()
                    .ref('Docs/' + filename)
                    .getDownloadURL();
                  graduationUrls.push(url);
                  i = i + 1;
                  if (i === this.state.graduationCertificates.length) {
                    console.log('Graduation Documents Uploaded');
                    this.body['graduationCertificates'] = graduationUrls;
                    this.setState(
                      {
                        counter: this.state.counter + 1,
                      },
                      this.uploadSyndicateIdCertificates,
                    );
                  }
                }
              })
              .catch(error => {
                console.log(error);
              });
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        await this.uploadToFirebase(path, filename)
          .then(async snapshot => {
            if (snapshot.state === 'success') {
              const url = await storage()
                .ref('Docs/' + filename)
                .getDownloadURL();
              graduationUrls.push(url);
              i = i + 1;
              if (i === this.state.graduationCertificates.length) {
                console.log('Graduation Documents Uploaded');
                this.body['graduationCertificates'] = graduationUrls;
                this.setState(
                  {
                    counter: this.state.counter + 1,
                  },
                  this.uploadSyndicateIdCertificates,
                );
              }
            }
          })
          .catch(error => {
            console.log(error);
          });
      }
    }
  }

  async uploadSyndicateIdCertificates() {
    var i = 0;
    var syndicateIdUrls = [];

    i = 0;
    while (i < this.state.syndicateIdCertificates.length) {
      let {path, type, filename} = this.state.syndicateIdCertificates[i];

      if (type === DocumentPicker.types.images) {
        await this.imageResizer(path)
          .then(async response => {
            uri =
              Platform.OS === 'ios'
                ? response.uri.replace('file://', '')
                : response.uri;

            await this.uploadToFirebase(uri, filename)
              .then(async snapshot => {
                if (snapshot.state === 'success') {
                  const url = await storage()
                    .ref('Docs/' + filename)
                    .getDownloadURL();
                  syndicateIdUrls.push(url);
                  i = i + 1;
                  if (i === this.state.syndicateIdCertificates.length) {
                    console.log('Syndicate Documents Uploaded');
                    this.body['syndicateIdDocuments'] = syndicateIdUrls;
                    this.setState(
                      {
                        counter: this.state.counter + 1,
                      },
                      this.uploadProfessionalCertificates,
                    );
                  }
                }
              })
              .catch(error => {
                console.log(error);
              });
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        await this.uploadToFirebase(path, filename)
          .then(async snapshot => {
            if (snapshot.state === 'success') {
              const url = await storage()
                .ref('Docs/' + filename)
                .getDownloadURL();
              syndicateIdUrls.push(url);
              i = i + 1;
              if (i === this.state.syndicateIdCertificates.length) {
                console.log('Syndicate Documents Uploaded');
                this.body['syndicateIdDocuments'] = syndicateIdUrls;
                this.setState(
                  {
                    counter: this.state.counter + 1,
                  },
                  this.uploadProfessionalCertificates,
                );
              }
            }
          })
          .catch(error => {
            console.log(error);
          });
      }
    }
  }

  async uploadProfessionalCertificates() {
    var i = 0;
    var professionalPracticeUrls = [];

    while (i < this.state.professionalPracticeCertificates.length) {
      let {path, type, filename} =
        this.state.professionalPracticeCertificates[i];

      if (type === DocumentPicker.types.images) {
        await this.imageResizer(path)
          .then(async response => {
            uri =
              Platform.OS === 'ios'
                ? response.uri.replace('file://', '')
                : response.uri;

            await this.uploadToFirebase(uri, filename)
              .then(async snapshot => {
                if (snapshot.state === 'success') {
                  const url = await storage()
                    .ref('Docs/' + filename)
                    .getDownloadURL();
                  professionalPracticeUrls.push(url);
                  i = i + 1;
                  if (
                    i === this.state.professionalPracticeCertificates.length
                  ) {
                    console.log('Professional Documents Uploaded');
                    this.body['professionalPracticeLetters'] =
                      professionalPracticeUrls;
                    this.setState(
                      {
                        counter: this.state.counter + 1,
                      },
                      this.uploadSpecializationsCertificates,
                    );
                  }
                }
              })
              .catch(error => {
                console.log(error);
              });
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        await this.uploadToFirebase(path, filename)
          .then(async snapshot => {
            if (snapshot.state === 'success') {
              const url = await storage()
                .ref('Docs/' + filename)
                .getDownloadURL();
              professionalPracticeUrls.push(url);
              i = i + 1;
              if (i === this.state.professionalPracticeCertificates.length) {
                console.log('Professional Documents Uploaded');
                this.body['professionalPracticeLetters'] =
                  professionalPracticeUrls;
                this.setState(
                  {
                    counter: this.state.counter + 1,
                  },
                  this.uploadSpecializationsCertificates,
                );
              }
            }
          })
          .catch(error => {
            console.log(error);
          });
      }
    }
  }

  async uploadSpecializationsCertificates() {
    var i = 0;
    var specializationUrls = [];

    while (i < this.state.specializationCertificates.length) {
      let {path, type, filename} = this.state.specializationCertificates[i];

      if (type === DocumentPicker.types.images) {
        await this.imageResizer(path)
          .then(async response => {
            uri =
              Platform.OS === 'ios'
                ? response.uri.replace('file://', '')
                : response.uri;

            await this.uploadToFirebase(uri, filename)
              .then(async snapshot => {
                if (snapshot.state === 'success') {
                  const url = await storage()
                    .ref('Docs/' + filename)
                    .getDownloadURL();
                  specializationUrls.push(url);
                  i = i + 1;
                  if (i === this.state.specializationCertificates.length) {
                    console.log('Specialization Documents Uploaded');
                    this.body['specializationCertificates'] =
                      specializationUrls;
                    this.setState(
                      {
                        counter: this.state.counter + 1,
                      },
                      this.uploadClinicCertificates,
                    );
                  }
                }
              })
              .catch(error => {
                console.log(error);
              });
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        await this.uploadToFirebase(path, filename)
          .then(async snapshot => {
            if (snapshot.state === 'success') {
              const url = await storage()
                .ref('Docs/' + filename)
                .getDownloadURL();
              specializationUrls.push(url);
              i = i + 1;
              if (i === this.state.specializationCertificates.length) {
                console.log('Specialization Documents Uploaded');
                this.body['specializationCertificates'] = specializationUrls;
                this.setState(
                  {
                    counter: this.state.counter + 1,
                  },
                  this.uploadClinicCertificates,
                );
              }
            }
          })
          .catch(error => {
            console.log(error);
          });
      }
    }
  }

  async uploadClinicCertificates() {
    var i = 0;
    var clinicUrls = [];

    if (this.state.clinicCertificates.length > 0) {
      while (i < this.state.clinicCertificates.length) {
        let {path, type, filename} = this.state.clinicCertificates[i];

        if (type === DocumentPicker.types.images) {
          await this.imageResizer(path)
            .then(async response => {
              uri =
                Platform.OS === 'ios'
                  ? response.uri.replace('file://', '')
                  : response.uri;

              await this.uploadToFirebase(uri, filename)
                .then(async snapshot => {
                  if (snapshot.state === 'success') {
                    const url = await storage()
                      .ref('Docs/' + filename)
                      .getDownloadURL();
                    clinicUrls.push(url);
                    i = i + 1;
                    if (i === this.state.clinicCertificates.length) {
                      console.log('Clinic Documents Uploaded');
                      this.body['clinicalCertificate'] = clinicUrls;
                      this.setState(
                        {
                          counter: this.state.counter + 1,
                        },
                        this.performRegister,
                      );
                    }
                  }
                })
                .catch(error => {
                  console.log(error);
                });
            })
            .catch(error => {
              console.log(error);
            });
        } else {
          await this.uploadToFirebase(path, filename)
            .then(async snapshot => {
              if (snapshot.state === 'success') {
                const url = await storage()
                  .ref('Docs/' + filename)
                  .getDownloadURL();
                clinicUrls.push(url);
                i = i + 1;
                if (i === this.state.clinicCertificates.length) {
                  console.log('Clinic Documents Uploaded');
                  this.body['clinicalCertificate'] = clinicUrls;
                  this.setState(
                    {
                      counter: this.state.counter + 1,
                    },
                    this.performRegister,
                  );
                }
              }
            })
            .catch(error => {
              console.log('Firebase: ' + error);
            });
        }
      }
    } else {
      this.performRegister();
    }
  }

  performRegister() {
    if (this.state.counter > 3) {
      this.body['issuesInDocuments'] = [];
      axios
        .patch(BASE_URL + '/users/' + this.state.user.id, this.body, {
          headers: {
            Authorization: 'Bearer ' + this.state.user.accessToken,
          },
        })
        .then(response => {
          this.state.user.type = 'doctor';

          this.setState(
            {
              visible: false,
              user: this.state.user,
            },
            this.navigateToDashboard,
          );
        })
        .catch(error => {
          this.setState({
            visible: false,
          });
          console.log(error);
          if (Platform.OS === 'android') {
            ToastAndroid.showWithGravityAndOffset(
              error.message,
              ToastAndroid.LONG,
              ToastAndroid.BOTTOM,
              25,
              50,
            );
          }
        });
    } else {
      console.log('Not yet');
    }
  }

  manageRejections() {
    let i = 0;
    console.log(this.state.issues);
    while (i < this.state.issues.length) {
      const obj = this.state.issues[i];
      console.log(obj);
      if (obj.documentType === 'graduationCertificates') {
        if (this.state.rejection1 === '') {
          this.setState({
            rejection1: obj.message,
          });
        } else {
          this.setState({
            rejection1: this.state.rejection1 + '\n' + obj.message,
          });
        }
      } else if (obj.documentType === 'syndicateIdDocuments') {
        if (this.state.rejection1 === '') {
          this.setState({
            rejection2: obj.message,
          });
        } else {
          this.setState({
            rejection2: this.state.rejection2 + '\n' + obj.message,
          });
        }
      } else if (obj.documentType === 'professionalPracticeLetters') {
        if (this.state.rejection1 === '') {
          this.setState({
            rejection3: obj.message,
          });
        } else {
          this.setState({
            rejection3: this.state.rejection2 + '\n' + obj.message,
          });
        }
      } else if (obj.documentType === 'specializationCertificates') {
        if (this.state.rejection1 === '') {
          this.setState({
            rejection4: obj.message,
          });
        } else {
          this.setState({
            rejection4: this.state.rejection2 + '\n' + obj.message,
          });
        }
      } else if (obj.documentType === 'clinicalCertificate') {
        if (this.state.rejection1 === '') {
          this.setState({
            rejection5: obj.message,
          });
        } else {
          this.setState({
            rejection5: this.state.rejection2 + '\n' + obj.message,
          });
        }
      }
      i = i + 1;
    }
  }

  render() {
    const {issues} = this.state;
    if (issues.length > 0) {
      this.manageRejections();
    }
    const {height, width} = this.window;

    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="dark-content" backgroundColor={Colors.white} />
        <ProgressBar
          size={48}
          thickness={4}
          visible={this.state.visible}
          color={Colors.primary}
        />
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
          contentContainerStyle={{
            marginTop: 20,
            paddingBottom: this.navbarHeight,
            marginHorizontal: 17,
          }}
          showVerticalScrollIndicator={false}>
          <Text style={styles.title}>Sign up</Text>
          <Text style={styles.description}>
            Please fill in the information to complete the sign up process.
          </Text>

          <View style={{marginTop: 20}}>
            <Text
              style={
                this.state.rejection1 !== '' ? styles.redLabel : styles.label
              }>
              Graduation Certificate*{' '}
              {this.state.rejection1 !== '' ? '  (' : ''}
              {this.state.rejection1 !== '' ? this.state.rejection1 : ''}
              {this.state.rejection1 !== '' ? ')' : ''}
            </Text>
            <View
              style={
                this.state.totalGraduationCertificates.trim().length > 0
                  ? {
                      flexDirection: 'row',
                      borderRadius: 10,
                      borderWidth: 0.7,
                      borderColor: Colors.secondary,
                      paddingLeft: 10,
                    }
                  : {
                      flexDirection: 'row',
                      borderRadius: 10,
                      borderWidth: 0.7,
                      borderColor: Colors.light_grey,
                      paddingLeft: 10,
                    }
              }>
              <TextInput
                style={{
                  flex: 1,
                  fontSize: 16,
                  color: Colors.text_dark,
                  fontFamily: 'urbanist_regular',
                }}
                editable={false}
                value={this.state.totalGraduationCertificates}
                placeholder="Upload Files (3 max)"
                placeholderTextColor={Colors.light_grey}
              />
              <TouchableOpacity
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: Colors.secondary,
                  width: '18%',
                  borderBottomRightRadius: 10,
                  borderTopRightRadius: 10,
                }}
                onPress={() =>
                  this.requestCameraPermission('graduation_certificates')
                }>
                <Image
                  style={{
                    height: 20,
                    width: 20,
                    resizeMode: 'contain',
                    tintColor: Colors.white,
                  }}
                  source={require('../../assets/upload.png')}
                />
              </TouchableOpacity>
            </View>
            <Tags
              initialTags={this.state.graduationCertificates}
              textInputProps={{
                editable: false,
                style: {
                  height: 0,
                  width: 0,
                },
              }}
              onChangeTags={tags => console.log(tags)}
              inputContainerStyle={{
                height: 0,
                opacity: 0,
              }}
              inputStyle={{
                backgroundColor: Colors.white,
              }}
              renderTag={({
                tag,
                index,
                onPress,
                deleteTagOnPress,
                readonly,
              }) => (
                <View
                  style={{
                    flexDirection: 'row',
                    backgroundColor: Colors.tagBackground,
                    padding: '2%',
                    marginRight: '2%',
                    marginTop: '3%',
                    borderRadius: 5,
                  }}>
                  <Text
                    style={{
                      fontFamily: 'urbanist_regular',
                      color: Colors.text_dark,
                      fontSize: height * 0.015,
                    }}>
                    {tag.tag}
                  </Text>
                  <TouchableOpacity
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginLeft: '6%',
                    }}
                    onPress={() => {
                      let temp = this.state.graduationCertificates;
                      if (index > -1) {
                        temp.splice(index, 1);
                      }
                      let tDocs =
                        temp.length === 0
                          ? ''
                          : temp.length + ' file(s) selected';
                      this.setState(
                        {
                          graduationCertificates: temp,
                          totalGraduationCertificates: tDocs,
                        },
                        this.handleChange,
                      );
                    }}>
                    <Image
                      style={{
                        height: height * 0.012,
                        width: height * 0.012,
                        resizeMode: 'contain',
                      }}
                      source={require('../../assets/delete.png')}
                    />
                  </TouchableOpacity>
                </View>
              )}
            />
          </View>

          <View style={{marginTop: 20}}>
            <Text style={styles.label}>Syndicate Id*</Text>
            <TextInput
              style={
                this.state.syndicateId.trim().length > 0
                  ? styles.textInputFilled
                  : styles.textInput
              }
              value={this.state.syndicateId}
              onChangeText={text =>
                this.setState({syndicateId: text}, this.handleChange)
              }
              returnKeyType={'done'}
              placeholder="Enter Syndicate ID"
              placeholderTextColor={Colors.disabledIcon}
            />
          </View>

          <View style={{marginTop: 20}}>
            <Text
              style={
                this.state.rejection2 !== '' ? styles.redLabel : styles.label
              }>
              Doctor’s Syndicate ID Document*{' '}
              {this.state.rejection2 !== '' ? '  (' : ''}
              {this.state.rejection2 !== '' ? this.state.rejection2 : ''}
              {this.state.rejection2 !== '' ? ')' : ''}
            </Text>
            <View
              style={
                this.state.totalSyndicateIdCertificates.trim().length > 0
                  ? {
                      flexDirection: 'row',
                      borderRadius: 10,
                      borderWidth: 0.7,
                      borderColor: Colors.secondary,
                      paddingLeft: 10,
                    }
                  : {
                      flexDirection: 'row',
                      borderRadius: 10,
                      borderWidth: 0.7,
                      borderColor: Colors.light_grey,
                      paddingLeft: 10,
                    }
              }>
              <TextInput
                style={{
                  flex: 1,
                  fontSize: 16,
                  color: Colors.text_dark,
                  fontFamily: 'urbanist_regular',
                }}
                editable={false}
                value={this.state.totalSyndicateIdCertificates}
                placeholder="Upload Files (3 max)"
                placeholderTextColor={Colors.light_grey}
              />
              <TouchableOpacity
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: Colors.secondary,
                  width: '18%',
                  borderBottomRightRadius: 10,
                  borderTopRightRadius: 10,
                }}
                onPress={() =>
                  this.requestCameraPermission('syndicate_id_certificates')
                }>
                <Image
                  style={{
                    height: 20,
                    width: 20,
                    resizeMode: 'contain',
                    tintColor: Colors.white,
                  }}
                  source={require('../../assets/upload.png')}
                />
              </TouchableOpacity>
            </View>
            <Tags
              initialTags={this.state.syndicateIdCertificates}
              textInputProps={{
                editable: false,
                style: {
                  height: 0,
                  width: 0,
                },
              }}
              onChangeTags={tags => console.log(tags)}
              inputContainerStyle={{
                height: 0,
                opacity: 0,
              }}
              inputStyle={{
                backgroundColor: Colors.white,
              }}
              renderTag={({
                tag,
                index,
                onPress,
                deleteTagOnPress,
                readonly,
              }) => (
                <View
                  style={{
                    flexDirection: 'row',
                    backgroundColor: Colors.tagBackground,
                    padding: '2%',
                    marginRight: '2%',
                    marginTop: '3%',
                    borderRadius: 5,
                  }}>
                  <Text
                    style={{
                      fontFamily: 'urbanist_regular',
                      color: Colors.text_dark,
                      fontSize: height * 0.015,
                    }}>
                    {tag.tag}
                  </Text>
                  <TouchableOpacity
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginLeft: '6%',
                    }}
                    onPress={() => {
                      let temp = this.state.syndicateIdCertificates;
                      if (index > -1) {
                        temp.splice(index, 1);
                      }
                      let tDocs =
                        temp.length === 0
                          ? ''
                          : temp.length + ' file(s) selected';
                      this.setState(
                        {
                          syndicateIdCertificates: temp,
                          totalSyndicateIdCertificates: tDocs,
                        },
                        this.handleChange,
                      );
                    }}>
                    <Image
                      style={{
                        height: height * 0.012,
                        width: height * 0.012,
                        resizeMode: 'contain',
                      }}
                      source={require('../../assets/delete.png')}
                    />
                  </TouchableOpacity>
                </View>
              )}
            />
          </View>

          <View style={{marginTop: 20}}>
            <Text
              style={
                this.state.rejection3 !== '' ? styles.redLabel : styles.label
              }>
              Profession Practice Letter*{' '}
              {this.state.rejection3 !== '' ? '  (' : ''}
              {this.state.rejection3 !== '' ? this.state.rejection3 : ''}
              {this.state.rejection3 !== '' ? ')' : ''}
            </Text>
            <View
              style={
                this.state.totalProfessionalPracticeCertificates.trim().length >
                0
                  ? {
                      flexDirection: 'row',
                      borderRadius: 10,
                      borderWidth: 0.7,
                      borderColor: Colors.secondary,
                      paddingLeft: 10,
                    }
                  : {
                      flexDirection: 'row',
                      borderRadius: 10,
                      borderWidth: 0.7,
                      borderColor: Colors.light_grey,
                      paddingLeft: 10,
                    }
              }>
              <TextInput
                style={{
                  flex: 1,
                  fontSize: 16,
                  color: Colors.text_dark,
                  fontFamily: 'urbanist_regular',
                }}
                editable={false}
                value={this.state.totalProfessionalPracticeCertificates}
                placeholder="Upload Files (3 max)"
                placeholderTextColor={Colors.disabledIcon}
              />
              <TouchableOpacity
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: Colors.secondary,
                  width: '18%',
                  borderBottomRightRadius: 10,
                  borderTopRightRadius: 10,
                }}
                onPress={() =>
                  this.requestCameraPermission(
                    'professional_practice_certificates',
                  )
                }>
                <Image
                  style={{
                    height: 20,
                    width: 20,
                    resizeMode: 'contain',
                    tintColor: Colors.white,
                  }}
                  source={require('../../assets/upload.png')}
                />
              </TouchableOpacity>
            </View>
            <Tags
              initialTags={this.state.professionalPracticeCertificates}
              textInputProps={{
                editable: false,
                style: {
                  height: 0,
                  width: 0,
                },
              }}
              onChangeTags={tags => console.log(tags)}
              inputContainerStyle={{
                height: 0,
                opacity: 0,
              }}
              inputStyle={{
                backgroundColor: Colors.white,
              }}
              renderTag={({
                tag,
                index,
                onPress,
                deleteTagOnPress,
                readonly,
              }) => (
                <View
                  style={{
                    flexDirection: 'row',
                    backgroundColor: Colors.tagBackground,
                    padding: '2%',
                    marginRight: '2%',
                    marginTop: '3%',
                    borderRadius: 5,
                  }}>
                  <Text
                    style={{
                      fontFamily: 'urbanist_regular',
                      color: Colors.text_dark,
                      fontSize: height * 0.015,
                    }}>
                    {tag.tag}
                  </Text>
                  <TouchableOpacity
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginLeft: '6%',
                    }}
                    onPress={() => {
                      let temp = this.state.professionalPracticeCertificates;
                      if (index > -1) {
                        temp.splice(index, 1);
                      }
                      let tDocs =
                        temp.length === 0
                          ? ''
                          : temp.length + ' file(s) selected';
                      this.setState(
                        {
                          professionalPracticeCertificates: temp,
                          totalProfessionalPracticeCertificates: tDocs,
                        },
                        this.handleChange,
                      );
                    }}>
                    <Image
                      style={{
                        height: height * 0.012,
                        width: height * 0.012,
                        resizeMode: 'contain',
                      }}
                      source={require('../../assets/delete.png')}
                    />
                  </TouchableOpacity>
                </View>
              )}
            />
          </View>

          <View style={{marginTop: 20}}>
            <Text
              style={
                this.state.rejection4 !== '' ? styles.redLabel : styles.label
              }>
              Specialization Certificate*{' '}
              {this.state.rejection4 !== '' ? '  (' : ''}
              {this.state.rejection4 !== '' ? this.state.rejection4 : ''}
              {this.state.rejection4 !== '' ? ')' : ''}
            </Text>
            <View
              style={
                this.state.totalSpecializationCertificates.trim().length > 0
                  ? {
                      flexDirection: 'row',
                      borderRadius: 10,
                      borderWidth: 0.7,
                      borderColor: Colors.secondary,
                      paddingLeft: 10,
                    }
                  : {
                      flexDirection: 'row',
                      borderRadius: 10,
                      borderWidth: 0.7,
                      borderColor: Colors.light_grey,
                      paddingLeft: 10,
                    }
              }>
              <TextInput
                style={{
                  flex: 1,
                  fontSize: 16,
                  color: Colors.text_dark,
                  fontFamily: 'urbanist_regular',
                }}
                editable={false}
                value={this.state.totalSpecializationCertificates}
                placeholder="Upload Files (3 max)"
                placeholderTextColor={Colors.disabledIcon}
              />
              <TouchableOpacity
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: Colors.secondary,
                  width: '18%',
                  borderBottomRightRadius: 10,
                  borderTopRightRadius: 10,
                }}
                onPress={() =>
                  this.requestCameraPermission('specialization_certificates')
                }>
                <Image
                  style={{
                    height: 20,
                    width: 20,
                    resizeMode: 'contain',
                    tintColor: Colors.white,
                  }}
                  source={require('../../assets/upload.png')}
                />
              </TouchableOpacity>
            </View>
            <Tags
              initialTags={this.state.specializationCertificates}
              textInputProps={{
                editable: false,
                style: {
                  height: 0,
                  width: 0,
                },
              }}
              onChangeTags={tags => console.log(tags)}
              inputContainerStyle={{
                height: 0,
                opacity: 0,
              }}
              inputStyle={{
                backgroundColor: Colors.white,
              }}
              renderTag={({
                tag,
                index,
                onPress,
                deleteTagOnPress,
                readonly,
              }) => (
                <View
                  style={{
                    flexDirection: 'row',
                    backgroundColor: Colors.tagBackground,
                    padding: '2%',
                    marginRight: '2%',
                    marginTop: '3%',
                    borderRadius: 5,
                  }}>
                  <Text
                    style={{
                      fontFamily: 'urbanist_regular',
                      color: Colors.text_dark,
                      fontSize: height * 0.015,
                    }}>
                    {tag.tag}
                  </Text>
                  <TouchableOpacity
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginLeft: '6%',
                    }}
                    onPress={() => {
                      let temp = this.state.specializationCertificates;
                      if (index > -1) {
                        temp.splice(index, 1);
                      }
                      let tDocs =
                        temp.length === 0
                          ? ''
                          : temp.length + ' file(s) selected';
                      this.setState(
                        {
                          specializationCertificates: temp,
                          totalSpecializationCertificates: tDocs,
                        },
                        this.handleChange,
                      );
                    }}>
                    <Image
                      style={{
                        height: height * 0.012,
                        width: height * 0.012,
                        resizeMode: 'contain',
                      }}
                      source={require('../../assets/delete.png')}
                    />
                  </TouchableOpacity>
                </View>
              )}
            />
          </View>

          <View style={{marginTop: 20}}>
            <Text
              style={
                this.state.rejection5 !== '' ? styles.redLabel : styles.label
              }>
              Clinic Certificate (optional){' '}
              {this.state.rejection5 !== '' ? '  (' : ''}
              {this.state.rejection5 !== '' ? this.state.rejection5 : ''}
              {this.state.rejection5 !== '' ? ')' : ''}
            </Text>
            <View
              style={
                this.state.totalClinicCertificates.trim().length > 0
                  ? {
                      flexDirection: 'row',
                      borderRadius: 10,
                      borderWidth: 0.7,
                      borderColor: Colors.secondary,
                      paddingLeft: 10,
                    }
                  : {
                      flexDirection: 'row',
                      borderRadius: 10,
                      borderWidth: 0.7,
                      borderColor: Colors.light_grey,
                      paddingLeft: 10,
                    }
              }>
              <TextInput
                style={{
                  flex: 1,
                  fontSize: 16,
                  color: Colors.text_dark,
                  fontFamily: 'urbanist_regular',
                }}
                editable={false}
                value={this.state.totalClinicCertificates}
                placeholder="Upload Files (3 max)"
                placeholderTextColor={Colors.disabledIcon}
              />
              <TouchableOpacity
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: Colors.secondary,
                  width: '18%',
                  borderBottomRightRadius: 10,
                  borderTopRightRadius: 10,
                }}
                onPress={() =>
                  this.requestCameraPermission('clinic_certificates')
                }>
                <Image
                  style={{
                    height: 20,
                    width: 20,
                    resizeMode: 'contain',
                    tintColor: Colors.white,
                  }}
                  source={require('../../assets/upload.png')}
                />
              </TouchableOpacity>
            </View>
            <Tags
              initialTags={this.state.clinicCertificates}
              textInputProps={{
                editable: false,
                style: {
                  height: 0,
                  width: 0,
                },
              }}
              onChangeTags={tags => console.log(tags)}
              inputContainerStyle={{
                height: 0,
                opacity: 0,
              }}
              inputStyle={{
                backgroundColor: Colors.white,
              }}
              renderTag={({
                tag,
                index,
                onPress,
                deleteTagOnPress,
                readonly,
              }) => (
                <View
                  style={{
                    flexDirection: 'row',
                    backgroundColor: Colors.tagBackground,
                    padding: '2%',
                    marginRight: '2%',
                    marginTop: '3%',
                    borderRadius: 5,
                  }}>
                  <Text
                    style={{
                      fontFamily: 'urbanist_regular',
                      color: Colors.text_dark,
                      fontSize: height * 0.015,
                    }}>
                    {tag.tag}
                  </Text>
                  <TouchableOpacity
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginLeft: '6%',
                    }}
                    onPress={() => {
                      let temp = this.state.clinicCertificates;
                      if (index > -1) {
                        temp.splice(index, 1);
                      }
                      let tDocs =
                        temp.length === 0
                          ? ''
                          : temp.length + ' file(s) selected';
                      this.setState(
                        {
                          clinicCertificates: temp,
                          totalClinicCertificates: tDocs,
                        },
                        this.handleChange,
                      );
                    }}>
                    <Image
                      style={{
                        height: height * 0.012,
                        width: height * 0.012,
                        resizeMode: 'contain',
                      }}
                      source={require('../../assets/delete.png')}
                    />
                  </TouchableOpacity>
                </View>
              )}
            />
          </View>

          <View style={{width: '100%', marginTop: 15}}>
            <TouchableOpacity
              style={this.state.buttonStyle}
              disabled={!this.state.buttonDisability || this.state.visible}
              onPress={function () {
                this.setState(
                  {
                    visible: true,
                  },
                  this.processUploads,
                );
              }.bind(this)}>
              <Text
                style={{
                  color: Colors.white,
                  alignSelf: 'center',
                  fontSize: 16,
                  fontFamily: 'urbanist_bold',
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    alignItems: 'center',
  },
  title: {
    fontSize: 32,
    color: Colors.text_dark,
    fontFamily: 'urbanist_bold',
  },
  description: {
    fontSize: 16,
    fontFamily: 'urbanist_regular',
    color: Colors.text_light,
  },
  button1: {
    width: '100%',
    alignSelf: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  button2: {
    width: '100%',
    alignSelf: 'center',
    backgroundColor: Colors.disabled,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  label: {
    fontFamily: 'urbanist_bold',
    fontSize: 16,
    color: Colors.text_dark,
    marginBottom: 5,
  },
  redLabel: {
    fontFamily: 'urbanist_bold',
    fontSize: 16,
    color: 'red',
    marginBottom: 5,
  },
  textInput: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.light_grey,
    fontSize: 16,
    paddingHorizontal: 10,
    color: Colors.disabledIcon,
    fontFamily: 'urbanist_regular',
  },
  textInputFilled: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.secondary,
    fontSize: 16,
    paddingHorizontal: 10,
    color: Colors.text_dark,
    fontFamily: 'urbanist_regular',
  },
});

const mapStateToProps = state => ({
  issues: state.issues,
});

// export default connect(mapStateToProps)(DoctorRegistration);
