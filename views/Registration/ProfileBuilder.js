import Colors from '../../utils/Colors.js';
import React, {Component} from 'react';
import {
  BackHandler,
  StyleSheet,
  Dimensions,
  ScrollView,
  StatusBar,
  Text,
  TextInput,
  ToastAndroid,
  KeyboardAvoidingView,
  PermissionsAndroid,
  View,
  Platform,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {Avatar} from 'react-native-elements';
import {CommonActions} from '@react-navigation/native';
import axios from 'react-native-axios';
import Tags from 'react-native-tags';
import SharedPreferences from 'react-native-shared-preferences';
import {BASE_URL} from '@env';
import storage from '@react-native-firebase/storage';
import ImageResizer from 'react-native-image-resizer';
import {ProgressBar} from 'react-native-material-indicators';
import {YellowBox} from 'react-native';
import User from '../../models/User.js';
import {CustomPicker} from 'react-native-custom-picker';
import DatePicker from 'react-native-date-picker';
import Globals from '../../utils/Globals';
import DocumentPicker from 'react-native-document-picker';

export default class ProfileBuilder extends Component {
  state = {
    fullName: '',
    nameRejection: '',
    selectedGender: '',
    genderRejection: '',
    selectedMedicalStaff: 'none',
    selectedUserType: 'user',
    date: '',
    dateRejection: '',
    dateObj: new Date(),
    email: '',
    emailRejection: '',
    biography: '',
    biographyRejection: '',
    showPicker: false,
    selectedCity: '',
    cityRejection: '',
    selectedDistrict: '',
    districtRejection: '',
    selectedCountry: '',
    countryRejection: '',
    selectedSpecialization: '',
    specializationRejection: '',
    cities: [],
    buttonStyle: styles.button2,
    uris: [],
    buttonDisability: false,
    visible: false,
    avatar: {uri: ''},
    avatarRejection: '',
  };

  constructor(props) {
    super(props);
    this.toUpdate =
      this.props.route.params.toUpdate !== undefined ? true : false;
    this.imageName = '';
    this.body = '';
    this.user = Globals.user;
    this.country = '';

    this.districts = [
      'Al Anbar',
      'Muthanna',
      'Qadisiyyah',
      'Babil',
      'Baghdad',
      'Basra',
      'Dhi',
      'Diyala',
      'Karbala',
      'Kirkuk',
      'Maysan',
      'Najaf',
      'Nineveh',
      'Saladin',
      'Wasit',
      'Dohuk',
      'Erbil',
      'Sulaymaniyah',
      'Halabja',
    ];

    this.countries = [
      'Afghanistan',
      'Albania',
      'Algeria',
      'American Samoa',
      'Andorra',
      'Angola',
      'Anguilla',
      'Antarctica',
      'Antigua and Barbuda',
      'Argentina',
      'Armenia',
      'Aruba',
      'Australia',
      'Austria',
      'Azerbaijan',
      'Bahamas (the)',
      'Bahrain',
      'Bangladesh',
      'Barbados',
      'Belarus',
      'Belgium',
      'Belize',
      'Benin',
      'Bermuda',
      'Bhutan',
      'Bolivia (Plurinational State of)',
      'Bonaire, Sint Eustatius and Saba',
      'Bosnia and Herzegovina',
      'Botswana',
      'Bouvet Island',
      'Brazil',
      'British Indian Ocean Territory (the)',
      'Brunei Darussalam',
      'Bulgaria',
      'Burkina Faso',
      'Burundi',
      'Cabo Verde',
      'Cambodia',
      'Cameroon',
      'Canada',
      'Cayman Islands (the)',
      'Central African Republic (the)',
      'Chad',
      'Chile',
      'China',
      'Christmas Island',
      'Cocos (Keeling) Islands (the)',
      'Colombia',
      'Comoros (the)',
      'Congo (the Democratic Republic of the)',
      'Congo (the)',
      'Cook Islands (the)',
      'Costa Rica',
      'Croatia',
      'Cuba',
      'Curaçao',
      'Cyprus',
      'Czechia',
      "Côte d'Ivoire",
      'Denmark',
      'Djibouti',
      'Dominica',
      'Dominican Republic (the)',
      'Ecuador',
      'Egypt',
      'El Salvador',
      'Equatorial Guinea',
      'Eritrea',
      'Estonia',
      'Eswatini',
      'Ethiopia',
      'Falkland Islands (the) [Malvinas]',
      'Faroe Islands (the)',
      'Fiji',
      'Finland',
      'France',
      'French Guiana',
      'French Polynesia',
      'French Southern Territories (the)',
      'Gabon',
      'Gambia (the)',
      'Georgia',
      'Germany',
      'Ghana',
      'Gibraltar',
      'Greece',
      'Greenland',
      'Grenada',
      'Guadeloupe',
      'Guam',
      'Guatemala',
      'Guernsey',
      'Guinea',
      'Guinea-Bissau',
      'Guyana',
      'Haiti',
      'Heard Island and McDonald Islands',
      'Holy See (the)',
      'Honduras',
      'Hong Kong',
      'Hungary',
      'Iceland',
      'India',
      'Indonesia',
      'Iran (Islamic Republic of)',
      'Iraq',
      'Ireland',
      'Isle of Man',
      'Israel',
      'Italy',
      'Jamaica',
      'Japan',
      'Jersey',
      'Jordan',
      'Kazakhstan',
      'Kenya',
      'Kiribati',
      "Korea (the Democratic People's Republic of)",
      'Korea (the Republic of)',
      'Kuwait',
      'Kyrgyzstan',
      "Lao People's Democratic Republic (the)",
      'Latvia',
      'Lebanon',
      'Lesotho',
      'Liberia',
      'Libya',
      'Liechtenstein',
      'Lithuania',
      'Luxembourg',
      'Macao',
      'Madagascar',
      'Malawi',
      'Malaysia',
      'Maldives',
      'Mali',
      'Malta',
      'Marshall Islands (the)',
      'Martinique',
      'Mauritania',
      'Mauritius',
      'Mayotte',
      'Mexico',
      'Micronesia (Federated States of)',
      'Moldova (the Republic of)',
      'Monaco',
      'Mongolia',
      'Montenegro',
      'Montserrat',
      'Morocco',
      'Mozambique',
      'Myanmar',
      'Namibia',
      'Nauru',
      'Nepal',
      'Netherlands (the)',
      'New Caledonia',
      'New Zealand',
      'Nicaragua',
      'Niger (the)',
      'Nigeria',
      'Niue',
      'Norfolk Island',
      'Northern Mariana Islands (the)',
      'Norway',
      'Oman',
      'Pakistan',
      'Palau',
      'Palestine, State of',
      'Panama',
      'Papua New Guinea',
      'Paraguay',
      'Peru',
      'Philippines (the)',
      'Pitcairn',
      'Poland',
      'Portugal',
      'Puerto Rico',
      'Qatar',
      'Republic of North Macedonia',
      'Romania',
      'Russian Federation (the)',
      'Rwanda',
      'Réunion',
      'Saint Barthélemy',
      'Saint Helena, Ascension and Tristan da Cunha',
      'Saint Kitts and Nevis',
      'Saint Lucia',
      'Saint Martin (French part)',
      'Saint Pierre and Miquelon',
      'Saint Vincent and the Grenadines',
      'Samoa',
      'San Marino',
      'Sao Tome and Principe',
      'Saudi Arabia',
      'Senegal',
      'Serbia',
      'Seychelles',
      'Sierra Leone',
      'Singapore',
      'Sint Maarten (Dutch part)',
      'Slovakia',
      'Slovenia',
      'Solomon Islands',
      'Somalia',
      'South Africa',
      'South Georgia and the South Sandwich Islands',
      'South Sudan',
      'Spain',
      'Sri Lanka',
      'Sudan (the)',
      'Suriname',
      'Svalbard and Jan Mayen',
      'Sweden',
      'Switzerland',
      'Syrian Arab Republic',
      'Taiwan',
      'Tajikistan',
      'Tanzania, United Republic of',
      'Thailand',
      'Timor-Leste',
      'Togo',
      'Tokelau',
      'Tonga',
      'Trinidad and Tobago',
      'Tunisia',
      'Turkey',
      'Turkmenistan',
      'Turks and Caicos Islands (the)',
      'Tuvalu',
      'Uganda',
      'Ukraine',
      'United Arab Emirates (the)',
      'United Kingdom of Great Britain and Northern Ireland (the)',
      'United States Minor Outlying Islands (the)',
      'United States of America (the)',
      'Uruguay',
      'Uzbekistan',
      'Vanuatu',
      'Venezuela (Bolivarian Republic of)',
      'Viet Nam',
      'Virgin Islands (British)',
      'Virgin Islands (U.S.)',
      'Wallis and Futuna',
      'Western Sahara',
      'Yemen',
      'Zambia',
      'Zimbabwe',
      'Åland Islands',
    ];

    this.specializations = [
      'General Medicine',
      'Cardiology',
      'Neurology',
      'Ophthalmology',
      'Dental Surgery',
      'Dermatology',
      'Orthopaedic',
      'Urology',
      'Pulmonology',
      'Gynecology',
      'Paediatrics',
      'Oncology',
    ];

    this.requestCameraPermission = async () => {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Kashfia App Camera Permission',
            message:
              'Kashfia App needs access to your camera ' +
              'so you can take pictures.',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          try {
            const res = await DocumentPicker.pick({
              type: [DocumentPicker.types.images],
              copyTo: 'cachesDirectory',
            });
            let obj = {
              filename: res[0].name,
              uri: res[0].uri,
              path: res[0].fileCopyUri,
            };
            this.setState(
              {
                avatar: obj,
              },
              this.handleChange,
            );
          } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
              throw err;
            }
          }
        } else {
          console.log('Camera permission denied');
        }
      } catch (err) {
        console.warn(err);
      }
    };

    this.screenHeight = Dimensions.get('screen').height;
    this.windowHeight = Dimensions.get('window').height;
    this.navbarHeight =
      this.screenHeight - this.windowHeight + StatusBar.currentHeight;
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.getCities = this.getCities.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.getCities();
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );

    this.setState(
      {
        fullName: this.user.fullName,
        selectedGender: this.user.gender,
        selectedUserType: this.user.role,
        dateObj: new Date(),
        date: this.user.dateOfBirth,
        email: this.user.email,
        selectedCity: this.user.city,
        selectedDistrict: this.user.district,
        selectedCountry: this.user.country,
        selectedSpecialization: this.user.specialization,
        avatar: {uri: this.user.avatar},
      },
      this.handleChange,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    if (this.toUpdate) {
      this.props.navigation.goBack();
      return true;
    }
    return false;
  }

  getCities() {
    axios
      .post('https://countriesnow.space/api/v0.1/countries/cities/', {
        country: 'iraq',
      })
      .then(response => {
        this.state.cities.push.apply(this.state.cities, response.data.data);
        this.setState({
          cities: this.state.cities,
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleChange() {
    console.log('Entered');
    if (
      this.state.fullName.trim().length > 0 &&
      this.state.date !== '' &&
      this.state.selectedGender !== 'none' &&
      this.state.selectedCity !== '' &&
      this.state.selectedDistrict !== '' &&
      this.state.selectedCountry !== '' &&
      this.state.avatar !== null
    ) {
      if (this.user.role === 'doctor') {
        if (
          this.state.selectedSpecialization !== '' &&
          this.state.biography.trim().length > 0
        ) {
          this.setState({
            buttonStyle: styles.button1,
            buttonDisability: true,
          });
        } else {
          this.setState({
            buttonStyle: styles.button2,
            buttonDisability: false,
          });
        }
      } else if (
        this.user.role !== 'user' &&
        this.state.biography.trim().length > 0
      ) {
        this.setState({
          buttonStyle: styles.button1,
          buttonDisability: true,
        });
      } else {
        this.setState({
          buttonStyle: styles.button1,
          buttonDisability: true,
        });
      }
    } else {
      this.setState({
        buttonStyle: styles.button2,
        buttonDisability: false,
      });
    }
  }

  renderSpecializationHeader() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          padding: 20,
        }}>
        <Text
          style={{
            fontFamily: 'urbanist_bold',
            fontSize: 20,
            color: Colors.black,
          }}>
          Select Specialization
        </Text>
      </View>
    );
  }

  renderCountryHeader() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          padding: 20,
        }}>
        <Text
          style={{
            fontFamily: 'urbanist_bold',
            fontSize: 20,
            color: Colors.black,
          }}>
          Select Country
        </Text>
      </View>
    );
  }

  renderDistrictHeader() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          padding: 20,
        }}>
        <Text
          style={{
            fontFamily: 'urbanist_bold',
            fontSize: 20,
            color: Colors.black,
          }}>
          Select District
        </Text>
      </View>
    );
  }

  renderCityHeader() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          padding: 20,
        }}>
        <Text
          style={{
            fontFamily: 'urbanist_bold',
            fontSize: 20,
            color: Colors.black,
          }}>
          Select City
        </Text>
      </View>
    );
  }

  renderField(settings) {
    const {selectedItem, defaultText, getLabel} = settings;
    return (
      <View style={{flexDirection: 'row', alignItems: 'center', width: '100%'}}>
        {!selectedItem && (
          <Text style={[styles.placeholder_text, {flex: 1}]}>
            {defaultText}
          </Text>
        )}
        {selectedItem && (
          <Text style={[styles.text, {flex: 1}]}>{getLabel(selectedItem)}</Text>
        )}
        <Image
          style={{
            resizeMode: 'contain',
            tintColor: Colors.light_grey,
            width: 10,
            height: 10,
          }}
          source={require('../../assets/ic_down.png')}
        />
      </View>
    );
  }

  renderField2(settings) {
    const {selectedItem, defaultText, getLabel} = settings;
    return (
      <View style={{flexDirection: 'row', alignItems: 'center', width: '100%'}}>
        {!selectedItem && (
          <Text style={[styles.text, {flex: 1}]}>{defaultText}</Text>
        )}
        {selectedItem && (
          <Text style={[styles.text, {flex: 1}]}>{getLabel(selectedItem)}</Text>
        )}
        <Image
          style={{
            resizeMode: 'contain',
            tintColor: Colors.light_grey,
            width: 10,
            height: 10,
          }}
          source={require('../../assets/ic_down.png')}
        />
      </View>
    );
  }

  renderOption(settings) {
    const {item, getLabel} = settings;
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
          paddingHorizontal: 15,
        }}>
        <Text
          style={{
            color: Colors.black,
            alignSelf: 'flex-start',
            padding: 15,
          }}>
          {getLabel(item)}
        </Text>
        <Text
          style={{height: 1, backgroundColor: Colors.secondary, width: '100%'}}>
          {' '}
        </Text>
      </View>
    );
  }

  layout() {
    const user = this.user;
    return (
      <View style={{flex: 1, marginTop: 10}}>
        {/* FullName */}
        <Text style={styles.label}>Full Name*</Text>
        {this.state.nameRejection !== '' && (
          <Text
            style={{
              color: 'red',
              fontFamily: 'urbanist_semi_bold',
              alignSelf: 'center',
            }}>
            {this.state.nameRejection}
          </Text>
        )}
        <TextInput
          style={
            this.state.fullName.trim().length > 0
              ? styles.textInputFilled
              : styles.textInput
          }
          value={this.state.fullName}
          onChangeText={text =>
            this.setState({fullName: text}, this.handleChange)
          }
          returnKeyType={'done'}
          autoCorrect={false}
          placeholder="Enter Full Name"
          placeholderTextColor={Colors.light_grey}
        />

        <View style={{marginTop: 20}} />

        {/* Gender */}
        <Text style={styles.label}>Gender*</Text>
        {this.state.genderRejection !== '' && (
          <Text
            style={{
              color: 'red',
              fontFamily: 'urbanist_semi_bold',
              alignSelf: 'center',
            }}>
            {this.state.genderRejection}
          </Text>
        )}
        <View style={styles.genderLayout}>
          <TouchableOpacity
            style={
              this.state.selectedGender === 'male'
                ? styles.enabledGender
                : this.state.selectedGender !== 'none'
                ? styles.filledGender
                : styles.disabledGender
            }
            onPress={() => {
              this.setState({['selectedGender']: 'male'}, this.handleChange);
            }}>
            <Image
              style={
                this.state.selectedGender === 'male'
                  ? styles.enabledIcon
                  : this.state.selectedGender !== 'none'
                  ? styles.filledIcon
                  : styles.disabledIcon
              }
              source={require('../../assets/male.png')}
            />
            <Text
              style={
                this.state.selectedGender === 'male'
                  ? {
                      fontSize: 14,
                      fontFamily: 'urbanist_regular',
                      color: Colors.white,
                    }
                  : this.state.selectedGender !== 'none'
                  ? {
                      fontSize: 14,
                      fontFamily: 'urbanist_regular',
                      color: Colors.secondary,
                    }
                  : {
                      fontSize: 14,
                      fontFamily: 'urbanist_regular',
                      color: Colors.disabledIcon,
                    }
              }>
              Male
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={
              this.state.selectedGender === 'female'
                ? styles.enabledGender
                : this.state.selectedGender !== 'none'
                ? styles.filledGender
                : styles.disabledGender
            }
            onPress={() => {
              this.setState({['selectedGender']: 'female'}, this.handleChange);
            }}>
            <Image
              style={
                this.state.selectedGender === 'female'
                  ? styles.enabledIcon
                  : this.state.selectedGender !== 'none'
                  ? styles.filledIcon
                  : styles.disabledIcon
              }
              source={require('../../assets/female.png')}
            />
            <Text
              style={
                this.state.selectedGender === 'female'
                  ? {
                      fontSize: 14,
                      fontFamily: 'urbanist_regular',
                      color: Colors.white,
                    }
                  : this.state.selectedGender !== 'none'
                  ? {
                      fontSize: 14,
                      fontFamily: 'urbanist_regular',
                      color: Colors.secondary,
                    }
                  : {
                      fontSize: 14,
                      fontFamily: 'urbanist_regular',
                      color: Colors.disabledIcon,
                    }
              }>
              Female
            </Text>
          </TouchableOpacity>
        </View>

        <View style={{marginTop: 20}} />

        {/* Date of Birth */}
        <Text style={styles.label}>Date of Birth*</Text>
        {this.state.dateRejection !== '' && (
          <Text
            style={{
              color: 'red',
              fontFamily: 'urbanist_semi_bold',
              alignSelf: 'center',
            }}>
            {this.state.dateRejection}
          </Text>
        )}
        <DatePicker
          modal
          mode="date"
          maximumDate={new Date(Date.now())}
          textColor={Colors.secondary}
          open={this.state.showPicker}
          date={this.state.dateObj}
          onConfirm={date => {
            let strDate =
              (date.getDate() > 9 ? date.getDate() : '0' + date.getDate()) +
              '-' +
              (date.getMonth() > 8
                ? date.getMonth() + 1
                : '0' + (date.getMonth() + 1)) +
              '-' +
              date.getFullYear();
            const currentDate = strDate || this.state.date;

            this.setState({
              date: currentDate,
              showPicker: false,
              dateObj: date,
            });
          }}
          onCancel={() => {
            this.setState({
              showPicker: false,
            });
          }}
        />
        <View
          style={
            this.state.date !== ''
              ? {
                  flexDirection: 'row',
                  borderRadius: 10,
                  borderWidth: 0.7,
                  borderColor: Colors.secondary,
                  paddingHorizontal: 10,
                }
              : {
                  flexDirection: 'row',
                  borderRadius: 10,
                  borderWidth: 0.7,
                  borderColor: Colors.light_grey,
                  paddingHorizontal: 10,
                }
          }>
          <TextInput
            style={{
              flex: 1,
              fontSize: 16,
              color: Colors.text_dark,
              fontFamily: 'urbanist_regular',
            }}
            onFocus={() => {
              this.setState({showPicker: true});
            }}
            editable={false}
            value={this.state.date}
            onChangeText={text => this.handleChange()}
            maxLength={15}
            returnKeyType={'done'}
            autoCorrect={false}
            placeholder="Select Date"
            placeholderTextColor={Colors.disabledIcon}
          />
          <TouchableOpacity
            style={{justifyContent: 'center'}}
            onPress={() => {
              this.setState({showPicker: true});
            }}>
            <Image
              style={
                this.state.date !== '' > 0
                  ? {
                      height: 20,
                      width: 20,
                      resizeMode: 'contain',
                      tintColor: Colors.secondary,
                    }
                  : {
                      height: 20,
                      width: 20,
                      resizeMode: 'contain',
                      tintColor: Colors.light_grey,
                    }
              }
              source={require('../../assets/calendar.png')}
            />
          </TouchableOpacity>
        </View>

        <View style={{marginTop: 20}} />

        {/* Email */}
        <Text style={styles.label}>Email Address</Text>
        {this.state.emailRejection !== '' && (
          <Text
            style={{
              color: 'red',
              fontFamily: 'urbanist_semi_bold',
              alignSelf: 'center',
            }}>
            {this.state.emailRejection}
          </Text>
        )}
        <TextInput
          style={
            this.state.email.trim().length > 0
              ? styles.textInputFilled
              : styles.textInput
          }
          maxLength={35}
          value={this.state.email}
          onChangeText={text => this.setState({email: text})}
          keyboardType="email-address"
          returnKeyType={'done'}
          autoCorrect={false}
          placeholder="abc@jkl.xyz"
          placeholderTextColor={Colors.light_grey}
        />

        {/* Specialization */}
        {user.role === 'doctor' && (
          <View>
            <View style={{marginTop: 20}} />
            <Text style={styles.label}>Specialization*</Text>
            {this.state.specializationRejection !== '' && (
              <Text
                style={{
                  color: 'red',
                  fontFamily: 'urbanist_semi_bold',
                  alignSelf: 'center',
                }}>
                {this.state.specializationRejection}
              </Text>
            )}
            <CustomPicker
              style={[
                this.state.selectedSpecialization !== ''
                  ? styles.textInputFilled
                  : styles.textInput,
                {height: 50, justifyContent: 'center'},
              ]}
              getLabel={item => item}
              fieldTemplate={
                this.state.selectedSpecialization === ''
                  ? this.renderField
                  : this.renderField2
              }
              optionTemplate={this.renderOption}
              headerTemplate={this.renderSpecializationHeader}
              placeholder={
                this.state.selectedSpecialization === ''
                  ? 'Please select your specialization'
                  : this.state.selectedSpecialization
              }
              options={this.specializations}
              onValueChange={value => {
                this.setState(
                  {selectedSpecialization: value !== null ? value : ''},
                  this.handleChange,
                );
              }}
            />
          </View>
        )}

        <View style={{marginTop: 20}} />

        {/* Country */}
        <Text style={styles.label}>Country*</Text>
        {this.state.countryRejection !== '' && (
          <Text
            style={{
              color: 'red',
              fontFamily: 'urbanist_semi_bold',
              alignSelf: 'center',
            }}>
            {this.state.countryRejection}
          </Text>
        )}
        <CustomPicker
          style={[
            this.state.selectedCountry !== ''
              ? styles.textInputFilled
              : styles.textInput,
            {height: 50, justifyContent: 'center'},
          ]}
          getLabel={item => item}
          fieldTemplate={
            this.state.selectedCountry === ''
              ? this.renderField
              : this.renderField2
          }
          optionTemplate={this.renderOption}
          headerTemplate={this.renderCountryHeader}
          placeholder={
            this.state.selectedCountry === ''
              ? 'Please select your country'
              : this.state.selectedCountry
          }
          options={this.countries}
          onValueChange={value => {
            this.setState(
              {selectedCountry: value !== null ? value : ''},
              this.handleChange,
            );
          }}
        />

        <View style={{marginTop: 20}} />

        {/* District */}
        <Text style={styles.label}>District*</Text>
        {this.state.districtRejection !== '' && (
          <Text
            style={{
              color: 'red',
              fontFamily: 'urbanist_semi_bold',
              alignSelf: 'center',
            }}>
            {this.state.districtRejection}
          </Text>
        )}
        <CustomPicker
          style={[
            this.state.selectedDistrict !== ''
              ? styles.textInputFilled
              : styles.textInput,
            {height: 50, justifyContent: 'center'},
          ]}
          getLabel={item => item}
          fieldTemplate={
            this.state.selectedDistrict === ''
              ? this.renderField
              : this.renderField2
          }
          optionTemplate={this.renderOption}
          headerTemplate={this.renderDistrictHeader}
          placeholder={
            this.state.selectedDistrict === ''
              ? 'Please select your district'
              : this.state.selectedDistrict
          }
          options={this.districts}
          onValueChange={value => {
            this.setState(
              {selectedDistrict: value !== null ? value : ''},
              this.handleChange,
            );
          }}
        />

        <View style={{marginTop: 20}} />

        {/* City */}
        <Text style={styles.label}>City*</Text>
        {this.state.cityRejection !== '' && (
          <Text
            style={{
              color: 'red',
              fontFamily: 'urbanist_semi_bold',
              alignSelf: 'center',
            }}>
            {this.state.cityRejection}
          </Text>
        )}
        <CustomPicker
          style={[
            this.state.selectedCity !== ''
              ? styles.textInputFilled
              : styles.textInput,
            {height: 50, justifyContent: 'center'},
          ]}
          getLabel={item => item}
          fieldTemplate={
            this.state.selectedCity === ''
              ? this.renderField
              : this.renderField2
          }
          optionTemplate={this.renderOption}
          headerTemplate={this.renderCityHeader}
          placeholder={
            this.state.selectedCity === ''
              ? 'Please select your city'
              : this.state.selectedCity
          }
          options={this.state.cities}
          onValueChange={value => {
            this.setState(
              {selectedCity: value !== null ? value : ''},
              this.handleChange,
            );
          }}
        />

        {/* Biography */}
        {user.role !== 'user' && (
          <View style={{marginTop: 20}}>
            <Text style={[styles.label]}>Biography*</Text>
            {this.state.biographyRejection !== '' && (
              <Text
                style={{
                  color: 'red',
                  fontFamily: 'urbanist_semi_bold',
                  alignSelf: 'center',
                }}>
                {this.state.biographyRejection}
              </Text>
            )}
            <View
              style={
                this.state.biography.trim().length > 0
                  ? styles.textInputFilled
                  : styles.textInput
              }>
              <TextInput
                style={{
                  fontSize: 17,
                  color: Colors.dark_grey,
                  fontFamily: 'urbanist_light',
                  textAlignVertical: 'top',
                  height: 120,
                }}
                maxLength={150}
                value={this.state.biography}
                onChangeText={text =>
                  this.setState(
                    {
                      biography: text,
                    },
                    this.handleChange,
                  )
                }
                multiline={true}
                numberOfLines={10}
                blurOnSubmit={false}
                editable={true}
                returnKeyType={'done'}
                autoCorrect={false}
                placeholder="Describe yourself..."
                placeholderTextColor={Colors.light_grey}
              />
              <Text
                style={{
                  color: Colors.light_grey,
                  width: '100%',
                  textAlign: 'right',
                }}>
                {this.state.biography.length}/150 characters
              </Text>
            </View>
          </View>
        )}
      </View>
    );
  }

  renderLayouts() {
    return this.layout();
  }

  navigateToDashboard() {
    SharedPreferences.setItem('user', JSON.stringify(this.user));
    if (this.user.role !== 'user')
      this.props.navigation.navigate('under_review');
    else {
      const resetAction = CommonActions.reset({
        index: 0,
        routes: [{name: 'dashboard'}],
      });
      this.props.navigation.dispatch(resetAction);
    }
  }

  upload() {
    this.body = {
      name: this.state.fullName,
      gender: this.state.selectedGender,
      dob: this.user.dateObj,
      email: this.state.email,
      district: this.state.selectedDistrict,
      city: this.state.selectedCity,
      country: this.state.selectedCountry,
      biography: this.state.biography,
    };

    this.user.fullName = this.state.fullName;
    this.user.gender = this.state.selectedGender;
    this.user.dateOfBirth = this.state.date;
    this.user.email = this.state.email;
    this.user.district = this.state.selectedDistrict;
    this.user.city = this.state.selectedCity;
    if (this.user.role !== 'user') this.user.biography = this.state.biography;

    console.log('Entering To upload image');
    this.checkForImage(this.body);
  }

  imageResizer(uri) {
    return new Promise((resolve, reject) => {
      ImageResizer.createResizedImage(uri, 600, 600, 'JPEG', 100, 0)
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  uploadToFirebase(uri, filename) {
    return new Promise((resolve, reject) => {
      storage()
        .ref('Docs/' + filename)
        .putFile(uri)
        .then(snapshot => {
          resolve(snapshot);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  async checkForImage(body) {
    let {path, filename} = this.state.avatar;
    await this.imageResizer(path)
      .then(async response => {
        let uploadUri =
          Platform.OS === 'ios'
            ? response.uri.replace('file://', '')
            : response.uri;

        await this.uploadToFirebase(uploadUri, filename)
          .then(async snapshot => {
            if (snapshot.state === 'success') {
              const url = await storage()
                .ref('Docs/' + filename)
                .getDownloadURL();
              console.log(url);
              this.body['avatar'] = url;
              this.user.avatar = url;
              this.body['issuesInProfile'] = [];
              Globals.user = this.user;
              this.performAPI();
            }
          })
          .catch(error => {
            console.log(error);
          });
      })
      .catch(error => {
        console.log(error);
      });
  }

  performAPI() {
    console.log(this.body);
    axios
      .patch(BASE_URL + '/users/' + this.user.id, this.body, {
        headers: {
          Authorization: 'Bearer ' + this.user.accessToken,
        },
      })
      .then(response => {
        if (this.toUpdate) {
          this.setState(
            {
              visible: false,
            },
            this.handleBackButtonClick,
          );
        } else {
          this.setState(
            {
              visible: false,
            },
            this.navigateToDashboard,
          );
        }
      })
      .catch(error => {
        this.setState({
          visible: false,
        });
        console.log(error);
        if (Platform.OS === 'android') {
          ToastAndroid.showWithGravityAndOffset(
            error.message,
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50,
          );
        }
      });
  }

  navigate() {
    const resetAction = CommonActions.reset({
      index: 0,
      routes: [{name: 'dashboard'}],
    });

    this.props.navigation.dispatch(resetAction);
  }

  manageRejections(issues) {
    for (obj in issues) {
      if (obj.type === 'name') {
        this.setState({
          nameRejection: obj.message,
        });
      } else if (obj.type === 'gender') {
        this.setState({
          genderRejection: obj.message,
        });
      } else if (obj.type === 'dob') {
        this.setState({
          dateRejection: obj.message,
        });
      } else if (obj.type === 'email') {
        this.setState({
          emailRejection: obj.message,
        });
      } else if (obj.type === 'name') {
        this.setState({
          nameRejection: obj.message,
        });
      } else if (obj.type === 'specialization') {
        this.setState({
          specializationRejection: obj.message,
        });
      } else if (obj.type === 'country') {
        this.setState({
          countryRejection: obj.message,
        });
      } else if (obj.type === 'district') {
        this.setState({
          districtRejection: obj.message,
        });
      } else if (obj.type === 'city') {
        this.setState({
          cityRejection: obj.message,
        });
      } else if (obj.type === 'biography') {
        this.setState({
          biographyRejection: obj.message,
        });
      } else if (obj.type === 'avatar') {
        this.setState({
          avatarRejection: obj.message,
        });
      }
    }
  }

  render() {
    const {issues} = this.props;
    this.manageRejections(issues);

    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <ProgressBar
          size={48}
          determinate={false}
          thickness={4}
          visible={this.state.visible}
          color={Colors.primary}
        />

        <SafeAreaView style={{flex: 1}}>
          <StatusBar barStyle="dark-content" backgroundColor={Colors.white} />

          <View
            style={{
              flexDirection: 'row',
              padding: Globals.pHorizontal,
              borderBottomColor: Colors.lighter_gery,
              borderBottomWidth: 1,
            }}>
            <Text
              style={{
                fontFamily: 'urbanist_semi_bold',
                fontSize: 20,
                color: Colors.text_dark,
                flex: 1,
                textAlign: 'center',
                alignSelf: 'center',
              }}>
              Update Profile
            </Text>
          </View>

          <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="always"
            keyboardDismissMode="on-drag"
            contentContainerStyle={{
              marginTop: 30,
              paddingHorizontal: 10,
              paddingBottom: this.navbarHeight,
              marginHorizontal: 7,
            }}
            showVerticalScrollIndicator={false}>
            {!this.toUpdate && (
              <Text style={styles.title}>Set up your Profile</Text>
            )}
            {!this.toUpdate && (
              <Text style={styles.description}>
                Please fill in the information to complete the sign up process.
              </Text>
            )}

            {!this.toUpdate && <View style={{marginTop: 20}} />}

            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
              }}>
              <Avatar
                size="xlarge"
                onPress={
                  this.state.avatar.uri === '' &&
                  function () {
                    this.requestCameraPermission();
                  }.bind(this)
                }
                containerStyle={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: Colors.avatar_bg,
                  borderColor: Colors.secondary,
                  borderWidth: 2,
                }}
                rounded
                source={{uri: this.state.avatar.uri}}>
                {this.state.avatar.uri === '' && (
                  <Image
                    style={{
                      height: 50,
                      width: 50,
                      position: 'absolute',
                      resizeMode: 'contain',
                    }}
                    source={require('../../assets/thumbnail.png')}
                  />
                )}
              </Avatar>
              {this.state.avatar.uri !== '' && (
                <Avatar
                  size="small"
                  onPress={function () {
                    this.requestCameraPermission();
                  }.bind(this)}
                  containerStyle={{
                    backgroundColor: Colors.secondary,
                    alignSelf: 'flex-start',
                    right: 0,
                    marginTop: 10,
                    position: 'absolute',
                  }}
                  rounded
                  source={require('../../assets/edit_avatar.png')}
                />
              )}
            </View>
            {this.state.avatarRejection !== '' && (
              <Text
                style={{
                  color: 'red',
                  fontFamily: 'urbanist_semi_bold',
                  alignSelf: 'center',
                }}>
                {this.state.avatarRejection}
              </Text>
            )}

            {this.renderLayouts()}

            <View style={{width: '100%', marginTop: 15}}>
              <TouchableOpacity
                style={this.state.buttonStyle}
                disabled={!this.state.buttonDisability || this.state.visible}
                onPress={() => {
                  this.setState(
                    {
                      visible: true,
                    },
                    this.upload,
                  );
                }}>
                <Text
                  style={{
                    color: Colors.white,
                    alignSelf: 'center',
                    fontSize: 16,
                    fontFamily: 'urbanist_bold',
                  }}>
                  {this.toUpdate
                    ? 'Update'
                    : this.state.selectedTab === 0
                    ? 'Submit'
                    : this.state.mediaType === 'doctor'
                    ? 'Continue'
                    : 'Submit'}
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>

          <TouchableOpacity
            style={styles.imgLL}
            onPress={() => {
              this.handleBackButtonClick();
            }}>
            <Image
              style={styles.back}
              source={require('../../assets/back2.png')}
            />
          </TouchableOpacity>
        </SafeAreaView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  back: {
    width: 25,
    height: 25,
  },
  imgLL: {
    position: 'absolute',
    top: Globals.pHorizontal,
    left: Globals.pHorizontal,
  },
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  title: {
    fontSize: 32,
    color: Colors.text_dark,
    fontFamily: 'urbanist_bold',
  },
  description: {
    fontSize: 16,
    fontFamily: 'urbanist_regular',
    color: Colors.text_light,
  },
  tabLayout: {
    backgroundColor: Colors.enabled,
    borderRadius: 10,
    padding: 5,
    margin: 7,
    flexDirection: 'row',
  },
  enabled_tab: {
    flex: 0.5,
    backgroundColor: Colors.secondary,
    borderRadius: 10,
    padding: 10,
  },
  enabled_tab_text: {
    color: Colors.white,
    textAlign: 'center',
    fontFamily: 'urbanist_regular',
    fontSize: 14,
  },
  disabled_tab: {
    flex: 0.5,
    backgroundColor: Colors.enabled,
    borderRadius: 10,
    padding: 10,
  },
  disabled_tab_text: {
    color: Colors.secondary,
    fontFamily: 'urbanist_regular',
    fontSize: 14,
    textAlign: 'center',
  },
  label: {
    fontFamily: 'urbanist_bold',
    fontSize: 16,
    color: Colors.text_dark,
    marginBottom: 5,
  },
  textInput: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.light_grey,
    fontSize: 16,
    paddingHorizontal: 10,
    color: Colors.disabledIcon,
    fontFamily: 'urbanist_regular',
  },
  textInputFilled: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.secondary,
    fontSize: 16,
    paddingHorizontal: 10,
    color: Colors.text_dark,
    fontFamily: 'urbanist_regular',
  },
  disabledGender: {
    flex: 0.48,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.7,
    fontSize: 14,
    alignItems: 'center',
    padding: 10,
    borderColor: Colors.disabledIcon,
  },
  filledGender: {
    flex: 0.48,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.7,
    alignItems: 'center',
    padding: 10,
    borderColor: Colors.secondary,
  },
  enabledGender: {
    flex: 0.48,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    borderRadius: 7,
    padding: 10,
  },
  disabledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.disabledIcon,
  },
  enabledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.white,
  },
  filledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.secondary,
  },
  genderLayout: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button1: {
    width: '100%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  button2: {
    width: '100%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.disabled,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  placeholder_text: {
    fontFamily: 'urbanist_regular',
    fontSize: 16,
    color: Colors.disabledIcon,
  },
  text: {
    fontFamily: 'urbanist_regular',
    fontSize: 16,
    color: Colors.text_dark,
  },
});
