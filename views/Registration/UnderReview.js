import React, {Component} from 'react';
import {BackHandler} from 'react-native';
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import SharedPreferences from 'react-native-shared-preferences';
import Colors from '../../utils/Colors';
import {CommonActions} from '@react-navigation/native';
import axios from 'react-native-axios';
import {BASE_URL} from '@env';
import User from '../../models/User';
import Globals from '../../utils/Globals';

export default class UnderReview extends Component {
  state = {
    user: Globals.user,
    mode: !Globals.user.isVerified ? 'documents' : 'profile',
    path: '../../assets/verifying.png',
    isVerifying: true,
  };

  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.navigateToProfile = this.navigateToProfile.bind(this);
    this.verify = this.verify.bind(this);
    this.dispatchIssue = this.dispatchIssue.bind(this);

    this.timer = setInterval(() => this.verify(), 10000);
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    console.log(this.props);
    if (this.props.route.params !== undefined)
      this.props.route.params.onGoBack();
    else {
      const resetAction = CommonActions.reset({
        index: 0,
        routes: [
          {
            name:
              this.state.user.role === 'doctor'
                ? 'doctor_registration'
                : 'basic_registration',
            params: {
              user: this.state.user,
            },
          },
        ],
      });
      this.props.navigation.dispatch(resetAction);
    }
    this.props.navigation.goBack();
    return true;
  }

  verify() {
    axios
      .get(BASE_URL + '/users/' + this.state.user.id, {
        headers: {
          Authorization: 'Bearer ' + this.state.user.accessToken,
        },
      })
      .then(response => {
        if (this.state.mode === 'documents') {
          if (response.data.data.isVerified) {
            this.state.user.isVerified = true;
            Globals.user = this.state.user;
            this.setState(
              {
                user: this.state.user,
              },
              this.navigateToProfile,
            );
          } else if (response.data.data.issuesInDocuments.length > 0) {
            Globals.issues = response.data.data.issuesInDocuments;
            console.log('Issues: ' + JSON.stringify(Globals.issues));
            this.setState({
              path: '../../assets/apology.png',
              isVerifying: false,
            });
            clearInterval(this.timer);
          }
        } else {
          if (response.data.data.isApproved) {
            this.state.user.isApproved = true;
            Globals.user = this.state.user;
            this.setState(
              {
                user: this.state.user,
              },
              this.navigateToProfile,
            );
          } else if (response.data.data.issuesInProfile.length > 0) {
            Globals.issues = JSON.parse(response.data.data.issuesInProfile);
            this.setState({
              path: '../../assets/apology.png',
              isVerifying: false,
            });
            clearInterval(this.timer);
          }
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  dispatchIssue(data) {
    let {issues, actions} = this.props;
    issues = data;
    actions(issues);
  }

  navigateToProfile() {
    if (this.state.mode === 'documents') {
      SharedPreferences.setItem('user', JSON.stringify(this.state.user));

      const resetAction = CommonActions.reset({
        index: 0,
        routes: [{name: 'profile_builder'}],
      });
      this.props.navigation.dispatch(resetAction);
    } else {
      SharedPreferences.setItem('user', JSON.stringify(this.state.user));

      const resetAction = CommonActions.reset({
        index: 0,
        routes: [{name: 'dashboard'}],
      });
      this.props.navigation.dispatch(resetAction);
    }
    clearInterval(this.timer);
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="dark-content" backgroundColor={Colors.white} />
        <Image
          source={
            this.state.isVerifying
              ? require('../../assets/verifying.png')
              : require('../../assets/apology.png')
          }
          style={[styles.success, {marginTop: 60}]}
        />

        {this.state.isVerifying && (
          <Text style={styles.text1}>Verifying...</Text>
        )}
        {!this.state.isVerifying && (
          <Text style={styles.text1}>Apologies!</Text>
        )}

        {this.state.isVerifying && (
          <Text style={styles.text2}>
            We have received your request. Your profile is under review, please
            allow us sometime
          </Text>
        )}
        {!this.state.isVerifying && (
          <Text style={styles.text2}>
            We cannot move with your request this time because of some missing
            documents.
          </Text>
        )}

        {!this.state.isVerifying && (
          <View style={{bottom: 20, position: 'absolute', width: '100%'}}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                this.handleBackButtonClick();
              }}>
              <Text style={styles.text3}>Review Application</Text>
            </TouchableOpacity>
          </View>
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  success: {
    width: 250,
    height: 250,
    resizeMode: 'contain',
  },
  button: {
    width: '90%',
    alignSelf: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  text1: {
    width: '100%',
    textAlign: 'center',
    fontSize: 32,
    fontFamily: 'urbanist_bold',
    color: Colors.text_dark,
    marginTop: 15,
    marginHorizontal: 30,
  },
  text2: {
    width: '100%',
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'urbanist_regular',
    color: Colors.text_light,
    marginHorizontal: 30,
  },
  text3: {
    color: Colors.white,
    alignSelf: 'center',
    fontSize: 16,
    fontFamily: 'urbanist_bold',
  },
});
