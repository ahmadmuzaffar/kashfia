import React, {Component} from 'react';
import {
  BackHandler,
  StyleSheet,
  Dimensions,
  ScrollView,
  StatusBar,
  Text,
  TextInput,
  ToastAndroid,
  KeyboardAvoidingView,
  PermissionsAndroid,
  View,
  Platform,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import Colors from '../../utils/Colors';
import {CommonActions} from '@react-navigation/native';
import axios from 'react-native-axios';
import Tags from 'react-native-tags';
import SharedPreferences from 'react-native-shared-preferences';
import {BASE_URL} from '@env';
import storage from '@react-native-firebase/storage';
import ImageResizer from 'react-native-image-resizer';
import {ProgressBar} from 'react-native-material-indicators';
import {YellowBox} from 'react-native';
import User from '../../models/User';
import {CustomPicker} from 'react-native-custom-picker';
import DatePicker from 'react-native-date-picker';
import Globals from '../../utils/Globals';
import DocumentPicker from 'react-native-document-picker';
import DocTags from '../../custom/views/DocTags';

export default class BasicRegistration extends Component {
  state = {
    selectedTab: 0,
    fullName: '',
    selectedGender: 'none',
    selectedMedicalStaff: 'none',
    selectedUserType: 'User',
    date: '',
    dateObj: new Date(),
    email: '',
    showPicker: false,
    selectedCity: '',
    selectedDistrict: '',
    selectedCountry: 'Iraq',
    selectedSpecialization: '',
    cities: [],
    buttonStyle: styles.button2,
    buttonDisability: false,
    totalDocuments: '',
    tags: [],
    visible: false,
    counter: 0,
    issues: Globals.issues,
  };

  constructor(props) {
    super(props);
    console.log(props);

    YellowBox.ignoreWarnings(['Warning: ...']);
    console.disableYellowBox = true;
    this.districtRef = React.createRef(null);
    this.body = {};
    this.imageName = '';
    this.dateToSend = '';
    this.urls = [];
    this.user = Globals.user;
    this.requestCameraPermission = async () => {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Kashfia App Camera Permission',
            message:
              'Kashfia App needs access to your camera ' +
              'so you can take pictures.',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          try {
            const results = await DocumentPicker.pickMultiple({
              type: [DocumentPicker.types.images, DocumentPicker.types.pdf],
              copyTo: 'cachesDirectory',
            });
            if (this.state.tags.length + results.length > 3) {
              ToastAndroid.showWithGravityAndOffset(
                'Max 3 files can be uploaded',
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
                25,
                50,
              );
              return;
            }
            for (const res of results) {
              let mb = res.size / 1024.0 / 1024.0;
              mb = mb.toFixed(2);
              let entry = {
                name: res.name,
                type: res.type,
                uri: res.uri,
                size: res.size,
                path: res.fileCopyUri,
              };
              this.state.tags.push(res.name + '  (' + mb + ' MB)');
              this.user.graduationCertificates.files.push(entry);
              console.log(this.user.graduationCertificates.files);
            }
            this.setState(
              {
                tags: this.state.tags,
                totalDocuments: this.state.tags.length + ' file(s) selected',
              },
              this.handleChange,
            );
          } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
              throw err;
            }
          }
        } else {
          console.log('Permission denied');
        }
      } catch (err) {
        console.warn(err);
      }
    };

    this.districts = [
      'Al Anbar',
      'Muthanna',
      'Qadisiyyah',
      'Babil',
      'Baghdad',
      'Basra',
      'Dhi',
      'Diyala',
      'Karbala',
      'Kirkuk',
      'Maysan',
      'Najaf',
      'Nineveh',
      'Saladin',
      'Wasit',
      'Dohuk',
      'Erbil',
      'Sulaymaniyah',
      'Halabja',
    ];

    this.countries = [
      'Afghanistan',
      'Albania',
      'Algeria',
      'American Samoa',
      'Andorra',
      'Angola',
      'Anguilla',
      'Antarctica',
      'Antigua and Barbuda',
      'Argentina',
      'Armenia',
      'Aruba',
      'Australia',
      'Austria',
      'Azerbaijan',
      'Bahamas (the)',
      'Bahrain',
      'Bangladesh',
      'Barbados',
      'Belarus',
      'Belgium',
      'Belize',
      'Benin',
      'Bermuda',
      'Bhutan',
      'Bolivia (Plurinational State of)',
      'Bonaire, Sint Eustatius and Saba',
      'Bosnia and Herzegovina',
      'Botswana',
      'Bouvet Island',
      'Brazil',
      'British Indian Ocean Territory (the)',
      'Brunei Darussalam',
      'Bulgaria',
      'Burkina Faso',
      'Burundi',
      'Cabo Verde',
      'Cambodia',
      'Cameroon',
      'Canada',
      'Cayman Islands (the)',
      'Central African Republic (the)',
      'Chad',
      'Chile',
      'China',
      'Christmas Island',
      'Cocos (Keeling) Islands (the)',
      'Colombia',
      'Comoros (the)',
      'Congo (the Democratic Republic of the)',
      'Congo (the)',
      'Cook Islands (the)',
      'Costa Rica',
      'Croatia',
      'Cuba',
      'Curaçao',
      'Cyprus',
      'Czechia',
      "Côte d'Ivoire",
      'Denmark',
      'Djibouti',
      'Dominica',
      'Dominican Republic (the)',
      'Ecuador',
      'Egypt',
      'El Salvador',
      'Equatorial Guinea',
      'Eritrea',
      'Estonia',
      'Eswatini',
      'Ethiopia',
      'Falkland Islands (the) [Malvinas]',
      'Faroe Islands (the)',
      'Fiji',
      'Finland',
      'France',
      'French Guiana',
      'French Polynesia',
      'French Southern Territories (the)',
      'Gabon',
      'Gambia (the)',
      'Georgia',
      'Germany',
      'Ghana',
      'Gibraltar',
      'Greece',
      'Greenland',
      'Grenada',
      'Guadeloupe',
      'Guam',
      'Guatemala',
      'Guernsey',
      'Guinea',
      'Guinea-Bissau',
      'Guyana',
      'Haiti',
      'Heard Island and McDonald Islands',
      'Holy See (the)',
      'Honduras',
      'Hong Kong',
      'Hungary',
      'Iceland',
      'India',
      'Indonesia',
      'Iran (Islamic Republic of)',
      'Iraq',
      'Ireland',
      'Isle of Man',
      'Israel',
      'Italy',
      'Jamaica',
      'Japan',
      'Jersey',
      'Jordan',
      'Kazakhstan',
      'Kenya',
      'Kiribati',
      "Korea (the Democratic People's Republic of)",
      'Korea (the Republic of)',
      'Kuwait',
      'Kyrgyzstan',
      "Lao People's Democratic Republic (the)",
      'Latvia',
      'Lebanon',
      'Lesotho',
      'Liberia',
      'Libya',
      'Liechtenstein',
      'Lithuania',
      'Luxembourg',
      'Macao',
      'Madagascar',
      'Malawi',
      'Malaysia',
      'Maldives',
      'Mali',
      'Malta',
      'Marshall Islands (the)',
      'Martinique',
      'Mauritania',
      'Mauritius',
      'Mayotte',
      'Mexico',
      'Micronesia (Federated States of)',
      'Moldova (the Republic of)',
      'Monaco',
      'Mongolia',
      'Montenegro',
      'Montserrat',
      'Morocco',
      'Mozambique',
      'Myanmar',
      'Namibia',
      'Nauru',
      'Nepal',
      'Netherlands (the)',
      'New Caledonia',
      'New Zealand',
      'Nicaragua',
      'Niger (the)',
      'Nigeria',
      'Niue',
      'Norfolk Island',
      'Northern Mariana Islands (the)',
      'Norway',
      'Oman',
      'Pakistan',
      'Palau',
      'Palestine, State of',
      'Panama',
      'Papua New Guinea',
      'Paraguay',
      'Peru',
      'Philippines (the)',
      'Pitcairn',
      'Poland',
      'Portugal',
      'Puerto Rico',
      'Qatar',
      'Republic of North Macedonia',
      'Romania',
      'Russian Federation (the)',
      'Rwanda',
      'Réunion',
      'Saint Barthélemy',
      'Saint Helena, Ascension and Tristan da Cunha',
      'Saint Kitts and Nevis',
      'Saint Lucia',
      'Saint Martin (French part)',
      'Saint Pierre and Miquelon',
      'Saint Vincent and the Grenadines',
      'Samoa',
      'San Marino',
      'Sao Tome and Principe',
      'Saudi Arabia',
      'Senegal',
      'Serbia',
      'Seychelles',
      'Sierra Leone',
      'Singapore',
      'Sint Maarten (Dutch part)',
      'Slovakia',
      'Slovenia',
      'Solomon Islands',
      'Somalia',
      'South Africa',
      'South Georgia and the South Sandwich Islands',
      'South Sudan',
      'Spain',
      'Sri Lanka',
      'Sudan (the)',
      'Suriname',
      'Svalbard and Jan Mayen',
      'Sweden',
      'Switzerland',
      'Syrian Arab Republic',
      'Taiwan',
      'Tajikistan',
      'Tanzania, United Republic of',
      'Thailand',
      'Timor-Leste',
      'Togo',
      'Tokelau',
      'Tonga',
      'Trinidad and Tobago',
      'Tunisia',
      'Turkey',
      'Turkmenistan',
      'Turks and Caicos Islands (the)',
      'Tuvalu',
      'Uganda',
      'Ukraine',
      'United Arab Emirates (the)',
      'United Kingdom of Great Britain and Northern Ireland (the)',
      'United States Minor Outlying Islands (the)',
      'United States of America (the)',
      'Uruguay',
      'Uzbekistan',
      'Vanuatu',
      'Venezuela (Bolivarian Republic of)',
      'Viet Nam',
      'Virgin Islands (British)',
      'Virgin Islands (U.S.)',
      'Wallis and Futuna',
      'Western Sahara',
      'Yemen',
      'Zambia',
      'Zimbabwe',
      'Åland Islands',
    ];

    this.specializations = [
      'General Medicine',
      'Cardiology',
      'Neurology',
      'Ophthalmology',
      'Dental Surgery',
      'Dermatology',
      'Orthopaedic',
      'Urology',
      'Pulmonology',
      'Gynecology',
      'Paediatrics',
      'Oncology',
    ];

    this.window = Dimensions.get('window');
    this.screenHeight = Dimensions.get('screen').height;
    this.windowHeight = Dimensions.get('window').height;
    this.navbarHeight =
      this.screenHeight - this.windowHeight + StatusBar.currentHeight;
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.getCities = this.getCities.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.getCities();
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );

    const user = this.user;
    this.setState(
      {
        selectedTab: user.role === 'user' ? 0 : 1,
        fullName: user.fullName,
        selectedGender: user.gender !== '' ? user.gender : 'none',
        selectedMedicalStaff: user.role !== 'user' ? user.role : 'none',
        selectedUserType:
          user.role === 'user' || user.role === 'none'
            ? 'User'
            : user.role === 'doctor'
            ? 'Doctor'
            : user.role === 'nurse'
            ? 'Nurse'
            : 'Pathological Analyst',
        date: user.dateOfBirth,
        dateObj: new Date(user.dateObj),
        email: user.email,
        selectedCity: user.city,
        selectedDistrict: user.district,
        selectedCountry: user.country !== '' ? user.country : 'Iraq',
        selectedSpecialization: user.specialization,
        totalDocuments:
          user.graduationCertificates.tags.length > 0
            ? user.graduationCertificates.tags.length + ' file(s) selected'
            : '',
        tags:
          user.graduationCertificates.tags.length > 0
            ? user.graduationCertificates.tags
            : this.state.tags,
      },
      this.handleChange,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    BackHandler.exitApp();
    return true;
  }

  getCities() {
    axios
      .post('https://countriesnow.space/api/v0.1/countries/cities/', {
        country: 'iraq',
      })
      .then(response => {
        this.state.cities.push.apply(this.state.cities, response.data.data);
        this.setState({
          cities: this.state.cities,
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleChange() {
    if (
      this.state.fullName.trim().length > 0 &&
      this.state.date.trim().length > 0 &&
      this.state.selectedGender !== 'none' &&
      this.state.selectedCity !== '' &&
      this.state.selectedDistrict !== '' &&
      this.state.selectedCountry !== ''
    ) {
      if (
        this.state.selectedTab === 1 &&
        this.state.selectedMedicalStaff !== 'doctor'
      ) {
        if (
          this.state.selectedMedicalStaff !== 'none' &&
          this.state.tags.length > 0
        ) {
          this.setState({
            buttonStyle: styles.button1,
            buttonDisability: true,
          });
        } else {
          this.setState({
            buttonStyle: styles.button2,
            buttonDisability: false,
          });
        }
      } else if (
        this.state.selectedTab === 1 &&
        this.state.selectedMedicalStaff === 'doctor'
      ) {
        if (this.state.selectedSpecialization !== '') {
          this.setState({
            buttonStyle: styles.button1,
            buttonDisability: true,
          });
        }
      } else {
        this.setState({
          buttonStyle: styles.button1,
          buttonDisability: true,
        });
      }
    } else {
      this.setState({
        buttonStyle: styles.button2,
        buttonDisability: false,
      });
    }
  }

  renderSpecializationHeader() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          padding: 20,
        }}>
        <Text
          style={{
            fontFamily: 'urbanist_bold',
            fontSize: 20,
            color: Colors.black,
          }}>
          Select Specialization
        </Text>
      </View>
    );
  }

  renderCountryHeader() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          padding: 20,
        }}>
        <Text
          style={{
            fontFamily: 'urbanist_bold',
            fontSize: 20,
            color: Colors.black,
          }}>
          Select Country
        </Text>
      </View>
    );
  }

  renderDistrictHeader() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          padding: 20,
        }}>
        <Text
          style={{
            fontFamily: 'urbanist_bold',
            fontSize: 20,
            color: Colors.black,
          }}>
          Select District
        </Text>
      </View>
    );
  }

  renderCityHeader() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          padding: 20,
        }}>
        <Text
          style={{
            fontFamily: 'urbanist_bold',
            fontSize: 20,
            color: Colors.black,
          }}>
          Select City
        </Text>
      </View>
    );
  }

  renderField(settings) {
    const {selectedItem, defaultText, getLabel, clear} = settings;
    return (
      <View style={{flexDirection: 'row', alignItems: 'center', width: '100%'}}>
        {!selectedItem && (
          <Text style={[styles.placeholder_text, {flex: 1}]}>
            {defaultText}
          </Text>
        )}
        {selectedItem && (
          <Text style={[styles.text, {flex: 1}]}>{getLabel(selectedItem)}</Text>
        )}
        <Image
          style={{
            resizeMode: 'contain',
            tintColor: Colors.light_grey,
            width: 10,
            height: 10,
          }}
          source={require('../../assets/ic_down.png')}
        />
      </View>
    );
  }

  renderField2(settings) {
    const {selectedItem, defaultText, getLabel} = settings;
    return (
      <View style={{flexDirection: 'row', alignItems: 'center', width: '100%'}}>
        {!selectedItem && (
          <Text style={[styles.text, {flex: 1}]}>{defaultText}</Text>
        )}
        {selectedItem && (
          <Text style={[styles.text, {flex: 1}]}>{getLabel(selectedItem)}</Text>
        )}
        <Image
          style={{
            resizeMode: 'contain',
            tintColor: Colors.light_grey,
            width: 10,
            height: 10,
          }}
          source={require('../../assets/ic_down.png')}
        />
      </View>
    );
  }

  renderOption(settings) {
    const {item, getLabel} = settings;
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
          paddingHorizontal: 15,
        }}>
        <Text
          style={{
            color: Colors.black,
            alignSelf: 'flex-start',
            padding: 15,
          }}>
          {getLabel(item)}
        </Text>
        <Text
          style={{height: 1, backgroundColor: Colors.secondary, width: '100%'}}>
          {' '}
        </Text>
      </View>
    );
  }

  userLayout() {
    return (
      <View style={{flex: 1, marginTop: 10}}>
        {/* FullName */}
        <Text style={styles.label}>Full Name*</Text>
        <TextInput
          style={
            this.state.fullName.trim().length > 0
              ? styles.textInputFilled
              : styles.textInput
          }
          value={this.state.fullName}
          onChangeText={text =>
            this.setState({fullName: text}, this.handleChange)
          }
          returnKeyType={'done'}
          autoCorrect={false}
          placeholder="Enter Full Name"
          placeholderTextColor={Colors.light_grey}
        />

        <View style={{marginTop: 20}} />

        {/* Gender */}
        <Text style={styles.label}>Gender*</Text>
        <View style={styles.genderLayout}>
          <TouchableOpacity
            style={
              this.state.selectedGender === 'male'
                ? styles.enabledGender
                : this.state.selectedGender !== 'none'
                ? styles.filledGender
                : styles.disabledGender
            }
            onPress={() => {
              this.setState({['selectedGender']: 'male'}, this.handleChange);
            }}>
            <Image
              style={
                this.state.selectedGender === 'male'
                  ? styles.enabledIcon
                  : this.state.selectedGender !== 'none'
                  ? styles.filledIcon
                  : styles.disabledIcon
              }
              source={require('../../assets/male.png')}
            />
            <Text
              style={
                this.state.selectedGender === 'male'
                  ? {
                      fontSize: 14,
                      fontFamily: 'urbanist_regular',
                      color: Colors.white,
                    }
                  : this.state.selectedGender !== 'none'
                  ? {
                      fontSize: 14,
                      fontFamily: 'urbanist_regular',
                      color: Colors.secondary,
                    }
                  : {
                      fontSize: 14,
                      fontFamily: 'urbanist_regular',
                      color: Colors.disabledIcon,
                    }
              }>
              Male
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={
              this.state.selectedGender === 'female'
                ? styles.enabledGender
                : this.state.selectedGender !== 'none'
                ? styles.filledGender
                : styles.disabledGender
            }
            onPress={() => {
              this.setState({['selectedGender']: 'female'}, this.handleChange);
            }}>
            <Image
              style={
                this.state.selectedGender === 'female'
                  ? styles.enabledIcon
                  : this.state.selectedGender !== 'none'
                  ? styles.filledIcon
                  : styles.disabledIcon
              }
              source={require('../../assets/female.png')}
            />
            <Text
              style={
                this.state.selectedGender === 'female'
                  ? {
                      fontSize: 14,
                      fontFamily: 'urbanist_regular',
                      color: Colors.white,
                    }
                  : this.state.selectedGender !== 'none'
                  ? {
                      fontSize: 14,
                      fontFamily: 'urbanist_regular',
                      color: Colors.secondary,
                    }
                  : {
                      fontSize: 14,
                      fontFamily: 'urbanist_regular',
                      color: Colors.disabledIcon,
                    }
              }>
              Female
            </Text>
          </TouchableOpacity>
        </View>

        <View style={{marginTop: 20}} />

        {/* Date of Birth */}
        <Text style={styles.label}>Date of Birth*</Text>
        <DatePicker
          modal
          mode="date"
          maximumDate={new Date(Date.now())}
          textColor={Colors.secondary}
          open={this.state.showPicker}
          date={this.state.dateObj}
          onConfirm={date => {
            this.dateToSend = date.toISOString();
            let strDate =
              (date.getDate() > 9 ? date.getDate() : '0' + date.getDate()) +
              '-' +
              (date.getMonth() > 8
                ? date.getMonth() + 1
                : '0' + (date.getMonth() + 1)) +
              '-' +
              date.getFullYear();
            const currentDate = strDate || this.state.date;

            this.setState({
              date: currentDate,
              showPicker: false,
              dateObj: date,
            });
          }}
          onCancel={() => {
            this.setState({
              showPicker: false,
            });
          }}
        />
        <View
          style={
            this.state.date.trim().length > 0
              ? {
                  flexDirection: 'row',
                  borderRadius: 10,
                  borderWidth: 0.7,
                  borderColor: Colors.secondary,
                  paddingHorizontal: 10,
                }
              : {
                  flexDirection: 'row',
                  borderRadius: 10,
                  borderWidth: 0.7,
                  borderColor: Colors.light_grey,
                  paddingHorizontal: 10,
                }
          }>
          <TextInput
            style={{
              flex: 1,
              fontSize: 16,
              color: Colors.text_dark,
              fontFamily: 'urbanist_regular',
            }}
            onFocus={() => {
              this.setState({showPicker: true});
            }}
            editable={false}
            value={this.state.date}
            onChangeText={text => this.handleChange()}
            maxLength={15}
            returnKeyType={'done'}
            autoCorrect={false}
            placeholder="Select Date"
            placeholderTextColor={Colors.disabledIcon}
          />
          <TouchableOpacity
            style={{justifyContent: 'center'}}
            onPress={() => {
              this.setState({showPicker: true});
            }}>
            <Image
              style={
                this.state.date.trim().length > 0
                  ? {
                      height: 20,
                      width: 20,
                      resizeMode: 'contain',
                      tintColor: Colors.secondary,
                    }
                  : {
                      height: 20,
                      width: 20,
                      resizeMode: 'contain',
                      tintColor: Colors.light_grey,
                    }
              }
              source={require('../../assets/calendar.png')}
            />
          </TouchableOpacity>
        </View>

        <View style={{marginTop: 20}} />

        {/* Email */}
        <Text style={styles.label}>Email Address</Text>
        <TextInput
          style={
            this.state.email.trim().length > 0
              ? styles.textInputFilled
              : styles.textInput
          }
          value={this.state.email}
          onChangeText={text => this.setState({email: text})}
          keyboardType="email-address"
          returnKeyType={'done'}
          autoCorrect={false}
          placeholder="abc@jkl.xyz"
          placeholderTextColor={Colors.light_grey}
        />

        {this.state.selectedMedicalStaff === 'doctor' && (
          <View style={{marginTop: 20, width: '100%'}}>
            {/* Specialization */}
            <Text style={styles.label}>Specialization*</Text>
            <CustomPicker
              style={[
                this.state.selectedSpecialization !== ''
                  ? styles.textInputFilled
                  : styles.textInput,
                {height: 50, justifyContent: 'center'},
              ]}
              getLabel={item => item}
              fieldTemplate={this.renderField}
              optionTemplate={this.renderOption}
              headerTemplate={this.renderSpecializationHeader}
              placeholder={'Please select your specialization'}
              options={this.specializations}
              onValueChange={value => {
                this.setState(
                  {selectedSpecialization: value !== null ? value : ''},
                  this.handleChange,
                );
              }}
            />
          </View>
        )}

        <View style={{marginTop: 20}} />

        {/* Country */}
        <Text style={styles.label}>Country*</Text>
        <CustomPicker
          style={[
            this.state.selectedCountry !== ''
              ? styles.textInputFilled
              : styles.textInput,
            {height: 50, justifyContent: 'center'},
          ]}
          getLabel={item => item}
          fieldTemplate={
            this.state.selectedCountry === ''
              ? this.renderField
              : this.renderField2
          }
          optionTemplate={this.renderOption}
          headerTemplate={this.renderCountryHeader}
          placeholder={
            this.state.selectedCountry === ''
              ? 'Please select your country'
              : this.state.selectedCountry
          }
          options={this.countries}
          onValueChange={value => {
            this.setState(
              {selectedCountry: value !== null ? value : ''},
              this.handleChange,
            );
          }}
        />

        <View style={{marginTop: 20}} />

        {/* District */}
        <Text style={styles.label}>District*</Text>
        <CustomPicker
          style={[
            this.state.selectedDistrict !== ''
              ? styles.textInputFilled
              : styles.textInput,
            {height: 50, justifyContent: 'center'},
          ]}
          getLabel={item => item}
          fieldTemplate={
            this.state.selectedDistrict === ''
              ? this.renderField
              : this.renderField2
          }
          optionTemplate={this.renderOption}
          headerTemplate={this.renderDistrictHeader}
          placeholder={
            this.state.selectedDistrict === ''
              ? 'Please select your district'
              : this.state.selectedDistrict
          }
          options={this.districts}
          onValueChange={value => {
            this.setState(
              {selectedDistrict: value !== null ? value : ''},
              this.handleChange,
            );
          }}
        />

        <View style={{marginTop: 20}} />

        {/* City */}
        <Text style={styles.label}>City*</Text>
        <CustomPicker
          style={[
            this.state.selectedCity !== ''
              ? styles.textInputFilled
              : styles.textInput,
            {height: 50, justifyContent: 'center'},
          ]}
          getLabel={item => item}
          fieldTemplate={
            this.state.selectedCity === ''
              ? this.renderField
              : this.renderField2
          }
          optionTemplate={this.renderOption}
          headerTemplate={this.renderCityHeader}
          placeholder={
            this.state.selectedCity === ''
              ? 'Please select your city'
              : this.state.selectedCity
          }
          options={this.state.cities}
          onValueChange={value => {
            this.setState(
              {selectedCity: value !== null ? value : ''},
              this.handleChange,
            );
          }}
        />
      </View>
    );
  }

  refresh() {
    this.setState({
      issues: Globals.issues,
    });
  }

  medicalStaffLayout() {
    const {issues} = this.state;
    const {height, width} = this.window;

    return (
      <View style={{marginTop: 10}}>
        <Text style={styles.label}>Describe Yourself*</Text>
        <View style={{flexDirection: 'column'}}>
          <View style={styles.genderLayout}>
            <TouchableOpacity
              style={
                this.state.selectedMedicalStaff === 'doctor'
                  ? styles.enabledGender
                  : this.state.selectedMedicalStaff !== 'none'
                  ? styles.filledGender
                  : styles.disabledGender
              }
              onPress={() => {
                this.setState(
                  {selectedMedicalStaff: 'doctor', selectedUserType: 'Doctor'},
                  this.handleChange,
                );
              }}>
              <Text
                style={
                  this.state.selectedMedicalStaff === 'doctor'
                    ? {
                        fontSize: 14,
                        fontFamily: 'urbanist_regular',
                        color: Colors.white,
                      }
                    : this.state.selectedMedicalStaff !== 'none'
                    ? {
                        fontSize: 14,
                        fontFamily: 'urbanist_regular',
                        color: Colors.secondary,
                      }
                    : {
                        fontSize: 14,
                        fontFamily: 'urbanist_regular',
                        color: Colors.disabledIcon,
                      }
                }>
                Doctor
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                this.state.selectedMedicalStaff === 'nurse'
                  ? styles.enabledGender
                  : this.state.selectedMedicalStaff !== 'none'
                  ? styles.filledGender
                  : styles.disabledGender
              }
              onPress={() => {
                this.setState(
                  {selectedMedicalStaff: 'nurse', selectedUserType: 'Nurse'},
                  this.handleChange,
                );
              }}>
              <Text
                style={
                  this.state.selectedMedicalStaff === 'nurse'
                    ? {
                        fontSize: 14,
                        fontFamily: 'urbanist_regular',
                        color: Colors.white,
                      }
                    : this.state.selectedMedicalStaff !== 'none'
                    ? {
                        fontSize: 14,
                        fontFamily: 'urbanist_regular',
                        color: Colors.secondary,
                      }
                    : {
                        fontSize: 14,
                        fontFamily: 'urbanist_regular',
                        color: Colors.disabledIcon,
                      }
                }>
                Nurse
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{marginTop: 10}} />

          <TouchableOpacity
            style={
              this.state.selectedMedicalStaff === 'pathological_analyst'
                ? styles.enabledGender
                : this.state.selectedMedicalStaff !== 'none'
                ? styles.filledGender
                : styles.disabledGender
            }
            onPress={() => {
              this.setState(
                {
                  selectedMedicalStaff: 'pathological_analyst',
                  selectedUserType: 'Pathological Analyst',
                },
                this.handleChange,
              );
            }}>
            <Text
              style={
                this.state.selectedMedicalStaff === 'pathological_analyst'
                  ? {
                      fontSize: 14,
                      fontFamily: 'urbanist_regular',
                      color: Colors.white,
                    }
                  : this.state.selectedMedicalStaff !== 'none'
                  ? {
                      fontSize: 14,
                      fontFamily: 'urbanist_regular',
                      color: Colors.secondary,
                    }
                  : {
                      fontSize: 14,
                      fontFamily: 'urbanist_regular',
                      color: Colors.disabledIcon,
                    }
              }>
              Pathological Analyst
            </Text>
          </TouchableOpacity>
        </View>

        {this.userLayout()}

        {(this.state.selectedMedicalStaff === 'nurse' ||
          this.state.selectedMedicalStaff === 'pathological_analyst') && (
          <View style={{flexDirection: 'column', marginTop: 20}}>
            <Text style={issues.length > 0 ? styles.redLabel : styles.label}>
              Graduation Certificate* {issues.length > 0 ? '  (' : ''}
              {issues.length > 0 ? issues[0].message : ''}
              {issues.length > 0 ? ')' : ''}
            </Text>
            <View
              style={
                this.state.totalDocuments.trim().length > 0
                  ? {
                      flexDirection: 'row',
                      borderRadius: 10,
                      borderWidth: 0.7,
                      borderColor: Colors.secondary,
                      paddingLeft: 10,
                    }
                  : {
                      flexDirection: 'row',
                      borderRadius: 10,
                      borderWidth: 0.7,
                      borderColor: Colors.light_grey,
                      paddingLeft: 10,
                    }
              }>
              <TextInput
                style={{
                  flex: 1,
                  fontSize: 16,
                  color: Colors.text_dark,
                  fontFamily: 'urbanist_regular',
                }}
                editable={false}
                value={this.state.totalDocuments}
                placeholder="Upload Files (3 max)"
                placeholderTextColor={Colors.disabledIcon}
              />
              <TouchableOpacity
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: Colors.secondary,
                  width: '18%',
                  borderBottomRightRadius: 10,
                  borderTopRightRadius: 10,
                }}
                onPress={this.requestCameraPermission}>
                <Image
                  style={{
                    height: 20,
                    width: 20,
                    resizeMode: 'contain',
                    tintColor: Colors.white,
                  }}
                  source={require('../../assets/upload.png')}
                />
              </TouchableOpacity>
            </View>
            <Tags
              initialTags={this.state.tags}
              textInputProps={{
                editable: false,
                style: {
                  height: 0,
                  width: 0,
                },
              }}
              onChangeTags={tags => console.log(tags)}
              inputContainerStyle={{
                height: 0,
                opacity: 0,
              }}
              inputStyle={{
                backgroundColor: Colors.white,
              }}
              renderTag={({
                tag,
                index,
                onPress,
                deleteTagOnPress,
                readonly,
              }) => (
                <View
                  key={`${tag}-${index}`}
                  style={{
                    flexDirection: 'row',
                    backgroundColor: Colors.tagBackground,
                    padding: '2%',
                    marginRight: '2%',
                    marginTop: '3%',
                    borderRadius: 5,
                  }}>
                  <Text
                    style={{
                      fontFamily: 'urbanist_regular',
                      color: Colors.text_dark,
                      fontSize: height * 0.015,
                    }}>
                    {tag}
                  </Text>
                  <TouchableOpacity
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginLeft: '6%',
                    }}
                    onPress={() => {
                      let temp = this.state.tags;
                      if (index > -1) {
                        temp.splice(index, 1);
                        this.user.graduationCertificates.tags.splice(index, 1);
                        this.user.graduationCertificates.files.splice(index, 1);
                        if (
                          this.user.graduationCertificates.urls.length > index
                        )
                          this.user.graduationCertificates.urls.splice(
                            index,
                            1,
                          );
                      }
                      let tDocs =
                        temp.length === 0
                          ? ''
                          : temp.length + ' file(s) selected';
                      this.setState(
                        {
                          tags: temp,
                          totalDocuments: tDocs,
                        },
                        this.handleChange,
                      );
                    }}>
                    <Image
                      style={{
                        height: height * 0.012,
                        width: height * 0.012,
                        resizeMode: 'contain',
                      }}
                      source={require('../../assets/delete.png')}
                    />
                  </TouchableOpacity>
                </View>
              )}
            />
          </View>
        )}
      </View>
    );
  }

  renderLayouts() {
    if (this.state.selectedTab === 0) {
      return this.userLayout();
    } else {
      return this.medicalStaffLayout();
    }
  }

  navigateToDashboard() {
    SharedPreferences.setItem('isLoggedIn', JSON.stringify(true));
    SharedPreferences.setItem('user', JSON.stringify(this.user));
    Globals.user = this.user;
    this.props.navigation.navigate('under_review', {
      onGoBack: function (params) {
        this.refresh();
      }.bind(this),
    });
  }

  navigateToUserDashboard() {
    SharedPreferences.setItem('isLoggedIn', JSON.stringify(true));
    SharedPreferences.setItem('user', JSON.stringify(this.user));
    Globals.user = this.user;
    this.props.navigation.navigate('profile_builder');
  }

  upload() {
    this.body = {
      name: this.state.fullName,
      gender: this.state.selectedGender,
      dob: this.dateToSend,
      email: this.state.email,
      district: this.state.selectedDistrict,
      city: this.state.selectedCity,
      country: this.state.selectedCountry,
      role:
        this.state.selectedTab === 0 ? 'user' : this.state.selectedMedicalStaff,
    };

    this.user.fullName = this.state.fullName;
    this.user.gender = this.state.selectedGender;
    this.user.dateOfBirth = this.state.date;
    this.user.email = this.state.email;
    this.user.district = this.state.selectedDistrict;
    this.user.city = this.state.selectedCity;
    this.user.country = this.state.selectedCountry;
    this.user.role =
      this.state.selectedTab === 0 ? 'user' : this.state.selectedMedicalStaff;

    if (this.state.selectedTab === 0) {
      axios
        .patch(BASE_URL + '/users/' + this.user.id, this.body, {
          headers: {
            Authorization: 'Bearer ' + this.user.accessToken,
          },
        })
        .then(response => {
          if (response.data.status) {
            this.setState(
              {
                visible: false,
              },
              this.navigateToUserDashboard,
            );
          } else {
            if (Platform.OS === 'android') {
              ToastAndroid.showWithGravityAndOffset(
                response.data.message,
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
                25,
                50,
              );
            }
          }
          this.setState({
            visible: false,
          });
        })
        .catch(error => {
          this.setState({
            visible: false,
          });
          console.log(error);
          if (Platform.OS === 'android') {
            ToastAndroid.showWithGravityAndOffset(
              error.message,
              ToastAndroid.LONG,
              ToastAndroid.BOTTOM,
              25,
              50,
            );
          }
        });
    } else if (
      this.state.selectedMedicalStaff === 'nurse' ||
      this.state.selectedMedicalStaff === 'pathological_analyst'
    ) {
      console.log('Entered Medical Staff');
      this.user.role = this.state.selectedMedicalStaff;
      this.checkForImages();
    } else {
      this.user.specialization = this.state.selectedSpecialization;
      this.user.role = this.state.selectedMedicalStaff;
      Globals.user = this.user;
      this.body['specialization'] = this.state.selectedSpecialization;
      this.setState({
        visible: false,
      });
      this.props.navigation.navigate('doctor_registration', {
        body: this.body,
      });
    }
  }

  imageResizer(obj) {
    return new Promise((resolve, reject) => {
      ImageResizer.createResizedImage(obj, 600, 600, 'JPEG', 100, 0)
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  uploadToFirebase(uri, filename) {
    return new Promise((resolve, reject) => {
      console.log('Uri: ' + uri + ' - ' + 'Filename: ' + filename);

      storage()
        .ref('Docs/' + filename)
        .putFile(uri)
        .then(snapshot => {
          resolve(snapshot);
          console.log('response');
        })
        .catch(error => {
          console.log('error');
          reject(error);
        });
    });
  }

  async checkForImages() {
    var i = 0;
    this.urls = [];
    while (i < this.user.graduationCertificates.files.length) {
      const {path, type, name} = this.user.graduationCertificates.files[i];

      if (type === DocumentPicker.types.images) {
        await this.imageResizer(path)
          .then(async response => {
            console.log(response);
            await this.uploadToFirebase(response.uri, name)
              .then(async snapshot => {
                if (snapshot.state === 'success') {
                  const url = await storage()
                    .ref('Docs/' + name)
                    .getDownloadURL();
                  console.log(url);
                  this.urls.push(url);
                  this.user.graduationCertificates.urls.push(url);
                  i = i + 1;
                  if (i === this.user.graduationCertificates.files.length) {
                    this.body['graduationCertificates'] = this.urls;
                    this.body['issuesInDocuments'] = [];
                    this.body['role'] = this.state.selectedMedicalStaff;
                    this.setState(
                      {
                        counter: 1,
                      },
                      this.performAPI,
                    );
                  }
                }
              })
              .catch(error => {
                console.log('Firebase: ' + error);
              });
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        await this.uploadToFirebase(path, name)
          .then(async snapshot => {
            if (snapshot.state === 'success') {
              const url = await storage()
                .ref('Docs/' + name)
                .getDownloadURL();
              console.log(url);
              this.urls.push(url);
              this.user.graduationCertificates.urls.push(url);
              i = i + 1;
              if (i === this.user.graduationCertificates.files.length) {
                this.body['graduationCertificates'] = this.urls;
                this.body['issuesInDocuments'] = [];
                this.body['role'] = this.state.selectedMedicalStaff;
                this.setState(
                  {
                    counter: 1,
                  },
                  this.performAPI,
                );
              }
            }
          })
          .catch(error => {
            console.log('Firebase: ' + error);
          });
      }
    }
  }

  performAPI() {
    if (this.state.counter > 0) {
      this.user.graduationCertificates.tags = this.state.tags;
      axios
        .patch(BASE_URL + '/users/' + this.user.id, this.body, {
          headers: {
            Authorization: 'Bearer ' + this.user.accessToken,
          },
        })
        .then(response => {
          this.setState(
            {
              visible: false,
            },
            this.navigateToDashboard,
          );
        })
        .catch(error => {
          this.setState({
            visible: false,
          });
          console.log(error);
          if (Platform.OS === 'android') {
            ToastAndroid.showWithGravityAndOffset(
              error.message,
              ToastAndroid.LONG,
              ToastAndroid.BOTTOM,
              25,
              50,
            );
          }
        });
    }
  }

  navigate() {
    const resetAction = CommonActions.reset({
      index: 0,
      routes: [{name: 'dashboard'}],
    });

    this.props.navigation.dispatch(resetAction);
  }

  render() {
    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <ProgressBar
          size={48}
          determinate={false}
          thickness={4}
          visible={this.state.visible}
          color={Colors.primary}
        />

        <SafeAreaView style={{flex: 1, paddingHorizontal: 10}}>
          <StatusBar barStyle="dark-content" backgroundColor={Colors.white} />

          <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="always"
            keyboardDismissMode="on-drag"
            contentContainerStyle={{
              marginTop: 20,
              paddingBottom: this.navbarHeight,
              marginHorizontal: 7,
            }}
            showVerticalScrollIndicator={false}>
            <Text style={styles.title}>Sign up</Text>
            <Text style={styles.description}>
              Please fill in the information to complete the sign up process.
            </Text>

            <View style={styles.tabLayout}>
              <TouchableOpacity
                style={
                  this.state.selectedTab === 0
                    ? styles.enabled_tab
                    : styles.disabled_tab
                }
                onPress={() => {
                  this.setState({selectedTab: 0, selectedUserType: 'User'});
                }}>
                <Text
                  style={
                    this.state.selectedTab === 0
                      ? styles.enabled_tab_text
                      : styles.disabled_tab_text
                  }>
                  User
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  this.state.selectedTab === 0
                    ? styles.disabled_tab
                    : styles.enabled_tab
                }
                onPress={() => {
                  this.setState({selectedTab: 1});
                }}>
                <Text
                  style={
                    this.state.selectedTab === 0
                      ? styles.disabled_tab_text
                      : styles.enabled_tab_text
                  }>
                  Medical Staff
                </Text>
              </TouchableOpacity>
            </View>

            {this.renderLayouts()}

            <View
              style={{
                width: '100%',
                marginTop: 15,
              }}>
              <TouchableOpacity
                style={
                  this.state.visible ? styles.button1 : this.state.buttonStyle
                }
                disabled={!this.state.buttonDisability || this.state.visible}
                onPress={() => {
                  this.setState(
                    {
                      visible: true,
                    },
                    this.upload,
                  );
                }}>
                <Text
                  style={{
                    color: Colors.white,
                    alignSelf: 'center',
                    fontSize: 16,
                    fontFamily: 'urbanist_bold',
                  }}>
                  {this.state.selectedTab === 0
                    ? 'Submit'
                    : this.state.selectedMedicalStaff === 'doctor'
                    ? 'Continue'
                    : 'Submit'}
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </SafeAreaView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    alignItems: 'center',
  },
  title: {
    fontSize: 32,
    color: Colors.text_dark,
    fontFamily: 'urbanist_bold',
  },
  description: {
    fontSize: 16,
    fontFamily: 'urbanist_regular',
    marginTop: 7,
    color: Colors.text_light,
  },
  tabLayout: {
    backgroundColor: Colors.enabled,
    borderRadius: 10,
    padding: 5,
    margin: 7,
    flexDirection: 'row',
  },
  enabled_tab: {
    flex: 0.5,
    backgroundColor: Colors.secondary,
    borderRadius: 10,
    padding: 10,
  },
  enabled_tab_text: {
    color: Colors.white,
    textAlign: 'center',
    fontFamily: 'urbanist_regular',
    fontSize: 14,
  },
  disabled_tab: {
    flex: 0.5,
    backgroundColor: Colors.enabled,
    borderRadius: 10,
    padding: 10,
  },
  disabled_tab_text: {
    color: Colors.secondary,
    fontFamily: 'urbanist_regular',
    fontSize: 14,
    textAlign: 'center',
  },
  label: {
    fontFamily: 'urbanist_bold',
    fontSize: 16,
    color: Colors.text_dark,
    marginBottom: 5,
  },
  redLabel: {
    fontFamily: 'urbanist_bold',
    fontSize: 16,
    color: 'red',
    marginBottom: 5,
  },
  textInput: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.light_grey,
    fontSize: 16,
    paddingHorizontal: 10,
    color: Colors.disabledIcon,
    fontFamily: 'urbanist_regular',
  },
  textInputFilled: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.secondary,
    fontSize: 16,
    paddingHorizontal: 10,
    color: Colors.text_dark,
    fontFamily: 'urbanist_regular',
  },
  disabledGender: {
    flex: 0.48,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.7,
    fontSize: 14,
    alignItems: 'center',
    padding: 10,
    borderColor: Colors.disabledIcon,
  },
  filledGender: {
    flex: 0.48,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.7,
    alignItems: 'center',
    padding: 10,
    borderColor: Colors.secondary,
  },
  enabledGender: {
    flex: 0.48,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    borderRadius: 7,
    padding: 10,
  },
  disabledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.disabledIcon,
  },
  enabledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.white,
  },
  filledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.secondary,
  },
  genderLayout: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button1: {
    width: '100%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  button2: {
    width: '100%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.disabled,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  placeholder_text: {
    fontFamily: 'urbanist_regular',
    fontSize: 16,
    color: Colors.disabledIcon,
  },
  text: {
    fontFamily: 'urbanist_regular',
    fontSize: 16,
    color: Colors.text_dark,
  },
});
