import React, {Component, useState} from 'react';
import {
  BackHandler,
  StyleSheet,
  Text,
  Keyboard,
  View,
  Platform,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  StatusBar,
  SafeAreaView,
  ToastAndroid,
  KeyboardAvoidingView,
  Image,
  Button,
  TextInput,
  Alert,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import Colors from '../../utils/Colors';
import axios from 'react-native-axios';
import CountDown from 'react-native-countdown-component';
import {BASE_URL} from '@env';
import SharedPreferences from 'react-native-shared-preferences';
import {ProgressBar} from 'react-native-material-indicators';
import {LogBox} from 'react-native';
import Globals from '../../utils/Globals';

export default class Otp extends Component {
  state = {
    pin1: '',
    pin2: '',
    pin3: '',
    pin4: '',
    pin1Focus: styles.disabledPin,
    pin2Focus: styles.disabledPin,
    pin3Focus: styles.disabledPin,
    pin4Focus: styles.disabledPin,
    showResend: false,
    showDesign: false,
    visible: false,
  };

  constructor(props) {
    super(props);

    // this.pin = '';
    this.pin = Globals.user.authCode;
    this.auth_type = this.props.route.params.auth_type;
    this.title =
      this.auth_type === 'sign_in'
        ? 'Sign In'
        : this.auth_type === 'change_number'
        ? 'Change Phone Number'
        : 'Create an account';
    this.user = Globals.user;
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    this.screenHeight = Dimensions.get('screen').height;
    this.windowHeight = Dimensions.get('window').height;
    this.navbarHeight =
      this.screenHeight - this.windowHeight + StatusBar.currentHeight;
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    this.setState(
      {
        pin1: this.pin.charAt(0),
        pin2: this.pin.charAt(1),
        pin3: this.pin.charAt(2),
        pin4: this.pin.charAt(3),
      },
      this.navigate,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    // this.props.navigation.navigate('sign_in', {
    //   go_back_key: this.props.navigation.key,
    // });
    const resetAction = CommonActions.reset({
      index: 0,
      routes: [{name: 'start'}],
    });
    this.props.navigation.dispatch(resetAction);
    return true;
  }

  navigate() {
    axios
      .patch(
        BASE_URL + '/users/code/verify/' + this.user.id,
        {
          code: this.pin,
        },
        {
          headers: {
            Authorization: 'Bearer ' + this.user.accessToken,
          },
        },
      )
      .then(response => {
        if (response.data.status) {
          this.user.accessToken = response.data.data.token;
          this.user.isVerified = response.data.data.isVerified;
          this.user.isApproved = response.data.data.isApproved;
          this.user.avatar = response.data.data.avatar;

          SharedPreferences.setItem('user', JSON.stringify(this.user));
          Globals.user = this.user;

          if (this.auth_type === 'sign_up') {
            const resetAction = CommonActions.reset({
              index: 0,
              routes: [{name: 'congrats'}],
            });

            this.props.navigation.dispatch(resetAction);
          } else {
            SharedPreferences.setItem('isLoggedIn', JSON.stringify(true));
            if (response.data.data.role === 'user') {
              const resetAction = CommonActions.reset({
                index: 0,
                routes: [{name: 'dashboard'}],
              });

              this.props.navigation.dispatch(resetAction);
            } else {
              if (response.data.data.isVerified) {
                if (!response.data.data.isApproved) {
                  const resetAction = CommonActions.reset({
                    index: 0,
                    routes: [{name: 'profile_builder'}],
                  });

                  this.props.navigation.dispatch(resetAction);
                } else {
                  const resetAction = CommonActions.reset({
                    index: 0,
                    routes: [{name: 'dashboard'}],
                  });

                  this.props.navigation.dispatch(resetAction);
                }
              } else {
                const resetAction = CommonActions.reset({
                  index: 0,
                  routes: [{name: 'basic_registration'}],
                });

                this.props.navigation.dispatch(resetAction);
              }
            }
          }
        } else {
          if (Platform.OS === 'android') {
            ToastAndroid.showWithGravityAndOffset(
              response.data.message,
              ToastAndroid.LONG,
              ToastAndroid.BOTTOM,
              25,
              50,
            );
          } else {
            Toast.showWithGravity(response.data.message, Toast.LONG, Toast.TOP);
          }
        }

        this.setState({
          visible: false,
        });
      })
      .catch(error => {
        this.setState({
          visible: false,
        });
        console.log(error);
        if (Platform.OS === 'android') {
          ToastAndroid.showWithGravityAndOffset(
            error.message,
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50,
          );
        } else {
          Toast.showWithGravity(error.message, Toast.LONG, Toast.TOP);
        }
      });
    this.handleBlur('pin4Focus');
  }

  handleChange = (value, key) => {
    if (key === 'pin4') {
      this.pin = this.state.pin1 + this.state.pin2 + this.state.pin3 + value;
      if (this.pin.length == 4) {
        Keyboard.dismiss();
        this.setState(
          {
            visible: true,
          },
          this.navigate,
        );
      }
    }

    this.setState({
      [key]: value,
    });
  };

  handleFocus = value => {
    this.setState({
      [value]: styles.enabledPin,
    });
  };

  handleBlur = value => {
    this.setState({
      [value]: styles.disabledPin,
    });
  };

  async resendCode() {
    await axios
      .patch(
        BASE_URL + '/users/code/resend/' + this.user.id,
        {
          code: this.pin,
        },
        {
          headers: {
            Authorization: 'Bearer ' + this.user.accessToken,
          },
        },
      )
      .then(response => {
        if (response.data.success) {
          this.setState({
            showResend: false,
          });
        } else {
          if (Platform.OS === 'android') {
            ToastAndroid.showWithGravityAndOffset(
              response.data.message,
              ToastAndroid.LONG,
              ToastAndroid.BOTTOM,
              25,
              50,
            );
          } else {
            Toast.showWithGravity(response.data.message, Toast.LONG, Toast.TOP);
          }
        }
        this.setState({
          visible: false,
        });
      })
      .catch(error => {
        this.setState({
          visible: false,
        });
        console.log(error);
        if (Platform.OS === 'android') {
          ToastAndroid.showWithGravityAndOffset(
            error.message,
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50,
          );
        } else {
          Toast.showWithGravity(error.message, Toast.LONG, Toast.TOP);
        }
      });
  }

  renderResend = function () {
    if (this.state.showResend) {
      return (
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              color: Colors.text_dark,
              fontSize: 16,
              fontFamily: 'urbanist_regular',
            }}>
            Didn't receive the code?
          </Text>
          <TouchableOpacity
            style={{marginHorizontal: 10}}
            onPress={() => {
              this.setState(
                {
                  visible: true,
                },
                this.resendCode,
              );
            }}>
            <Text
              style={{
                alignSelf: 'center',
                fontSize: 16,
                color: Colors.secondary,
                fontFamily: 'urbanist_semi_bold',
              }}>
              Request again
            </Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              color: Colors.text_dark,
              fontSize: 16,
              fontFamily: 'urbanist_regular',
            }}>
            Resend Code in
          </Text>
          <CountDown
            style={{height: 13, margin: 0, padding: 0}}
            until={61}
            timeLabelStyle={{color: Colors.white}}
            digitStyle={{backgroundColor: '#FFF', padding: 0, margin: 0}}
            digitTxtStyle={{
              color: Colors.black,
              fontFamily: 'urbanist_medium',
              fontWeight: 'normal',
            }}
            timeToShow={['M']}
          />
          <Text style={{fontFamily: 'urbanist_bold'}}> : </Text>
          <CountDown
            style={{height: 13, margin: 0, padding: 0}}
            until={61}
            onFinish={() => {
              this.setState({
                ['showResend']: true,
              });
            }}
            timeLabelStyle={{color: Colors.white}}
            digitStyle={{backgroundColor: '#FFF', padding: 0, margin: 0}}
            digitTxtStyle={{
              color: Colors.black,
              fontFamily: 'urbanist_medium',
              fontWeight: 'normal',
            }}
            timeToShow={['S']}
          />
        </View>
      );
    }
  };

  render() {
    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <StatusBar barStyle="dark-content" backgroundColor={Colors.white} />
        <View style={{flex: 1}}>
          <ProgressBar
            size={48}
            thickness={4}
            visible={this.state.visible}
            color={Colors.primary}
          />

          <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="always"
            keyboardDismissMode="on-drag"
            contentContainerStyle={{
              flex: 1,
              marginTop: 30,
              paddingBottom: this.navbarHeight * 0.4,
              marginHorizontal: 7,
            }}
            showVerticalScrollIndicator={false}>
            <View
              style={{
                flexDirection: 'row',
                paddingHorizontal: 30,
                paddingVertical: 20,
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.handleBackButtonClick();
                }}>
                <Image
                  style={styles.back}
                  source={require('../../assets/ic_back.png')}
                />
              </TouchableOpacity>

              <Text style={styles.title}>{this.title}</Text>
            </View>

            <Text
              style={{
                fontSize: 28,
                fontFamily: 'urbanist_bold',
                color: Colors.text_dark,
                marginTop: 15,
                marginHorizontal: 30,
              }}>
              Enter 4 digit code sent to Mobile Number
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontFamily: 'urbanist_regular',
                color: Colors.text_light,
                marginHorizontal: 30,
              }}>
              We sent it to the number {this.user.phoneNumber}
            </Text>

            <View
              style={{
                flexGrow: 1,
                flexDirection: 'column',
                justifyContent: 'space-between',
                paddingBottom: (this.navbarHeight * 60) / 100,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  margin: 20,
                }}>
                <View style={this.state.pin1Focus}>
                  <TextInput
                    style={styles.pin}
                    keyboardType="numeric"
                    maxLength={1}
                    returnKeyType={'next'}
                    blurOnSubmit={false}
                    onSubmitEditing={() => {
                      this.secondTextInput.focus();
                    }}
                    onFocus={() => this.handleFocus('pin1Focus')}
                    onBlur={() => this.handleBlur('pin1Focus')}
                    value={this.state.pin1}
                    onChangeText={text =>
                      text.length == 1
                        ? (this.handleChange(text, 'pin1'),
                          this.refs.Pin2.focus())
                        : this.handleChange(text, 'pin1')
                    }
                    autoCorrect={false}
                    ref={'Pin1'}
                  />
                </View>

                <View style={this.state.pin2Focus}>
                  <TextInput
                    style={styles.pin}
                    keyboardType="numeric"
                    returnKeyType={'next'}
                    maxLength={1}
                    blurOnSubmit={false}
                    onSubmitEditing={() => {
                      this.thirdTextInput.focus();
                    }}
                    onFocus={() => this.handleFocus('pin2Focus')}
                    onBlur={() => this.handleBlur('pin2Focus')}
                    value={this.state.pin2}
                    onChangeText={text =>
                      text.length == 1
                        ? (this.handleChange(text, 'pin2'),
                          this.refs.Pin3.focus())
                        : this.handleChange(text, 'pin2')
                    }
                    autoCorrect={false}
                    onKeyPress={({nativeEvent}) => {
                      nativeEvent.key === 'Backspace'
                        ? this.refs.Pin1.focus()
                        : this.handleChange('', '');
                    }}
                    ref={'Pin2'}
                  />
                </View>

                <View style={this.state.pin3Focus}>
                  <TextInput
                    style={styles.pin}
                    keyboardType="numeric"
                    maxLength={1}
                    returnKeyType={'next'}
                    blurOnSubmit={false}
                    onSubmitEditing={() => {
                      this.fourthTextInput.focus();
                    }}
                    onFocus={() => this.handleFocus('pin3Focus')}
                    onBlur={() => this.handleBlur('pin3Focus')}
                    value={this.state.pin3}
                    onChangeText={text =>
                      text.length == 1
                        ? (this.handleChange(text, 'pin3'),
                          this.refs.Pin4.focus())
                        : this.handleChange(text, 'pin3')
                    }
                    autoCorrect={false}
                    onKeyPress={({nativeEvent}) => {
                      nativeEvent.key === 'Backspace'
                        ? this.refs.Pin2.focus()
                        : this.handleChange('', '');
                    }}
                    ref={'Pin3'}
                  />
                </View>

                <View style={this.state.pin4Focus}>
                  <TextInput
                    style={styles.pin}
                    keyboardType="numeric"
                    maxLength={1}
                    onFocus={() => this.handleFocus('pin4Focus')}
                    onBlur={() => this.handleBlur('pin4Focus')}
                    value={this.state.pin4}
                    onChangeText={text => this.handleChange(text, 'pin4')}
                    autoCorrect={false}
                    onKeyPress={({nativeEvent}) => {
                      nativeEvent.key === 'Backspace'
                        ? this.refs.Pin3.focus()
                        : this.handleChange('', '');
                    }}
                    ref={'Pin4'}
                  />
                </View>
              </View>

              {this.renderResend()}
            </View>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  title: {
    fontFamily: 'urbanist_semi_bold',
    fontSize: 20,
    color: Colors.text_dark,
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
  },
  back: {
    width: 30,
    height: 30,
  },
  button1: {
    width: '90%',
    alignSelf: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  button2: {
    width: '90%',
    alignSelf: 'center',
    backgroundColor: Colors.disabled,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  disabledPin: {
    flex: 0.25,
    borderRadius: 5,
    backgroundColor: Colors.white,
    borderColor: Colors.light_grey,
    borderWidth: 1,
    margin: 10,
  },
  enabledPin: {
    flex: 0.25,
    borderRadius: 5,
    backgroundColor: Colors.enabled,
    borderColor: Colors.secondary,
    borderWidth: 1,
    margin: 10,
  },
  pin: {
    fontSize: 24,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    textAlign: 'center',
    color: Colors.text_dark,
    fontFamily: 'urbanist_semi_bold',
  },
});
