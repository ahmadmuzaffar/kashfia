import React, {Component, useState} from 'react';
import {BackHandler} from 'react-native';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {
  SafeAreaView,
  Platform,
  ScrollView,
  Keyboard,
  KeyboardAvoidingView,
  ToastAndroid,
  Image,
  Button,
  StatusBar,
  TextInput,
  Alert,
} from 'react-native';
import Colors from '../../utils/Colors';
import axios from 'react-native-axios';
import {BASE_URL} from '@env';
import User from '../../models/User.js';
import {ProgressBar} from 'react-native-material-indicators';
import {LogBox} from 'react-native';
import Globals from '../../utils/Globals';

export default class SignIn extends Component {
  state = {
    number: '',
    title:
      this.props.route.params.auth_type === 'sign_in'
        ? 'Sign In'
        : this.props.route.params.auth_type === 'change_number'
        ? 'Change Phone Number'
        : 'Create An Account',
    isChangeNumber: this.props.route.params.auth_type === 'change_number',
    buttonStyle: styles.button2,
    visible: false,
    buttonDisability: false,
  };

  constructor(props) {
    super(props);
    this.user = new User();
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.goBack();
    return true;
  }

  navigate() {
    this.props.navigation.navigate('otp', {
      auth_type: this.props.route.params.auth_type,
    });
  }

  process() {
    if (this.props.route.params.auth_type === 'change_number') {
      //TODO
    } else {
      axios
        .post(BASE_URL + '/users', {
          phoneNumber: '+' + this.state.number,
        })
        .then(response => {
          console.log(JSON.stringify(response));
          this.user.id = response.data.data._id;
          this.user.fullName = response.data.data.name
            ? response.data.data.name
            : '';
          this.user.email = response.data.data.email
            ? response.data.data.email
            : '';
          this.user.gender = response.data.data.gender
            ? response.data.data.gender
            : '';
          this.user.gender = response.data.data.gender
            ? response.data.data.gender
            : '';
          if (response.data.data.dob) {
            let date = new Date(response.data.data.dob);
            this.user.dateObj = date;
            let strDate =
              (date.getDate() > 9 ? date.getDate() : '0' + date.getDate()) +
              '-' +
              (date.getMonth() > 8
                ? date.getMonth() + 1
                : '0' + (date.getMonth() + 1)) +
              '-' +
              date.getFullYear();
            this.user.dateOfBirth = strDate;
          } else this.user.dateOfBirth = '';
          this.user.role = response.data.data.role
            ? response.data.data.role
            : '';
          this.user.district = response.data.data.district
            ? response.data.data.district
            : '';
          this.user.city = response.data.data.city
            ? response.data.data.city
            : '';
          this.user.country = response.data.data.country
            ? response.data.data.country
            : '';
          this.user.phoneNumber = response.data.data.phoneNumber;
          this.user.graduationCertificates.urls =
            response.data.data.graduationCertificates;
          this.user.syndicateIdDocuments.urls =
            response.data.data.syndicateIdDocuments;
          this.user.professionalPracticeLetters.urls =
            response.data.data.professionalPracticeLetters;
          this.user.specializationCertificates.urls =
            response.data.data.specializationCertificates;
          this.user.clinicalCertificate.urls =
            response.data.data.clinicalCertificate;
          this.user.accessToken = response.data.data.token;
          this.user.authCode = response.data.data.authVerificationCode;
          Globals.user = this.user;

          this.setState(
            {
              visible: false,
            },
            this.navigate,
          );
        })
        .catch(error => {
          this.setState({
            visible: false,
          });
          console.log(error);

          if (Platform.OS === 'android') {
            ToastAndroid.showWithGravityAndOffset(
              error.message,
              ToastAndroid.LONG,
              ToastAndroid.BOTTOM,
              25,
              50,
            );
          } else {
            Toast.showWithGravity(error.message, Toast.LONG, Toast.TOP);
          }
        });
    }
  }

  handleChange = (value, key) => {
    if (key === 'number') {
      if (value.length >= 11 && value.length <= 13) {
        this.setState({
          buttonStyle: styles.button1,
          buttonDisability: true,
        });
      } else {
        this.setState({
          buttonStyle: styles.button2,
          buttonDisability: false,
        });
      }
    }

    this.setState({
      [key]: value,
    });
  };

  render() {
    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <SafeAreaView style={{flex: 1}}>
          <StatusBar barStyle="dark-content" backgroundColor={Colors.white} />
          <ProgressBar
            size={48}
            determinate={false}
            thickness={4}
            visible={this.state.visible}
            color={Colors.primary}
          />
          <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="always"
            keyboardDismissMode="on-drag"
            contentContainerStyle={{
              flex: 0.9,
              marginTop: 30,
              paddingBottom: this.navbarHeight,
              marginHorizontal: 7,
            }}
            showVerticalScrollIndicator={false}>
            <View
              style={{
                flexDirection: 'row',
                paddingHorizontal: 30,
                paddingVertical: 20,
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.handleBackButtonClick();
                }}>
                <Image
                  style={styles.back}
                  source={require('../../assets/ic_back.png')}
                />
              </TouchableOpacity>

              <Text style={styles.title}>{this.state.title}</Text>
            </View>

            <Text
              style={{
                fontSize: 28,
                fontFamily: 'urbanist_bold',
                color: Colors.text_dark,
                marginTop: 15,
                marginHorizontal: 30,
              }}>
              Enter Your Mobile Number
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontFamily: 'urbanist_regular',
                color: Colors.text_light,
                marginHorizontal: 30,
              }}>
              We’ll send you a confirmation code
            </Text>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                borderRadius: 3,
                borderColor: Colors.lighter_gery,
                borderWidth: 1,
                margin: 20,
              }}>
              <Text
                style={{
                  alignSelf: 'center',
                  marginLeft: 15,
                  fontSize: 28,
                  fontFamily: 'urbanist_medium',
                }}>
                +
              </Text>

              <TextInput
                style={{
                  flex: 1,
                  alignSelf: 'center',
                  fontSize: 28,
                  paddingHorizontal: 10,
                  color: Colors.black,
                  fontFamily: 'urbanist_medium',
                }}
                keyboardType="numeric"
                maxLength={13}
                value={this.state.number}
                onChangeText={text => this.handleChange(text, 'number')}
                autoCorrect={false}
                placeholder="109 123 456 7890"
                placeholderTextColor={Colors.disabledIcon}
              />
            </View>

            <View style={{bottom: 0, position: 'absolute', width: '100%'}}>
              <TouchableOpacity
                style={
                  this.state.visible ? styles.button1 : this.state.buttonStyle
                }
                disabled={!this.state.buttonDisability || this.state.visible}
                onPress={function () {
                  Keyboard.dismiss();
                  this.setState(
                    {
                      visible: true,
                    },
                    this.process,
                  );
                }.bind(this)}>
                <Text
                  style={{
                    color: Colors.white,
                    alignSelf: 'center',
                    fontSize: 16,
                    fontFamily: 'urbanist_bold',
                  }}>
                  Continue
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </SafeAreaView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  lottie: {
    width: 100,
    height: 100,
    zIndex: 1,
  },
  button: {
    alignSelf: 'stretch',
    alignItems: 'center',
    backgroundColor: Colors.primary,
    borderRadius: 7,
    paddingHorizontal: 25,
    paddingVertical: 10,
    marginHorizontal: 40,
    marginVertical: 10,
  },
  title: {
    fontFamily: 'urbanist_semi_bold',
    fontSize: 20,
    flex: 1,
    color: Colors.text_dark,
    textAlign: 'center',
    alignSelf: 'center',
  },
  back: {
    width: 30,
    height: 30,
  },
  inputLayout: {
    margin: 25,
  },
  input: {
    height: 40,
    margin: 12,
    color: Colors.black,
    borderWidth: 1,
    borderColor: Colors.light_grey,
    padding: 10,
  },
  button1: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  button2: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.disabled,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
});
