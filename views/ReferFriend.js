import {
  Text,
  StyleSheet,
  View,
  SafeAreaView,
  Image,
  Pressable,
  TouchableOpacity,
  BackHandler,
  TextInput,
} from 'react-native';
import React, {Component} from 'react';
import Globals from '../utils/Globals';
import Colors from '../utils/Colors';

export default class ReferFriend extends Component {
  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    this.setState({visible: true}, this.getCards);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.goBack();
    return true;
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
        <View
          style={{
            flexDirection: 'row',
            padding: Globals.pHorizontal,
            borderBottomColor: Colors.lighter_gery,
            borderBottomWidth: 1,
          }}>
          <Text style={styles.title}>Refer a Friend</Text>
        </View>

        <View style={{padding: Globals.pHorizontal}}>
          <Text
            style={{
              fontFamily: 'urbanist_regular',
              color: Colors.text_dark,
              fontSize: Globals.screenWidth * 0.04,
            }}>
            Please copy that link and share it with your friends or share via
            different platforms.
          </Text>

          <View
            style={{
              flexDirection: 'row',
              padding: Globals.mTop,
              backgroundColor: '#FFFAEE',
              borderStyle: 'dashed',
              borderWidth: 1,
              borderColor: '#FBD775',
              borderRadius: 10,
              marginTop: Globals.mTop,
            }}>
            <Text
              numberOfLines={1}
              ellipsizeMode="tail"
              style={{
                fontFamily: 'urbanist_regular',
                marginRight: '3%',
                flex: 1,
                color: Colors.text_dark,
                fontSize: Globals.screenWidth * 0.04,
              }}>
              www.kashfia.com/odumqDjdd
            </Text>
            <Text
              numberOfLines={1}
              ellipsizeMode="tail"
              style={{
                fontFamily: 'urbanist_regular',
                marginLeft: '3%',
                color: Colors.text_light,
                fontSize: Globals.screenWidth * 0.035,
              }}>
              Tap to copy
            </Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              marginTop: Globals.pHorizontal,
              justifyContent: 'center',
            }}>
            <View
              style={{
                flex: 0.25,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View
                style={{
                  height: Globals.screenWidth * 0.17,
                  width: Globals.screenWidth * 0.17,
                  borderRadius: 7,
                  borderWidth: 1,
                  borderColor: Colors.light_grey,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Image
                  source={require('../assets/linkedin.png')}
                  style={{
                    height: Globals.screenWidth * 0.09,
                    width: Globals.screenWidth * 0.09,
                    resizeMode: 'contain',
                  }}
                />
              </View>
              <Text
                style={{
                  fontFamily: 'urbanist_regular',
                  color: Colors.text_light,
                  fontSize: Globals.screenWidth * 0.035,
                }}>
                LinkedIn
              </Text>
            </View>

            <View
              style={{
                flex: 0.25,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View
                style={{
                  height: Globals.screenWidth * 0.17,
                  width: Globals.screenWidth * 0.17,
                  borderRadius: 7,
                  borderWidth: 1,
                  borderColor: Colors.light_grey,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Image
                  source={require('../assets/facebook.png')}
                  style={{
                    height: Globals.screenWidth * 0.09,
                    width: Globals.screenWidth * 0.09,
                    resizeMode: 'contain',
                  }}
                />
              </View>
              <Text
                style={{
                  fontFamily: 'urbanist_regular',
                  color: Colors.text_light,
                  fontSize: Globals.screenWidth * 0.035,
                }}>
                Facebook
              </Text>
            </View>

            <View
              style={{
                flex: 0.25,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View
                style={{
                  height: Globals.screenWidth * 0.17,
                  width: Globals.screenWidth * 0.17,
                  borderRadius: 7,
                  borderWidth: 1,
                  borderColor: Colors.light_grey,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Image
                  source={require('../assets/whatsapp.png')}
                  style={{
                    height: Globals.screenWidth * 0.09,
                    width: Globals.screenWidth * 0.09,
                    resizeMode: 'contain',
                  }}
                />
              </View>
              <Text
                style={{
                  fontFamily: 'urbanist_regular',
                  color: Colors.text_light,
                  fontSize: Globals.screenWidth * 0.035,
                }}>
                Whatsapp
              </Text>
            </View>

            <View
              style={{
                flex: 0.25,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View
                style={{
                  height: Globals.screenWidth * 0.17,
                  width: Globals.screenWidth * 0.17,
                  borderRadius: 7,
                  borderWidth: 1,
                  borderColor: Colors.light_grey,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Image
                  source={require('../assets/twitter.png')}
                  style={{
                    height: Globals.screenWidth * 0.09,
                    width: Globals.screenWidth * 0.09,
                    resizeMode: 'contain',
                  }}
                />
              </View>
              <Text
                style={{
                  fontFamily: 'urbanist_regular',
                  color: Colors.text_light,
                  fontSize: Globals.screenWidth * 0.035,
                }}>
                Twitter
              </Text>
            </View>
          </View>
        </View>

        <TouchableOpacity
          style={styles.imgLL}
          onPress={() => {
            this.handleBackButtonClick();
          }}>
          <Image style={styles.back} source={require('../assets/back2.png')} />
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontFamily: 'urbanist_semi_bold',
    fontSize: 20,
    color: Colors.text_dark,
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
  },
  back: {
    width: 25,
    height: 25,
  },
  imgLL: {
    position: 'absolute',
    top: Globals.pHorizontal,
    left: Globals.pHorizontal,
  },
});
