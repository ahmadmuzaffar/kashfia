import React, {Component} from 'react';
import {
  BackHandler,
  StyleSheet,
  Dimensions,
  ScrollView,
  StatusBar,
  Text,
  TouchableHighlight,
  TextInput,
  ToastAndroid,
  KeyboardAvoidingView,
  PermissionsAndroid,
  View,
  Platform,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import SharedPreferences from 'react-native-shared-preferences';
import User from '../../../models/User';
import Colors from '../../../utils/Colors';
import StarRating from 'react-native-star-rating';
import {BarChart} from 'react-native-chart-kit';
import AdSlider from '../../../custom/views/AdSlider';
import Globals from '../../../utils/Globals';
import LinearGradient from 'react-native-linear-gradient';
import CardView from 'react-native-cardview';

export default class NurseHome extends Component {
  state = {
    width: 0,
    starCount: 3.5,
  };

  constructor(props) {
    super(props);

    this.window = Dimensions.get('window');
    this.screenHeight = Dimensions.get('screen').height;
    this.windowHeight = Dimensions.get('window').height;
    this.navbarHeight =
      this.screenHeight - this.windowHeight + StatusBar.currentHeight;
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating,
    });
  }

  listData() {
    return [
      {
        id: 1,
        logo: require('../../../assets/ad_logo.png'),
        title: 'Change your smile',
        description: 'Choose the experts for your smile. Free Consultation!',
        buttonText: 'Know More',
        ad_pic: require('../../../assets/ad_pic.png'),
        buttonListener: () => {
          console.log('HAHAHAHAAHAA');
        },
      },
      {
        id: 2,
        logo: require('../../../assets/ad_logo.png'),
        title: 'Change your smile',
        description: 'Choose the experts for your smile. Free Consultation!',
        buttonText: 'Know More',
        ad_pic: require('../../../assets/ad_pic.png'),
        buttonListener: () => {},
      },
      {
        id: 3,
        logo: require('../../../assets/ad_logo.png'),
        title: 'Change your smile',
        description: 'Choose the experts for your smile. Free Consultation!',
        buttonText: 'Know More',
        ad_pic: require('../../../assets/ad_pic.png'),
        buttonListener: () => {},
      },
      {
        id: 4,
        logo: require('../../../assets/ad_logo.png'),
        title: 'Change your smile',
        description: 'Choose the experts for your smile. Free Consultation!',
        buttonText: 'Know More',
        ad_pic: require('../../../assets/ad_pic.png'),
        buttonListener: () => {},
      },
    ];
  }

  render() {
    const user = Globals.user;
    const {height, width} = this.window;

    const chartConfig = {
      backgroundGradientFrom: Colors.white,
      backgroundGradientFromOpacity: 0,
      backgroundGradientTo: Colors.white,
      backgroundGradientToOpacity: 0.5,
      color: (opacity = 1) => `rgba(42, 57, 75, ${opacity})`,
      strokeWidth: 2,
      barPercentage: 0.5,
      useShadowColorFromDataset: false,
    };

    const data = {
      labels: ['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'],
      datasets: [
        {
          data: [20, 45, 28, 80, 99, 43, 99],
        },
      ],
    };

    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="dark-content" backgroundColor={Colors.white} />

        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
          contentContainerStyle={{
            paddingBottom: height * 0.075,
            backgroundColor: Colors.mid_grey,
          }}
          showVerticalScrollIndicator={false}>
          <View>
            <View
              style={{
                flexDirection: 'row',
                paddingHorizontal: width * 0.05,
                paddingVertical: height * 0.035,
                backgroundColor: Colors.white,
              }}>
              <View
                style={{
                  flex: 1,
                  paddingRight: width * 0.06,
                  justifyContent: 'center',
                }}>
                <Text style={styles.title}>Hello {user.fullName}</Text>
                <Text style={styles.description}>
                  Let us make you feel better
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  borderRadius: (width * 0.35) / 2,
                  height: height * 0.055,
                  justifyContent: 'center',
                  width: width * 0.27,
                  backgroundColor: '#EEEEEE',
                }}>
                <Image
                  style={{
                    height: height * 0.055,
                    width: height * 0.055,
                    borderRadius: (height * 0.07) / 2,
                  }}
                  source={
                    user.avatar === '' || user.avatar === undefined
                      ? require('../../../assets/doc1.png')
                      : {uri: user.avatar}
                  }
                />
                <View
                  style={{
                    alignItems: 'center',
                    flex: 1,
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 15,
                      fontFamily: 'urbanist_bold',
                      color: Colors.text_dark,
                    }}>
                    2520
                  </Text>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'urbanist_semi_bold',
                      color: Colors.text_light,
                    }}>
                    IQD
                  </Text>
                </View>
              </View>
            </View>

            <AdSlider data={this.listData()} />
            <View style={{paddingHorizontal: width * 0.05}}>
              <View style={{marginTop: height * 0.025}} />
              <Text
                style={[
                  styles.title,
                  {
                    fontFamily: 'urbanist_semi_bold',
                    fontSize: 24,
                    color: Colors.text_dark,
                    marginHorizontal: 3,
                  },
                ]}>
                Summary
              </Text>

              <CardView
                style={{marginTop: height * 0.01}}
                cornerRadius={10}
                cardElevation={10}
                cardMaxElevation={10}>
                <TouchableOpacity
                  style={{
                    alignSelf: 'stretch',
                    padding: 15,
                    backgroundColor: Colors.white,
                  }}>
                  <View>
                    <View
                      style={{
                        flexDirection: 'row',
                      }}>
                      <Text style={{flex: 1, fontFamily: 'urbanist_semi_bold'}}>
                        Today, 03:31 PM
                      </Text>
                    </View>

                    <View
                      style={{flexDirection: 'row', flex: 1, marginTop: 10}}>
                      <Image
                        style={{height: 40, width: 40, borderRadius: 20}}
                        source={require('../../../assets/doc3.png')}
                      />
                      <View style={{marginHorizontal: 5}}>
                        <Text
                          style={{
                            fontSize: 15,
                            fontFamily: 'urbanist_semi_bold',
                          }}>
                          Abram Westervelt
                        </Text>

                        <View style={{flexDirection: 'row', marginTop: 5}}>
                          <StarRating
                            disabled={false}
                            maxStars={5}
                            starSize={15}
                            halfStarColor={Colors.star}
                            fullStarColor={Colors.star}
                            emptyStarColor={'#E7E7E7'}
                            rating={4}
                            starStyle={{marginHorizontal: '1%'}}
                            selectedStar={rating =>
                              this.onStarRatingPress(rating)
                            }
                          />
                          <Text
                            style={{
                              fontSize: 14,
                              fontFamily: 'urbanist_semi_bold',
                              marginLeft: 10,
                              color: Colors.text_light,
                            }}>
                            5 Reviews
                          </Text>
                        </View>
                      </View>
                    </View>

                    <Text
                      style={[
                        styles.description,
                        {
                          marginVertical: height * 0.02,
                        },
                      ]}>
                      “Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Non diam pellentesque volutpat id aliquam tortor ipsum nec
                      porttitor.”
                    </Text>

                    <View
                      style={{
                        width: '100%',
                      }}>
                      <TouchableOpacity style={styles.button1}>
                        <Text
                          style={{
                            color: Colors.white,
                            alignSelf: 'center',
                            fontSize: 14,
                            fontFamily: 'urbanist_bold',
                          }}>
                          Apply to this Job
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </TouchableOpacity>
              </CardView>

              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  marginTop: height * 0.02,
                }}>
                <CardView
                  style={{
                    flex: 1,
                    marginRight: width * 0.02,
                    alignSelf: 'stretch',
                  }}
                  cornerRadius={10}
                  cardElevation={10}
                  cardMaxElevation={10}>
                  <LinearGradient
                    colors={['#5390ED', '#2860CC']}
                    style={{
                      borderRadius: 10,
                      padding: 10,
                    }}>
                    <TouchableOpacity
                      style={{
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        style={{
                          fontFamily: 'urbanist_regular',
                          fontSize: 12,
                          color: Colors.white,
                        }}>
                        New{'\n'}Consultations
                      </Text>
                      <Text
                        style={{
                          fontFamily: 'urbanist_bold',
                          fontSize: 30,
                          marginTop: 10,
                          color: Colors.white,
                        }}>
                        15
                      </Text>
                    </TouchableOpacity>
                  </LinearGradient>
                </CardView>

                <CardView
                  style={{
                    flex: 1,
                    marginHorizontal: width * 0.01,
                    alignSelf: 'stretch',
                  }}
                  cornerRadius={10}
                  cardElevation={10}
                  cardMaxElevation={10}>
                  <LinearGradient
                    colors={['#F7A956', '#CA7112']}
                    style={{
                      borderRadius: 10,
                      alignSelf: 'stretch',
                      padding: 10,
                    }}>
                    <TouchableOpacity
                      style={{
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        style={{
                          fontFamily: 'urbanist_regular',
                          fontSize: 12,
                          color: Colors.white,
                        }}>
                        Pending{'\n'}Consultations
                      </Text>
                      <Text
                        style={{
                          fontFamily: 'urbanist_bold',
                          fontSize: 30,
                          marginTop: 10,
                          color: Colors.white,
                        }}>
                        5
                      </Text>
                    </TouchableOpacity>
                  </LinearGradient>
                </CardView>

                <CardView
                  style={{
                    flex: 1,
                    marginLeft: width * 0.02,
                    alignSelf: 'stretch',
                  }}
                  cornerRadius={10}
                  cardElevation={10}
                  cardMaxElevation={10}>
                  <LinearGradient
                    colors={['#B0B0B0', '#80808B']}
                    style={{
                      borderRadius: 10,
                      alignSelf: 'stretch',
                      padding: 10,
                    }}>
                    <TouchableOpacity
                      style={{
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        style={{
                          fontFamily: 'urbanist_regular',
                          fontSize: 12,
                          color: Colors.white,
                        }}>
                        Finished{'\n'}Consultations
                      </Text>
                      <Text
                        style={{
                          fontFamily: 'urbanist_bold',
                          fontSize: 30,
                          marginTop: 10,
                          color: Colors.white,
                        }}>
                        5
                      </Text>
                    </TouchableOpacity>
                  </LinearGradient>
                </CardView>
              </View>

              <Text
                style={[
                  styles.title,
                  {
                    fontFamily: 'urbanist_semi_bold',
                    fontSize: 24,
                    color: Colors.text_dark,
                    marginHorizontal: 3,
                    marginTop: height * 0.025,
                  },
                ]}>
                Earnings
              </Text>
              <CardView
                style={{
                  flex: 1,
                  marginTop: height * 0.01,
                  alignSelf: 'stretch',
                }}
                cornerRadius={10}
                cardElevation={10}
                cardMaxElevation={10}>
                <View
                  style={{
                    alignSelf: 'stretch',
                    padding: 10,
                    backgroundColor: Colors.white,
                  }}>
                  <BarChart
                    data={data}
                    width={Dimensions.get('window').width - 60}
                    height={220}
                    yAxisLabel="IQD "
                    chartConfig={chartConfig}
                    verticalLabelRotation={30}
                  />
                </View>
              </CardView>

              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  marginTop: height * 0.02,
                }}>
                <CardView
                  style={{
                    flex: 1,
                    marginRight: width * 0.02,
                    alignSelf: 'stretch',
                  }}
                  cornerRadius={10}
                  cardElevation={10}
                  cardMaxElevation={10}>
                  <View
                    style={{
                      height: 105,
                      padding: 10,
                      backgroundColor: Colors.white,
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignSelf: 'stretch',
                      }}>
                      <Text
                        style={{
                          fontSize: 11,
                          flex: 1,
                          marginRight: '1%',
                          fontFamily: 'urbanist_regular',
                          color: '#8C94A0',
                        }}>
                        Total{'\n'}Earnings
                      </Text>
                      <Image
                        style={{
                          height: 15,
                          width: 15,
                          resizeMode: 'contain',
                        }}
                        source={require('../../../assets/info.png')}
                      />
                    </View>
                    <Text
                      style={{
                        fontFamily: 'urbanist_bold',
                        fontSize: 18,
                        marginTop: 10,
                        color: Colors.text_dark,
                      }}>
                      25000 {'\n'}IQD
                    </Text>
                  </View>
                </CardView>

                <CardView
                  style={{
                    flex: 1,
                    marginHorizontal: width * 0.01,
                    alignSelf: 'stretch',
                  }}
                  cornerRadius={10}
                  cardElevation={10}
                  cardMaxElevation={10}>
                  <View
                    style={{
                      height: 105,
                      padding: 10,
                      backgroundColor: Colors.white,
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignSelf: 'stretch',
                      }}>
                      <Text
                        style={{
                          flex: 1,
                          fontSize: 11,
                          fontFamily: 'urbanist_regular',
                          color: '#8C94A0',
                        }}>
                        Platform
                      </Text>
                      <Image
                        style={{
                          height: 15,
                          width: 15,
                          resizeMode: 'contain',
                        }}
                        source={require('../../../assets/info.png')}
                      />
                    </View>
                    <Text
                      style={{
                        fontSize: 11,
                        fontFamily: 'urbanist_regular',
                        color: '#8C94A0',
                      }}>
                      Highest Earning
                    </Text>
                    <Text
                      style={{
                        fontFamily: 'urbanist_bold',
                        fontSize: 18,
                        marginTop: 10,
                        color: Colors.text_dark,
                      }}>
                      104,000 {'\n'}IQD
                    </Text>
                  </View>
                </CardView>

                <CardView
                  style={{
                    flex: 1,
                    marginLeft: width * 0.02,
                    alignSelf: 'stretch',
                  }}
                  cornerRadius={10}
                  cardElevation={10}
                  cardMaxElevation={10}>
                  <View
                    style={{
                      padding: 10,
                      height: 105,
                      backgroundColor: Colors.white,
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignSelf: 'stretch',
                      }}>
                      <Text
                        style={{
                          fontSize: 11,
                          flex: 1,
                          marginRight: '1%',
                          fontFamily: 'urbanist_regular',
                          color: '#8C94A0',
                        }}>
                        Your Rating
                      </Text>
                      <View style={{marginLeft: '1%'}}>
                        <Image
                          style={{
                            height: 15,
                            width: 15,
                            resizeMode: 'contain',
                          }}
                          source={require('../../../assets/info.png')}
                        />
                      </View>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Text
                        style={{
                          fontFamily: 'urbanist_bold',
                          fontSize: 33,
                          marginTop: 10,
                          color: Colors.black,
                        }}>
                        4.5
                      </Text>
                      <View style={{alignSelf: 'flex-end', paddingBottom: 5}}>
                        <StarRating
                          disabled={false}
                          maxStars={1}
                          starSize={15}
                          halfStarColor={Colors.star}
                          fullStarColor={Colors.star}
                          rating={1}
                          selectedStar={rating =>
                            this.onStarRatingPress(rating)
                          }
                        />
                      </View>
                    </View>
                    <Text
                      style={{
                        fontFamily: 'urbanist_bold',
                        fontSize: 12,
                        color: Colors.text_light,
                      }}>
                      25 Reviews
                    </Text>
                  </View>
                </CardView>
              </View>

              <View style={{marginTop: 10}} />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg,
  },
  title: {
    fontSize: 28,
    color: Colors.text_dark,
    fontFamily: 'urbanist_bold',
  },
  description: {
    fontSize: 14,
    fontFamily: 'urbanist_regular',
    color: Colors.text_light,
  },
  tabLayout: {
    backgroundColor: Colors.enabled,
    borderRadius: 10,
    padding: 5,
    margin: 7,
    flexDirection: 'row',
  },
  enabled_tab: {
    flex: 0.5,
    backgroundColor: Colors.secondary,
    borderRadius: 10,
    padding: 10,
  },
  enabled_tab_text: {
    color: Colors.white,
    textAlign: 'center',
    fontFamily: 'urbanist_bold',
    fontSize: 16,
  },
  disabled_tab: {
    flex: 0.5,
    backgroundColor: Colors.enabled,
    borderRadius: 10,
    padding: 10,
  },
  disabled_tab_text: {
    color: Colors.secondary,
    fontFamily: 'urbanist_regular',
    fontSize: 16,
    textAlign: 'center',
  },
  label: {
    fontFamily: 'urbanist_bold',
    fontSize: 18,
  },
  textInput: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.light_grey,
    fontSize: 17,
    paddingHorizontal: 10,
    color: Colors.dark_grey,
    fontFamily: 'urbanist_light',
  },
  textInputFilled: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.secondary,
    fontSize: 17,
    paddingHorizontal: 10,
    color: Colors.dark_grey,
    fontFamily: 'urbanist_light',
  },
  disabledGender: {
    flex: 0.48,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.7,
    alignItems: 'center',
    padding: 10,
    borderColor: Colors.light_grey,
  },
  filledGender: {
    flex: 0.48,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.7,
    alignItems: 'center',
    padding: 10,
    borderColor: Colors.secondary,
  },
  enabledGender: {
    flex: 0.48,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    borderRadius: 7,
    padding: 10,
  },
  disabledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.disabledIcon,
  },
  enabledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.white,
  },
  filledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.secondary,
  },
  genderLayout: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button1: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  button2: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.disabled,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  placeholder_text: {
    fontFamily: 'urbanist_regular',
    fontSize: 17,
    color: Colors.light_grey,
  },
  text: {
    fontFamily: 'urbanist_regular',
    fontSize: 17,
    color: Colors.black,
  },
});
