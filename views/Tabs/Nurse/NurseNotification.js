import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import Colors from '../../../utils/Colors';

export default class NurseNotification extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: Colors.white,
        }}>
        <Text>Notification Screen is in development phase</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
