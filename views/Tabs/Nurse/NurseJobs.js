import React, {Component} from 'react';
import {
  BackHandler,
  StyleSheet,
  Dimensions,
  ScrollView,
  StatusBar,
  Text,
  TouchableHighlight,
  TextInput,
  ToastAndroid,
  KeyboardAvoidingView,
  PermissionsAndroid,
  View,
  Platform,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import Colors from '../../../utils/Colors';
import {CustomPicker} from 'react-native-custom-picker';
import DoctorConsultationsCard from '../../../custom/views/DoctorConsultationsCard';

export default class NurseJobs extends Component {
  state = {
    width: 0,
    starCount: 3.5,
  };

  constructor(props) {
    super(props);

    this.window = Dimensions.get('window');
    this.screenHeight = Dimensions.get('screen').height;
    this.windowHeight = Dimensions.get('window').height;
    this.navbarHeight =
      this.screenHeight - this.windowHeight + StatusBar.currentHeight;

    this.types = ['1Km', '2Km', '4Km', '8Km', '15Km', '30Km'];
  }

  renderHeader() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          padding: 20,
        }}>
        <Text
          style={{
            fontFamily: 'urbanist_bold',
            fontSize: 20,
            color: Colors.black,
          }}>
          Select Radius
        </Text>
      </View>
    );
  }

  renderField(settings) {
    const {selectedItem, defaultText, getLabel, clear} = settings;
    return (
      <View>
        {!selectedItem && (
          <Text style={styles.placeholder_text}>{defaultText}</Text>
        )}
        {selectedItem && (
          <Text style={styles.text}>{getLabel(selectedItem)}</Text>
        )}
      </View>
    );
  }

  renderOption(settings) {
    const {item, getLabel} = settings;
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
          paddingHorizontal: 15,
        }}>
        <Text
          style={{
            color: Colors.black,
            alignSelf: 'flex-start',
            padding: 15,
          }}>
          {getLabel(item)}
        </Text>
        <Text
          style={{height: 1, backgroundColor: Colors.secondary, width: '100%'}}>
          {' '}
        </Text>
      </View>
    );
  }

  render() {
    const {height, width} = this.window;
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="dark-content" backgroundColor={Colors.white} />

        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
          contentContainerStyle={{
            paddingBottom: this.navbarHeight,
          }}
          showVerticalScrollIndicator={false}>
          <View>
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'stretch',
                paddingHorizontal: 10,
                paddingVertical: 15,
                alignItems: 'center',
                justifyContent: 'space-around',
                backgroundColor: Colors.white,
              }}>
              <Text style={[styles.title, {flex: 1, paddingRight: '5%'}]}>
                Choose Radius
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-end',
                  width: width * 0.35,
                }}>
                <Image
                  source={require('../../../assets/marker.png')}
                  style={{height: 20, width: 20, resizeMode: 'contain'}}
                />
                <CustomPicker
                  style={{height: 50, justifyContent: 'center'}}
                  getLabel={item => item}
                  fieldTemplate={this.renderField}
                  optionTemplate={this.renderOption}
                  headerTemplate={this.renderHeader}
                  placeholder={'Select radius'}
                  options={this.types}
                  onValueChange={value => {
                    this.setState(
                      {consultationType: value !== null ? value : ''},
                      this.handleChange,
                    );
                  }}
                />
              </View>
            </View>

            <View style={{paddingHorizontal: 15, alignItems: 'center'}}>
              <DoctorConsultationsCard
                style={{
                  marginTop: '5%',
                }}
                meetingTime={'21st December 2021 - 23rd December 2021'}
                meetingTimeStyle={{
                  flex: 1,
                  fontFamily: 'urbanist_semi_bold',
                  fontSize: 14,
                  color: Colors.text_dark,
                }}
                locVisibility={true}
                patientName={'Abram Westervelt'}
                cardStatusColor={Colors.new}
                cardStatusVisibility={true}
                description={
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nondiam pellentesque volutpat id aliquam tortor ipsum necporttitor.'
                }
                rating={4}
                totalReviews={7}
                numOfApplicatons={true}
                jobBtn={true}
                avatar={require('../../../assets/doc1.png')}
              />

              <DoctorConsultationsCard
                style={{
                  marginTop: '5%',
                }}
                meetingTime={'21st December 2021 - 23rd December 2021'}
                meetingTimeStyle={{
                  flex: 1,
                  fontFamily: 'urbanist_semi_bold',
                  fontSize: 14,
                  color: Colors.text_dark,
                }}
                locVisibility={true}
                patientName={'Abram Westervelt'}
                cardStatusColor={Colors.pending}
                cardStatusVisibility={true}
                description={
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nondiam pellentesque volutpat id aliquam tortor ipsum necporttitor.'
                }
                rating={4}
                totalReviews={7}
                numOfApplicatons={true}
                jobBtn={true}
                avatar={require('../../../assets/doc1.png')}
              />

              <DoctorConsultationsCard
                style={{
                  marginTop: '5%',
                }}
                meetingTime={'21st December 2021 - 23rd December 2021'}
                meetingTimeStyle={{
                  flex: 1,
                  fontFamily: 'urbanist_semi_bold',
                  fontSize: 14,
                  color: Colors.text_dark,
                }}
                locVisibility={true}
                patientName={'Abram Westervelt'}
                cardStatusColor={Colors.finished}
                cardStatusVisibility={true}
                description={
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nondiam pellentesque volutpat id aliquam tortor ipsum necporttitor.'
                }
                rating={4}
                totalReviews={7}
                numOfApplicatons={true}
                jobBtn={true}
                avatar={require('../../../assets/doc1.png')}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg,
  },
  title: {
    fontSize: 25,
    color: Colors.black,
    fontFamily: 'urbanist_bold',
  },
  description: {
    fontSize: 16,
    fontFamily: 'urbanist_regular',
    color: Colors.light_grey,
  },
  tabLayout: {
    backgroundColor: Colors.enabled,
    borderRadius: 10,
    padding: 5,
    margin: 7,
    flexDirection: 'row',
  },
  enabled_tab: {
    flex: 0.5,
    backgroundColor: Colors.secondary,
    borderRadius: 10,
    padding: 10,
  },
  enabled_tab_text: {
    color: Colors.white,
    textAlign: 'center',
    fontFamily: 'urbanist_bold',
    fontSize: 16,
  },
  disabled_tab: {
    flex: 0.5,
    backgroundColor: Colors.enabled,
    borderRadius: 10,
    padding: 10,
  },
  disabled_tab_text: {
    color: Colors.secondary,
    fontFamily: 'urbanist_regular',
    fontSize: 16,
    textAlign: 'center',
  },
  label: {
    fontFamily: 'urbanist_bold',
    fontSize: 18,
  },
  textInput: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.light_grey,
    fontSize: 17,
    paddingHorizontal: 10,
    color: Colors.dark_grey,
    fontFamily: 'urbanist_light',
  },
  textInputFilled: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.secondary,
    fontSize: 17,
    paddingHorizontal: 10,
    color: Colors.dark_grey,
    fontFamily: 'urbanist_light',
  },
  disabledGender: {
    flex: 0.48,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.7,
    alignItems: 'center',
    padding: 10,
    borderColor: Colors.light_grey,
  },
  filledGender: {
    flex: 0.48,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.7,
    alignItems: 'center',
    padding: 10,
    borderColor: Colors.secondary,
  },
  enabledGender: {
    flex: 0.48,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    borderRadius: 7,
    padding: 10,
  },
  disabledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.disabledIcon,
  },
  enabledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.white,
  },
  filledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.secondary,
  },
  genderLayout: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button1: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    borderRadius: 10,
  },
  button2: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.disabled,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  placeholder_text: {
    fontFamily: 'urbanist_regular',
    fontSize: 17,
    color: Colors.light_grey,
  },
  text: {
    fontFamily: 'urbanist_regular',
    fontSize: 17,
    color: Colors.black,
  },
});
