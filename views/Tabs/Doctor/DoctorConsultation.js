import React, {Component} from 'react';
import {
  BackHandler,
  StyleSheet,
  Dimensions,
  ScrollView,
  StatusBar,
  Text,
  TouchableHighlight,
  TextInput,
  ToastAndroid,
  KeyboardAvoidingView,
  PermissionsAndroid,
  View,
  Platform,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import SharedPreferences from 'react-native-shared-preferences';
import User from '../../../models/User';
import Colors from '../../../utils/Colors';
import StarRating from 'react-native-star-rating';
import {CustomPicker} from 'react-native-custom-picker';
import {Avatar} from 'react-native-elements';
import DoctorConsultationsCard from '../../../custom/views/DoctorConsultationsCard';

export default class DoctorConsultation extends Component {
  state = {
    user: User,
    entries: ['../../../assets/apology.png', '../../../assets/apology.png'],
    activeSlide: 0,
    width: 0,
    starCount: 3.5,
    consultationType: '',
  };

  constructor(props) {
    super(props);

    this.screenHeight = Dimensions.get('screen').height;
    this.windowHeight = Dimensions.get('window').height;
    this.navbarHeight =
      this.screenHeight - this.windowHeight + StatusBar.currentHeight;
    SharedPreferences.getItem(
      'user',
      function (value) {
        this.setState({
          user: JSON.parse(value),
        });
      }.bind(this),
    );

    this.types = ['All', 'New', 'Pending', 'Finished'];
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating,
    });
  }
  renderHeader() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          padding: 20,
        }}>
        <Text
          style={{
            fontFamily: 'urbanist_bold',
            fontSize: 20,
            color: Colors.black,
          }}>
          Select Consultation Type
        </Text>
      </View>
    );
  }

  renderField(settings) {
    const {selectedItem, defaultText, getLabel, clear} = settings;
    return (
      <View style={{flexDirection: 'row', alignItems: 'center', width: '100%'}}>
        {!selectedItem && (
          <Text style={[styles.placeholder_text, {flex: 1}]}>
            {defaultText}
          </Text>
        )}
        {selectedItem && (
          <Text style={[styles.text, {flex: 1}]}>{getLabel(selectedItem)}</Text>
        )}
        <Image
          style={{
            resizeMode: 'contain',
            tintColor: Colors.light_grey,
            width: 10,
            height: 10,
          }}
          source={require('../../../assets/ic_down.png')}
        />
      </View>
    );
  }

  renderOption(settings) {
    const {item, getLabel} = settings;
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
          paddingHorizontal: 15,
        }}>
        <Text
          style={{
            color: Colors.black,
            alignSelf: 'flex-start',
            padding: 15,
          }}>
          {getLabel(item)}
        </Text>
        <Text
          style={{height: 1, backgroundColor: Colors.secondary, width: '100%'}}>
          {' '}
        </Text>
      </View>
    );
  }

  handleChange() {}

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="dark-content" backgroundColor={Colors.white} />

        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
          contentContainerStyle={{
            paddingBottom: this.navbarHeight,
          }}
          showVerticalScrollIndicator={false}>
          <View>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                alignSelf: 'stretch',
                paddingHorizontal: 25,
                paddingVertical: 15,
                alignItems: 'center',
                backgroundColor: Colors.white,
              }}>
              <Text style={[styles.title, {flex: 1}]}>Filters</Text>
              <CustomPicker
                style={[
                  this.state.consultationType !== ''
                    ? styles.textInputFilled
                    : styles.textInput,
                  {
                    padding: 10,
                    width: Dimensions.get('window').width / 2.5,
                    justifyContent: 'center',
                  },
                ]}
                getLabel={item => item}
                fieldTemplate={this.renderField}
                optionTemplate={this.renderOption}
                headerTemplate={this.renderHeader}
                placeholder={'Select'}
                options={this.types}
                onValueChange={value => {
                  this.setState(
                    {consultationType: value !== null ? value : ''},
                    this.handleChange,
                  );
                }}
              />
            </View>

            <View style={{paddingHorizontal: 15}}>
              <DoctorConsultationsCard
                style={{marginTop: '5%'}}
                mainButtonListener={() => {}}
                meetingTime={'Today, 03:31 PM'}
                meetingTimeStyle={{
                  flex: 1,
                  fontFamily: 'urbanist_semi_bold',
                  fontSize: 14,
                  color: Colors.text_dark,
                }}
                patientName={'Abram Westervelt'}
                cardStatusColor={Colors.pending}
                cardStatusVisibility={true}
                messageIconVisibility={true}
                onMessageClick={() => {}}
                locationIconVisibility={false}
                description={
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nondiam pellentesque volutpat id aliquam tortor ipsum necporttitor.'
                }
                rating={3.5}
                totalReviews={5}
                reportLayout={true}
                avatar={require('../../../assets/doc3.png')}
              />

              <DoctorConsultationsCard
                style={{
                  marginTop: '5%',
                }}
                mainButtonListener={() => {}}
                mainButtonStyle={{
                  backgroundColor: '#FEF8F3',
                  padding: 10,
                  alignItems: 'center',
                  borderRadius: 5,
                }}
                mainButtonText={'Home Consultation'}
                mainButtonTextStyle={{
                  color: '#FEA154',
                  fontSize: 12,
                  fontFamily: 'urbanist_semi_bold',
                }}
                meetingTime={'Today, 03:31 PM'}
                meetingTimeStyle={{
                  flex: 1,
                  fontFamily: 'urbanist_semi_bold',
                  fontSize: 14,
                  color: Colors.text_dark,
                }}
                patientName={'Abram Westervelt'}
                cardStatusColor={Colors.new}
                cardStatusVisibility={true}
                messageIconVisibility={true}
                onMessageClick={() => {}}
                locationIconVisibility={true}
                onLocationClick={() => {}}
                description={
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nondiam pellentesque volutpat id aliquam tortor ipsum necporttitor.'
                }
                rating={4}
                totalReviews={7}
                reportLayout={true}
                avatar={require('../../../assets/doc1.png')}
              />

              <DoctorConsultationsCard
                style={{
                  marginTop: '5%',
                }}
                mainButtonListener={() => {}}
                mainButtonStyle={{
                  backgroundColor: '#FEF8F3',
                  padding: 10,
                  alignItems: 'center',
                  borderRadius: 5,
                }}
                mainButtonText={'Home Consultation'}
                mainButtonTextStyle={{
                  color: '#FEA154',
                  fontSize: 12,
                  fontFamily: 'urbanist_semi_bold',
                }}
                meetingTime={'Today, 03:31 PM'}
                meetingTimeStyle={{
                  flex: 1,
                  fontFamily: 'urbanist_semi_bold',
                  fontSize: 14,
                  color: Colors.text_dark,
                }}
                patientName={'Abram Westervelt'}
                cardStatusColor={Colors.finished}
                cardStatusVisibility={true}
                messageIconVisibility={true}
                onMessageClick={() => {}}
                description={
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nondiam pellentesque volutpat id aliquam tortor ipsum necporttitor.'
                }
                rating={4}
                totalReviews={7}
                reportLayout={true}
                avatar={require('../../../assets/doc1.png')}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Colors.lightest_grey,
  },
  title: {
    fontSize: 25,
    color: Colors.black,
    fontFamily: 'urbanist_bold',
  },
  description: {
    fontSize: 16,
    fontFamily: 'urbanist_regular',
    color: Colors.light_grey,
  },
  tabLayout: {
    backgroundColor: Colors.enabled,
    borderRadius: 10,
    padding: 5,
    margin: 7,
    flexDirection: 'row',
  },
  enabled_tab: {
    flex: 0.5,
    backgroundColor: Colors.secondary,
    borderRadius: 10,
    padding: 10,
  },
  enabled_tab_text: {
    color: Colors.white,
    textAlign: 'center',
    fontFamily: 'urbanist_bold',
    fontSize: 16,
  },
  disabled_tab: {
    flex: 0.5,
    backgroundColor: Colors.enabled,
    borderRadius: 10,
    padding: 10,
  },
  disabled_tab_text: {
    color: Colors.secondary,
    fontFamily: 'urbanist_regular',
    fontSize: 16,
    textAlign: 'center',
  },
  label: {
    fontFamily: 'urbanist_bold',
    fontSize: 18,
  },
  textInput: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.light_grey,
    fontSize: 17,
    paddingHorizontal: 10,
    color: Colors.dark_grey,
    fontFamily: 'urbanist_light',
  },
  textInputFilled: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.secondary,
    fontSize: 17,
    paddingHorizontal: 10,
    color: Colors.dark_grey,
    fontFamily: 'urbanist_light',
  },
  disabledGender: {
    flex: 0.48,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.7,
    alignItems: 'center',
    padding: 10,
    borderColor: Colors.light_grey,
  },
  filledGender: {
    flex: 0.48,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.7,
    alignItems: 'center',
    padding: 10,
    borderColor: Colors.secondary,
  },
  enabledGender: {
    flex: 0.48,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    borderRadius: 7,
    padding: 10,
  },
  disabledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.disabledIcon,
  },
  enabledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.white,
  },
  filledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.secondary,
  },
  genderLayout: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button1: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  button2: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.disabled,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  placeholder_text: {
    fontFamily: 'urbanist_regular',
    fontSize: 17,
    color: Colors.light_grey,
  },
  text: {
    fontFamily: 'urbanist_regular',
    fontSize: 17,
    color: Colors.black,
  },
});
