import React, {Component} from 'react';
import {
  BackHandler,
  StyleSheet,
  Dimensions,
  ScrollView,
  StatusBar,
  Text,
  TouchableHighlight,
  TextInput,
  ToastAndroid,
  KeyboardAvoidingView,
  PermissionsAndroid,
  View,
  Platform,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import SharedPreferences from 'react-native-shared-preferences';
import Colors from '../../../utils/Colors';
import StarRating from 'react-native-star-rating';
import CardView from 'react-native-cardview';

export default class DoctorRequest extends Component {
  state = {
    width: 0,
    starCount: 3.5,
  };

  constructor(props) {
    super(props);

    this.screenHeight = Dimensions.get('screen').height;
    this.windowHeight = Dimensions.get('window').height;
    this.navbarHeight =
      this.screenHeight - this.windowHeight + StatusBar.currentHeight;

    this.types = ['All', 'New', 'Pending', 'Finished'];
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating,
    });
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="dark-content" backgroundColor={Colors.white} />

        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
          contentContainerStyle={{
            paddingBottom: this.navbarHeight,
          }}
          showVerticalScrollIndicator={false}>
          <View>
            <View
              style={{
                alignSelf: 'stretch',
                paddingHorizontal: 15,
                paddingVertical: 25,
                backgroundColor: Colors.white,
              }}>
              <Text style={[styles.title]}>Requests</Text>
            </View>

            <View style={{paddingHorizontal: 15}}>
              <CardView
                style={{marginTop: '4%'}}
                cardElevation={10}
                cardMaxElevation={10}
                cornerRadius={10}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'stretch',
                    padding: 10,
                    backgroundColor: Colors.white,
                  }}>
                  <View>
                    <View
                      style={{
                        paddingHorizontal: 7,
                      }}>
                      <View
                        style={{flexDirection: 'row', flex: 1, marginTop: 15}}>
                        <Image
                          style={{height: 40, width: 40, borderRadius: 20}}
                          source={require('../../../assets/doc3.png')}
                        />
                        <View
                          style={{
                            marginHorizontal: 5,
                            flex: 1,
                          }}>
                          <Text
                            style={{
                              fontSize: 14,
                              color: Colors.text_dark,
                              fontFamily: 'urbanist_semi_bold',
                            }}>
                            Abram Westervelt
                          </Text>

                          <View style={{flexDirection: 'row', marginTop: 5}}>
                            <StarRating
                              disabled={false}
                              maxStars={5}
                              starSize={15}
                              halfStarColor={Colors.star}
                              fullStarColor={Colors.star}
                              emptyStarColor={'#E7E7E7'}
                              rating={4}
                              starStyle={{marginHorizontal: '1%'}}
                              selectedStar={rating =>
                                this.onStarRatingPress(rating)
                              }
                            />
                            <Text
                              style={{
                                fontSize: 14,
                                fontFamily: 'urbanist_semi_bold',
                                marginLeft: 10,
                                color: Colors.text_light,
                              }}>
                              5 Reviews
                            </Text>
                          </View>
                        </View>
                        <Text
                          style={{
                            color: Colors.text_light,
                            fontFamily: 'urbanist_regular',
                            fontSize: 12,
                          }}>
                          2 minutes ago
                        </Text>
                      </View>

                      <Text
                        style={[
                          styles.description,
                          {
                            fontSize: 14,
                            color: Colors.text_light,
                            marginVertical: 15,
                          },
                        ]}>
                        “Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit. Non diam pellentesque volutpat id aliquam tortor
                        ipsum nec porttitor.”
                      </Text>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginHorizontal: 5,
                          alignItems: 'center',
                        }}>
                        <Image
                          style={{height: 25, width: 25}}
                          source={require('../../../assets/jpg.png')}
                        />
                        <Text
                          style={{
                            fontFamily: 'urbanist_regular',
                            fontSize: 14,
                            color: Colors.text_dark,
                          }}>
                          CBC.jpeg
                        </Text>
                      </View>
                    </View>

                    <View
                      style={{
                        alignSelf: 'stretch',
                        marginVertical: 15,
                        backgroundColor: Colors.lightest_grey,
                        height: 1,
                      }}
                    />

                    <View
                      style={{
                        flexDirection: 'row',
                        paddingHorizontal: 7,
                        alignItems: 'center',
                      }}>
                      <Image
                        style={{height: 40, width: 40, borderRadius: 20}}
                        source={require('../../../assets/doc1.png')}
                      />
                      <TextInput
                        style={{
                          height: 40,
                          backgroundColor: Colors.bb,
                          flex: 1,
                          marginLeft: 7,
                          borderRadius: 25,
                          padding: 7,
                          fontFamily: 'urbanist_regular',
                        }}
                        placeholder={'Write a comment...'}
                      />
                    </View>
                  </View>
                </View>
              </CardView>

              <CardView
                style={{marginTop: '4%'}}
                cardElevation={10}
                cardMaxElevation={10}
                cornerRadius={10}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'stretch',
                    padding: 10,
                    backgroundColor: Colors.white,
                  }}>
                  <View>
                    <View
                      style={{
                        paddingHorizontal: 7,
                      }}>
                      <View
                        style={{flexDirection: 'row', flex: 1, marginTop: 15}}>
                        <Image
                          style={{height: 40, width: 40, borderRadius: 20}}
                          source={require('../../../assets/doc2.png')}
                        />
                        <View
                          style={{
                            marginHorizontal: 5,
                            flex: 1,
                          }}>
                          <Text
                            style={{
                              fontSize: 14,
                              color: Colors.text_dark,
                              fontFamily: 'urbanist_semi_bold',
                            }}>
                            Phillip Vaccaro
                          </Text>

                          <View style={{flexDirection: 'row', marginTop: 5}}>
                            <StarRating
                              disabled={false}
                              maxStars={5}
                              starSize={15}
                              halfStarColor={Colors.star}
                              fullStarColor={Colors.star}
                              emptyStarColor={'#E7E7E7'}
                              rating={4}
                              starStyle={{marginHorizontal: '1%'}}
                              selectedStar={rating =>
                                this.onStarRatingPress(rating)
                              }
                            />
                            <Text
                              style={{
                                fontSize: 14,
                                fontFamily: 'urbanist_semi_bold',
                                marginLeft: 10,
                                color: Colors.text_light,
                              }}>
                              5 Reviews
                            </Text>
                          </View>
                        </View>
                        <Text
                          style={{
                            color: Colors.text_light,
                            fontFamily: 'urbanist_regular',
                            fontSize: 12,
                          }}>
                          2 minutes ago
                        </Text>
                      </View>

                      <Text
                        style={[
                          styles.description,
                          {
                            fontSize: 14,
                            color: Colors.text_light,
                            marginVertical: 15,
                          },
                        ]}>
                        “Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit. Non diam pellentesque volutpat id aliquam tortor
                        ipsum nec porttitor.”
                      </Text>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginHorizontal: 5,
                          alignItems: 'center',
                        }}>
                        <Image
                          style={{height: 25, width: 25}}
                          source={require('../../../assets/jpg.png')}
                        />
                        <Text
                          style={{
                            fontFamily: 'urbanist_regular',
                            fontSize: 14,
                            color: Colors.text_dark,
                          }}>
                          Yellow.jpeg
                        </Text>
                      </View>
                    </View>

                    <View
                      style={{
                        alignSelf: 'stretch',
                        marginVertical: 15,
                        backgroundColor: Colors.lightest_grey,
                        height: 1,
                      }}
                    />

                    <View
                      style={{
                        flexDirection: 'row',
                        paddingHorizontal: 7,
                        alignItems: 'center',
                      }}>
                      <Image
                        style={{height: 40, width: 40, borderRadius: 20}}
                        source={require('../../../assets/doc1.png')}
                      />
                      <TextInput
                        style={{
                          height: 40,
                          backgroundColor: Colors.bb,
                          flex: 1,
                          marginLeft: 7,
                          borderRadius: 25,
                          padding: 7,
                          fontFamily: 'urbanist_regular',
                        }}
                        placeholder={'Write a comment...'}
                      />
                    </View>
                  </View>
                </View>
              </CardView>

              <CardView
                style={{marginTop: '4%'}}
                cardElevation={10}
                cardMaxElevation={10}
                cornerRadius={10}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'stretch',
                    padding: 10,
                    borderRadius: 10,
                    backgroundColor: Colors.white,
                  }}>
                  <View>
                    <View
                      style={{
                        paddingHorizontal: 7,
                      }}>
                      <View
                        style={{flexDirection: 'row', flex: 1, marginTop: 15}}>
                        <Image
                          style={{height: 40, width: 40, borderRadius: 20}}
                          source={require('../../../assets/doc3.png')}
                        />
                        <View
                          style={{
                            marginHorizontal: 5,
                            flex: 1,
                          }}>
                          <Text
                            style={{
                              fontSize: 14,
                              color: Colors.text_dark,
                              fontFamily: 'urbanist_semi_bold',
                            }}>
                            Abram Westervelt
                          </Text>

                          <View style={{flexDirection: 'row', marginTop: 5}}>
                            <StarRating
                              disabled={false}
                              maxStars={5}
                              starSize={15}
                              halfStarColor={Colors.star}
                              fullStarColor={Colors.star}
                              emptyStarColor={'#E7E7E7'}
                              rating={4}
                              starStyle={{marginHorizontal: '1%'}}
                              selectedStar={rating =>
                                this.onStarRatingPress(rating)
                              }
                            />
                            <Text
                              style={{
                                fontSize: 14,
                                fontFamily: 'urbanist_semi_bold',
                                marginLeft: 10,
                                color: Colors.text_light,
                              }}>
                              5 Reviews
                            </Text>
                          </View>
                        </View>
                        <Text
                          style={{
                            color: Colors.text_light,
                            fontFamily: 'urbanist_regular',
                            fontSize: 12,
                          }}>
                          2 minutes ago
                        </Text>
                      </View>

                      <Text
                        style={[
                          styles.description,
                          {
                            fontSize: 14,
                            color: Colors.text_light,
                            marginVertical: 15,
                          },
                        ]}>
                        “Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit. Non diam pellentesque volutpat id aliquam tortor
                        ipsum nec porttitor.”
                      </Text>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginHorizontal: 5,
                          alignItems: 'center',
                        }}>
                        <Image
                          style={{height: 25, width: 25}}
                          source={require('../../../assets/jpg.png')}
                        />
                        <Text
                          style={{
                            fontFamily: 'urbanist_regular',
                            fontSize: 14,
                            color: Colors.text_dark,
                          }}>
                          CBC.jpeg
                        </Text>
                      </View>
                    </View>

                    <View
                      style={{
                        alignSelf: 'stretch',
                        marginVertical: 15,
                        backgroundColor: Colors.lightest_grey,
                        height: 1,
                      }}
                    />

                    <View
                      style={{
                        flexDirection: 'row',
                        paddingHorizontal: 7,
                        alignItems: 'center',
                      }}>
                      <Image
                        style={{height: 40, width: 40, borderRadius: 20}}
                        source={require('../../../assets/doc1.png')}
                      />
                      <TextInput
                        style={{
                          height: 40,
                          backgroundColor: Colors.bb,
                          flex: 1,
                          marginLeft: 7,
                          borderRadius: 25,
                          padding: 7,
                          fontFamily: 'urbanist_regular',
                        }}
                        placeholder={'Write a comment...'}
                      />
                    </View>
                  </View>
                </View>
              </CardView>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.lightest_grey,
  },
  title: {
    fontSize: 25,
    color: Colors.black,
    fontFamily: 'urbanist_bold',
  },
  description: {
    fontSize: 16,
    fontFamily: 'urbanist_regular',
    color: Colors.light_grey,
  },
  tabLayout: {
    backgroundColor: Colors.enabled,
    borderRadius: 10,
    padding: 5,
    margin: 7,
    flexDirection: 'row',
  },
  enabled_tab: {
    flex: 0.5,
    backgroundColor: Colors.secondary,
    borderRadius: 10,
    padding: 10,
  },
  enabled_tab_text: {
    color: Colors.white,
    textAlign: 'center',
    fontFamily: 'urbanist_bold',
    fontSize: 16,
  },
  disabled_tab: {
    flex: 0.5,
    backgroundColor: Colors.enabled,
    borderRadius: 10,
    padding: 10,
  },
  disabled_tab_text: {
    color: Colors.secondary,
    fontFamily: 'urbanist_regular',
    fontSize: 16,
    textAlign: 'center',
  },
  label: {
    fontFamily: 'urbanist_bold',
    fontSize: 18,
  },
  textInput: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.light_grey,
    fontSize: 17,
    paddingHorizontal: 10,
    color: Colors.dark_grey,
    fontFamily: 'urbanist_light',
  },
  textInputFilled: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.secondary,
    fontSize: 17,
    paddingHorizontal: 10,
    color: Colors.dark_grey,
    fontFamily: 'urbanist_light',
  },
  disabledGender: {
    flex: 0.48,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.7,
    alignItems: 'center',
    padding: 10,
    borderColor: Colors.light_grey,
  },
  filledGender: {
    flex: 0.48,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.7,
    alignItems: 'center',
    padding: 10,
    borderColor: Colors.secondary,
  },
  enabledGender: {
    flex: 0.48,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    borderRadius: 7,
    padding: 10,
  },
  disabledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.disabledIcon,
  },
  enabledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.white,
  },
  filledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.secondary,
  },
  genderLayout: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button1: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  button2: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.disabled,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  placeholder_text: {
    fontFamily: 'urbanist_regular',
    fontSize: 17,
    color: Colors.light_grey,
  },
  text: {
    fontFamily: 'urbanist_regular',
    fontSize: 17,
    color: Colors.black,
  },
});
