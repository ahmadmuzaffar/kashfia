import React, {Component} from 'react';
import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';
import Globals from '../../../utils/Globals';
import SharedPreferences from 'react-native-shared-preferences';
import {CommonActions} from '@react-navigation/native';

export default class DoctorMore extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <TouchableOpacity
          onPress={() => {
            Globals.user = null;
            SharedPreferences.clear();
            const resetAction = CommonActions.reset({
              index: 0,
              routes: [{name: 'start'}],
            });

            this.props.navigation.dispatch(resetAction);
          }}>
          <Text
            style={{color: 'red', fontFamily: 'urbanist_bold', fontSize: 20}}>
            Logout
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
