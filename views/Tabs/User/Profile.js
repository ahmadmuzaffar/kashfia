import {
  Text,
  StyleSheet,
  View,
  SafeAreaView,
  Image,
  Pressable,
  ScrollView,
  TouchableOpacity,
  BackHandler,
  TextInput,
} from 'react-native';
import React, {Component} from 'react';
import Globals from '../../../utils/Globals';
import Colors from '../../../utils/Colors';
import {Avatar} from 'react-native-elements';

export default class Profile extends Component {
  state = {
    uri: '',
    user: Globals.user,
  };

  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    this.setState({uri: this.state.user.avatar});
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.goBack();
    return true;
  }

  Capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  render() {
    const {user} = this.state;

    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
        <View
          style={{
            flexDirection: 'row',
            padding: Globals.pHorizontal,
            borderBottomColor: Colors.lighter_gery,
            borderBottomWidth: 1,
          }}>
          <Text style={styles.title}>Profile</Text>
        </View>

        <ScrollView>
          <View
            style={{
              padding: Globals.pHorizontal,
            }}>
            <View style={{alignItems: 'center'}}>
              <Pressable
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: Globals.screenWidth * 0.3,
                  width: Globals.screenWidth * 0.3,
                  backgroundColor: Colors.avatar_bg,
                  borderColor: Colors.secondary,
                  borderRadius: (Globals.screenWidth * 0.3) / 2,
                  overflow: 'hidden',
                  borderWidth: 2,
                }}>
                <Image
                  style={{
                    height: Globals.screenWidth * 0.3,
                    width: Globals.screenWidth * 0.3,
                  }}
                  source={{uri: this.state.uri}}
                />
                {this.state.uri === '' && (
                  <Image
                    style={{
                      height: 50,
                      width: 50,
                      position: 'absolute',
                      resizeMode: 'contain',
                    }}
                    source={require('../../../assets/thumbnail.png')}
                  />
                )}
              </Pressable>

              <Text
                style={{
                  alignSelf: 'stretch',
                  textAlign: 'center',
                  fontFamily: 'urbanist_bold',
                  fontSize: Globals.screenWidth * 0.06,
                }}>
                {user.fullName}
              </Text>

              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('profile_builder', {
                    toUpdate: true,
                  });
                }}
                style={{
                  paddingVertical: Globals.mTop / 3,
                  paddingHorizontal: Globals.mTop * 2,
                  borderWidth: 1,
                  borderColor: Colors.secondary,
                  borderRadius: 5,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    color: Colors.secondary,
                    fontFamily: 'urbanist_semi_bold',
                  }}>
                  Update Profile
                </Text>
              </TouchableOpacity>
            </View>

            <Text
              style={{
                fontFamily: 'urbanist_semi_bold',
                marginTop: Globals.mTop,
                color: Colors.text_light,
                fontSize: Globals.screenWidth * 0.035,
              }}>
              Phone Number
            </Text>
            <Text
              style={{
                fontFamily: 'urbanist_bold',
                fontSize: Globals.screenWidth * 0.055,
              }}>
              {user.phoneNumber}
            </Text>

            <Text
              style={{
                fontFamily: 'urbanist_semi_bold',
                marginTop: Globals.mTop,
                color: Colors.text_light,
                fontSize: Globals.screenWidth * 0.035,
              }}>
              Gender
            </Text>
            <Text
              style={{
                fontFamily: 'urbanist_bold',
                fontSize: Globals.screenWidth * 0.055,
              }}>
              {this.Capitalize(user.gender)}
            </Text>

            <Text
              style={{
                fontFamily: 'urbanist_semi_bold',
                marginTop: Globals.mTop,
                color: Colors.text_light,
                fontSize: Globals.screenWidth * 0.035,
              }}>
              Date of Birth
            </Text>
            <Text
              style={{
                fontFamily: 'urbanist_bold',
                fontSize: Globals.screenWidth * 0.055,
              }}>
              {user.dateOfBirth}
            </Text>

            <Text
              style={{
                fontFamily: 'urbanist_semi_bold',
                marginTop: Globals.mTop,
                color: Colors.text_light,
                fontSize: Globals.screenWidth * 0.035,
              }}>
              Email Address
            </Text>
            <Text
              style={{
                fontFamily: 'urbanist_bold',
                fontSize: Globals.screenWidth * 0.055,
              }}>
              {user.email ? user.email : 'Nil'}
            </Text>

            <Text
              style={{
                fontFamily: 'urbanist_semi_bold',
                marginTop: Globals.mTop,
                color: Colors.text_light,
                fontSize: Globals.screenWidth * 0.035,
              }}>
              Country
            </Text>
            <Text
              style={{
                fontFamily: 'urbanist_bold',
                fontSize: Globals.screenWidth * 0.055,
              }}>
              {user.country}
            </Text>

            <Text
              style={{
                fontFamily: 'urbanist_semi_bold',
                marginTop: Globals.mTop,
                color: Colors.text_light,
                fontSize: Globals.screenWidth * 0.035,
              }}>
              City
            </Text>
            <Text
              style={{
                fontFamily: 'urbanist_bold',
                fontSize: Globals.screenWidth * 0.055,
              }}>
              {user.city}
            </Text>

            <Text
              style={{
                fontFamily: 'urbanist_semi_bold',
                marginTop: Globals.mTop,
                color: Colors.text_light,
                fontSize: Globals.screenWidth * 0.035,
              }}>
              District
            </Text>
            <Text
              style={{
                fontFamily: 'urbanist_bold',
                fontSize: Globals.screenWidth * 0.055,
              }}>
              {user.district}
            </Text>
          </View>
        </ScrollView>

        <TouchableOpacity
          style={styles.imgLL}
          onPress={() => {
            this.handleBackButtonClick();
          }}>
          <Image
            style={styles.back}
            source={require('../../../assets/back2.png')}
          />
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontFamily: 'urbanist_semi_bold',
    fontSize: 20,
    color: Colors.text_dark,
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
  },
  back: {
    width: 25,
    height: 25,
  },
  imgLL: {
    position: 'absolute',
    top: Globals.pHorizontal,
    left: Globals.pHorizontal,
  },
});
