import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Switch,
  Pressable,
  Image,
  BackHandler,
  StyleSheet,
} from 'react-native';
import React, {Component} from 'react';
import Globals from '../../../utils/Globals';
import Colors from '../../../utils/Colors';

export class UserSettings extends Component {
  state = {
    isNotification: false,
  };

  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.goBack();
    return true;
  }

  render() {
    const {isNotification} = this.state;

    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
        <View
          style={{
            flexDirection: 'row',
            padding: Globals.pHorizontal,
            borderBottomColor: Colors.lighter_gery,
            borderBottomWidth: 1,
          }}>
          <Text style={styles.title}>Settings</Text>
        </View>

        <View style={{padding: Globals.mTop}}>
          <Text
            style={{
              fontFamily: 'urbanist_bold',
              color: Colors.secondary,
              fontSize: Globals.screenWidth * 0.055,
            }}>
            Account Settings
          </Text>
          <Pressable
            onPress={() => {
              this.props.navigation.navigate('sign_in', {
                auth_type: 'change_number',
              });
            }}
            style={{
              flexDirection: 'row',
              marginTop: Globals.mTop,
              alignSelf: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                flex: 1,
                fontFamily: 'urbanist_regular',
                color: Colors.text_dark,
                fontSize: Globals.screenWidth * 0.05,
              }}>
              Change phone number
            </Text>

            <Image
              source={require('../../../assets/next.png')}
              style={{resizeMode: 'contain', height: 25, width: 25}}
            />
          </Pressable>

          <Pressable
            style={{
              flexDirection: 'row',
              marginTop: Globals.mTop,
              alignSelf: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                flex: 1,
                fontFamily: 'urbanist_regular',
                color: Colors.text_dark,
                fontSize: Globals.screenWidth * 0.05,
              }}>
              Push notifications
            </Text>

            <Switch
              trackColor={{false: '#E9E9EA', true: '#34C759'}}
              thumbColor={Colors.white}
              ios_backgroundColor="#E9E9EA"
              onValueChange={state => {
                this.setState({isNotification: !isNotification});
              }}
              value={isNotification}
            />
          </Pressable>

          <Text
            style={{
              fontFamily: 'urbanist_bold',
              color: Colors.secondary,
              marginTop: Globals.pHorizontal,
              fontSize: Globals.screenWidth * 0.055,
            }}>
            More
          </Text>
          <Pressable
            style={{
              flexDirection: 'row',
              marginTop: Globals.mTop,
              alignSelf: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                flex: 1,
                fontFamily: 'urbanist_regular',
                color: Colors.text_dark,
                fontSize: Globals.screenWidth * 0.05,
              }}>
              Privacy Policy
            </Text>

            <Image
              source={require('../../../assets/next.png')}
              style={{resizeMode: 'contain', height: 25, width: 25}}
            />
          </Pressable>

          <Pressable
            style={{
              flexDirection: 'row',
              marginTop: Globals.mTop,
              alignSelf: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                flex: 1,
                fontFamily: 'urbanist_regular',
                color: Colors.text_dark,
                fontSize: Globals.screenWidth * 0.05,
              }}>
              Terms &amp; Conditions
            </Text>

            <Image
              source={require('../../../assets/next.png')}
              style={{resizeMode: 'contain', height: 25, width: 25}}
            />
          </Pressable>
        </View>

        <TouchableOpacity
          style={styles.imgLL}
          onPress={() => {
            this.handleBackButtonClick();
          }}>
          <Image
            style={styles.back}
            source={require('../../../assets/back2.png')}
          />
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

export default UserSettings;

const styles = StyleSheet.create({
  title: {
    fontFamily: 'urbanist_semi_bold',
    fontSize: 20,
    color: Colors.text_dark,
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
  },
  back: {
    width: 25,
    height: 25,
  },
  imgLL: {
    position: 'absolute',
    top: Globals.pHorizontal,
    left: Globals.pHorizontal,
  },
});
