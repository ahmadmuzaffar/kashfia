import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  SafeAreaView,
  FlatList,
  Image,
} from 'react-native';
import Colors from '../../../utils/Colors';
import Globals from '../../../utils/Globals';
import {Avatar} from 'react-native-elements';

export default class UserNotification extends Component {
  constructor(props) {
    super(props);
  }

  getNotifications() {
    return [
      {
        id: 0,
        avatar:
          'https://www.freepnglogos.com/uploads/doctor-png/doctor-align-with-lower-cost-and-increased-quality-alignment-21.png',
        name: 'Dr. Ching Yu Min',
        description: 'This is your prescribed medicine',
        file_name: 'Prescription.pdf',
        file_url: require('../../../assets/pdf.png'),
        file_type: 'pdf',
        type: 'prescription',
      },
      {
        id: 1,
        avatar: require('../../../assets/app_logo.png'),
        name: 'Payment Successful',
        description:
          'You have successfully paid 25,000 IQD for online consultation',
        type: 'payment',
      },
      {
        id: 2,
        avatar: require('../../../assets/app_logo.png'),
        name: 'Welcome to Kashfia',
        description: 'We hope you enjoy our services',
        type: 'info',
      },
    ];
  }

  Item({notificationObj}) {
    return (
      <View
        style={{
          flexDirection: 'row',
          marginVertical: '1%',
          paddingVertical: Globals.mTop / 2,
          paddingHorizontal: Globals.mTop / 1.7,
          backgroundColor: Colors.white,
        }}>
        <Avatar
          size="large"
          containerStyle={{
            height: Globals.screenWidth * 0.18,
            width: Globals.screenWidth * 0.18,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: Colors.white,
            borderWidth: 1,
            borderColor: Colors.light_grey,
          }}
          rounded
          source={
            typeof notificationObj.avatar === 'string'
              ? {uri: notificationObj.avatar}
              : notificationObj.avatar
          }
        />

        <View style={{flex: 1, paddingHorizontal: '2%', alignSelf: 'center'}}>
          <Text
            style={{
              fontFamily: 'urbanist_bold',
              fontSize: Globals.screenWidth * 0.045,
              color: Colors.text_dark,
              width: '100%',
              textAlign: 'left',
              alignSelf: 'center',
            }}>
            {notificationObj.name}
          </Text>

          <Text
            style={{
              fontFamily: 'urbanist_regular',
              fontSize: Globals.screenWidth * 0.035,
              color: Colors.text_light,
              marginTop: '1%',
              width: '100%',
              textAlign: 'left',
              alignSelf: 'center',
            }}>
            {notificationObj.description}
          </Text>

          {notificationObj.type === 'prescription' && (
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'baseline',
                borderRadius: 5,
                alignItems: 'center',
                backgroundColor: Colors.lighter_gery,
                paddingVertical: '2%',
                paddingHorizontal: '3%',
                marginTop: '1%',
              }}>
              <Image
                style={{
                  height: 15,
                  width: 15,
                  resizeMode: 'contain',
                  marginRight: '3%',
                }}
                source={
                  typeof notificationObj.file_url === 'string'
                    ? {uri: notificationObj.file_url}
                    : notificationObj.file_url
                }
              />

              <Text
                style={{
                  fontSize: Globals.screenWidth * 0.033,
                  fontFamily: 'urbanist_regular',
                  color: Colors.text_light,
                }}>
                {notificationObj.file_name}
              </Text>
            </View>
          )}
        </View>
      </View>
    );
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#E5E5E5'}}>
        <Text
          style={[
            styles.title,
            {
              padding: Globals.pHorizontal,
              backgroundColor: Colors.white,
            },
          ]}>
          Notifications
        </Text>

        <FlatList
          style={{
            marginVertical: '2%',
          }}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          data={this.getNotifications()}
          renderItem={({item}) => <this.Item notificationObj={item} />}
          keyExtractor={item => item.id}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontFamily: 'urbanist_bold',
    fontSize: Globals.screenWidth * 0.06,
    color: Colors.text_dark,
    width: '100%',
    textAlign: 'left',
    alignSelf: 'center',
  },
});
