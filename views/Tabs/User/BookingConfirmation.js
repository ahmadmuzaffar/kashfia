import React, {Component} from 'react';
import {
  BackHandler,
  Text,
  StyleSheet,
  View,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  Pressable,
  ScrollView,
  Image,
} from 'react-native';
import Colors from '../../../utils/Colors';
import CardView from 'react-native-cardview';
import Globals from '../../../utils/Globals';
import StarRating from 'react-native-star-rating';

export default class BookingConfirmation extends Component {
  state = {
    starCount: 0,
  };

  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.goBack();
    return true;
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating,
    });
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
        <StatusBar
          translucent={false}
          barStyle="dark-content"
          backgroundColor={Colors.white}
        />
        <View
          style={{
            flexDirection: 'row',
            padding: Globals.pHorizontal,
            borderBottomColor: Colors.lighter_gery,
            borderBottomWidth: 1,
          }}>
          <TouchableOpacity
            onPress={() => {
              this.handleBackButtonClick();
            }}>
            <Image
              style={styles.back}
              source={require('../../../assets/back2.png')}
            />
          </TouchableOpacity>

          <Text
            style={{
              fontFamily: 'urbanist_bold',
              fontSize: Globals.screenWidth * 0.05,
              color: Colors.text_dark,
              flex: 1,
              textAlign: 'center',
              alignSelf: 'center',
            }}>
            Booking Confirmation
          </Text>

          <TouchableOpacity
            onPress={() => {
              this.handleBackButtonClick();
            }}>
            <Image
              style={styles.back}
              source={require('../../../assets/share.png')}
            />
          </TouchableOpacity>
        </View>

        <ScrollView>
          <View
            style={{
              paddingHorizontal: Globals.pHorizontal,
            }}>
            {/* Tick Layout */}
            <View
              style={{
                flexDirection: 'row',
                marginTop: Globals.mTop,
                paddingVertical: Globals.pHorizontal,
              }}>
              <View
                style={{
                  backgroundColor: '#3CAF47',
                  height: Globals.screenWidth * 0.2,
                  width: Globals.screenWidth * 0.2,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: (Globals.screenWidth * 0.2) / 2,
                }}>
                <Image
                  style={{height: 25, width: 25, resizeMode: 'contain'}}
                  source={require('../../../assets/tick.png')}
                />
              </View>

              <View
                style={{flex: 1, paddingHorizontal: '2%', alignSelf: 'center'}}>
                <Text
                  style={{
                    fontFamily: 'urbanist_bold',
                    fontSize: Globals.screenWidth * 0.045,
                    color: Colors.text_dark,
                    width: '100%',
                    textAlign: 'left',
                    alignSelf: 'center',
                  }}>
                  Thank You!
                </Text>

                <Text
                  style={{
                    fontFamily: 'urbanist_regular',
                    fontSize: Globals.screenWidth * 0.04,
                    color: Colors.text_dark,
                    marginTop: '1%',
                    width: '100%',
                    textAlign: 'left',
                    alignSelf: 'center',
                  }}>
                  Your booking #BE12345 has been confirmed.
                </Text>
              </View>
            </View>

            {/* Sent an Email */}
            <Text
              style={{
                fontFamily: 'urbanist_regular',
                fontSize: Globals.screenWidth * 0.04,
                color: Colors.text_dark,
                marginTop: '1%',
                width: '100%',
                textAlign: 'left',
                alignSelf: 'center',
              }}>
              We sent an email to banuelson@mail.com with your booking
              confirmation and invoice.
            </Text>

            {/* Time Layout */}
            <View style={{flexDirection: 'row', marginTop: Globals.mTop}}>
              <Text
                style={{
                  fontFamily: 'urbanist_semi_bold',
                  fontSize: Globals.screenWidth * 0.04,
                  color: Colors.text_dark,
                  marginRight: '3%',
                  textAlign: 'left',
                  alignSelf: 'center',
                }}>
                Time placed:
              </Text>

              <Text
                style={{
                  fontFamily: 'urbanist_regular',
                  fontSize: Globals.screenWidth * 0.04,
                  color: Colors.text_dark,
                  textAlign: 'left',
                  alignSelf: 'center',
                }}>
                17/02/2020 12:45 CEST
              </Text>
            </View>

            {/* Booking Details */}
            <Text
              style={{
                fontFamily: 'urbanist_bold',
                fontSize: Globals.screenWidth * 0.05,
                marginTop: Globals.mTop,
                color: Colors.text_dark,
                width: '100%',
                textAlign: 'left',
                alignSelf: 'center',
              }}>
              Booking Details
            </Text>

            {/* Booking Layout */}
            <View style={{flexDirection: 'row', marginTop: Globals.mTop / 2}}>
              <Image
                style={{
                  height: Globals.screenWidth * 0.3,
                  width: Globals.screenWidth * 0.3,
                  resizeMode: 'contain',
                }}
                source={require('../../../assets/doc1.png')}
              />
              <View
                style={{
                  flex: 1,
                  marginLeft: '3%',
                  marginTop: Globals.mTop / 3,
                }}>
                <Text
                  style={{
                    fontFamily: 'urbanist_semi_bold',
                    fontSize: Globals.screenWidth * 0.05,
                    marginTop: 5,
                    color: Colors.text_dark,
                    textAlign: 'left',
                  }}>
                  Dr. Ching Ming Yu
                </Text>

                <Text
                  style={{
                    fontFamily: 'urbanist_semi_bold',
                    fontSize: Globals.screenWidth * 0.04,
                    marginTop: 5,
                    color: Colors.secondary,
                    textAlign: 'left',
                  }}>
                  Online Consultation
                </Text>

                <View style={{flexDirection: 'row', marginTop: 5}}>
                  <StarRating
                    disabled={false}
                    maxStars={5}
                    starSize={15}
                    halfStarColor={Colors.star}
                    fullStarColor={Colors.star}
                    emptyStarColor={'#E7E7E7'}
                    rating={4}
                    starStyle={{marginHorizontal: '1%'}}
                    selectedStar={rating => this.onStarRatingPress(rating)}
                  />
                  <Text
                    style={{
                      fontSize: 14,
                      fontFamily: 'urbanist_semi_bold',
                      marginLeft: 10,
                      color: Colors.text_light,
                    }}>
                    6 Reviews
                  </Text>
                </View>
              </View>
            </View>

            {/* Payment Details */}
            <Text
              style={{
                fontFamily: 'urbanist_bold',
                fontSize: Globals.screenWidth * 0.05,
                marginTop: Globals.mTop,
                color: Colors.text_dark,
                width: '100%',
                textAlign: 'left',
                alignSelf: 'center',
              }}>
              Payment Details:
            </Text>

            {/* Details */}
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: Globals.mTop / 1.5,
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    flex: 1,
                    fontFamily: 'urbanist_semi_bold',
                    fontSize: Globals.screenWidth * 0.04,
                    color: Colors.text_light,
                    textAlign: 'left',
                  }}>
                  Online Consultation
                </Text>

                <Text
                  style={{
                    flex: 1,
                    fontFamily: 'urbanist_semi_bold',
                    fontSize: Globals.screenWidth * 0.05,
                    color: Colors.text_dark,
                    textAlign: 'right',
                  }}>
                  18000 IQD
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  marginTop: Globals.mTop / 1.5,
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    flex: 1,
                    fontFamily: 'urbanist_semi_bold',
                    fontSize: Globals.screenWidth * 0.04,
                    color: Colors.text_light,
                    textAlign: 'left',
                  }}>
                  G.S.T
                </Text>

                <Text
                  style={{
                    flex: 1,
                    fontFamily: 'urbanist_semi_bold',
                    fontSize: Globals.screenWidth * 0.05,
                    color: Colors.text_dark,
                    textAlign: 'right',
                  }}>
                  0 IQD
                </Text>
              </View>
            </View>
          </View>

          {/* Total */}
          <View
            style={{
              flexDirection: 'row',
              marginTop: Globals.mTop * 1.5,
              alignItems: 'center',
              padding: Globals.mTop,
              backgroundColor: '#F6F6F6',
            }}>
            <Text
              style={{
                flex: 1,
                fontFamily: 'urbanist_semi_bold',
                fontSize: Globals.screenWidth * 0.04,
                color: Colors.text_dark,
                textAlign: 'left',
              }}>
              Total
            </Text>

            <Text
              style={{
                flex: 1,
                fontFamily: 'urbanist_semi_bold',
                fontSize: Globals.screenWidth * 0.06,
                color: Colors.text_dark,
                textAlign: 'right',
              }}>
              18000 IQD
            </Text>
          </View>

          <TouchableOpacity
            style={{
              alignSelf: 'stretch',
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: Colors.secondary,
              paddingVertical: Globals.mTop,
              marginVertical: Globals.mTop * 2,
              marginHorizontal: Globals.pHorizontal,
              borderRadius: 10,
            }}
            onPress={() => {
              this.props.navigation.navigate('chat_screen');
            }}>
            <Text
              style={{
                color: Colors.white,
                alignSelf: 'center',
                fontSize: 16,
                fontFamily: 'urbanist_bold',
              }}>
              Book Now
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  back: {
    width: 30,
    height: 30,
  },
});
