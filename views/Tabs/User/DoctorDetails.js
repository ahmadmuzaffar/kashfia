import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  StatusBar,
  SafeAreaView,
  PermissionsAndroid,
  TextInput,
  ImageBackground,
  Dimensions,
  ScrollView,
  Image,
  View,
} from 'react-native';
import Colors from '../../../utils/Colors';
import Globals from '../../../utils/Globals';
import CardView from 'react-native-cardview';
import Modal from 'react-native-modal';
import LinearGradient from 'react-native-linear-gradient';

export default class DoctorDetails extends Component {
  state = {
    isModalVisible: false,
  };

  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.goBack();
    return true;
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
        }}>
        <StatusBar translucent backgroundColor="transparent" />
        <LinearGradient
          colors={['#C4D8E8', '#E8F2FA']}
          style={{
            borderRadius: 10,
            padding: 10,
            alignItems: 'center',
          }}>
          <Image
            style={{
              height: (Globals.screenHeight * 0.4) / 1.3,
              width: Globals.screenWidth,
              marginTop: Globals.screenHeight * 0.1,
              resizeMode: 'contain',
            }}
            source={require('../../../assets/doc1_bg.png')}
          />
          <TouchableOpacity
            onPress={() => {
              this.handleBackButtonClick();
            }}
            style={{
              alignSelf: 'flex-start',
              position: 'absolute',
              marginLeft: Globals.screenWidth * 0.05,
              marginTop: Globals.screenHeight * 0.07,
            }}>
            <Image
              style={{
                height: Globals.screenWidth * 0.07,
                width: Globals.screenWidth * 0.07,
              }}
              source={require('../../../assets/back2.png')}
            />
          </TouchableOpacity>
        </LinearGradient>

        <View
          style={{
            flex: 1,
            backgroundColor: 'white',
            top: Globals.screenHeight * -0.07,
            borderTopLeftRadius: 30,
            borderTopRightRadius: 30,
            padding: Globals.pHorizontal + Globals.pHorizontal / 2.5,
          }}>
          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{paddingBottom: Globals.mTop}}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                style={{
                  flex: 1,
                  fontSize: Globals.screenWidth * 0.06,
                  color: Colors.text_dark,
                  fontFamily: 'urbanist_bold',
                }}>
                Dr. Ching Ming Yu
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  height: 20,
                }}>
                <View
                  style={{
                    backgroundColor: Colors.green,
                    height: 10,
                    width: 10,
                    borderRadius: 5,
                  }}
                />
                <Text
                  style={{
                    fontFamily: 'urbanist_regular',
                    color: Colors.green,
                    marginLeft: 5,
                  }}>
                  Online
                </Text>
              </View>
            </View>
            <Text
              style={{
                fontSize: 15,
                fontFamily: 'urbanist_semi_bold',
                color: Colors.secondary,
              }}>
              General Medicine
            </Text>

            <View
              style={{
                flexDirection: 'row',
                marginTop: 20,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 1,
                  padding: 10,
                  flex: 0.5,
                  marginRight: 5,
                  alignItems: 'center',
                  borderColor: Colors.light_grey,
                  borderRadius: 10,
                }}>
                <View style={{alignItems: 'center'}}>
                  <Text
                    style={{
                      color: Colors.secondary,
                      fontFamily: 'urbanist_regular',
                      fontSize: 12,
                    }}>
                    Online Consultation
                  </Text>
                  <View style={{flexDirection: 'row'}}>
                    <Text
                      style={{
                        color: Colors.black,
                        fontFamily: 'urbanist_bold',
                        fontSize: 20,
                      }}>
                      150,000
                    </Text>
                    <Text
                      style={{
                        color: Colors.black,
                        marginLeft: 5,
                        fontFamily: 'urbanist_bold',
                        alignSelf: 'flex-end',
                        fontSize: 14,
                      }}>
                      IQD
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  borderWidth: 1,
                  padding: 10,
                  marginLeft: 5,
                  flex: 0.5,
                  alignItems: 'center',
                  borderColor: Colors.light_grey,
                  borderRadius: 10,
                }}>
                <View style={{alignItems: 'center'}}>
                  <Text
                    style={{
                      color: Colors.home_consult,
                      fontFamily: 'urbanist_regular',
                      fontSize: 12,
                    }}>
                    Home Consultation
                  </Text>
                  <View style={{flexDirection: 'row'}}>
                    <Text
                      style={{
                        color: Colors.black,
                        fontFamily: 'urbanist_bold',
                        fontSize: 20,
                      }}>
                      240,000
                    </Text>
                    <Text
                      style={{
                        color: Colors.black,
                        marginLeft: 5,
                        fontFamily: 'urbanist_bold',
                        alignSelf: 'flex-end',
                        fontSize: 14,
                      }}>
                      IQD
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            <Text
              style={{
                fontSize: Globals.screenWidth * 0.04,
                marginTop: Globals.mTop,
                color: Colors.text_dark,
                fontFamily: 'urbanist_bold',
              }}>
              Biography
            </Text>
            <Text
              numberOfLines={4}
              ellipsizeMode="tail"
              style={{
                fontSize: Globals.screenWidth * 0.035,
                color: Colors.text_light,
                fontFamily: 'urbanist_regular',
              }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam erat
              vestibulum cursus eget eleifend maecenas aenean in aenean. Nunc
              dignissim enim, vitae tincidunt morbi massa sed...
            </Text>

            <View
              style={{
                flexDirection: 'row',
                marginTop: 20,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 1,
                  paddingVertical: 10,
                  paddingHorizontal: 7,
                  flex: 1,
                  alignItems: 'flex-start',
                  borderColor: Colors.light_grey,
                  borderRadius: 10,
                }}>
                <View>
                  <Text
                    style={{
                      color: Colors.text_light,
                      fontFamily: 'urbanist_regular',
                      fontSize: Globals.screenWidth * 0.03,
                    }}>
                    Online Patients
                  </Text>
                  <View style={{flexDirection: 'row', marginTop: 10}}>
                    <Image
                      style={{
                        height: 20,
                        width: 20,
                        resizeMode: 'contain',
                        marginRight: 5,
                      }}
                      source={require('../../../assets/online_patients.png')}
                    />
                    <Text
                      style={{
                        color: Colors.black,
                        fontFamily: 'urbanist_bold',
                        fontSize: Globals.screenWidth * 0.04,
                      }}>
                      200+
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  borderWidth: 1,
                  paddingVertical: 10,
                  paddingHorizontal: 7,
                  flex: 1,
                  marginLeft: 5,
                  marginRight: 5,
                  alignItems: 'flex-start',
                  borderColor: Colors.light_grey,
                  borderRadius: 10,
                }}>
                <View>
                  <Text
                    style={{
                      color: Colors.text_light,
                      fontFamily: 'urbanist_regular',
                      fontSize: Globals.screenWidth * 0.03,
                    }}>
                    Home Visits
                  </Text>
                  <View style={{flexDirection: 'row', marginTop: 10}}>
                    <Image
                      style={{
                        height: 20,
                        width: 20,
                        resizeMode: 'contain',
                        marginRight: 5,
                      }}
                      source={require('../../../assets/home_visits.png')}
                    />
                    <Text
                      style={{
                        color: Colors.black,
                        fontFamily: 'urbanist_bold',
                        fontSize: Globals.screenWidth * 0.04,
                      }}>
                      30+
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  borderWidth: 1,
                  paddingVertical: 10,
                  paddingHorizontal: 7,
                  flex: 1,
                  alignItems: 'flex-start',
                  borderColor: Colors.light_grey,
                  borderRadius: 10,
                }}>
                <View>
                  <Text
                    style={{
                      color: Colors.text_light,
                      fontFamily: 'urbanist_regular',
                      fontSize: Globals.screenWidth * 0.03,
                    }}>
                    Experience
                  </Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginTop: 10,
                      alignItems: 'center',
                    }}>
                    <Image
                      style={{
                        height: 20,
                        width: 20,
                        resizeMode: 'contain',
                        marginRight: 5,
                      }}
                      source={require('../../../assets/experience.png')}
                    />
                    <Text
                      style={{
                        color: Colors.black,
                        fontFamily: 'urbanist_bold',
                        fontSize: Globals.screenWidth * 0.04,
                      }}>
                      2 Years
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: Globals.mTop,
              }}>
              <Text
                style={{
                  fontSize: Globals.screenWidth * 0.04,
                  color: Colors.text_dark,
                  fontFamily: 'urbanist_bold',
                }}>
                Feedbacks
              </Text>
              <Image
                source={require('../../../assets/star.png')}
                style={{height: 15, width: 15, marginLeft: 10}}
              />
              <Text
                style={{
                  fontSize: Globals.screenWidth * 0.04,
                  marginLeft: 2,
                  color: Colors.text_dark,
                  fontFamily: 'urbanist_semi_bold',
                }}>
                4.8
              </Text>
              <Text
                style={{
                  flex: 1,
                  textAlign: 'right',
                  fontSize: Globals.screenWidth * 0.04,
                  color: Colors.secondary,
                  fontFamily: 'urbanist_semi_bold',
                }}>
                See all
              </Text>
            </View>

            <CardView
              style={{
                borderWidth: 1,
                borderColor: '#E9E9E9',
                backgroundColor: '#F8F8F8',
                padding: 15,
                marginTop: Globals.mTop,
              }}
              cardElevation={0}
              cardMaxElevation={0}
              cornerRadius={10}>
              <View style={{flexDirection: 'row'}}>
                <Text
                  style={{
                    flex: 1,
                    fontFamily: 'urbanist_semi_bold',
                    color: Colors.text_dark,
                    fontSize: Globals.screenWidth * 0.045,
                  }}>
                  Report an issue
                </Text>
                <Image
                  source={require('../../../assets/report.png')}
                  style={{height: 25, width: 25}}
                />
              </View>
            </CardView>
          </ScrollView>
        </View>

        {/* fixed positioned layout for user to book/save doctor  */}
        <View
          style={{
            position: 'absolute',
            bottom: 0,
            right: 0,
            borderTopColor: Colors.lightest_grey,
            borderTopWidth: 1,
            left: 0,
            alignItems: 'center',
            padding: Globals.pHorizontal - Globals.pHorizontal / 1.5,
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <CardView
              style={{
                backgroundColor: '#F4F4F4',
                height: 45,
                width: 45,
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: 10,
              }}
              cardElevation={0}
              cardMaxElevation={0}
              cornerRadius={7}>
              <Image
                style={{
                  height: 20,
                  width: 20,
                  resizeMode: 'contain',
                }}
                source={require('../../../assets/bookmark.png')}
              />
            </CardView>

            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                this.setState({
                  isModalVisible: true,
                });
              }}>
              <Text
                style={{
                  color: Colors.white,
                  alignSelf: 'center',
                  fontSize: 16,
                  fontFamily: 'urbanist_bold',
                }}>
                Book Now
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <Modal
          isVisible={this.state.isModalVisible}
          onRequestClose={function () {
            this.setState({
              isModalVisible: false,
            });
          }.bind(this)}>
          <View
            style={{
              backgroundColor: 'white',
              borderRadius: 15,
              alignItems: 'center',
              padding: Globals.pHorizontal,
            }}>
            <Text
              style={{
                fontFamily: 'urbanist_semi_bold',
                fontSize: Globals.screenWidth * 0.05,
                color: Colors.text_dark,
              }}>
              Select Booking Type
            </Text>

            <View
              style={{
                flexDirection: 'row',
                marginTop: 20,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 1,
                  padding: 10,
                  flex: 0.5,
                  marginRight: 5,
                  alignItems: 'center',
                  borderColor: Colors.light_grey,
                  borderRadius: 10,
                }}>
                <View style={{alignItems: 'center'}}>
                  <Text
                    style={{
                      color: Colors.secondary,
                      fontFamily: 'urbanist_regular',
                      fontSize: 12,
                    }}>
                    Online Consultation
                  </Text>
                  <View style={{flexDirection: 'row'}}>
                    <Text
                      style={{
                        color: Colors.black,
                        fontFamily: 'urbanist_bold',
                        fontSize: 20,
                      }}>
                      150,000
                    </Text>
                    <Text
                      style={{
                        color: Colors.black,
                        marginLeft: 5,
                        fontFamily: 'urbanist_bold',
                        alignSelf: 'flex-end',
                        fontSize: 14,
                      }}>
                      IQD
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  borderWidth: 1,
                  padding: 10,
                  marginLeft: 5,
                  flex: 0.5,
                  alignItems: 'center',
                  borderColor: Colors.light_grey,
                  borderRadius: 10,
                }}>
                <View style={{alignItems: 'center'}}>
                  <Text
                    style={{
                      color: Colors.home_consult,
                      fontFamily: 'urbanist_regular',
                      fontSize: 12,
                    }}>
                    Home Consultation
                  </Text>
                  <View style={{flexDirection: 'row'}}>
                    <Text
                      style={{
                        color: Colors.black,
                        fontFamily: 'urbanist_bold',
                        fontSize: 20,
                      }}>
                      240,000
                    </Text>
                    <Text
                      style={{
                        color: Colors.black,
                        marginLeft: 5,
                        fontFamily: 'urbanist_bold',
                        alignSelf: 'flex-end',
                        fontSize: 14,
                      }}>
                      IQD
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            <TouchableOpacity
              style={[
                styles.button,
                {width: '90%', flex: 0, marginTop: Globals.mTop},
              ]}
              onPress={() => {
                this.setState({
                  isModalVisible: false,
                });
                this.props.navigation.navigate('payment');
              }}>
              <Text
                style={{
                  color: Colors.white,
                  alignSelf: 'center',
                  fontSize: 16,
                  fontFamily: 'urbanist_bold',
                }}>
                Book Now
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
});
