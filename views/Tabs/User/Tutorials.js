import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  StatusBar,
  SafeAreaView,
  PermissionsAndroid,
  TextInput,
  ImageBackground,
  Dimensions,
  ScrollView,
  Image,
  View,
} from 'react-native';
import Colors from '../../../utils/Colors';
import Globals from '../../../utils/Globals';
import Tags from 'react-native-tags';
import SpecializationList from '../../../custom/views/SpecializationList';
import CardView from 'react-native-cardview';

export default class Tutorials extends Component {
  constructor(props) {
    super(props);
    this.screenHeight = Dimensions.get('screen').height;
    this.windowHeight = Dimensions.get('window').height;
    this.windowWidth = Dimensions.get('window').width;
    this.navbarHeight =
      this.screenHeight - this.windowHeight + StatusBar.currentHeight;
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.goBack();
    return true;
  }

  render() {
    const height = this.windowHeight;
    const width = this.windowWidth;

    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
        <View
          style={{
            flexDirection: 'row',
            paddingVertical: 20,
            paddingHorizontal: 20,
          }}>
          <TouchableOpacity
            onPress={() => {
              this.handleBackButtonClick();
            }}>
            <Image
              style={styles.back}
              source={require('../../../assets/back2.png')}
            />
          </TouchableOpacity>

          <Text style={styles.title}>Tutorials</Text>
        </View>

        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
          contentContainerStyle={{
            paddingBottom: Globals.pBottomScrollBar,
            paddingHorizontal: Globals.pHorizontal,
          }}
          showVerticalScrollIndicator={false}>
          <CardView
            style={{
              marginTop: Globals.cardInternalTop,
              backgroundColor: 'white',
            }}
            cardElevation={10}
            cardMaxElevation={10}
            cornerRadius={10}>
            <TouchableOpacity
              style={{
                alignSelf: 'stretch',
                borderRadius: 10,
                backgroundColor: Colors.white,
              }}>
              <Image
                style={{
                  height: Globals.tutorialsCardHeight,
                  width: Globals.screenWidth * 0.9,
                  resizeMode: 'cover',
                }}
                source={require('../../../assets/tutorial_1.png')}
              />
              <View style={{padding: Globals.pHorizontal}}>
                <Text
                  style={{
                    fontSize: Globals.screenWidth * 0.045,
                    fontFamily: 'urbanist_semi_bold',
                    color: Colors.text_dark,
                  }}>
                  How to book a doctor?
                </Text>
                <Text
                  numberOfLines={2}
                  ellipsizeMode="tail"
                  style={{
                    fontSize: Globals.screenWidth * 0.035,
                    fontFamily: 'urbanist_regular',
                    color: Colors.text_light,
                  }}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam
                  erat vestibulum cursus eget eleifend maecenas ...
                </Text>
              </View>
            </TouchableOpacity>
          </CardView>

          <CardView
            style={{
              marginTop: Globals.cardInternalTop,
              backgroundColor: 'white',
            }}
            cardElevation={10}
            cardMaxElevation={10}
            cornerRadius={10}>
            <TouchableOpacity
              style={{
                alignSelf: 'stretch',
                borderRadius: 10,
                backgroundColor: Colors.white,
              }}>
              <ImageBackground
                style={{
                  height: Globals.tutorialsCardHeight,
                  width: Globals.screenWidth * 0.9,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                source={require('../../../assets/tutorial_2.png')}
                resizeMode="cover">
                <Image
                  style={{
                    height: Globals.tutorialsCardHeight,
                    width: Globals.tutorialsCardHeight,
                    resizeMode: 'contain',
                  }}
                  source={require('../../../assets/tutorial_2_image.png')}
                />
              </ImageBackground>
              <View style={{padding: Globals.pHorizontal}}>
                <Text
                  style={{
                    fontSize: Globals.screenWidth * 0.045,
                    fontFamily: 'urbanist_semi_bold',
                    color: Colors.text_dark,
                  }}>
                  How to update account info?
                </Text>
                <Text
                  numberOfLines={2}
                  ellipsizeMode="tail"
                  style={{
                    fontSize: Globals.screenWidth * 0.035,
                    fontFamily: 'urbanist_regular',
                    color: Colors.text_light,
                  }}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam
                  erat vestibulum cursus eget eleifend maecenas ...
                </Text>
              </View>
            </TouchableOpacity>
          </CardView>

          <CardView
            style={{
              marginTop: Globals.cardInternalTop,
              backgroundColor: 'white',
            }}
            cardElevation={10}
            cardMaxElevation={10}
            cornerRadius={10}>
            <TouchableOpacity
              style={{
                alignSelf: 'stretch',
                borderRadius: 10,
                backgroundColor: Colors.white,
              }}>
              <Image
                style={{
                  height: Globals.tutorialsCardHeight,
                  width: Globals.screenWidth * 0.9,
                  resizeMode: 'cover',
                }}
                source={require('../../../assets/tutorial_3.png')}
              />
              <View style={{padding: Globals.pHorizontal}}>
                <Text
                  style={{
                    fontSize: Globals.screenWidth * 0.045,
                    fontFamily: 'urbanist_semi_bold',
                    color: Colors.text_dark,
                  }}>
                  Add new address?
                </Text>
                <Text
                  numberOfLines={2}
                  ellipsizeMode="tail"
                  style={{
                    fontSize: Globals.screenWidth * 0.035,
                    fontFamily: 'urbanist_regular',
                    color: Colors.text_light,
                  }}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam
                  erat vestibulum cursus eget eleifend maecenas ...
                </Text>
              </View>
            </TouchableOpacity>
          </CardView>

          <CardView
            style={{
              marginTop: Globals.cardInternalTop,
              backgroundColor: 'white',
            }}
            cardElevation={10}
            cardMaxElevation={10}
            cornerRadius={10}>
            <TouchableOpacity
              style={{
                alignSelf: 'stretch',
                borderRadius: 10,
                backgroundColor: Colors.white,
              }}>
              <Image
                style={{
                  height: Globals.tutorialsCardHeight,
                  width: Globals.screenWidth * 0.9,
                  resizeMode: 'cover',
                }}
                source={require('../../../assets/tutorial_4.png')}
              />
              <View style={{padding: Globals.pHorizontal}}>
                <Text
                  style={{
                    fontSize: Globals.screenWidth * 0.045,
                    fontFamily: 'urbanist_semi_bold',
                    color: Colors.text_dark,
                  }}>
                  How to make payments?
                </Text>
                <Text
                  numberOfLines={2}
                  ellipsizeMode="tail"
                  style={{
                    fontSize: Globals.screenWidth * 0.035,
                    fontFamily: 'urbanist_regular',
                    color: Colors.text_light,
                  }}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam
                  erat vestibulum cursus eget eleifend maecenas ...
                </Text>
              </View>
            </TouchableOpacity>
          </CardView>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontFamily: 'urbanist_semi_bold',
    fontSize: 20,
    color: Colors.text_dark,
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
  },
  back: {
    width: 30,
    height: 30,
  },
  description: {
    fontSize: 16,
    fontFamily: 'urbanist_regular',
    color: Colors.text_light,
  },
  button1: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  button2: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.disabled,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  label: {
    fontFamily: 'urbanist_bold',
    fontSize: 18,
    color: Colors.black,
  },
  textInput: {
    width: '100%',
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.light_grey,
    fontSize: 17,
    marginTop: 10,
    paddingHorizontal: 10,
    color: Colors.dark_grey,
    fontFamily: 'urbanist_light',
  },
  textInputFilled: {
    width: '100%',
    borderRadius: 10,
    borderWidth: 0.8,
    marginTop: 10,
    borderColor: Colors.secondary,
    fontSize: 17,
    paddingHorizontal: 10,
    color: Colors.dark_grey,
    fontFamily: 'urbanist_light',
  },
});
