import React, {Component} from 'react';
import {
  BackHandler,
  Text,
  StyleSheet,
  View,
  SafeAreaView,
  FlatList,
  StatusBar,
  TouchableOpacity,
  Pressable,
  TextInput,
  ScrollView,
  Image,
} from 'react-native';
import Colors from '../../../utils/Colors';
import Globals from '../../../utils/Globals';
import CardView from 'react-native-cardview';

export default class ChatScreen extends Component {
  state = {
    listData: [],
    msgText: '',
    refresh: false,
  };

  constructor(props) {
    super(props);

    this.msgRef = null;
    this.list = null;
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.goBack();
    return true;
  }

  Item({messageObj}) {
    return (
      <View
        style={{
          margin: '1%',
        }}>
        <CardView
          cardElevation={0}
          cardMaxElevation={0}
          cornerRadius={7}
          style={{
            padding: Globals.mTop / 1.3,
            alignSelf:
              messageObj.alignment === 'right' ? 'flex-end' : 'flex-start',
            maxWidth: '85%',
            backgroundColor:
              messageObj.alignment === 'right'
                ? Colors.lightest_grey
                : Colors.secondary,
          }}>
          <Text
            style={{
              fontFamily: 'urbanist_regular',
              fontSize: Globals.screenWidth * 0.035,
              color:
                messageObj.alignment === 'right' ? Colors.black : Colors.white,
            }}>
            {messageObj.msg}
          </Text>
        </CardView>
      </View>
    );
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
        <StatusBar
          translucent={false}
          barStyle="dark-content"
          backgroundColor={Colors.white}
        />

        {/* header */}
        <View
          style={{
            flexDirection: 'row',
            padding: Globals.pHorizontal,
            alignItems: 'center',
            borderBottomColor: Colors.lighter_gery,
            borderBottomWidth: 1,
          }}>
          <TouchableOpacity
            onPress={() => {
              this.handleBackButtonClick();
            }}>
            <Image
              style={styles.back}
              source={require('../../../assets/back2.png')}
            />
          </TouchableOpacity>

          <View
            style={{
              backgroundColor: Colors.white,
              height: Globals.screenWidth * 0.08,
              width: Globals.screenWidth * 0.08,
              borderRadius: (Globals.screenWidth * 0.08) / 2,
              borderWidth: 1,
              borderColor: Colors.light_grey,
              borderRadius: (Globals.screenWidth * 0.08) / 2,
              overflow: 'hidden',
            }}>
            <Image
              source={require('../../../assets/doc1_bg.png')}
              style={{
                height: Globals.screenWidth * 0.08,
                width: Globals.screenWidth * 0.08,
                resizeMode: 'center',
              }}
            />
          </View>

          <Text
            style={{
              fontFamily: 'urbanist_bold',
              fontSize: Globals.screenWidth * 0.045,
              color: Colors.text_dark,
              marginLeft: '1%',
              flex: 1,
            }}>
            Dr. Ching Ming Yu
          </Text>

          <TouchableOpacity
            onPress={() => {
              this.handleBackButtonClick();
            }}>
            <Image
              style={styles.back}
              source={require('../../../assets/audio.png')}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              this.handleBackButtonClick();
            }}>
            <Image
              style={styles.back}
              source={require('../../../assets/video.png')}
            />
          </TouchableOpacity>
        </View>

        {/* FlatList */}
        <FlatList
          ref={ref => (this.list = ref)}
          style={{
            padding: Globals.mTop / 1.8,
            marginBottom: Globals.screenHeight * 0.085,
            flex: 1,
          }}
          showsVerticalScrollIndicator={false}
          data={this.state.listData}
          extraData={this.state.refresh}
          renderItem={({item}) => <this.Item messageObj={item} />}
          keyExtractor={item => item.id}
        />

        {/* bottomLayout */}
        <View
          style={{
            width: '100%',
            height: Globals.screenHeight * 0.085,
            position: 'absolute',
            bottom: 0,
            alignItems: 'center',
            paddingHorizontal: Globals.pHorizontal / 2,
            flexDirection: 'row',
            borderTopColor: Colors.lightest_grey,
            borderTopWidth: 1,
          }}>
          {/* camera_btn */}
          <TouchableOpacity style={{marginHorizontal: '2%'}}>
            <Image
              source={require('../../../assets/camera.png')}
              style={{height: 22, width: 22, resizeMode: 'contain'}}
            />
          </TouchableOpacity>

          {/* gallery_btn */}
          <TouchableOpacity style={{marginHorizontal: '2%'}}>
            <Image
              source={require('../../../assets/gallery.png')}
              style={{height: 22, width: 22, resizeMode: 'contain'}}
            />
          </TouchableOpacity>

          {/* microphone_btn */}
          <TouchableOpacity style={{marginHorizontal: '2%'}}>
            <Image
              source={require('../../../assets/microphone.png')}
              style={{height: 22, width: 22, resizeMode: 'contain'}}
            />
          </TouchableOpacity>

          {/* message_et */}
          <TextInput
            ref={input => {
              this.msgRef = input;
            }}
            maxLength={300}
            multiline
            numberOfLines={5}
            style={{
              flex: 1,
              borderRadius: 40,
              paddingHorizontal: Globals.mTop,
              backgroundColor: Colors.lightest_grey,
              marginHorizontal: '2%',
              fontSize: Globals.screenWidth * 0.03,
              height: Globals.screenHeight * 0.06,
            }}
            value={this.state.msgText}
            onChangeText={msgText => this.setState({msgText})}
            placeholder={'Type message here...'}
          />

          {/* send_message_btn */}
          <TouchableOpacity
            style={{marginHorizontal: '2%'}}
            onPress={() => {
              let obj = {
                id: this.state.listData.length,
                msg: this.state.msgText,
                alignment:
                  this.state.listData.length % 2 === 0 ? 'right' : 'left',
              };
              this.state.listData.push(obj);
              this.setState({listData: this.state.listData, refresh: true});
              this.msgRef.clear();
              this.list.scrollToEnd({animated: true});
              console.log(JSON.stringify(this.state.listData));
            }}>
            <Image
              source={require('../../../assets/send_message.png')}
              style={{
                height: 22,
                width: 22,
                resizeMode: 'contain',
                tintColor: '#7B8293',
              }}
            />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  back: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginHorizontal: '2%',
  },
});
