import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  StatusBar,
  SafeAreaView,
  PermissionsAndroid,
  FlatList,
  Pressable,
  TextInput,
  Dimensions,
  ScrollView,
  Image,
  View,
} from 'react-native';
import Colors from '../../../utils/Colors';
import CardView from 'react-native-cardview';
import Globals from '../../../utils/Globals';
import Modal from 'react-native-modal';

export default class Payment extends Component {
  state = {
    cardHolder: '',
    cardNumber: '',
    expiry: '',
    cvv: '',

    addCardDialog: false,

    setLocationDialog: false,
    detectLocationColor: Colors.text_light,
    addLocationColor: Colors.text_light,

    disabled: true,
    firstIcon: -1,
    secondIcon: require('../../../assets/unselected_done.png'),

    listData: [],
    refresh: false,
  };

  constructor(props) {
    super(props);

    this.list = null;
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.navigate = this.navigate.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.goBack();
    return true;
  }

  navigate(screenName, params) {
    this.props.navigation.navigate(screenName, params);
  }

  Item({cardObj, firstIcon}) {
    return (
      <Pressable
        onPress={cardObj.clicker}
        style={{
          borderColor: '#F0F0F0',
          borderWidth: 1,
          borderRadius: 10,
          justifyContent: 'center',
          height: Globals.screenHeight * 0.088,
          paddingHorizontal: Globals.mTop,
          marginHorizontal: Globals.pHorizontal,
          marginBottom: Globals.mTop,
        }}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Image
            source={require('../../../assets/master_card.png')}
            style={{
              height: 24,
              width: 24,
              resizeMode: 'contain',
            }}
          />

          <View
            style={{
              flex: 1,
            }}>
            <Text
              style={{
                fontFamily: 'urbanist_regular',
                color: Colors.text_light,
                marginHorizontal: '5%',
                fontSize: Globals.screenWidth * 0.038,
              }}>
              {cardObj.holder_name}
            </Text>

            <Text
              style={{
                fontFamily: 'urbanist_bold',
                color: Colors.text_dark,
                marginHorizontal: '5%',
                fontSize: Globals.screenWidth * 0.038,
              }}>
              {cardObj.card_number}
            </Text>
          </View>

          <Image
            source={
              firstIcon === cardObj.id
                ? require('../../../assets/selected_done.png')
                : require('../../../assets/unselected_done.png')
            }
            style={{
              height: 20,
              width: 20,
              resizeMode: 'contain',
            }}
          />
        </View>
      </Pressable>
    );
  }

  render() {
    const {
      disabled,
      firstIcon,
      secondIcon,
      cardHolder,
      cardNumber,
      expiry,
      cvv,
      detectLocationColor,
      addLocationColor,
      listData,
    } = this.state;

    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
        <StatusBar
          translucent={false}
          barStyle="dark-content"
          backgroundColor={Colors.white}
        />
        <View
          style={{
            flexDirection: 'row',
            padding: Globals.pHorizontal,
            borderBottomColor: Colors.lighter_gery,
            borderBottomWidth: 1,
          }}>
          <TouchableOpacity
            onPress={() => {
              this.handleBackButtonClick();
            }}>
            <Image
              style={styles.back}
              source={require('../../../assets/back2.png')}
            />
          </TouchableOpacity>

          <Text style={styles.title}>Secure Payment</Text>
        </View>

        <Pressable
          onPress={() => {
            this.setState({
              addCardDialog: true,
            });
          }}
          style={{
            backgroundColor: '#F9F9F9',
            borderRadius: 10,
            height: Globals.screenHeight * 0.088,
            paddingHorizontal: Globals.mTop,
            marginHorizontal: Globals.pHorizontal,
            justifyContent: 'center',
            marginVertical: Globals.mTop,
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image
              source={require('../../../assets/card.png')}
              style={{
                height: 20,
                width: 20,
                resizeMode: 'contain',
              }}
            />

            <Text
              style={{
                flex: 1,
                fontFamily: 'urbanist_semi_bold',
                color: Colors.text_dark,
                marginHorizontal: '5%',
                fontSize: Globals.screenWidth * 0.045,
              }}>
              Add Credit / Debit Card
            </Text>

            <Image
              source={require('../../../assets/right_arrow.png')}
              style={{
                height: 14,
                width: 14,
                resizeMode: 'contain',
              }}
            />
          </View>
        </Pressable>

        {/* FlatList */}
        {listData.length > 0 && (
          <FlatList
            ref={ref => (this.list = ref)}
            style={{
              flexGrow: 0,
              //   padding: Globals.mTop / 1.8,
            }}
            showsVerticalScrollIndicator={false}
            data={listData}
            extraData={this.state.refresh}
            renderItem={({item}) => (
              <this.Item cardObj={item} firstIcon={firstIcon} />
            )}
            keyExtractor={item => item.id}
          />
        )}

        <Pressable
          onPress={() => {
            this.setState({
              disabled: false,
              firstIcon: -1,
              secondIcon: require('../../../assets/selected_done.png'),
            });
          }}
          style={{
            borderColor: '#F0F0F0',
            borderWidth: 1,
            borderRadius: 10,
            height: Globals.screenHeight * 0.088,
            paddingHorizontal: Globals.mTop,
            justifyContent: 'center',
            marginHorizontal: Globals.pHorizontal,
            marginBottom: Globals.mTop,
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <Image
                source={require('../../../assets/app_logo.png')}
                style={{
                  height: 20,
                  width: 20,
                  resizeMode: 'contain',
                }}
              />
              <Image
                source={require('../../../assets/logo_name.png')}
                style={{
                  height: 20,
                  width: 20,
                  resizeMode: 'contain',
                }}
              />
            </View>

            <View
              style={{
                flex: 1,
              }}>
              <Text
                style={{
                  fontFamily: 'urbanist_semi_bold',
                  color: Colors.text_dark,
                  marginHorizontal: '5%',
                  fontSize: Globals.screenWidth * 0.045,
                }}>
                Kashfia Pay
              </Text>

              <Text
                style={{
                  fontFamily: 'urbanist_regular',
                  color: Colors.text_dark,
                  marginHorizontal: '5%',
                  fontSize: Globals.screenWidth * 0.03,
                }}>
                IQD 540
              </Text>
            </View>

            <Image
              source={secondIcon}
              style={{
                height: 20,
                width: 20,
                resizeMode: 'contain',
              }}
            />
          </View>
        </Pressable>

        <View
          style={{
            borderColor: '#F0F0F0',
            borderWidth: 1,
            borderRadius: 10,
            height: Globals.screenHeight * 0.088,
            paddingHorizontal: Globals.mTop,
            justifyContent: 'center',
            marginHorizontal: Globals.pHorizontal,
            marginBottom: Globals.mTop,
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image
              source={require('../../../assets/zain_cash.png')}
              style={{
                height: 24,
                width: 24,
                resizeMode: 'contain',
              }}
            />

            <View
              style={{
                flex: 1,
              }}>
              <Text
                style={{
                  fontFamily: 'urbanist_semi_bold',
                  color: Colors.text_dark,
                  marginHorizontal: '5%',
                  fontSize: Globals.screenWidth * 0.045,
                }}>
                Zain Cash
              </Text>

              <Text
                style={{
                  fontFamily: 'urbanist_regular',
                  color: Colors.text_light,
                  marginHorizontal: '5%',
                  fontSize: Globals.screenWidth * 0.03,
                }}>
                Connect your zain cash account
              </Text>
            </View>

            <CardView
              style={{
                backgroundColor: '#E2EDFF',
                paddingHorizontal: Globals.mTop,
                paddingVertical: Globals.mTop / 1.9,
              }}
              cornerRadius={7}
              cardElevation={0}
              cardMaxElevation={0}>
              <Text
                style={{
                  color: Colors.secondary,
                  fontFamily: 'urbanist_semi_bold',
                }}>
                Connect
              </Text>
            </CardView>
          </View>
        </View>

        <CardView
          style={{
            bottom: 0,
            right: 0,
            left: 0,
            position: 'absolute',
          }}
          cardElevation={25}>
          <View style={{margin: Globals.pHorizontal}}>
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 0.5}}>
                <Text
                  style={{
                    fontFamily: 'urbanist_regular',
                    color: Colors.text_light,
                    fontSize: Globals.screenWidth * 0.035,
                  }}>
                  Total
                </Text>
                <Text
                  style={{
                    fontFamily: 'urbanist_bold',
                    color: Colors.text_dark,
                    fontSize: Globals.screenWidth * 0.065,
                  }}>
                  IQD 147.45
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    setLocationDialog: true,
                    addLocationColor: Colors.text_light,
                    detectLocationColor: Colors.text_light,
                  });
                  // this.props.navigation.navigate('booking_confirmation');
                }}
                disabled={disabled}
                style={{
                  flex: 0.5,
                  backgroundColor: !disabled
                    ? Colors.secondary
                    : Colors.disabledPayableBtn,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 7,
                }}>
                <Text
                  style={{
                    fontFamily: 'urbanist_semi_bold',
                    color: Colors.white,
                    fontSize: Globals.screenWidth * 0.05,
                  }}>
                  Pay Now
                </Text>
              </TouchableOpacity>
            </View>
            <Text
              style={{
                marginTop: Globals.pHorizontal,
                color: '#868E96',
                fontFamily: 'urbanist_regular',
                fontSize: Globals.screenWidth * 0.035,
              }}>
              This is the final step, after you touching Pay Now button, the
              payment will be processed.
            </Text>
          </View>
        </CardView>

        <Modal
          isVisible={this.state.addCardDialog}
          onRequestClose={function () {
            this.setState({
              cardHolder: '',
              cardNumber: '',
              expiry: '',
              cvv: '',
              addCardDialog: false,
            });
          }.bind(this)}>
          <View
            style={{
              backgroundColor: '#F9F9F9',
              borderRadius: 15,
              padding: Globals.pHorizontal,
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image
                source={require('../../../assets/card.png')}
                style={{
                  height: 25,
                  width: 25,
                  resizeMode: 'contain',
                }}
              />

              <Text
                style={{
                  flex: 1,
                  fontFamily: 'urbanist_semi_bold',
                  color: Colors.text_dark,
                  marginHorizontal: '5%',
                  fontSize: Globals.screenWidth * 0.045,
                }}>
                Add Credit / Debit Card
              </Text>

              <Pressable
                onPress={() => {
                  this.setState({
                    addCardDialog: false,
                  });
                }}>
                <Image
                  source={require('../../../assets/close.png')}
                  style={{
                    height: 25,
                    width: 25,
                    resizeMode: 'contain',
                  }}
                />
              </Pressable>
            </View>

            <View style={{marginTop: Globals.screenHeight * 0.05}} />

            <TextInput
              style={{
                borderRadius: 7,
                backgroundColor: Colors.white,
                fontFamily: 'urbanist_regular',
                padding: Globals.mTop / 1.7,
              }}
              placeholder={"Card Holder's Name"}
              placeholderTextColor={Colors.light_grey}
              maxLength={35}
              value={cardHolder}
              onChangeText={text =>
                this.setState({
                  cardHolder: text,
                })
              }
            />

            <TextInput
              style={{
                borderRadius: 7,
                marginTop: Globals.mTop / 1.7,
                backgroundColor: Colors.white,
                fontFamily: 'urbanist_regular',
                padding: Globals.mTop / 1.7,
              }}
              placeholder={'Card Number'}
              placeholderTextColor={Colors.light_grey}
              maxLength={16}
              keyboardType="numeric"
              value={cardNumber}
              onChangeText={text =>
                this.setState({
                  cardNumber: text,
                })
              }
            />

            <View style={{marginTop: Globals.mTop / 1.7}}>
              <View style={{flexDirection: 'row'}}>
                <Text
                  style={{
                    flex: 0.5,
                    marginRight: '2%',
                    fontSize: Globals.screenWidth * 0.04,
                    fontFamily: 'urbanist_bold',
                  }}>
                  Expiry Date
                </Text>
                <Text
                  style={{
                    flex: 0.5,
                    marginLeft: '2%',
                    fontSize: Globals.screenWidth * 0.04,
                    fontFamily: 'urbanist_bold',
                  }}>
                  CVV
                </Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <TextInput
                  style={{
                    borderRadius: 7,
                    marginTop: Globals.mTop / 1.7,
                    marginRight: '2%',
                    flex: 0.5,
                    backgroundColor: Colors.white,
                    fontFamily: 'urbanist_regular',
                    padding: Globals.mTop / 2,
                  }}
                  placeholder={'mm/yy'}
                  placeholderTextColor={Colors.light_grey}
                  maxLength={5}
                  keyboardType="numeric"
                  value={expiry}
                  onChangeText={text => {
                    this.setState({
                      expiry: text,
                    });
                  }}
                />

                <TextInput
                  style={{
                    borderRadius: 7,
                    marginTop: Globals.mTop / 2,
                    marginLeft: '2%',
                    flex: 0.5,
                    backgroundColor: Colors.white,
                    fontFamily: 'urbanist_regular',
                    padding: Globals.mTop / 1.7,
                  }}
                  placeholder={'...'}
                  placeholderTextColor={Colors.light_grey}
                  maxLength={3}
                  keyboardType="numeric"
                  value={cvv}
                  onChangeText={text =>
                    this.setState({
                      cvv: text,
                    })
                  }
                />
              </View>
            </View>

            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                let obj = {
                  id: listData.length,
                  holder_name: cardHolder,
                  card_number: cardNumber,
                  clicker: function () {
                    this.setState({
                      disabled: false,
                      firstIcon: listData.length,
                      secondIcon: require('../../../assets/unselected_done.png'),
                      listData,
                      refresh: true,
                    });
                  }.bind(this),
                };
                listData.push(obj);
                this.setState({
                  addCardDialog: false,
                  listData,
                });
              }}>
              <Text
                style={{
                  color: Colors.white,
                  alignSelf: 'center',
                  fontSize: 16,
                  fontFamily: 'urbanist_bold',
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>

        <Modal
          isVisible={this.state.setLocationDialog}
          onRequestClose={function () {
            this.setState({
              setLocationDialog: false,
            });
          }.bind(this)}>
          <View
            style={{
              backgroundColor: Colors.white,
              borderRadius: 15,
              paddingVertical: Globals.pHorizontal,
            }}>
            <Text
              style={{
                fontSize: Globals.screenWidth * 0.045,
                textAlign: 'center',
                color: Colors.text_dark,
                fontFamily: 'urbanist_semi_bold',
              }}>
              Select your location
            </Text>
            <View
              style={{
                paddingHorizontal: Globals.pHorizontal,
                marginTop: Globals.mTop,
              }}>
              <Pressable
                onPress={() => {
                  this.setState({
                    detectLocationColor: Colors.secondary,
                    addLocationColor: Colors.text_light,
                  });
                }}
                style={{
                  flexDirection: 'row',
                  padding: Globals.mTop / 1.5,
                  alignItems: 'center',
                }}>
                <Image
                  style={{
                    height: 18,
                    width: 18,
                    marginRight: '4%',
                    resizeMode: 'contain',
                    tintColor: detectLocationColor,
                  }}
                  source={require('../../../assets/location_detect.png')}
                />
                <Text
                  style={{
                    fontSize: Globals.screenWidth * 0.035,
                    color: detectLocationColor,
                    fontFamily: 'urbanist_semi_bold',
                  }}>
                  Detect my location
                </Text>
              </Pressable>

              <Pressable
                onPress={() => {
                  this.setState(
                    {
                      addLocationColor: Colors.secondary,
                      detectLocationColor: Colors.text_light,
                      setLocationDialog: false,
                    },
                    this.navigate('add_location', {}),
                  );
                }}
                style={{
                  flexDirection: 'row',
                  padding: Globals.mTop / 1.5,
                  alignItems: 'center',
                }}>
                <Image
                  style={{
                    height: 18,
                    width: 18,
                    marginRight: '4%',
                    resizeMode: 'contain',
                    tintColor: addLocationColor,
                  }}
                  source={require('../../../assets/add.png')}
                />
                <Text
                  style={{
                    fontSize: Globals.screenWidth * 0.035,
                    color: addLocationColor,
                    fontFamily: 'urbanist_semi_bold',
                  }}>
                  Add new location
                </Text>
              </Pressable>
            </View>

            {/* <View style={{backgroundColor: Colors.lighter_gery, height: 1}} /> */}
          </View>
        </Modal>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontFamily: 'urbanist_semi_bold',
    fontSize: 20,
    color: Colors.text_dark,
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
  },
  back: {
    width: 30,
    height: 30,
  },
  button: {
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: Globals.mTop,
    marginTop: Globals.mTop,
    borderRadius: 10,
  },
});
