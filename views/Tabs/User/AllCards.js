import {
  Text,
  StyleSheet,
  View,
  SafeAreaView,
  TextInput,
  BackHandler,
  FlatList,
  TouchableOpacity,
  Image,
  Pressable,
} from 'react-native';
import React, {Component} from 'react';
import Globals from '../../../utils/Globals';
import Colors from '../../../utils/Colors';
import CardView from 'react-native-cardview';
import SharedPreferences from 'react-native-shared-preferences';
import creditCardType from 'credit-card-type';
import Modal from 'react-native-modal';
import {TextInputMask, TextMask} from 'react-native-masked-text';
import axios from 'react-native-axios';
import {BASE_URL} from '@env';
import {ProgressBar} from 'react-native-material-indicators';
// import MKTextField from 'react-native-material-kit';

export default class AllCards extends Component {
  state = {
    listData: [],
    addCardModal: false,
    cardHolder: '',
    cardNumber: '',
    expiry: '',
    cvv: '',
    cardType: '',
    type: '',
    isEnabled: false,
    isEdit: false,
    selectedId: '',
    visible: false,
  };

  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.getCards = this.getCards.bind(this);
    this.addCard = this.addCard.bind(this);
    this.updateCard = this.updateCard.bind(this);
    this.deleteCard = this.deleteCard.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    this.setState({visible: true}, this.getCards);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.goBack();
    return true;
  }

  getCards() {
    axios
      .get(BASE_URL + '/cards', {
        headers: {
          Authorization: 'Bearer ' + Globals.user.accessToken,
        },
      })
      .then(response => {
        if (response.data.status) {
          this.setState(
            {listData: response.data.data, visible: false},
            this.dispatchResults,
          );
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  addCard(obj) {
    axios
      .post(BASE_URL + '/cards', obj, {
        headers: {
          Authorization: 'Bearer ' + Globals.user.accessToken,
        },
      })
      .then(response => {
        if (response.data.status) {
          this.state.listData.push(response.data.data);
          this.setState({
            addCardModal: false,
            cardHolder: '',
            cardNumber: '',
            expiry: '',
            cvv: '',
            cardType: '',
            type: '',
            isEnabled: false,
            listData: this.state.listData,
            visible: false,
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  updateCard(obj) {
    axios
      .patch(BASE_URL + '/cards/' + this.state.selectedId, obj, {
        headers: {
          Authorization: 'Bearer ' + Globals.user.accessToken,
        },
      })
      .then(response => {
        if (response.data.status) {
          const item = this.state.listData.find(
            item => item._id === this.state.selectedId,
          );
          item.number = obj.number;
          item.name = obj.name;
          item.cvv = obj.cvv;
          item.expiry = obj.expiry;
          item.cardType = obj.cardType;
          this.setState({
            addCardModal: false,
            cardHolder: '',
            cardNumber: '',
            expiry: '',
            cvv: '',
            cardType: '',
            type: '',
            isEnabled: false,
            isEdit: false,
            listData: this.state.listData,
            visible: false,
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  deleteCard(_id) {
    axios
      .delete(BASE_URL + '/cards/' + _id, {
        headers: {
          Authorization: 'Bearer ' + Globals.user.accessToken,
        },
      })
      .then(response => {
        if (response.data.status) {
          const filteredData = this.state.listData.filter(
            item => item._id !== _id,
          );
          this.setState(
            {listData: filteredData, visible: false},
            this.dispatchResults,
          );
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  dispatchResults() {
    Globals.cards = this.state.listData;
  }

  Item({cardObj, instance}) {
    let cardImg = '';
    if (cardObj.cardType === 'visa')
      cardImg = require('../../../assets/visa.png');
    else if (cardObj.cardType === 'maestro')
      cardImg = require('../../../assets/maestro.png');
    else if (cardObj.cardType === 'unionpay')
      cardImg = require('../../../assets/unionpay.png');
    else if (cardObj.cardType === 'jcb')
      cardImg = require('../../../assets/jcb.png');
    else if (cardObj.cardType === 'discover')
      cardImg = require('../../../assets/discover.png');
    else if (cardObj.cardType === 'mastercard')
      cardImg = require('../../../assets/master_card.png');
    else if (cardObj.cardType === 'american-express')
      cardImg = require('../../../assets/american_express.png');

    return (
      <Pressable
        style={{
          borderColor: '#F0F0F0',
          borderWidth: 1,
          borderRadius: 10,
          padding: Globals.mTop / 1.5,
          marginBottom: Globals.mTop,
        }}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          {cardImg && (
            <Image
              source={cardImg}
              style={{
                height: 40,
                width: 40,
                resizeMode: 'contain',
              }}
            />
          )}

          <View
            style={{
              flex: 1,
              marginLeft: '5%',
            }}>
            <Text
              style={{
                fontFamily: 'urbanist_bold',
                fontSize: Globals.screenWidth * 0.04,
              }}>
              {cardObj.name}
            </Text>

            <Text
              style={{
                fontFamily: 'urbanist_regular',
                fontSize: Globals.screenWidth * 0.035,
              }}>
              ⬤⬤⬤⬤ ⬤⬤⬤⬤ ⬤⬤⬤⬤{' '}
              {cardObj.number.substring(
                cardObj.number.length - 4,
                cardObj.number.length,
              )}
            </Text>

            {/* <TextMask
              value={cardObj.number}
              type={'credit-card'}
              options={{
                obfuscated: false,
              }}
            /> */}
          </View>

          <Pressable
            onPress={function () {
              instance.setState({
                addCardModal: true,
                isEdit: true,
                cardNumber: cardObj.number,
                cardHolder: cardObj.name,
                cvv: cardObj.cvv.toString(),
                expiry: cardObj.expiry,
                cardType: cardImg,
                type: cardObj.cardType,
                selectedId: cardObj._id,
                isEnabled: false,
              });
            }.bind(this)}>
            <Image
              source={require('../../../assets/typo_edit.png')}
              style={{resizeMode: 'contain', height: 22, width: 22}}
            />
          </Pressable>

          <Pressable
            style={{marginLeft: '3%'}}
            onPress={() => {
              instance.setState(
                {visible: true},
                instance.deleteCard(cardObj._id),
              );
            }}>
            <Image
              source={require('../../../assets/delete_2.png')}
              style={{resizeMode: 'contain', height: 22, width: 22}}
            />
          </Pressable>
        </View>
      </Pressable>
    );
  }

  checkCardType(text) {
    let temp = '';
    let {type, cardType, cardNumber} = this.state;
    const results = creditCardType(text);

    if (text.length === 0) {
      cardType = '';
      type = '';

      this.setState({
        cardType,
        type,
      });
    } else if (results.length === 1) {
      type = results[0].type;
      if (type === 'visa') cardType = require('../../../assets/visa.png');
      else if (type === 'maestro')
        cardType = require('../../../assets/maestro.png');
      else if (type === 'unionpay')
        cardType = require('../../../assets/unionpay.png');
      else if (type === 'jcb') cardType = require('../../../assets/jcb.png');
      else if (type === 'discover')
        cardType = require('../../../assets/discover.png');
      else if (type === 'mastercard')
        cardType = require('../../../assets/master_card.png');
      else if (type === 'american-express')
        cardType = require('../../../assets/american_express.png');

      this.setState({
        cardType,
        type,
      });
    }
    this.handleChange();
  }

  handleChange() {
    const {isEnabled, cardHolder, cardNumber, expiry, cvv} = this.state;
    if (
      cardHolder.trim().length > 0 &&
      cardNumber.trim().length > 0 &&
      expiry.length === 5 &&
      cvv.length >= 3
    ) {
      this.setState({isEnabled: true});
    } else {
      this.setState({isEnabled: false});
    }
  }

  render() {
    const {
      listData,
      addCardModal,
      cardHolder,
      cardNumber,
      expiry,
      cvv,
      cardType,
      type,
      isEnabled,
      isEdit,
    } = this.state;

    // const expiryTextField = MKTextField.textfield()
    //   .withPlaceholder('MM/YY')
    //   .withStyle({
    //     borderRadius: 7,
    //     marginTop: Globals.mTop / 2.5,
    //     marginRight: '2%',
    //     flex: 0.5,
    //     backgroundColor: Colors.white,
    //     fontFamily: 'urbanist_regular',
    //     padding: Globals.mTop / 2,
    //   })
    //   .build();

    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
        <ProgressBar
          size={48}
          determinate={false}
          thickness={4}
          visible={this.state.visible}
          color={Colors.primary}
        />

        <View
          style={{
            flexDirection: 'row',
            padding: Globals.pHorizontal,
            borderBottomColor: Colors.lighter_gery,
            borderBottomWidth: 1,
          }}>
          <Text style={styles.title}>Cards</Text>
        </View>

        <View style={{padding: Globals.pHorizontal}}>
          <Pressable
            style={{
              borderRadius: 10,
              flexDirection: 'row',
              backgroundColor: Colors.secondary,
              padding: Globals.mTop,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => {
              this.setState({
                addCardModal: true,
              });
            }}>
            <Text
              style={{
                fontFamily: 'urbanist_bold',
                flex: 1,
                color: Colors.white,
                fontSize: Globals.screenWidth * 0.04,
              }}>
              Add new card
            </Text>
            <Image
              source={require('../../../assets/next.png')}
              style={{
                resizeMode: 'contain',
                height: 25,
                width: 25,
                tintColor: Colors.white,
              }}
            />
          </Pressable>

          {listData.length > 0 && (
            <FlatList
              ref={ref => (this.list = ref)}
              style={{
                flexGrow: 0,
                marginTop: Globals.mTop,
              }}
              showsVerticalScrollIndicator={false}
              data={listData}
              extraData={this.state.refresh}
              renderItem={({item}) => (
                <this.Item cardObj={item} instance={this} />
              )}
              keyExtractor={(item, index) => item._id}
            />
          )}
        </View>

        <TouchableOpacity
          style={styles.imgLL}
          onPress={() => {
            this.handleBackButtonClick();
          }}>
          <Image
            style={styles.back}
            source={require('../../../assets/back2.png')}
          />
        </TouchableOpacity>

        <Modal
          isVisible={addCardModal}
          onRequestClose={function () {
            this.setState({
              cardHolder: '',
              cardNumber: '',
              expiry: '',
              cvv: '',
              cardType: '',
              type: '',
              isEnabled: false,
              isEdit: false,
              addCardModal: false,
            });
          }.bind(this)}>
          <View
            style={{
              backgroundColor: '#F9F9F9',
              borderRadius: 15,
              padding: Globals.pHorizontal,
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image
                source={require('../../../assets/card.png')}
                style={{
                  height: 25,
                  width: 25,
                  resizeMode: 'contain',
                }}
              />

              <Text
                style={{
                  flex: 1,
                  fontFamily: 'urbanist_semi_bold',
                  color: Colors.text_dark,
                  marginHorizontal: '5%',
                  fontSize: Globals.screenWidth * 0.045,
                }}>
                Add Credit / Debit Card
              </Text>

              <Pressable
                onPress={() => {
                  this.setState({
                    addCardModal: false,
                  });
                }}>
                <Image
                  source={require('../../../assets/close.png')}
                  style={{
                    height: 25,
                    width: 25,
                    resizeMode: 'contain',
                  }}
                />
              </Pressable>
            </View>

            <View style={{marginTop: Globals.screenHeight * 0.05}} />

            <Text
              style={{
                fontSize: Globals.screenWidth * 0.04,
                fontFamily: 'urbanist_bold',
                color: Colors.text_dark,
              }}>
              Card Holder's Name
            </Text>
            <TextInput
              style={{
                backgroundColor: Colors.white,
                padding: Globals.mTop / 1.7,
                marginTop: Globals.mTop / 2.5,
                borderRadius: 7,
                fontFamily: 'urbanist_regular',
              }}
              maxLength={35}
              value={cardHolder}
              onChangeText={text =>
                this.setState(
                  {
                    cardHolder: text,
                  },
                  this.handleChange,
                )
              }
            />

            <Text
              style={{
                fontSize: Globals.screenWidth * 0.04,
                fontFamily: 'urbanist_bold',
                color: Colors.text_dark,
                marginTop: Globals.mTop / 1.7,
              }}>
              Card Number
            </Text>
            <View
              style={{
                flexDirection: 'row',
                backgroundColor: Colors.white,
                paddingHorizontal: Globals.mTop / 1.7,
                marginTop: Globals.mTop / 2.5,
                borderRadius: 7,
                alignItems: 'center',
              }}>
              <TextInputMask
                type={'credit-card'}
                style={{
                  flex: 1,
                  fontFamily: 'urbanist_regular',
                }}
                options={{
                  obfuscated: false,
                  issuer: 'visa-or-mastercard',
                }}
                value={cardNumber}
                onChangeText={text => {
                  this.setState(
                    {
                      cardNumber: text,
                    },
                    this.checkCardType(text),
                  );
                }}
              />
              {cardType !== '' && (
                <Image
                  source={cardType}
                  style={{resizeMode: 'contain', height: 25, width: 35}}
                />
              )}
            </View>

            <View style={{marginTop: Globals.mTop / 1.7}}>
              <View style={{flexDirection: 'row'}}>
                <Text
                  style={{
                    flex: 0.5,
                    marginRight: '2%',
                    fontSize: Globals.screenWidth * 0.04,
                    fontFamily: 'urbanist_bold',
                  }}>
                  Expiry Date
                </Text>
                <Text
                  style={{
                    flex: 0.5,
                    marginLeft: '2%',
                    fontSize: Globals.screenWidth * 0.04,
                    fontFamily: 'urbanist_bold',
                  }}>
                  CVV
                </Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <TextInputMask
                  type={'datetime'}
                  style={{
                    borderRadius: 7,
                    marginTop: Globals.mTop / 2.5,
                    marginRight: '2%',
                    flex: 0.5,
                    backgroundColor: Colors.white,
                    fontFamily: 'urbanist_regular',
                    padding: Globals.mTop / 2,
                  }}
                  options={{
                    format: 'MM/YY',
                  }}
                  // customTextInput={expiryTextField}
                  maxLength={5}
                  value={expiry}
                  onChangeText={text => {
                    this.setState(
                      {
                        expiry: text,
                      },
                      this.handleChange,
                    );
                  }}
                />

                <TextInput
                  style={{
                    borderRadius: 7,
                    marginTop: Globals.mTop / 2.5,
                    marginLeft: '2%',
                    flex: 0.5,
                    backgroundColor: Colors.white,
                    fontFamily: 'urbanist_regular',
                    padding: Globals.mTop / 1.7,
                  }}
                  placeholder={'...'}
                  placeholderTextColor={Colors.light_grey}
                  maxLength={4}
                  keyboardType="numeric"
                  value={cvv}
                  onChangeText={text =>
                    this.setState(
                      {
                        cvv: text,
                      },
                      this.handleChange,
                    )
                  }
                />
              </View>
            </View>

            <TouchableOpacity
              style={
                isEnabled
                  ? {
                      alignSelf: 'stretch',
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: Colors.secondary,
                      paddingVertical: Globals.mTop / 1.3,
                      marginTop: Globals.mTop,
                      borderRadius: 10,
                    }
                  : {
                      alignSelf: 'stretch',
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: Colors.disabled,
                      paddingVertical: Globals.mTop / 1.3,
                      marginTop: Globals.mTop,
                      borderRadius: 10,
                    }
              }
              disabled={!isEnabled}
              onPress={() => {
                let obj = {
                  name: cardHolder,
                  number: cardNumber,
                  cvv: cvv,
                  cardType: type,
                  expiry: expiry,
                };
                if (isEdit)
                  this.setState({visible: true}, this.updateCard(obj));
                else this.setState({visible: true}, this.addCard(obj));
              }}>
              <Text
                style={{
                  color: Colors.white,
                  alignSelf: 'center',
                  fontSize: 16,
                  fontFamily: 'urbanist_bold',
                }}>
                {isEdit ? 'Update Card' : 'Add Card'}
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontFamily: 'urbanist_semi_bold',
    fontSize: 20,
    color: Colors.text_dark,
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
  },
  back: {
    width: 25,
    height: 25,
  },
  imgLL: {
    position: 'absolute',
    top: Globals.pHorizontal,
    left: Globals.pHorizontal,
  },
});
