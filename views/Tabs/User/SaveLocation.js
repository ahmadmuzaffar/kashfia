import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  StatusBar,
  Keyboard,
  SafeAreaView,
  ToastAndroid,
  PermissionsAndroid,
  Pressable,
  TextInput,
  Dimensions,
  ScrollView,
  Image,
  View,
} from 'react-native';
import Colors from '../../../utils/Colors';
import Globals from '../../../utils/Globals';
import CardView from 'react-native-cardview';
import SharedPreferences from 'react-native-shared-preferences';
import Location from '../../../models/Location';
import axios from 'react-native-axios';
import {BASE_URL} from '@env';
import {ProgressBar} from 'react-native-material-indicators';

export default class SaveLocation extends Component {
  state = {
    street: '',
    location: '',
    building: '',
    apartmentNumber: '',
    nearByLandmark: '',
    contactNumber: '',
    instructions: '',
    isKeyboardOpen: false,
    locations: Globals.locations,
    isEnabled: false,
    visible: false,
    selectedId: '',
  };

  constructor(props) {
    super(props);

    console.log(props);
    this.uri = props.route.params.uri;
    this.address = props.route.params.address;
    this.country = props.route.params.country;
    this.addLocation = this.addLocation.bind(this);
    this.updateLocation = this.updateLocation.bind(this);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.keyboardDidShow = this.keyboardDidShow.bind(this);
    this.keyboardDidHide = this.keyboardDidHide.bind(this);
  }

  keyboardDidShow() {
    this.setState({
      isKeyboardOpen: true,
    });
  }

  keyboardDidHide() {
    this.setState({
      isKeyboardOpen: false,
    });
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    this.keyboardOpenListener = Keyboard.addListener(
      'keyboardDidShow',
      this.keyboardDidShow,
    );
    this.keyboardHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this.keyboardDidHide,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    this.keyboardOpenListener.remove();
    this.keyboardHideListener.remove();
  }

  handleBackButtonClick() {
    // this.props.navigation.navigate('all_locations', {
    //   success: success !== undefined ? success : false,
    // });
    this.props.navigation.goBack();
    return true;
  }

  addLocation(obj) {
    axios
      .post(BASE_URL + '/locations', obj, {
        headers: {
          Authorization: 'Bearer ' + Globals.user.accessToken,
        },
      })
      .then(response => {
        if (response.data.status) {
          this.state.locations.push(response.data.data);
          this.setState({visible: false}, this.handleBackButtonClick);
        }
      })
      .catch(error => {
        this.setState({visible: false});
        console.log(error);
      });
  }

  updateLocation(obj) {
    axios
      .patch(BASE_URL + '/locations/' + this.state.selectedId, obj, {
        headers: {
          Authorization: 'Bearer ' + Globals.user.accessToken,
        },
      })
      .then(response => {
        if (response.data.status) {
          const item = this.state.locations.find(
            item => item._id === this.state.selectedId,
          );
          item.name = obj.name;
          item.address = obj.address;
          item.lat = this.lat;
          item.lng = this.lng;
          item.street = obj.street;
          item.building = obj.building;
          item.apartment = obj.apartment;
          item.nearbyLandmark = obj.nearbyLandmark;
          item.instructions = obj.instructions;
          item.contactNumber = obj.contactNumber;
          item.image = this.uri;
          this.setState({
            isEnabled: false,
            locations: this.state.locations,
            visible: false,
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  dispatchResults() {
    Globals.locations = this.state.locations;
  }

  handleChange() {
    if (
      this.uri !== '' &&
      this.state.location.trim().length > 0 &&
      this.state.street.trim().length > 0 &&
      this.state.building.trim().length > 0 &&
      this.state.apartmentNumber.trim().length > 0 &&
      this.state.nearByLandmark.trim().length > 0 &&
      this.state.contactNumber.trim().length > 0 &&
      this.state.instructions.trim().length > 0
    ) {
      this.setState({isEnabled: true});
    } else {
      this.setState({isEnabled: false});
    }
  }

  render() {
    const {
      locations,
      location,
      street,
      building,
      apartmentNumber,
      nearByLandmark,
      contactNumber,
      instructions,
      isEnabled,
    } = this.state;

    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
        <StatusBar
          translucent={false}
          barStyle="dark-content"
          backgroundColor={Colors.white}
        />

        <ProgressBar
          size={48}
          determinate={false}
          thickness={4}
          visible={this.state.visible}
          color={Colors.primary}
        />

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            height: Globals.headerHeight,
            paddingHorizontal: Globals.pHorizontal,
            borderBottomColor: Colors.lighter_gery,
            borderBottomWidth: 1,
          }}>
          <TouchableOpacity
            onPress={() => {
              this.handleBackButtonClick();
            }}>
            <Image
              style={styles.back}
              source={require('../../../assets/back2.png')}
            />
          </TouchableOpacity>

          <Text style={styles.title}>Add Location</Text>
        </View>

        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
          showVerticalScrollIndicator={false}>
          <View
            style={{
              flex: 1,
            }}>
            <Image
              style={{
                height: Globals.screenHeight * 0.3,
                width: Globals.screenWidth,
              }}
              source={{uri: this.uri}}
            />

            <View style={{padding: Globals.mTop}}>
              <CardView
                style={{
                  padding: Globals.mTop / 1.5,
                }}
                cornerRadius={10}
                cardElevation={10}
                carMaxElevaton={10}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      resizeMode: 'contain',
                      marginHorizontal: '1%',
                    }}
                    source={require('../../../assets/location.png')}
                  />
                  <View style={{flex: 1, marginHorizontal: '1%'}}>
                    <Text
                      style={{
                        fontFamily: 'urbanist_bold',
                        fontSize: Globals.screenWidth * 0.04,
                      }}>
                      {this.address}
                    </Text>
                    {/* <Text>{this.country}</Text> */}
                  </View>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      resizeMode: 'contain',
                      marginHorizontal: '1%',
                    }}
                    source={require('../../../assets/edit.png')}
                  />
                </View>
              </CardView>

              {/* location_title */}
              <Text
                style={{
                  color: Colors.text_dark,
                  fontFamily: 'urbanist_bold',
                  marginTop: Globals.mTop,
                  fontSize: Globals.screenWidth * 0.045,
                }}>
                Location Title
              </Text>
              <TextInput
                style={{
                  borderColor: Colors.light_grey,
                  borderRadius: 7,
                  borderWidth: 1,
                  padding: Globals.mTop / 1.7,
                  fontFamily: 'urbanist_regular',
                  fontSize: Globals.screenWidth * 0.036,
                  color: Colors.text_dark,
                }}
                value={location}
                onChangeText={text => {
                  this.setState(
                    {
                      location: text,
                    },
                    this.handleChange,
                  );
                }}
              />

              {/* street */}
              <Text
                style={{
                  color: Colors.text_dark,
                  fontFamily: 'urbanist_bold',
                  marginTop: Globals.mTop,
                  fontSize: Globals.screenWidth * 0.045,
                }}>
                Street
              </Text>
              <TextInput
                style={{
                  borderColor: Colors.light_grey,
                  borderRadius: 7,
                  borderWidth: 1,
                  padding: Globals.mTop / 1.7,
                  fontFamily: 'urbanist_regular',
                  fontSize: Globals.screenWidth * 0.036,
                  color: Colors.text_dark,
                }}
                value={street}
                onChangeText={text => {
                  this.setState(
                    {
                      street: text,
                    },
                    this.handleChange,
                  );
                }}
              />

              {/* building */}
              <Text
                style={{
                  color: Colors.text_dark,
                  fontFamily: 'urbanist_bold',
                  marginTop: Globals.mTop,
                  fontSize: Globals.screenWidth * 0.045,
                }}>
                Building
              </Text>
              <TextInput
                style={{
                  borderColor: Colors.light_grey,
                  borderRadius: 7,
                  borderWidth: 1,
                  padding: Globals.mTop / 1.7,
                  fontFamily: 'urbanist_regular',
                  fontSize: Globals.screenWidth * 0.036,
                  color: Colors.text_dark,
                }}
                value={building}
                onChangeText={text => {
                  this.setState(
                    {
                      building: text,
                    },
                    this.handleChange,
                  );
                }}
              />

              {/* apartment# */}
              <Text
                style={{
                  color: Colors.text_dark,
                  fontFamily: 'urbanist_bold',
                  marginTop: Globals.mTop,
                  fontSize: Globals.screenWidth * 0.045,
                }}>
                Apartment#
              </Text>
              <TextInput
                style={{
                  borderColor: Colors.light_grey,
                  borderRadius: 7,
                  borderWidth: 1,
                  padding: Globals.mTop / 1.7,
                  fontFamily: 'urbanist_regular',
                  fontSize: Globals.screenWidth * 0.036,
                  color: Colors.text_dark,
                }}
                value={apartmentNumber}
                onChangeText={text => {
                  this.setState(
                    {
                      apartmentNumber: text,
                    },
                    this.handleChange,
                  );
                }}
              />

              {/* nearby_landmark */}
              <Text
                style={{
                  color: Colors.text_dark,
                  fontFamily: 'urbanist_bold',
                  marginTop: Globals.mTop,
                  fontSize: Globals.screenWidth * 0.045,
                }}>
                Nearby Landmark
              </Text>
              <TextInput
                style={{
                  borderColor: Colors.light_grey,
                  borderRadius: 7,
                  borderWidth: 1,
                  padding: Globals.mTop / 1.7,
                  fontFamily: 'urbanist_regular',
                  fontSize: Globals.screenWidth * 0.036,
                  color: Colors.text_dark,
                }}
                value={nearByLandmark}
                onChangeText={text => {
                  this.setState(
                    {
                      nearByLandmark: text,
                    },
                    this.handleChange,
                  );
                }}
              />

              {/* contact# */}
              <Text
                style={{
                  color: Colors.text_dark,
                  fontFamily: 'urbanist_bold',
                  marginTop: Globals.mTop,
                  fontSize: Globals.screenWidth * 0.045,
                }}>
                Contact#
              </Text>
              <TextInput
                style={{
                  borderColor: Colors.light_grey,
                  borderRadius: 7,
                  borderWidth: 1,
                  padding: Globals.mTop / 1.7,
                  fontFamily: 'urbanist_regular',
                  fontSize: Globals.screenWidth * 0.036,
                  color: Colors.text_dark,
                }}
                keyboardType="numeric"
                maxLength={13}
                value={contactNumber}
                onChangeText={text => {
                  this.setState(
                    {
                      contactNumber: text,
                    },
                    this.handleChange,
                  );
                }}
              />

              {/* instructions */}
              <Text
                style={{
                  color: Colors.text_dark,
                  fontFamily: 'urbanist_bold',
                  marginTop: Globals.mTop,
                  fontSize: Globals.screenWidth * 0.045,
                }}>
                Instructions
              </Text>
              <TextInput
                style={{
                  borderColor: Colors.light_grey,
                  borderRadius: 7,
                  borderWidth: 1,
                  padding: Globals.mTop / 1.7,
                  fontFamily: 'urbanist_regular',
                  fontSize: Globals.screenWidth * 0.036,
                  color: Colors.text_dark,
                  textAlignVertical: 'top',
                  height: 120,
                }}
                maxLength={150}
                multiline={true}
                numberOfLines={10}
                value={instructions}
                onChangeText={text => {
                  this.setState(
                    {
                      instructions: text,
                    },
                    this.handleChange,
                  );
                }}
              />

              <TouchableOpacity
                disabled={!isEnabled}
                style={
                  isEnabled
                    ? {
                        alignSelf: 'stretch',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: Colors.secondary,
                        paddingVertical: Globals.mTop,
                        marginTop: Globals.mTop,
                        borderRadius: 10,
                      }
                    : {
                        alignSelf: 'stretch',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: Colors.secondary,
                        paddingVertical: Globals.mTop,
                        marginTop: Globals.mTop,
                        borderRadius: 10,
                      }
                }
                onPress={() => {
                  let obj = {
                    address: this.address,
                    name: location,
                    street: street,
                    building: building,
                    apartment: apartmentNumber,
                    nearbyLandmark: nearByLandmark,
                    contactNumber: contactNumber,
                    instructions: instructions,
                    image: this.uri,
                  };
                  this.setState({visible: true}, this.addLocation(obj));
                }}>
                <Text
                  style={{
                    color: Colors.white,
                    alignSelf: 'center',
                    fontSize: 16,
                    fontFamily: 'urbanist_bold',
                  }}>
                  Save and Continue
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontFamily: 'urbanist_semi_bold',
    fontSize: 20,
    color: Colors.text_dark,
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
  },
  back: {
    width: 30,
    height: 30,
  },
});
