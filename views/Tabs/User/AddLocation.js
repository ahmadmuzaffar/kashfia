import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  StatusBar,
  SafeAreaView,
  ToastAndroid,
  PermissionsAndroid,
  Pressable,
  TextInput,
  Dimensions,
  ScrollView,
  Image,
  View,
} from 'react-native';
import Colors from '../../../utils/Colors';
import CardView from 'react-native-cardview';
import Globals from '../../../utils/Globals';
import Modal from 'react-native-modal';
import MapView, {PROVIDER_GOOGLE, Marker, MAP_TYPES} from 'react-native-maps';
import Geocoder from 'react-native-geocoding';
import GetLocation from 'react-native-get-location';
import {MAPS_API_KEY} from '@env';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import mapStyle from '../../../utils/mapStyle';
import {StackActions} from '@react-navigation/native';

export default class AddLocation extends Component {
  state = {
    region: {
      latitude: this.LATITUDE,
      longitude: this.LONGITUDE,
      latitudeDelta: this.LATITUDE_DELTA,
      longitudeDelta: this.LONGITUDE_DELTA,
    },
    address: '',
    country: '',
  };

  constructor(props) {
    super(props);

    console.log(props);
    Geocoder.init(MAPS_API_KEY);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.onRegionChange = this.onRegionChange.bind(this);

    this.ASPECT_RATIO =
      Globals.screenWidth /
      (Globals.screenHeight -
        (Globals.headerHeight + Globals.googlePlacesLayoutHeight));
    this.LATITUDE = 37.78825;
    this.LONGITUDE = -122.4324;
    this.LATITUDE_DELTA = 0.0922;
    this.LONGITUDE_DELTA = this.LATITUDE_DELTA * this.ASPECT_RATIO;

    console.log(this.LATITUDE_DELTA, this.LONGITUDE_DELTA);

    this.requestLocationPermission = async () => {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
          {
            title: 'Kashfia App Location Permission',
            message: 'Kashfia App needs access to your location ',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000,
          })
            .then(location => {
              // console.log(location);
              this.callDelta(location.latitude, location.longitude);
            })
            .catch(error => {
              const {code, message} = error;
              if (code === 'UNAVAILABLE') {
                ToastAndroid.showWithGravityAndOffset(
                  'Kindly turn on your location to detect current location',
                  ToastAndroid.LONG,
                  ToastAndroid.BOTTOM,
                  25,
                  50,
                );
                this.callDelta(33.3152, 44.3661);
              }
            });
        } else {
          console.log('Permission denied');
        }
      } catch (err) {
        console.warn(err);
      }
    };
  }

  async callDelta(lat, long) {
    Geocoder.from(lat, long)
      .then(json => {
        var addressComponent = json.results[0].formatted_address;
        var bounds = json.results[0].geometry.viewport;
        const northeastLat = bounds.northeast.lat;
        const southwestLat = bounds.southwest.lat;
        const latDelta = northeastLat - southwestLat;
        const lngDelta = latDelta * this.ASPECT_RATIO;

        this.setState({address: addressComponent});
        // this.searchBar.setAddressText(addressComponent);
        this.setState({
          region: {
            latitude: lat,
            longitude: long,
            latitudeDelta: latDelta,
            longitudeDelta: lngDelta,
          },
        });
        this.map.animateCamera({
          center: {
            latitude: lat,
            longitude: long,
          },
        });
      })
      .catch(error => console.warn(error));
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );

    this.requestLocationPermission();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.goBack();
    return true;
  }

  onRegionChange(region) {
    this.setState({region});
  }

  takeLocationSnapshot() {
    const {region} = this.state;
    const {navigate, dispatch} = this.props.navigation;

    const snapshot = this.map.takeSnapshot(300, 300, {
      // width: Globals.screenWidth, // optional, when omitted the view-width is used
      // height: Globals.screenHeight * 0.28, // optional, when omitted the view-height is used
      // region: region,
      latitude: region.latitude,
      longitude: region.longitude,
      latitudeDelta: region.latitudeDelta,
      longitudeDelta: region.longitudeDelta * this.ASPECT_RATIO,
      // iOS only, optional region to render
      format: 'jpg', // image formats: 'png', 'jpg' (default: 'png')
      quality: 0.8, // image quality: 0..1 (only relevant for jpg, default: 1)
      result: 'file', // result types: 'file', 'base64' (default: 'file')
    });
    snapshot.then(uri => {
      console.log(uri);
      dispatch(
        StackActions.replace('save_location', {
          uri: uri,
          address: this.state.address,
          country: this.state.country,
        }),
      );
    });
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
        <StatusBar
          translucent={false}
          barStyle="dark-content"
          backgroundColor={Colors.white}
        />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            height: Globals.headerHeight,
            paddingHorizontal: Globals.pHorizontal,
            borderBottomColor: Colors.lighter_gery,
            borderBottomWidth: 1,
          }}>
          <TouchableOpacity
            onPress={() => {
              this.handleBackButtonClick();
            }}>
            <Image
              style={styles.back}
              source={require('../../../assets/back2.png')}
            />
          </TouchableOpacity>

          <Text style={styles.title}>Add Location</Text>
        </View>

        <View
          style={{
            flexDirection: 'row',
            padding: Globals.mTop / 1.7,
          }}>
          <GooglePlacesAutocomplete
            placeholder="Search"
            ref={ref => {
              this.searchBar = ref;
            }}
            styles={{
              textInput: {
                flex: 1,
                height: Globals.screenWidth * 0.15,
                borderColor: Colors.light_grey,
                borderWidth: 1,
                borderRadius: 10,
                marginRight: '2%',
                paddingVertical: Globals.mTop / 1.7,
                paddingHorizontal: Globals.mTop / 1.5,
                fontFamily: 'urbanist_regular',
                color: Colors.text_dark,
                fontSize: Globals.screenWidth * 0.04,
              },
            }}
            onPress={(data, details) => {
              this.callDelta(
                details.geometry.location.lat,
                details.geometry.location.lng,
              );
            }}
            fetchDetails={true}
            query={{
              key: MAPS_API_KEY,
              language: 'en',
            }}
          />

          <Pressable
            onPress={() => {
              this.requestLocationPermission();
            }}
            style={{
              backgroundColor: Colors.secondary,
              height: Globals.screenWidth * 0.15,
              width: Globals.screenWidth * 0.15,
              alignItems: 'center',
              justifyContent: 'center',
              marginLeft: '2%',
              borderRadius: (Globals.screenWidth * 0.15) / 4.5,
            }}>
            <Image
              style={{
                height: (Globals.screenWidth * 0.15) / 2,
                width: (Globals.screenWidth * 0.15) / 2,
                resizeMode: 'contain',
                tintColor: Colors.white,
              }}
              source={require('../../../assets/location_detect.png')}
            />
          </Pressable>
        </View>

        {this.state.region.latitude ? (
          <MapView
            provider={PROVIDER_GOOGLE}
            style={{flex: 1}}
            // customMapStyle={mapStyle}
            ref={ref => {
              this.map = ref;
            }}
            // mapType={MAP_TYPES.TERRAIN}
            initialRegion={this.state.region}
            onRegionChangeComplete={() => {
              this.callDelta(
                this.state.region.latitude,
                this.state.region.longitude,
              );
            }}
            onRegionChange={region => this.onRegionChange(region)}>
            <MapView.Marker
              // draggable
              coordinate={{
                latitude: this.state.region.latitude,
                longitude: this.state.region.longitude,
              }}
              image={require('../../../assets/kashfia_marker.png')}
              // onDragEnd={e => {
              //   this.callDelta(
              //     e.nativeEvent.coordinate.latitude,
              //     e.nativeEvent.coordinate.longitude,
              //   );
              // }}
            />
          </MapView>
        ) : (
          <Text>cordinates not found</Text>
        )}

        <TouchableOpacity
          style={{
            alignSelf: 'stretch',
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: Colors.secondary,
            paddingVertical: Globals.mTop,
            marginBottom: Globals.mTop * 1.4,
            marginHorizontal: Globals.pHorizontal,
            borderRadius: 10,
            position: 'absolute',
            bottom: 0,
            right: 0,
            left: 0,
          }}
          onPress={() => {
            this.takeLocationSnapshot();
          }}>
          <Text
            style={{
              color: Colors.white,
              alignSelf: 'center',
              fontSize: 16,
              fontFamily: 'urbanist_bold',
            }}>
            Continue
          </Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontFamily: 'urbanist_semi_bold',
    fontSize: 20,
    color: Colors.text_dark,
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
  },
  back: {
    width: 30,
    height: 30,
  },
});
