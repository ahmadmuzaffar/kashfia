import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  StatusBar,
  SafeAreaView,
  PermissionsAndroid,
  TextInput,
  Dimensions,
  ScrollView,
  Image,
  View,
} from 'react-native';
import Colors from '../../../utils/Colors';
import Modal from 'react-native-modal';
import Tags from 'react-native-tags';
import SpecializationList from '../../../custom/views/SpecializationList';
import CardView from 'react-native-cardview';
import Globals from '../../../utils/Globals';

export default class Specialization extends Component {
  state = {
    symptoms: '',
    isModalVisible: false,
    totalDocuments: '',
    tags: [],
    uris: [],
    buttonStyle: styles.button2,
  };

  constructor(props) {
    super(props);
    this.screenHeight = Dimensions.get('screen').height;
    this.windowHeight = Dimensions.get('window').height;
    this.windowWidth = Dimensions.get('window').width;
    this.navbarHeight =
      this.screenHeight - this.windowHeight + StatusBar.currentHeight;
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    this.requestCameraPermission = async () => {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Kashfia App Camera Permission',
            message:
              'Kashfia App needs access to your camera ' +
              'so you can take pictures.',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          launchImageLibrary(
            {mediaType: 'photo', selectionLimit: 0},
            response => {
              let i = 0;
              if (this.state.uris.length + response.assets.length > 3) {
                ToastAndroid.showWithGravityAndOffset(
                  'Max 3 files can be uploaded',
                  ToastAndroid.LONG,
                  ToastAndroid.BOTTOM,
                  25,
                  50,
                );
                return;
              }
              while (i < response.assets.length) {
                let mb = response.assets[i].fileSize / 1024.0 / 1024.0;
                mb = mb.toFixed(2);
                let filename =
                  Date.now() + '.' + response.assets[i].fileName.split('.')[1];
                let entry = filename + '(' + mb + ' MB)';
                this.state.tags.push(entry);
                this.state.uris.push(response.assets[i].uri);
                i = i + 1;
              }
              let tDocs = this.state.tags.length + ' pictures selected';

              this.setState(
                {
                  tags: this.state.tags,
                  uris: this.state.uris,
                  totalDocuments: tDocs,
                },
                this.handleChange,
              );
            },
          );
        } else {
          console.log('Camera permission denied');
        }
      } catch (err) {
        console.warn(err);
      }
    };
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.goBack();
    return true;
  }

  handleChange() {
    if (this.state.symptoms.trim().length > 0 && this.state.uris.length > 0) {
      this.setState({
        buttonStyle: styles.button1,
      });
    } else {
      this.setState({
        buttonStyle: styles.button2,
      });
    }
  }

  listData() {
    return [
      {
        id: 1,
        logo: require('../../../assets/stethoscope.jpg'),
        text: 'General Medicine',
        clickListener: function () {
          this.props.navigation.navigate('doctor_listing', {
            type: 'General Medicine',
          });
        }.bind(this),
      },
      {
        id: 2,
        logo: require('../../../assets/heart.jpg'),
        text: 'Cardiology',
        clickListener: function () {
          this.props.navigation.navigate('doctor_listing', {
            type: 'Cardiology',
          });
        }.bind(this),
      },
      {
        id: 3,
        logo: require('../../../assets/neurology.png'),
        text: 'Neurology',
        clickListener: function () {
          this.props.navigation.navigate('doctor_listing', {
            type: 'Neurology',
          });
        }.bind(this),
      },
      {
        id: 4,
        logo: require('../../../assets/eye.png'),
        text: 'Ophthalmology',
        clickListener: function () {
          this.props.navigation.navigate('doctor_listing', {
            type: 'Ophthalmology',
          });
        }.bind(this),
      },
      {
        id: 5,
        logo: require('../../../assets/dentist.png'),
        text: 'Dentist',
        clickListener: function () {
          this.props.navigation.navigate('doctor_listing', {
            type: 'Dentist',
          });
        }.bind(this),
      },
      {
        id: 6,
        logo: require('../../../assets/derma.jpg'),
        text: 'Dermatology',
        clickListener: function () {
          this.props.navigation.navigate('doctor_listing', {
            type: 'Dermatology',
          });
        }.bind(this),
      },
      {
        id: 7,
        logo: require('../../../assets/ortho.png'),
        text: 'Orthopaedic',
        clickListener: function () {
          this.props.navigation.navigate('doctor_listing', {
            type: 'Orthopaedic',
          });
        }.bind(this),
      },
      {
        id: 8,
        logo: require('../../../assets/urology.jpg'),
        text: 'Urology',
        clickListener: function () {
          this.props.navigation.navigate('doctor_listing', {
            type: 'Urology',
          });
        }.bind(this),
      },
      {
        id: 9,
        logo: require('../../../assets/pulmonology.png'),
        text: 'Pulmonology',
        clickListener: function () {
          this.props.navigation.navigate('doctor_listing', {
            type: 'Pulmonology',
          });
        }.bind(this),
      },
      {
        id: 10,
        logo: require('../../../assets/gynecology.png'),
        text: 'Gynecology',
        clickListener: function () {
          this.props.navigation.navigate('doctor_listing', {
            type: 'Gynecology',
          });
        }.bind(this),
      },
    ];
  }

  render() {
    const height = this.windowHeight;
    const width = this.windowWidth;

    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
        <View
          style={{
            flexDirection: 'row',
            padding: Globals.pHorizontal,
            borderBottomColor: Colors.lighter_gery,
            borderBottomWidth: 1,
          }}>
          <Text style={styles.title}>Specialization</Text>
        </View>

        <View
          style={{
            paddingHorizontal: Globals.pHorizontal,
            marginTop: Globals.mTop,
          }}>
          <Text style={styles.description}>Choose your health concern</Text>

          <TouchableOpacity
            onPress={() => {
              this.setState({
                isModalVisible: true,
              });
            }}
            style={{
              alignSelf: 'stretch',
              borderWidth: 1,
              borderRadius: 5,
              alignItems: 'center',
              marginTop: 10,
              backgroundColor: Colors.light_secondary,
              padding: 15,
              borderColor: Colors.secondary,
            }}>
            <Text
              style={{
                fontFamily: 'urbanist_semi_bold',
                fontSize: 16,
                color: Colors.secondary,
              }}>
              Not sure what to choose?
            </Text>
          </TouchableOpacity>

          <SpecializationList
            data={this.listData()}
            height={height}
            width={width}
          />
        </View>

        <Modal
          isVisible={this.state.isModalVisible}
          onRequestClose={function () {
            this.setState({
              isModalVisible: false,
            });
          }.bind(this)}>
          <View
            style={{
              borderRadius: 15,
              padding: 20,
              alignItems: 'center',
              backgroundColor: Colors.white,
            }}>
            <Text
              style={{
                fontFamily: 'urbanist_bold',
                fontSize: 20,
                color: Colors.black,
              }}>
              Not sure what specialty to choose?
            </Text>
            <Text
              style={{
                fontFamily: 'urbanist_regular',
                fontSize: 14,
                textAlign: 'center',
                color: Colors.black,
              }}>
              Please describe your symptoms here and we’ll prescribe a doctor
              for you.
            </Text>

            <View
              style={
                this.state.symptoms.trim().length > 0
                  ? styles.textInputFilled
                  : styles.textInput
              }>
              <TextInput
                style={{
                  fontSize: 17,
                  color: Colors.dark_grey,
                  fontFamily: 'urbanist_light',
                  textAlignVertical: 'top',
                  height: 120,
                }}
                value={this.state.symptoms}
                onChangeText={text =>
                  this.setState(
                    {
                      symptoms: text,
                    },
                    this.handleChange,
                  )
                }
                returnKeyType={'done'}
                autoCorrect={false}
                placeholder="Type your symptoms"
                placeholderTextColor={Colors.light_grey}
              />
            </View>

            <View
              style={{
                flexDirection: 'column',
                marginTop: 20,
                alignSelf: 'stretch',
              }}>
              <Text style={styles.label}>Upload a picture</Text>
              <View
                style={
                  this.state.totalDocuments.trim().length > 0
                    ? {
                        flexDirection: 'row',
                        borderRadius: 10,
                        borderWidth: 0.7,
                        borderColor: Colors.secondary,
                        paddingLeft: 10,
                      }
                    : {
                        flexDirection: 'row',
                        borderRadius: 10,
                        borderWidth: 0.7,
                        borderColor: Colors.light_grey,
                        paddingLeft: 10,
                      }
                }>
                <TextInput
                  style={{
                    flex: 1,
                    fontSize: 17,
                    color: Colors.dark_grey,
                    fontFamily: 'urbanist_light',
                  }}
                  editable={false}
                  value={this.state.totalDocuments}
                  placeholder="Upload Pictures (15Mb max)"
                  placeholderTextColor={Colors.light_grey}
                />
                <TouchableOpacity
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: Colors.secondary,
                    width: '18%',
                    borderBottomRightRadius: 10,
                    borderTopRightRadius: 10,
                  }}
                  onPress={this.requestCameraPermission}>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      resizeMode: 'contain',
                      tintColor: Colors.white,
                    }}
                    source={require('../../../assets/upload.png')}
                  />
                </TouchableOpacity>
              </View>
              <Tags
                initialTags={this.state.tags}
                textInputProps={{
                  editable: false,
                  style: {
                    height: 0,
                    width: 0,
                  },
                }}
                onChangeTags={tags => console.log(tags)}
                inputContainerStyle={{
                  height: 0,
                  opacity: 0,
                }}
                inputStyle={{
                  backgroundColor: Colors.white,
                }}
                renderTag={({
                  tag,
                  index,
                  onPress,
                  deleteTagOnPress,
                  readonly,
                }) => (
                  <View
                    key={`${tag}-${index}`}
                    style={{
                      flexDirection: 'row',
                      backgroundColor: Colors.tagBackground,
                      padding: '2%',
                      marginRight: '2%',
                      marginTop: '3%',
                      borderRadius: 5,
                    }}>
                    <Text
                      style={{
                        fontFamily: 'urbanist_regular',
                        color: Colors.text_dark,
                        fontSize: height * 0.015,
                      }}>
                      {tag}
                    </Text>
                    <TouchableOpacity
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginLeft: '6%',
                      }}
                      onPress={() => {
                        let temp = this.state.tags;
                        let temp2 = this.state.uris;
                        if (index > -1) {
                          temp.splice(index, 1);
                          temp2.splice(index, 1);
                        }
                        let tDocs =
                          temp2.length === 0
                            ? ''
                            : temp2.length + ' pictures selected';
                        this.setState(
                          {
                            tags: temp,
                            uris: temp2,
                            totalDocuments: tDocs,
                          },
                          this.handleChange,
                        );
                      }}>
                      <Image
                        style={{
                          height: height * 0.012,
                          width: height * 0.012,
                          resizeMode: 'contain',
                        }}
                        source={require('../../../assets/delete.png')}
                      />
                    </TouchableOpacity>
                  </View>
                )}
              />
            </View>
            <TouchableOpacity
              style={[
                this.state.buttonStyle,
                {
                  marginTop: 20,
                },
              ]}
              onPress={() => {
                this.setState(
                  {
                    isModalVisible: false,
                  },
                  this.handleBackButtonClick,
                );
              }}>
              <Text
                style={{
                  color: Colors.white,
                  alignSelf: 'center',
                  fontSize: 16,
                  fontFamily: 'urbanist_bold',
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>

        <TouchableOpacity
          style={styles.imgLL}
          onPress={() => {
            this.handleBackButtonClick();
          }}>
          <Image
            style={styles.back}
            source={require('../../../assets/back2.png')}
          />
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontFamily: 'urbanist_semi_bold',
    fontSize: 20,
    color: Colors.text_dark,
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
  },
  back: {
    width: 30,
    height: 30,
  },
  imgLL: {
    position: 'absolute',
    top: Globals.pHorizontal,
    left: Globals.pHorizontal,
  },
  description: {
    fontSize: 16,
    fontFamily: 'urbanist_regular',
    color: Colors.text_light,
  },
  button1: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  button2: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.disabled,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  label: {
    fontFamily: 'urbanist_bold',
    fontSize: 18,
    color: Colors.black,
  },
  textInput: {
    width: '100%',
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.light_grey,
    fontSize: 17,
    marginTop: 10,
    paddingHorizontal: 10,
    color: Colors.dark_grey,
    fontFamily: 'urbanist_light',
  },
  textInputFilled: {
    width: '100%',
    borderRadius: 10,
    borderWidth: 0.8,
    marginTop: 10,
    borderColor: Colors.secondary,
    fontSize: 17,
    paddingHorizontal: 10,
    color: Colors.dark_grey,
    fontFamily: 'urbanist_light',
  },
});
