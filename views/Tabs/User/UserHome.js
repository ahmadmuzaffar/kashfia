import React, {Component} from 'react';
import {
  BackHandler,
  StyleSheet,
  Dimensions,
  ScrollView,
  StatusBar,
  Text,
  Pressable,
  TouchableHighlight,
  TextInput,
  ToastAndroid,
  KeyboardAvoidingView,
  PermissionsAndroid,
  View,
  Platform,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {FloatingAction} from 'react-native-floating-action';
import SharedPreferences from 'react-native-shared-preferences';
import User from '../../../models/User';
import Colors from '../../../utils/Colors';
import AdSlider from '../../../custom/views/AdSlider';
import Globals from '../../../utils/Globals';
import CardView from 'react-native-cardview';
import ActionButton from 'react-native-action-button';

export default class UserHome extends Component {
  state = {
    user: Globals.user,
    entries: ['../../../assets/apology.png', '../../../assets/apology.png'],
    activeSlide: 0,
    width: 0,
  };

  constructor(props) {
    super(props);

    this.window = Dimensions.get('window');
    this.screenHeight = Dimensions.get('screen').height;
    this.windowHeight = Dimensions.get('window').height;
    this.navbarHeight =
      this.screenHeight - this.windowHeight + StatusBar.currentHeight;
  }

  listData() {
    return [
      {
        id: 1,
        logo: require('../../../assets/ad_logo.png'),
        title: 'Change your smile',
        description: 'Choose the experts for your smile. Free Consultation!',
        buttonText: 'Know More',
        ad_pic: require('../../../assets/ad_pic.png'),
        buttonListener: () => {
          console.log('HAHAHAHAAHAA');
        },
      },
      {
        id: 2,
        logo: require('../../../assets/ad_logo.png'),
        title: 'Change your smile',
        description: 'Choose the experts for your smile. Free Consultation!',
        buttonText: 'Know More',
        ad_pic: require('../../../assets/ad_pic.png'),
        buttonListener: () => {},
      },
      {
        id: 3,
        logo: require('../../../assets/ad_logo.png'),
        title: 'Change your smile',
        description: 'Choose the experts for your smile. Free Consultation!',
        buttonText: 'Know More',
        ad_pic: require('../../../assets/ad_pic.png'),
        buttonListener: () => {},
      },
      {
        id: 4,
        logo: require('../../../assets/ad_logo.png'),
        title: 'Change your smile',
        description: 'Choose the experts for your smile. Free Consultation!',
        buttonText: 'Know More',
        ad_pic: require('../../../assets/ad_pic.png'),
        buttonListener: () => {},
      },
    ];
  }

  render() {
    const {user} = this.state;
    const {height, width} = this.window;

    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="dark-content" backgroundColor={Colors.white} />

        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
          contentContainerStyle={{
            paddingBottom: Globals.pBottomScrollBar,
          }}
          showVerticalScrollIndicator={false}>
          <View>
            <View
              style={{
                flexDirection: 'row',
                paddingHorizontal: Globals.pHorizontal,
                paddingVertical: Globals.pVertical,
                backgroundColor: Colors.white,
              }}>
              <View
                style={{
                  flex: 1,
                  paddingRight: Globals.pRightHeader,
                  justifyContent: 'center',
                }}>
                <Text style={styles.title}>Hello {user.fullName}</Text>
                <Text style={styles.description}>
                  Let us make you feel better
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  borderRadius: Globals.userInfoLLHeaderBorder,
                  height: Globals.userInfoLLHeaderHeight,
                  justifyContent: 'center',
                  width: Globals.userInfoLLHeaderWidth,
                  backgroundColor: '#EEEEEE',
                }}>
                <Image
                  style={{
                    height: Globals.userInfoLLHeaderHeight,
                    width: Globals.userInfoLLHeaderHeight,
                    borderRadius: Globals.userInfoLLImageHeaderBorder,
                  }}
                  source={
                    user.avatar === '' || user.avatar === undefined
                      ? require('../../../assets/doc1.png')
                      : {uri: user.avatar}
                  }
                />
                <View
                  style={{
                    alignItems: 'center',
                    flex: 1,
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 15,
                      fontFamily: 'urbanist_bold',
                      color: Colors.text_dark,
                    }}>
                    2520
                  </Text>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'urbanist_semi_bold',
                      color: Colors.text_light,
                    }}>
                    IQD
                  </Text>
                </View>
              </View>
            </View>

            <AdSlider data={this.listData()} />
            <View style={{paddingHorizontal: Globals.pHorizontal}}>
              <View style={{marginTop: Globals.mTop}} />
              <Text
                style={[
                  styles.title,
                  {
                    fontFamily: 'urbanist_semi_bold',
                    fontSize: 24,
                    color: Colors.text_dark,
                    marginHorizontal: 3,
                  },
                ]}>
                Discover
              </Text>

              <CardView
                style={{
                  marginTop: Globals.cardInternalTop,
                  height: Globals.cardHeight,
                }}
                cardElevation={10}
                cardMaxElevation={10}
                cornerRadius={10}>
                <Pressable
                  onPress={() => {
                    this.props.navigation.navigate('specialization');
                  }}
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'stretch',
                    borderRadius: 10,
                    backgroundColor: Colors.white,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      padding: 10,
                      marginTop: 10,
                      marginLeft: 10,
                    }}>
                    <Text
                      style={{
                        fontSize: 18,
                        fontFamily: 'urbanist_bold',
                        color: Colors.text_dark,
                      }}>
                      Find Doctors
                    </Text>
                    <Text
                      style={{
                        fontSize: 12,
                        fontFamily: 'urbanist_regular',
                        color: Colors.text_light,
                      }}>
                      Consult a doctor anytime,{'\n'}anywhere.
                    </Text>
                  </View>
                  <Image
                    style={{
                      marginTop: '4%',
                      width: Globals.cardImageWidth,
                      height: Globals.cardHeight,
                      resizeMode: 'contain',
                    }}
                    source={require('../../../assets/doctor.png')}
                  />
                </Pressable>
              </CardView>

              <CardView
                style={{
                  marginTop: Globals.cardInternalTop,
                  height: Globals.cardHeight,
                }}
                cardElevation={10}
                cardMaxElevation={10}
                cornerRadius={10}>
                <Pressable
                  onPress={() => {
                    this.props.navigation.navigate('request_nurse');
                  }}
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'stretch',
                    borderRadius: 10,
                    backgroundColor: Colors.white,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      padding: 10,
                      marginTop: 10,
                      marginLeft: 10,
                    }}>
                    <Text
                      style={{
                        fontSize: 18,
                        fontFamily: 'urbanist_bold',
                        color: Colors.text_dark,
                      }}>
                      Find Nurse
                    </Text>
                    <Text
                      style={{
                        fontSize: 12,
                        fontFamily: 'urbanist_regular',
                        color: Colors.text_light,
                      }}>
                      Serving your health needs{'\n'}is our priority.
                    </Text>
                  </View>
                  <Image
                    style={{
                      marginTop: '4%',
                      width: Globals.cardImageWidth,
                      height: Globals.cardHeight,
                      resizeMode: 'contain',
                    }}
                    source={require('../../../assets/nurse.png')}
                  />
                </Pressable>
              </CardView>

              <CardView
                style={{
                  marginTop: Globals.cardInternalTop,
                  height: Globals.cardHeight,
                }}
                cardElevation={10}
                cardMaxElevation={10}
                cornerRadius={10}>
                <Pressable
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'stretch',
                    borderRadius: 10,
                    backgroundColor: Colors.white,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      padding: 10,
                      marginTop: 10,
                      marginLeft: 10,
                    }}>
                    <Text
                      style={{
                        fontSize: 18,
                        fontFamily: 'urbanist_bold',
                        color: Colors.text_dark,
                      }}>
                      Find Pathological{'\n'}Analyst
                    </Text>
                    <Text
                      style={{
                        fontSize: 12,
                        fontFamily: 'urbanist_regular',
                        color: Colors.text_light,
                      }}>
                      Done with the labs? We’re {'\n'}here to give you the
                      analysis.
                    </Text>
                  </View>
                  <Image
                    style={{
                      marginTop: '4%',
                      width: Globals.cardImageWidth,
                      height: Globals.cardHeight,
                      resizeMode: 'contain',
                    }}
                    source={require('../../../assets/pathologist.png')}
                  />
                </Pressable>
              </CardView>

              <Pressable
                onPress={() => {
                  this.props.navigation.navigate('tutorials');
                }}
                style={{
                  flexDirection: 'row',
                  alignSelf: 'stretch',
                  padding: 10,
                  borderRadius: 10,
                  marginTop: Globals.cardInternalTop,
                  height: Globals.cardHeight,
                  backgroundColor: Colors.secondary,
                }}>
                <View
                  style={{
                    flex: 1,
                    marginTop: 10,
                    marginLeft: 10,
                  }}>
                  <Text
                    style={{
                      fontSize: 20,
                      fontFamily: 'urbanist_bold',
                      color: Colors.white,
                    }}>
                    Tutorials
                  </Text>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'urbanist_regular',
                      color: Colors.white,
                    }}>
                    Explore Kashfia with our{'\n'}tutorials videos
                  </Text>
                </View>
                <Image
                  style={{
                    width: 120,
                    height: 150,
                    resizeMode: 'contain',
                  }}
                  source={require('../../../assets/tutorials.png')}
                />
              </Pressable>
            </View>
          </View>
          <View
            style={{
              marginTop: Globals.mTop,
              backgroundColor: Colors.dark_blue2,
              padding: Globals.pHorizontal,
              alignItems: 'center',
            }}>
            <View>
              <Text
                style={{
                  color: Colors.white,
                  fontSize: height * 0.03,
                  fontFamily: 'urbanist_bold',
                }}>
                Completed Orders
              </Text>
              <View
                style={{
                  backgroundColor: Colors.white,
                  marginTop: '1.5%',
                  height: height * 0.004,
                }}
              />
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'stretch',
                marginTop: 40,
                justifyContent: 'space-evenly',
              }}>
              <View style={{alignItems: 'center'}}>
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: 'urbanist_regular',
                    color: Colors.white,
                  }}>
                  Online Diagnosis Done
                </Text>
                <Text
                  style={{
                    fontSize: 32,
                    marginTop: 7,
                    fontFamily: 'urbanist_bold',
                    color: Colors.white,
                  }}>
                  2000
                </Text>
              </View>
              <View style={{alignItems: 'center'}}>
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: 'urbanist_regular',
                    color: Colors.white,
                  }}>
                  Home Visits Done
                </Text>
                <Text
                  style={{
                    fontSize: 32,
                    marginTop: 7,
                    fontFamily: 'urbanist_bold',
                    color: Colors.white,
                  }}>
                  700
                </Text>
              </View>
            </View>
          </View>
          <View
            style={{
              padding: Globals.pHorizontal,
            }}>
            <Text
              style={{
                color: Colors.text_dark,
                fontFamily: 'urbanist_regular',
                fontSize: 18,
              }}>
              Our community of doctors and patients drive us to create
              technologies for better and affordable healthcare.
            </Text>
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  marginTop: Globals.mTop,
                }}>
                <View style={{flex: 0.5}}>
                  <Image
                    style={{height: 30, width: 30, resizeMode: 'contain'}}
                    source={require('../../../assets/user_avatar.png')}
                  />
                  <Text
                    style={{
                      fontFamily: 'urbanist_regular',
                      fontSize: 14,
                      color: '#828282',
                    }}>
                    Users
                  </Text>
                  <Text
                    style={{
                      fontFamily: 'urbanist_semi_bold',
                      fontSize: 32,
                      color: Colors.text_dark,
                    }}>
                    50000
                  </Text>
                </View>
                <View style={{flex: 0.5}}>
                  <Image
                    style={{height: 30, width: 30, resizeMode: 'contain'}}
                    source={require('../../../assets/doctor_avatar.png')}
                  />
                  <Text
                    style={{
                      fontFamily: 'urbanist_regular',
                      fontSize: 14,
                      color: '#828282',
                    }}>
                    Doctors
                  </Text>
                  <Text
                    style={{
                      fontFamily: 'urbanist_semi_bold',
                      fontSize: 32,
                      color: Colors.text_dark,
                    }}>
                    2000
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  marginTop: Globals.mTop,
                }}>
                <View style={{flex: 0.5}}>
                  <Image
                    style={{height: 30, width: 30, resizeMode: 'contain'}}
                    source={require('../../../assets/nurse_avatar.png')}
                  />
                  <Text
                    style={{
                      fontFamily: 'urbanist_regular',
                      fontSize: 14,
                      color: '#828282',
                    }}>
                    Nurses
                  </Text>
                  <Text
                    style={{
                      fontFamily: 'urbanist_semi_bold',
                      fontSize: 32,
                      color: Colors.text_dark,
                    }}>
                    1500
                  </Text>
                </View>
                <View style={{flex: 0.5}}>
                  <Image
                    style={{height: 30, width: 30, resizeMode: 'contain'}}
                    source={require('../../../assets/pathologist_avatar.png')}
                  />
                  <Text
                    style={{
                      fontFamily: 'urbanist_regular',
                      fontSize: 14,
                      color: '#828282',
                    }}>
                    Pathological Analyst
                  </Text>
                  <Text
                    style={{
                      fontFamily: 'urbanist_semi_bold',
                      fontSize: 32,
                      color: Colors.text_dark,
                    }}>
                    700
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>

        <ActionButton
          buttonColor={Colors.secondary}
          style={{marginBottom: Globals.actionBarBMargin}}
          renderIcon={() => {
            return (
              <Image
                style={{
                  height: Globals.actionBarHeight,
                  width: Globals.actionBarWidth,
                }}
                source={require('../../../assets/support.png')}
              />
            );
          }}
          onPress={() => {
            console.log('hi');
          }}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg,
  },
  title: {
    fontSize: 28,
    color: Colors.text_dark,
    fontFamily: 'urbanist_bold',
  },
  description: {
    fontSize: 14,
    fontFamily: 'urbanist_regular',
    color: Colors.text_light,
  },
  tabLayout: {
    backgroundColor: Colors.enabled,
    borderRadius: 10,
    padding: 5,
    margin: 7,
    flexDirection: 'row',
  },
  enabled_tab: {
    flex: 0.5,
    backgroundColor: Colors.secondary,
    borderRadius: 10,
    padding: 10,
  },
  enabled_tab_text: {
    color: Colors.white,
    textAlign: 'center',
    fontFamily: 'urbanist_bold',
    fontSize: 16,
  },
  disabled_tab: {
    flex: 0.5,
    backgroundColor: Colors.enabled,
    borderRadius: 10,
    padding: 10,
  },
  disabled_tab_text: {
    color: Colors.secondary,
    fontFamily: 'urbanist_regular',
    fontSize: 16,
    textAlign: 'center',
  },
  label: {
    fontFamily: 'urbanist_bold',
    fontSize: 18,
  },
  textInput: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.light_grey,
    fontSize: 17,
    paddingHorizontal: 10,
    color: Colors.dark_grey,
    fontFamily: 'urbanist_light',
  },
  textInputFilled: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.secondary,
    fontSize: 17,
    paddingHorizontal: 10,
    color: Colors.dark_grey,
    fontFamily: 'urbanist_light',
  },
  disabledGender: {
    flex: 0.48,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.7,
    alignItems: 'center',
    padding: 10,
    borderColor: Colors.light_grey,
  },
  filledGender: {
    flex: 0.48,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.7,
    alignItems: 'center',
    padding: 10,
    borderColor: Colors.secondary,
  },
  enabledGender: {
    flex: 0.48,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    borderRadius: 7,
    padding: 10,
  },
  disabledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.disabledIcon,
  },
  enabledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.white,
  },
  filledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.secondary,
  },
  genderLayout: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button1: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  button2: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.disabled,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  placeholder_text: {
    fontFamily: 'urbanist_regular',
    fontSize: 17,
    color: Colors.light_grey,
  },
  text: {
    fontFamily: 'urbanist_regular',
    fontSize: 17,
    color: Colors.black,
  },
});
