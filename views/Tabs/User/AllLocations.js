import {
  Text,
  StyleSheet,
  View,
  SafeAreaView,
  RefreshControl,
  ScrollView,
  BackHandler,
  FlatList,
  TouchableOpacity,
  Image,
  Pressable,
} from 'react-native';
import React, {Component} from 'react';
import Globals from '../../../utils/Globals';
import Colors from '../../../utils/Colors';
import CardView from 'react-native-cardview';
import SharedPreferences from 'react-native-shared-preferences';
import axios from 'react-native-axios';
import {BASE_URL} from '@env';
import {ProgressBar} from 'react-native-material-indicators';

export default class AllLocations extends Component {
  state = {
    listData: [],
    visible: false,
    refreshing: false,
  };

  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.getLocations = this.getLocations.bind(this);
    this.deleteLocation = this.deleteLocation.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    this.setState({visible: true}, this.getLocations);
  }

  componentDidUpdate(prevProps) {
    // if (prevProps.route.params?.success !== this.props.route.params?.success) {
    //   const result = this.props.route.params?.success;
    //   if (result)
    this.getLocations();
    // }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.goBack();
    return true;
  }

  dispatchResults() {
    Globals.locations = this.state.listData;
  }

  getLocations() {
    axios
      .get(BASE_URL + '/locations', {
        headers: {
          Authorization: 'Bearer ' + Globals.user.accessToken,
        },
      })
      .then(response => {
        if (response.data.status) {
          this.setState(
            {listData: response.data.data, visible: false, refreshing: false},
            this.dispatchResults,
          );
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  deleteLocation(_id) {
    axios
      .delete(BASE_URL + '/locations/' + _id, {
        headers: {
          Authorization: 'Bearer ' + Globals.user.accessToken,
        },
      })
      .then(response => {
        if (response.data.status) {
          const filteredData = this.state.listData.filter(
            item => item._id !== _id,
          );
          this.setState(
            {listData: filteredData, visible: false},
            this.dispatchResults,
          );
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  onRefresh() {
    this.setState({visible: true, refreshing: true}, this.getLocations);
  }

  Item({locationObj, instance}) {
    return (
      <Pressable
        style={{
          borderColor: '#F0F0F0',
          borderWidth: 1,
          borderRadius: 10,
          padding: Globals.mTop / 1.5,
          marginBottom: Globals.mTop,
        }}>
        <View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text
              style={{
                fontFamily: 'urbanist_semi_bold',
                flex: 1,
                fontSize: Globals.screenWidth * 0.04,
              }}>
              {locationObj.name}
            </Text>

            <Pressable onPress={() => {}}>
              <Image
                source={require('../../../assets/typo_edit.png')}
                style={{resizeMode: 'contain', height: 22, width: 22}}
              />
            </Pressable>

            <Pressable
              style={{marginLeft: '3%'}}
              onPress={() => {
                instance.setState(
                  {visible: true},
                  instance.deleteLocation(locationObj._id),
                );
              }}>
              <Image
                source={require('../../../assets/delete_2.png')}
                style={{resizeMode: 'contain', height: 22, width: 22}}
              />
            </Pressable>
          </View>
          <Text
            numberOfLines={1}
            ellipsizeMode="tail"
            style={{
              fontFamily: 'urbanist_regular',
              fontSize: Globals.screenWidth * 0.035,
            }}>
            {locationObj.address}
          </Text>
        </View>
      </Pressable>
    );
  }

  render() {
    const {listData} = this.state;

    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
        <ProgressBar
          size={48}
          determinate={false}
          thickness={4}
          visible={this.state.visible}
          color={Colors.primary}
        />

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
            />
          }>
          <View
            style={{
              flexDirection: 'row',
              padding: Globals.pHorizontal,
              borderBottomColor: Colors.lighter_gery,
              borderBottomWidth: 1,
            }}>
            <Text style={styles.title}>Locations</Text>
          </View>

          <View style={{padding: Globals.pHorizontal}}>
            <Pressable
              style={{
                borderRadius: 10,
                flexDirection: 'row',
                backgroundColor: Colors.secondary,
                padding: Globals.mTop,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => {
                this.props.navigation.navigate('add_location');
              }}>
              <Text
                style={{
                  fontFamily: 'urbanist_bold',
                  flex: 1,
                  color: Colors.white,
                  fontSize: Globals.screenWidth * 0.04,
                }}>
                Add Location
              </Text>
              <Image
                source={require('../../../assets/next.png')}
                style={{
                  resizeMode: 'contain',
                  height: 25,
                  width: 25,
                  tintColor: Colors.white,
                }}
              />
            </Pressable>

            <FlatList
              ref={ref => (this.list = ref)}
              style={{
                flexGrow: 0,
                marginTop: Globals.mTop,
              }}
              showsVerticalScrollIndicator={false}
              data={listData}
              extraData={this.state.refresh}
              renderItem={({item}) => (
                <this.Item locationObj={item} instance={this} />
              )}
              keyExtractor={(item, index) => item._id}
            />
          </View>
        </ScrollView>

        <TouchableOpacity
          style={styles.imgLL}
          onPress={() => {
            this.handleBackButtonClick();
          }}>
          <Image
            style={styles.back}
            source={require('../../../assets/back2.png')}
          />
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontFamily: 'urbanist_semi_bold',
    fontSize: 20,
    color: Colors.text_dark,
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
  },
  back: {
    width: 25,
    height: 25,
  },
  imgLL: {
    position: 'absolute',
    top: Globals.pHorizontal,
    left: Globals.pHorizontal,
  },
});
