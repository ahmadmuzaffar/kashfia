import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  TouchableOpacity,
  Pressable,
  BackHandler,
  StatusBar,
  SafeAreaView,
  Dimensions,
  ScrollView,
  Image,
  View,
} from 'react-native';
import Colors from '../../../utils/Colors';
import StarRating from 'react-native-star-rating';
import CardView from 'react-native-cardview';
import Globals from '../../../utils/Globals';

export default class DoctorListing extends Component {
  state = {
    starCount: 3.5,
  };

  constructor(props) {
    super(props);

    this.type = this.props.route.params.type;
    this.screenHeight = Dimensions.get('screen').height;
    this.windowHeight = Dimensions.get('window').height;
    this.navbarHeight =
      this.screenHeight - this.windowHeight + StatusBar.currentHeight;
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating,
    });
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.navigate('specialization', {
      go_back_key: this.props.navigation.key,
    });
    return true;
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
          contentContainerStyle={{
            paddingBottom: this.navbarHeight,
          }}
          showVerticalScrollIndicator={false}>
          <View
            style={{
              flexDirection: 'row',
              padding: Globals.pHorizontal,
              borderBottomColor: Colors.lighter_gery,
              borderBottomWidth: 1,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.handleBackButtonClick();
              }}>
              <Image
                style={styles.back}
                source={require('../../../assets/back2.png')}
              />
            </TouchableOpacity>

            <Text style={styles.title}>Request a Doctor</Text>
          </View>

          <View
            style={{
              paddingHorizontal: Globals.pHorizontal,
              marginTop: Globals.mTop,
            }}>
            <CardView
              style={{flex: 0.5, padding: '3%'}}
              cardElevation={5}
              cardMaxElevation={5}
              cornerRadius={10}>
              <Pressable
                onPress={() => {
                  this.props.navigation.navigate('doctor_details');
                }}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    style={{height: 80, width: 80, borderRadius: 10}}
                    source={require('../../../assets/doc2.png')}
                  />
                  <View
                    style={{marginHorizontal: 5, marginVertical: 5, flex: 1}}>
                    <Text
                      style={{fontSize: 15, fontFamily: 'urbanist_semi_bold'}}>
                      Dr. Yaser Qurban
                    </Text>
                    <Text
                      style={{
                        fontSize: 15,
                        fontFamily: 'urbanist_semi_bold',
                        color: Colors.secondary,
                      }}>
                      {this.type}
                    </Text>
                    <View style={{flexDirection: 'row', marginTop: 5}}>
                      <StarRating
                        disabled={false}
                        maxStars={5}
                        starSize={15}
                        halfStarColor={Colors.star}
                        fullStarColor={Colors.star}
                        emptyStarColor={'#E7E7E7'}
                        rating={4}
                        starStyle={{marginHorizontal: '1%'}}
                        selectedStar={rating => this.onStarRatingPress(rating)}
                      />
                      <Text
                        style={{
                          fontSize: 14,
                          fontFamily: 'urbanist_semi_bold',
                          marginLeft: 10,
                          color: Colors.text_light,
                        }}>
                        7 Reviews
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      height: 20,
                    }}>
                    <View
                      style={{
                        backgroundColor: 'grey',
                        height: 10,
                        width: 10,
                        borderRadius: 5,
                      }}
                    />
                    <Text
                      style={{
                        fontFamily: 'urbanist_regular',
                        color: 'grey',
                        marginLeft: 5,
                      }}>
                      Offline
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', marginTop: 15}}>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      marginRight: 7,
                      resizeMode: 'contain',
                    }}
                    source={require('../../../assets/hospital.png')}
                  />
                  <Text style={{fontFamily: 'urbanist_regular'}}>
                    Ibn e Sina Hospital, Baghdad
                  </Text>
                </View>

                <View style={{flexDirection: 'row', marginTop: 15}}>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      marginRight: 7,
                      resizeMode: 'contain',
                    }}
                    source={require('../../../assets/location.png')}
                  />
                  <Text style={{fontFamily: 'urbanist_regular'}}>
                    Al Mansour, Baghdad, Iraq
                  </Text>
                </View>

                <View style={{flexDirection: 'row', marginTop: 15}}>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      marginRight: 7,
                      resizeMode: 'contain',
                    }}
                    source={require('../../../assets/patients_attended.png')}
                  />
                  <View style={{flexDirection: 'row'}}>
                    <Text style={{fontFamily: 'urbanist_regular'}}>
                      Patients
                    </Text>
                    <Text
                      style={{
                        fontFamily: 'urbanist_semi_bold',
                        color: Colors.secondary,
                      }}>
                      32
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    flex: 1,
                    marginTop: 20,
                    marginHorizontal: 10,
                  }}>
                  <TouchableOpacity
                    style={{
                      borderWidth: 1,
                      padding: 10,
                      flex: 0.5,
                      marginRight: 5,
                      alignItems: 'center',
                      borderColor: Colors.light_grey,
                      borderRadius: 10,
                    }}>
                    <View style={{alignItems: 'center'}}>
                      <Text
                        style={{
                          color: Colors.secondary,
                          fontFamily: 'urbanist_regular',
                          fontSize: 12,
                        }}>
                        Online Consultation
                      </Text>
                      <View style={{flexDirection: 'row'}}>
                        <Text
                          style={{
                            color: Colors.black,
                            fontFamily: 'urbanist_bold',
                            fontSize: 20,
                          }}>
                          150,000
                        </Text>
                        <Text
                          style={{
                            color: Colors.black,
                            marginLeft: 5,
                            fontFamily: 'urbanist_bold',
                            alignSelf: 'flex-end',
                            fontSize: 14,
                          }}>
                          IQD
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      borderWidth: 1,
                      padding: 10,
                      marginLeft: 5,
                      flex: 0.5,
                      alignItems: 'center',
                      borderColor: Colors.light_grey,
                      borderRadius: 10,
                    }}>
                    <View style={{alignItems: 'center'}}>
                      <Text
                        style={{
                          color: Colors.home_consult,
                          fontFamily: 'urbanist_regular',
                          fontSize: 12,
                        }}>
                        Home Consultation
                      </Text>
                      <View style={{flexDirection: 'row'}}>
                        <Text
                          style={{
                            color: Colors.black,
                            fontFamily: 'urbanist_bold',
                            fontSize: 20,
                          }}>
                          240,000
                        </Text>
                        <Text
                          style={{
                            color: Colors.black,
                            marginLeft: 5,
                            fontFamily: 'urbanist_bold',
                            alignSelf: 'flex-end',
                            fontSize: 14,
                          }}>
                          IQD
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>

                <TouchableOpacity style={styles.button} onPress={() => {}}>
                  <Text
                    style={{
                      color: Colors.white,
                      alignSelf: 'center',
                      fontSize: 16,
                      fontFamily: 'urbanist_bold',
                    }}>
                    Book Now
                  </Text>
                </TouchableOpacity>
              </Pressable>
            </CardView>

            <CardView
              style={{flex: 0.5, marginTop: Globals.mTop, padding: '3%'}}
              cardElevation={5}
              cardMaxElevation={5}
              cornerRadius={10}>
              <View>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    style={{height: 80, width: 80, borderRadius: 10}}
                    source={require('../../../assets/doc2.png')}
                  />
                  <View
                    style={{marginHorizontal: 5, marginVertical: 5, flex: 1}}>
                    <Text
                      style={{fontSize: 15, fontFamily: 'urbanist_semi_bold'}}>
                      Dr. Yaser Qurban
                    </Text>
                    <Text
                      style={{
                        fontSize: 15,
                        fontFamily: 'urbanist_semi_bold',
                        color: Colors.secondary,
                      }}>
                      {this.type}
                    </Text>
                    <View style={{flexDirection: 'row', marginTop: 5}}>
                      <StarRating
                        disabled={false}
                        maxStars={5}
                        starSize={15}
                        halfStarColor={Colors.star}
                        fullStarColor={Colors.star}
                        emptyStarColor={'#E7E7E7'}
                        rating={4}
                        starStyle={{marginHorizontal: '1%'}}
                        selectedStar={rating => this.onStarRatingPress(rating)}
                      />
                      <Text
                        style={{
                          fontSize: 14,
                          fontFamily: 'urbanist_semi_bold',
                          marginLeft: 10,
                          color: Colors.text_light,
                        }}>
                        7 Reviews
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      height: 20,
                    }}>
                    <View
                      style={{
                        backgroundColor: Colors.green,
                        height: 10,
                        width: 10,
                        borderRadius: 5,
                      }}
                    />
                    <Text
                      style={{
                        fontFamily: 'urbanist_regular',
                        color: Colors.green,
                        marginLeft: 5,
                      }}>
                      Online
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', marginTop: 15}}>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      marginRight: 7,
                      resizeMode: 'contain',
                    }}
                    source={require('../../../assets/hospital.png')}
                  />
                  <Text style={{fontFamily: 'urbanist_regular'}}>
                    Ibn e Sina Hospital, Baghdad
                  </Text>
                </View>

                <View style={{flexDirection: 'row', marginTop: 15}}>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      marginRight: 7,
                      resizeMode: 'contain',
                    }}
                    source={require('../../../assets/location.png')}
                  />
                  <Text style={{fontFamily: 'urbanist_regular'}}>
                    Al Mansour, Baghdad, Iraq
                  </Text>
                </View>

                <View style={{flexDirection: 'row', marginTop: 15}}>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      marginRight: 7,
                      resizeMode: 'contain',
                    }}
                    source={require('../../../assets/patients_attended.png')}
                  />
                  <View style={{flexDirection: 'row'}}>
                    <Text style={{fontFamily: 'urbanist_regular'}}>
                      Patients
                    </Text>
                    <Text
                      style={{
                        fontFamily: 'urbanist_semi_bold',
                        color: Colors.secondary,
                      }}>
                      32
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    flex: 1,
                    marginTop: 20,
                    marginHorizontal: 10,
                  }}>
                  <TouchableOpacity
                    style={{
                      borderWidth: 1,
                      padding: 10,
                      flex: 0.5,
                      marginRight: 5,
                      alignItems: 'center',
                      borderColor: Colors.light_grey,
                      borderRadius: 10,
                    }}>
                    <View style={{alignItems: 'center'}}>
                      <Text
                        style={{
                          color: Colors.secondary,
                          fontFamily: 'urbanist_regular',
                          fontSize: 12,
                        }}>
                        Online Consultation
                      </Text>
                      <View style={{flexDirection: 'row'}}>
                        <Text
                          style={{
                            color: Colors.black,
                            fontFamily: 'urbanist_bold',
                            fontSize: 20,
                          }}>
                          150,000
                        </Text>
                        <Text
                          style={{
                            color: Colors.black,
                            marginLeft: 5,
                            fontFamily: 'urbanist_bold',
                            alignSelf: 'flex-end',
                            fontSize: 14,
                          }}>
                          IQD
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      borderWidth: 1,
                      padding: 10,
                      marginLeft: 5,
                      flex: 0.5,
                      alignItems: 'center',
                      borderColor: Colors.light_grey,
                      borderRadius: 10,
                    }}>
                    <View style={{alignItems: 'center'}}>
                      <Text
                        style={{
                          color: Colors.home_consult,
                          fontFamily: 'urbanist_regular',
                          fontSize: 12,
                        }}>
                        Home Consultation
                      </Text>
                      <View style={{flexDirection: 'row'}}>
                        <Text
                          style={{
                            color: Colors.black,
                            fontFamily: 'urbanist_bold',
                            fontSize: 20,
                          }}>
                          240,000
                        </Text>
                        <Text
                          style={{
                            color: Colors.black,
                            marginLeft: 5,
                            fontFamily: 'urbanist_bold',
                            alignSelf: 'flex-end',
                            fontSize: 14,
                          }}>
                          IQD
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>

                <TouchableOpacity style={styles.button} onPress={() => {}}>
                  <Text
                    style={{
                      color: Colors.white,
                      alignSelf: 'center',
                      fontSize: 16,
                      fontFamily: 'urbanist_bold',
                    }}>
                    Book Now
                  </Text>
                </TouchableOpacity>
              </View>
            </CardView>

            <CardView
              style={{flex: 0.5, marginTop: Globals.mTop, padding: '3%'}}
              cardElevation={5}
              cardMaxElevation={5}
              cornerRadius={10}>
              <View>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    style={{height: 80, width: 80, borderRadius: 10}}
                    source={require('../../../assets/doc2.png')}
                  />
                  <View
                    style={{marginHorizontal: 5, marginVertical: 5, flex: 1}}>
                    <Text
                      style={{fontSize: 15, fontFamily: 'urbanist_semi_bold'}}>
                      Dr. Yaser Qurban
                    </Text>
                    <Text
                      style={{
                        fontSize: 15,
                        fontFamily: 'urbanist_semi_bold',
                        color: Colors.secondary,
                      }}>
                      {this.type}
                    </Text>
                    <View style={{flexDirection: 'row', marginTop: 5}}>
                      <StarRating
                        disabled={false}
                        maxStars={5}
                        starSize={15}
                        halfStarColor={Colors.star}
                        fullStarColor={Colors.star}
                        emptyStarColor={'#E7E7E7'}
                        rating={4}
                        starStyle={{marginHorizontal: '1%'}}
                        selectedStar={rating => this.onStarRatingPress(rating)}
                      />
                      <Text
                        style={{
                          fontSize: 14,
                          fontFamily: 'urbanist_semi_bold',
                          marginLeft: 10,
                          color: Colors.text_light,
                        }}>
                        7 Reviews
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      height: 20,
                    }}>
                    <View
                      style={{
                        backgroundColor: 'red',
                        height: 10,
                        width: 10,
                        borderRadius: 5,
                      }}
                    />
                    <Text
                      style={{
                        fontFamily: 'urbanist_regular',
                        color: 'red',
                        marginLeft: 5,
                      }}>
                      Busy
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', marginTop: 15}}>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      marginRight: 7,
                      resizeMode: 'contain',
                    }}
                    source={require('../../../assets/hospital.png')}
                  />
                  <Text style={{fontFamily: 'urbanist_regular'}}>
                    Ibn e Sina Hospital, Baghdad
                  </Text>
                </View>

                <View style={{flexDirection: 'row', marginTop: 15}}>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      marginRight: 7,
                      resizeMode: 'contain',
                    }}
                    source={require('../../../assets/location.png')}
                  />
                  <Text style={{fontFamily: 'urbanist_regular'}}>
                    Al Mansour, Baghdad, Iraq
                  </Text>
                </View>

                <View style={{flexDirection: 'row', marginTop: 15}}>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      marginRight: 7,
                      resizeMode: 'contain',
                    }}
                    source={require('../../../assets/patients_attended.png')}
                  />
                  <View style={{flexDirection: 'row'}}>
                    <Text style={{fontFamily: 'urbanist_regular'}}>
                      Patients
                    </Text>
                    <Text
                      style={{
                        fontFamily: 'urbanist_semi_bold',
                        color: Colors.secondary,
                      }}>
                      32
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    flex: 1,
                    marginTop: 20,
                    marginHorizontal: 10,
                  }}>
                  <TouchableOpacity
                    style={{
                      borderWidth: 1,
                      padding: 10,
                      flex: 0.5,
                      marginRight: 5,
                      alignItems: 'center',
                      borderColor: Colors.light_grey,
                      borderRadius: 10,
                    }}>
                    <View style={{alignItems: 'center'}}>
                      <Text
                        style={{
                          color: Colors.secondary,
                          fontFamily: 'urbanist_regular',
                          fontSize: 12,
                        }}>
                        Online Consultation
                      </Text>
                      <View style={{flexDirection: 'row'}}>
                        <Text
                          style={{
                            color: Colors.black,
                            fontFamily: 'urbanist_bold',
                            fontSize: 20,
                          }}>
                          150,000
                        </Text>
                        <Text
                          style={{
                            color: Colors.black,
                            marginLeft: 5,
                            fontFamily: 'urbanist_bold',
                            alignSelf: 'flex-end',
                            fontSize: 14,
                          }}>
                          IQD
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      borderWidth: 1,
                      padding: 10,
                      marginLeft: 5,
                      flex: 0.5,
                      alignItems: 'center',
                      borderColor: Colors.light_grey,
                      borderRadius: 10,
                    }}>
                    <View style={{alignItems: 'center'}}>
                      <Text
                        style={{
                          color: Colors.home_consult,
                          fontFamily: 'urbanist_regular',
                          fontSize: 12,
                        }}>
                        Home Consultation
                      </Text>
                      <View style={{flexDirection: 'row'}}>
                        <Text
                          style={{
                            color: Colors.black,
                            fontFamily: 'urbanist_bold',
                            fontSize: 20,
                          }}>
                          240,000
                        </Text>
                        <Text
                          style={{
                            color: Colors.black,
                            marginLeft: 5,
                            fontFamily: 'urbanist_bold',
                            alignSelf: 'flex-end',
                            fontSize: 14,
                          }}>
                          IQD
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>

                <TouchableOpacity style={styles.button} onPress={() => {}}>
                  <Text
                    style={{
                      color: Colors.white,
                      alignSelf: 'center',
                      fontSize: 16,
                      fontFamily: 'urbanist_bold',
                    }}>
                    Book Now
                  </Text>
                </TouchableOpacity>
              </View>
            </CardView>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontFamily: 'urbanist_semi_bold',
    fontSize: 20,
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
  },
  back: {
    width: 30,
    height: 30,
  },
  button: {
    width: '90%',
    alignSelf: 'center',
    marginTop: 20,
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    marginVertical: 5,
    fontFamily: 'urbanist_bold',
    borderRadius: 10,
  },
});
