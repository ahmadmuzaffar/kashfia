import React, {Component} from 'react';
import {
  BackHandler,
  StyleSheet,
  Dimensions,
  ScrollView,
  StatusBar,
  Text,
  TextInput,
  ToastAndroid,
  KeyboardAvoidingView,
  PermissionsAndroid,
  View,
  Platform,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import Colors from '../../../utils/Colors';
import DatePicker from 'react-native-date-picker';
import Tags from 'react-native-tags';
import {CustomPicker} from 'react-native-custom-picker';
import CheckBox from '@react-native-community/checkbox';
import Modal from 'react-native-modal';
import DocumentPicker from 'react-native-document-picker';

export default class RequestNurse extends Component {
  state = {
    fromDate: '',
    toDate: '',
    dateObj: new Date(),
    showPicker: false,
    buttonStyle: styles.button2,
    buttonDisability: false,
    isSelected: false,
    biography: '',
    totalDocuments: '',
    tags: [],
    selectedLocation: '',
    isModalVisible: false,
  };

  constructor(props) {
    super(props);
    this.screenHeight = Dimensions.get('screen').height;
    this.windowHeight = Dimensions.get('window').height;
    this.navbarHeight =
      this.screenHeight - this.windowHeight + StatusBar.currentHeight;
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    this.requestCameraPermission = async () => {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Kashfia App Camera Permission',
            message:
              'Kashfia App needs access to your camera ' +
              'so you can take pictures.',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          try {
            const results = await DocumentPicker.pickMultiple({
              type: [DocumentPicker.types.images, DocumentPicker.types.pdf],
            });
            if (this.state.tags.length + results.length > 3) {
              ToastAndroid.showWithGravityAndOffset(
                'Max 3 files can be uploaded',
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
                25,
                50,
              );
              return;
            }
            for (const res of results) {
              let mb = res.size / 1024.0 / 1024.0;
              mb = mb.toFixed(2);
              let entry = {
                tag: res.name + '  (' + mb + ' MB)',
                filename: res.name,
                type: res.type,
                uri: res.uri,
              };
              this.state.tags.push(entry);
            }
            this.setState(
              {
                tags: this.state.tags,
                totalDocuments: this.state.tags.length + ' pictures selected',
              },
              this.handleChange,
            );
          } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
              throw err;
            }
          }
        } else {
          console.log('Camera permission denied');
        }
      } catch (err) {
        console.warn(err);
      }
    };

    this.locations = ['Home', 'Work'];
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.navigate('user_home', {
      go_back_key: this.props.navigation.key,
    });
    return true;
  }

  renderHeader() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          padding: 20,
        }}>
        <Text
          style={{
            fontFamily: 'urbanist_bold',
            fontSize: 20,
            color: Colors.black,
          }}>
          Select Location
        </Text>
      </View>
    );
  }

  renderField(settings) {
    const {selectedItem, defaultText, getLabel, clear} = settings;
    return (
      <View style={{flexDirection: 'row', alignItems: 'center', width: '100%'}}>
        {!selectedItem && (
          <Text style={[styles.placeholder_text, {flex: 1}]}>
            {defaultText}
          </Text>
        )}
        {selectedItem && (
          <Text style={[styles.text, {flex: 1}]}>{getLabel(selectedItem)}</Text>
        )}
        <Image
          style={{
            resizeMode: 'contain',
            tintColor: Colors.light_grey,
            width: 10,
            height: 10,
          }}
          source={require('../../../assets/ic_down.png')}
        />
      </View>
    );
  }

  handleChange() {
    if (
      this.state.fromDate.trim().length > 0 &&
      this.state.biography.trim().length > 0 &&
      this.state.uris.length > 0 &&
      this.selectedLocation != ''
    ) {
      if (this.state.isSelected) {
        if (this.state.toDate.trim().length > 0) {
          this.setState({
            buttonStyle: styles.button1,
            buttonDisability: true,
          });
        }
      } else {
        this.setState({
          buttonStyle: styles.button1,
          buttonDisability: true,
        });
      }
    } else {
      this.setState({
        buttonStyle: styles.button2,
        buttonDisability: false,
      });
    }
  }

  render() {
    return (
      <SafeAreaView
        style={{flex: 1, backgroundColor: Colors.white, paddingHorizontal: 20}}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
          contentContainerStyle={{
            marginTop: 10,
            paddingBottom: this.navbarHeight,
            marginHorizontal: 7,
          }}
          showVerticalScrollIndicator={false}>
          <View
            style={{
              flexDirection: 'row',
              paddingVertical: 20,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.handleBackButtonClick();
              }}>
              <Image
                style={styles.back}
                source={require('../../../assets/back2.png')}
              />
            </TouchableOpacity>

            <Text style={styles.title}>Request a Nurse</Text>
          </View>

          <Text style={styles.description}>
            Please fill in the details to request a nurse.
          </Text>

          <Text style={[styles.label, {marginTop: 15}]}>
            {this.state.isSelected ? 'From' : 'Date'}
          </Text>
          <DatePicker
            modal
            mode="date"
            maximumDate={new Date(Date.now())}
            textColor={Colors.secondary}
            open={this.state.showPicker}
            date={this.state.dateObj}
            onConfirm={date => {
              this.dateToSend = date.toISOString();
              let strDate =
                (date.getDate() > 9 ? date.getDate() : '0' + date.getDate()) +
                '-' +
                (date.getMonth() > 8
                  ? date.getMonth() + 1
                  : '0' + (date.getMonth() + 1)) +
                '-' +
                date.getFullYear();
              const currentDate = strDate || this.state.date;

              this.setState({
                fromDate: currentDate,
                showPicker: false,
                dateObj: date,
              });
            }}
            onCancel={() => {
              this.setState({
                showPicker: false,
              });
            }}
          />
          <View
            style={
              this.state.fromDate.trim().length > 0
                ? {
                    flexDirection: 'row',
                    borderRadius: 10,
                    borderWidth: 0.7,
                    borderColor: Colors.secondary,
                    paddingHorizontal: 10,
                  }
                : {
                    flexDirection: 'row',
                    borderRadius: 10,
                    borderWidth: 0.7,
                    borderColor: Colors.light_grey,
                    paddingHorizontal: 10,
                  }
            }>
            <TextInput
              style={{
                flex: 1,
                fontSize: 16,
                color: Colors.text_dark,
                fontFamily: 'urbanist_regular',
              }}
              onFocus={() => {
                this.setState({showPicker: true});
              }}
              editable={false}
              value={this.state.fromDate}
              onChangeText={text => this.handleChange()}
              maxLength={15}
              returnKeyType={'done'}
              autoCorrect={false}
              placeholder="Select Date"
              placeholderTextColor={Colors.disabledIcon}
            />
            <TouchableOpacity
              style={{justifyContent: 'center'}}
              onPress={() => {
                this.setState({showPicker: true});
              }}>
              <Image
                style={
                  this.state.fromDate.trim().length > 0
                    ? {
                        height: 20,
                        width: 20,
                        resizeMode: 'contain',
                        tintColor: Colors.secondary,
                      }
                    : {
                        height: 20,
                        width: 20,
                        resizeMode: 'contain',
                        tintColor: Colors.light_grey,
                      }
                }
                source={require('../../../assets/calendar.png')}
              />
            </TouchableOpacity>
          </View>

          {this.state.isSelected && (
            <View>
              <Text style={styles.label}>To</Text>
              <DatePicker
                modal
                mode="date"
                maximumDate={new Date(Date.now())}
                textColor={Colors.secondary}
                open={this.state.showPicker}
                date={this.state.dateObj}
                onConfirm={date => {
                  this.dateToSend = date.toISOString();
                  let strDate =
                    (date.getDate() > 9
                      ? date.getDate()
                      : '0' + date.getDate()) +
                    '-' +
                    (date.getMonth() > 8
                      ? date.getMonth() + 1
                      : '0' + (date.getMonth() + 1)) +
                    '-' +
                    date.getFullYear();
                  const currentDate = strDate || this.state.date;

                  this.setState({
                    toDate: currentDate,
                    showPicker: false,
                    dateObj: date,
                  });
                }}
                onCancel={() => {
                  this.setState({
                    showPicker: false,
                  });
                }}
              />
              <View
                style={
                  this.state.toDate.trim().length > 0
                    ? {
                        flexDirection: 'row',
                        borderRadius: 10,
                        borderWidth: 0.7,
                        borderColor: Colors.secondary,
                        paddingHorizontal: 10,
                      }
                    : {
                        flexDirection: 'row',
                        borderRadius: 10,
                        borderWidth: 0.7,
                        borderColor: Colors.light_grey,
                        paddingHorizontal: 10,
                      }
                }>
                <TextInput
                  style={{
                    flex: 1,
                    fontSize: 16,
                    color: Colors.text_dark,
                    fontFamily: 'urbanist_regular',
                  }}
                  onFocus={() => {
                    this.setState({showPicker: true});
                  }}
                  editable={false}
                  value={this.state.toDate}
                  onChangeText={text => this.handleChange()}
                  maxLength={15}
                  returnKeyType={'done'}
                  autoCorrect={false}
                  placeholder="Select Date"
                  placeholderTextColor={Colors.disabledIcon}
                />
                <TouchableOpacity
                  style={{justifyContent: 'center'}}
                  onPress={() => {
                    this.setState({showPicker: true});
                  }}>
                  <Image
                    style={
                      this.state.toDate.trim().length > 0
                        ? {
                            height: 20,
                            width: 20,
                            resizeMode: 'contain',
                            tintColor: Colors.secondary,
                          }
                        : {
                            height: 20,
                            width: 20,
                            resizeMode: 'contain',
                            tintColor: Colors.light_grey,
                          }
                    }
                    source={require('../../../assets/calendar.png')}
                  />
                </TouchableOpacity>
              </View>
            </View>
          )}

          <CheckBox
            title="More than a day?"
            tintColors={{true: Colors.secondary, false: Colors.disabledIcon}}
            value={this.state.isSelected}
            onValueChange={() => {
              this.setState({
                isSelected: !this.state.isSelected,
              });
            }}
          />

          <Text style={[styles.label, {marginTop: 10}]}>
            Describe your situation*
          </Text>
          <View
            style={
              this.state.biography.trim().length > 0
                ? styles.textInputFilled
                : styles.textInput
            }>
            <TextInput
              style={{
                fontSize: 16,
                color: Colors.text_dark,
                fontFamily: 'urbanist_regular',
                textAlignVertical: 'top',
                height: 120,
              }}
              maxLength={150}
              value={this.state.biography}
              onChangeText={text =>
                this.setState({
                  biography: text,
                })
              }
              returnKeyType={'done'}
              autoCorrect={false}
              placeholder="Describe situation..."
              placeholderTextColor={Colors.disabledIcon}
            />
            <Text
              style={{
                color: Colors.light_grey,
                width: '100%',
                textAlign: 'right',
              }}>
              {this.state.biography.length}/150 characters
            </Text>
          </View>

          <View style={{flexDirection: 'column', marginTop: 10}}>
            <Text style={styles.label}>Upload a picture</Text>
            <View
              style={
                this.state.totalDocuments.trim().length > 0
                  ? {
                      flexDirection: 'row',
                      borderRadius: 10,
                      borderWidth: 0.7,
                      borderColor: Colors.secondary,
                      paddingLeft: 10,
                    }
                  : {
                      flexDirection: 'row',
                      borderRadius: 10,
                      borderWidth: 0.7,
                      borderColor: Colors.light_grey,
                      paddingLeft: 10,
                    }
              }>
              <TextInput
                style={{
                  flex: 1,
                  fontSize: 17,
                  color: Colors.dark_grey,
                  fontFamily: 'urbanist_light',
                }}
                editable={false}
                value={this.state.totalDocuments}
                placeholder="Upload Pictures (15Mb max)"
                placeholderTextColor={Colors.light_grey}
              />
              <TouchableOpacity
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: Colors.secondary,
                  width: '18%',
                  borderBottomRightRadius: 10,
                  borderTopRightRadius: 10,
                }}
                onPress={this.requestCameraPermission}>
                <Image
                  style={{
                    height: 20,
                    width: 20,
                    resizeMode: 'contain',
                    tintColor: Colors.white,
                  }}
                  source={require('../../../assets/upload.png')}
                />
              </TouchableOpacity>
            </View>
            <Tags
              initialTags={this.state.tags}
              textInputProps={{
                editable: false,
              }}
              onChangeTags={tags => console.log(tags)}
              containerStyle={{justifyContent: 'center'}}
              inputContainerStyle={{
                height: 0,
                opacity: 0,
              }}
              inputStyle={{
                backgroundColor: Colors.white,
              }}
              renderTag={({
                tag,
                index,
                onPress,
                deleteTagOnPress,
                readonly,
              }) => (
                <View
                  key={`${tag}-${index}`}
                  style={{
                    flexDirection: 'row',
                    backgroundColor: Colors.tagBackground,
                    padding: 7,
                    marginRight: 10,
                    marginTop: 10,
                    borderRadius: 5,
                  }}>
                  <Text
                    style={{
                      fontFamily: 'urbanist_regular',
                      color: Colors.black,
                      fontSize: 18,
                    }}>
                    {tag.tag}
                  </Text>
                  <TouchableOpacity
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginRight: 10,
                      marginLeft: 20,
                    }}
                    onPress={() => {
                      let temp = this.state.tags;
                      if (index > -1) {
                        temp.splice(index, 1);
                      }
                      let tDocs =
                        temp.length === 0
                          ? ''
                          : temp.length + ' pictures selected';
                      this.setState(
                        {
                          tags: temp,
                          totalDocuments: tDocs,
                        },
                        this.handleChange,
                      );
                    }}>
                    <Image
                      style={{
                        height: 10,
                        width: 10,
                        resizeMode: 'contain',
                      }}
                      source={require('../../../assets/delete.png')}
                    />
                  </TouchableOpacity>
                </View>
              )}
            />
          </View>

          <Text style={styles.label}>Location</Text>
          <CustomPicker
            style={[
              this.state.selectedLocation !== ''
                ? styles.textInputFilled
                : styles.textInput,
              {height: 50, justifyContent: 'center'},
            ]}
            getLabel={item => item}
            fieldTemplate={this.renderField}
            optionTemplate={this.renderOption}
            headerTemplate={this.renderHeader}
            placeholder={'Please select your location'}
            options={this.locations}
            onValueChange={value => {
              this.setState(
                {selectedLocation: value !== null ? value : ''},
                this.handleChange,
              );
            }}
          />

          <Modal
            isVisible={this.state.isModalVisible}
            onRequestClose={function () {
              this.setState({
                isModalVisible: false,
              });
            }.bind(this)}>
            <View
              style={{
                borderRadius: 15,
                padding: 20,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: Colors.white,
              }}>
              <Image
                style={{height: 70, width: 70, resizeMode: 'contain'}}
                source={require('../../../assets/verifying.png')}
              />
              <Text
                style={{
                  fontFamily: 'urbanist_bold',
                  fontSize: 20,
                  color: Colors.black,
                }}>
                Request Submitted
              </Text>
              <Text
                style={{
                  fontFamily: 'urbanist_regular',
                  fontSize: 14,
                  textAlign: 'center',
                  color: Colors.black,
                }}>
                We have received your request. Please check the status of your
                request in the requests section.
              </Text>
              <TouchableOpacity
                style={[
                  this.state.buttonStyle,
                  {
                    marginTop: 20,
                  },
                ]}
                onPress={() => {
                  this.setState(
                    {
                      isModalVisible: false,
                    },
                    this.handleBackButtonClick,
                  );
                }}>
                <Text
                  style={{
                    color: Colors.white,
                    alignSelf: 'center',
                    fontSize: 16,
                    fontFamily: 'urbanist_bold',
                  }}>
                  View Status
                </Text>
              </TouchableOpacity>
            </View>
          </Modal>

          <View
            style={{
              width: '100%',
              marginTop: 15,
            }}>
            <TouchableOpacity
              style={this.state.buttonStyle}
              onPress={() => {
                this.setState({
                  isModalVisible: true,
                });
              }}>
              <Text
                style={{
                  color: Colors.white,
                  alignSelf: 'center',
                  fontSize: 16,
                  fontFamily: 'urbanist_bold',
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontFamily: 'urbanist_semi_bold',
    fontSize: 20,
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
  },
  back: {
    width: 30,
    height: 30,
  },
  description: {
    fontSize: 16,
    marginTop: 10,
    fontFamily: 'urbanist_regular',
    color: Colors.text_light,
  },
  label: {
    fontFamily: 'urbanist_bold',
    fontSize: 16,
    color: Colors.text_dark,
  },
  textInput: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.light_grey,
    fontSize: 17,
    paddingHorizontal: 10,
    color: Colors.dark_grey,
    fontFamily: 'urbanist_light',
  },
  textInputFilled: {
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: Colors.secondary,
    fontSize: 17,
    paddingHorizontal: 10,
    color: Colors.dark_grey,
    fontFamily: 'urbanist_light',
  },
  disabledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.disabledIcon,
  },
  enabledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.white,
  },
  filledIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
    resizeMode: 'contain',
    tintColor: Colors.secondary,
  },
  button1: {
    width: '100%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  button2: {
    width: '100%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.disabled,
    paddingVertical: 15,
    marginVertical: 5,
    borderRadius: 10,
  },
  placeholder_text: {
    fontFamily: 'urbanist_regular',
    fontSize: 17,
    color: Colors.light_grey,
  },
  text: {
    fontFamily: 'urbanist_regular',
    fontSize: 17,
    color: Colors.black,
  },
});
