import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  Pressable,
  SafeAreaView,
  ScrollView,
  StatusBar,
} from 'react-native';
import Globals from '../../../utils/Globals';
import Colors from '../../../utils/Colors';
import CardView from 'react-native-cardview';
import SharedPreferences from 'react-native-shared-preferences';
import {CommonActions} from '@react-navigation/native';

export default class UserMore extends Component {
  state = {
    user: Globals.user,
  };

  constructor(props) {
    super(props);
  }

  render() {
    const {user} = this.state;

    return (
      <SafeAreaView
        style={{
          flex: 1,
          paddingHorizontal: Globals.pHorizontal / 1.5,
          paddingBottom: Globals.mTop / 1.5,
          backgroundColor: 'white',
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignSelf: 'stretch',
            paddingVertical: Globals.mTop,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontFamily: 'urbanist_bold',
              fontSize: Globals.screenWidth * 0.06,
            }}>
            More
          </Text>
        </View>

        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingHorizontal: Globals.mTop / 1.5,
            paddingTop: Globals.mTop / 1.5,
            paddingBottom: Globals.pBottomScrollBar,
          }}>
          <CardView
            style={{
              padding: Globals.mTop / 1.5,
              justifyContent: 'center',
              backgroundColor: Colors.white,
            }}
            cardElevation={5}
            cardMaxElevation={5}
            cornerRadius={10}>
            <Pressable
              style={{flex: 1, flexDirection: 'row'}}
              onPress={() => {
                this.props.navigation.navigate('profile');
              }}>
              <Image
                style={{
                  height: Globals.userInfoLLHeaderHeight,
                  width: Globals.userInfoLLHeaderHeight,
                  borderRadius: Globals.userInfoLLImageHeaderBorder,
                }}
                source={
                  user.avatar === '' || user.avatar === undefined
                    ? require('../../../assets/doc1.png')
                    : {uri: user.avatar}
                }
              />

              <View
                style={{
                  marginLeft: '3%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'urbanist_bold',
                    fontSize: Globals.screenWidth * 0.045,
                  }}>
                  {user.fullName}
                </Text>

                <Text
                  style={{
                    fontFamily: 'urbanist_regular',
                    color: Colors.text_light,
                    fontSize: Globals.screenWidth * 0.035,
                  }}>
                  See your profile
                </Text>
              </View>
            </Pressable>
          </CardView>

          <CardView
            style={{
              padding: Globals.mTop / 1.5,
              marginTop: Globals.mTop,
              justifyContent: 'center',
              backgroundColor: Colors.white,
            }}
            cardElevation={5}
            cardMaxElevation={5}
            cornerRadius={10}>
            <Pressable style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  backgroundColor: Colors.secondary,
                  height: Globals.userInfoLLHeaderHeight,
                  width: Globals.userInfoLLHeaderHeight,
                  borderRadius: Globals.userInfoLLImageHeaderBorder,
                }}>
                <Image
                  style={{
                    height: Globals.userInfoLLHeaderHeight,
                    width: Globals.userInfoLLHeaderHeight,
                    resizeMode: 'center',
                  }}
                  source={require('../../../assets/wallet.png')}
                />
              </View>

              <View
                style={{
                  marginLeft: '3%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'urbanist_bold',
                    fontSize: Globals.screenWidth * 0.045,
                  }}>
                  Wallet
                </Text>

                <Text
                  style={{
                    fontFamily: 'urbanist_regular',
                    color: Colors.text_light,
                    fontSize: Globals.screenWidth * 0.035,
                  }}>
                  IQD 24,000
                </Text>
              </View>
            </Pressable>
          </CardView>

          <CardView
            style={{
              padding: Globals.mTop / 1.5,
              marginTop: Globals.mTop,
              justifyContent: 'center',
              backgroundColor: Colors.white,
            }}
            cardElevation={5}
            cardMaxElevation={5}
            cornerRadius={10}>
            <Pressable
              style={{flex: 1, flexDirection: 'row'}}
              onPress={() => {
                this.props.navigation.navigate('all_locations');
              }}>
              <View
                style={{
                  backgroundColor: Colors.secondary,
                  height: Globals.userInfoLLHeaderHeight,
                  width: Globals.userInfoLLHeaderHeight,
                  borderRadius: Globals.userInfoLLImageHeaderBorder,
                }}>
                <Image
                  style={{
                    height: Globals.userInfoLLHeaderHeight,
                    width: Globals.userInfoLLHeaderHeight,
                    resizeMode: 'center',
                    tintColor: Colors.white,
                  }}
                  source={require('../../../assets/location_outline.png')}
                />
              </View>

              <View
                style={{
                  marginLeft: '3%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'urbanist_bold',
                    fontSize: Globals.screenWidth * 0.045,
                  }}>
                  Locations
                </Text>
              </View>
            </Pressable>
          </CardView>

          <CardView
            style={{
              padding: Globals.mTop / 1.5,
              marginTop: Globals.mTop,
              backgroundColor: Colors.white,
              justifyContent: 'center',
            }}
            cardElevation={5}
            cardMaxElevation={5}
            cornerRadius={10}>
            <Pressable
              style={{flex: 1, flexDirection: 'row'}}
              onPress={() => {
                this.props.navigation.navigate('all_cards');
              }}>
              <View
                style={{
                  backgroundColor: Colors.secondary,
                  height: Globals.userInfoLLHeaderHeight,
                  width: Globals.userInfoLLHeaderHeight,
                  borderRadius: Globals.userInfoLLImageHeaderBorder,
                }}>
                <Image
                  style={{
                    height: Globals.userInfoLLHeaderHeight,
                    width: Globals.userInfoLLHeaderHeight,
                    resizeMode: 'center',
                    tintColor: Colors.white,
                  }}
                  source={require('../../../assets/card.png')}
                />
              </View>

              <View
                style={{
                  marginLeft: '3%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'urbanist_bold',
                    fontSize: Globals.screenWidth * 0.045,
                  }}>
                  Cards
                </Text>
              </View>
            </Pressable>
          </CardView>

          <CardView
            style={{
              padding: Globals.mTop / 1.5,
              marginTop: Globals.mTop,
              backgroundColor: Colors.white,
              justifyContent: 'center',
            }}
            cardElevation={5}
            cardMaxElevation={5}
            cornerRadius={10}>
            <Pressable
              style={{flex: 1, flexDirection: 'row'}}
              onPress={() => {
                this.props.navigation.navigate('refer_friend');
              }}>
              <View
                style={{
                  backgroundColor: Colors.secondary,
                  height: Globals.userInfoLLHeaderHeight,
                  width: Globals.userInfoLLHeaderHeight,
                  borderRadius: Globals.userInfoLLImageHeaderBorder,
                }}>
                <Image
                  style={{
                    height: Globals.userInfoLLHeaderHeight,
                    width: Globals.userInfoLLHeaderHeight,
                    resizeMode: 'center',
                    tintColor: Colors.white,
                  }}
                  source={require('../../../assets/forward.png')}
                />
              </View>

              <View
                style={{
                  marginLeft: '3%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'urbanist_bold',
                    fontSize: Globals.screenWidth * 0.045,
                  }}>
                  Refer a Friend
                </Text>
              </View>
            </Pressable>
          </CardView>

          <CardView
            style={{
              padding: Globals.mTop / 1.5,
              marginTop: Globals.mTop,
              backgroundColor: Colors.white,
              justifyContent: 'center',
            }}
            cardElevation={5}
            cardMaxElevation={5}
            cornerRadius={10}>
            <Pressable style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  backgroundColor: Colors.secondary,
                  height: Globals.userInfoLLHeaderHeight,
                  width: Globals.userInfoLLHeaderHeight,
                  borderRadius: Globals.userInfoLLImageHeaderBorder,
                }}>
                <Image
                  style={{
                    height: Globals.userInfoLLHeaderHeight,
                    width: Globals.userInfoLLHeaderHeight,
                    resizeMode: 'center',
                    tintColor: Colors.white,
                  }}
                  source={require('../../../assets/support_outline.png')}
                />
              </View>

              <View
                style={{
                  marginLeft: '3%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'urbanist_bold',
                    fontSize: Globals.screenWidth * 0.045,
                  }}>
                  Support
                </Text>
              </View>
            </Pressable>
          </CardView>

          <CardView
            style={{
              padding: Globals.mTop / 1.5,
              marginTop: Globals.mTop,
              backgroundColor: Colors.white,
              justifyContent: 'center',
            }}
            cardElevation={5}
            cardMaxElevation={5}
            cornerRadius={10}>
            <Pressable
              style={{flex: 1, flexDirection: 'row'}}
              onPress={() => {
                this.props.navigation.navigate('settings');
              }}>
              <View
                style={{
                  backgroundColor: Colors.secondary,
                  height: Globals.userInfoLLHeaderHeight,
                  width: Globals.userInfoLLHeaderHeight,
                  borderRadius: Globals.userInfoLLImageHeaderBorder,
                }}>
                <Image
                  style={{
                    height: Globals.userInfoLLHeaderHeight,
                    width: Globals.userInfoLLHeaderHeight,
                    resizeMode: 'center',
                    tintColor: Colors.white,
                  }}
                  source={require('../../../assets/settings.png')}
                />
              </View>

              <View
                style={{
                  marginLeft: '3%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'urbanist_bold',
                    fontSize: Globals.screenWidth * 0.045,
                  }}>
                  Settings
                </Text>
              </View>
            </Pressable>
          </CardView>

          <CardView
            style={{
              padding: Globals.mTop / 1.5,
              marginTop: Globals.mTop,
              backgroundColor: Colors.white,
              justifyContent: 'center',
            }}
            cardElevation={5}
            cardMaxElevation={5}
            cornerRadius={10}>
            <Pressable style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  backgroundColor: Colors.secondary,
                  height: Globals.userInfoLLHeaderHeight,
                  width: Globals.userInfoLLHeaderHeight,
                  borderRadius: Globals.userInfoLLImageHeaderBorder,
                }}>
                <Image
                  style={{
                    height: Globals.userInfoLLHeaderHeight,
                    width: Globals.userInfoLLHeaderHeight,
                    resizeMode: 'center',
                    tintColor: Colors.white,
                  }}
                  source={require('../../../assets/logout_outline.png')}
                />
              </View>

              <View
                style={{
                  marginLeft: '3%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'urbanist_bold',
                    fontSize: Globals.screenWidth * 0.045,
                  }}>
                  Logout
                </Text>
              </View>
            </Pressable>
          </CardView>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({});
