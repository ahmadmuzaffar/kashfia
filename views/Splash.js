import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import Colors from '../utils/Colors';
import SharedPreferences from 'react-native-shared-preferences';
import User from '../models/User';
import {CommonActions} from '@react-navigation/native';
import Globals from '../utils/Globals';

export default class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: new User(),
      isLoggedIn: false,
      locations: [],
      cards: [],
    };
  }

  async componentDidMount() {
    await SharedPreferences.getItems(
      ['isLoggedIn', 'user', 'locations', 'cards'],
      function (values) {
        this.setState(
          {
            isLoggedIn: JSON.parse(values[0]),
            user: Object.assign(new User(), JSON.parse(values[1])),
            locations: Object.assign(new Array(), JSON.parse(values[2])),
            cards: Object.assign(new Array(), JSON.parse(values[3])),
          },
          this.dispatchResults,
        );
      }.bind(this),
    );
  }

  dispatchResults() {
    Globals.user = this.state.user;
    Globals.locations = this.state.locations;
    Globals.cards = this.state.cards;

    setTimeout(() => {
      const {isLoggedIn, user} = this.state;
      if (isLoggedIn) {
        if ((!user.isVerified || !user.isApproved) && user.role !== 'user') {
          if (user.isVerified) {
            if (user.biography !== '') {
              this.navigate('under_review');
            } else this.navigate('profile_builder');
          } else this.navigate('under_review');
        } else {
          this.navigate('dashboard');
        }
      } else this.navigate('start');
    }, 1500);
  }

  navigate(screen) {
    const resetAction = CommonActions.reset({
      index: 0,
      routes: [{name: screen}],
    });

    this.props.navigation.dispatch(resetAction);
  }

  render() {
    return (
      <SafeAreaView
        style={{
          backgroundColor: Colors.white,
          flex: 1,
        }}>
        <StatusBar barStyle="dark-content" backgroundColor={Colors.white} />

        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View style={{alignItems: 'flex-end'}}>
            <Image
              style={{
                height: Dimensions.get('window').width * 0.3,
                width: Dimensions.get('window').width * 0.3,
                resizeMode: 'contain',
              }}
              source={require('../assets/app_logo.png')}
            />
            <Image
              style={{
                height: Dimensions.get('window').width * 0.3,
                width: Dimensions.get('window').width * 0.4,
                resizeMode: 'center',
              }}
              source={require('../assets/logo_name.png')}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
