import React, {Component} from 'react';
import {BackHandler} from 'react-native';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import Colors from '../utils/Colors';
import {CommonActions} from '@react-navigation/native';

export default class Congrats extends Component {
  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    return false;
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Image
          source={require('./../assets/success_anim.gif')}
          style={styles.success}
        />

        <Text style={styles.text1}>Congratulations!</Text>

        <Text style={styles.text2}>
          Your number is successfuly verified. Please submit your information to
          get started.
        </Text>

        <View style={{bottom: 20, position: 'absolute', width: '100%'}}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              const resetAction = CommonActions.reset({
                index: 0,
                routes: [{name: 'basic_registration'}],
              });

              this.props.navigation.dispatch(resetAction);
            }}>
            <Text style={styles.text3}>Continue</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  success: {
    width: 250,
    height: 250,
  },
  button: {
    width: '90%',
    alignSelf: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 15,
    fontSize: 16,
    marginVertical: 5,
    borderRadius: 10,
  },
  text1: {
    width: '100%',
    textAlign: 'center',
    fontSize: 32,
    fontFamily: 'urbanist_bold',
    color: Colors.text_dark,
    marginTop: 15,
    marginHorizontal: 30,
  },
  text2: {
    width: '100%',
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'urbanist_regular',
    color: Colors.text_light,
    marginHorizontal: 30,
  },
  text3: {
    color: Colors.white,
    alignSelf: 'center',
    fontSize: 16,
    fontFamily: 'urbanist_bold',
  },
});
