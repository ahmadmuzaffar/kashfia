import User from '../models/User';
import {Dimensions} from 'react-native';

class Globals {
  static screenHeight;
  static screenWidth;

  static mTop;
  static pHorizontal;

  static cardInternalTop;
  static cardHeight;
  static cardImageWidth;

  static sliderBorderRadius;
  static sliderCardWidth;
  static sliderCardHeight;
  static sliderImageHeight;
  static sliderImageWidth;
  static tutorialsCardHeight;

  static pVertical;
  static pRightHeader;

  static userInfoLLHeaderBorder;
  static userInfoLLHeaderHeight;
  static userInfoLLHeaderWidth;
  static userInfoLLImageHeaderBorder;

  static pBottomScrollBar;

  static actionBarBMargin;
  static actionBarHeight;
  static actionBarWidth;

  static headerHeight;

  static currentUser = new User();
  static issues = [];
  static locations = [];

  static getCurrentUser() {
    return currentUser;
  }

  static setResolution(height, width) {
    Globals.screenHeight = height;
    Globals.screenWidth = width;

    Globals.mTop = height * 0.025;
    Globals.pHorizontal = width * 0.05;

    Globals.cardInternalTop = height * 0.015;
    Globals.cardHeight = height - (height * 80) / 100;
    Globals.cardImageWidth = width * 0.4;

    Globals.tutorialsCardHeight = height - (height * 75) / 100;

    Globals.sliderBorderRadius = width * 0.9 * 0.04;
    Globals.sliderCardWidth = width * 0.9;
    Globals.sliderCardHeight = height - (height * 75) / 100;
    Globals.sliderImageHeight = height - (height * 75) / 100;
    Globals.sliderImageWidth = width - (width * 60) / 100;

    Globals.pVertical = height * 0.035;
    Globals.pRightHeader = width * 0.06;

    Globals.userInfoLLHeaderBorder = (width * 0.35) / 2;
    Globals.userInfoLLHeaderHeight = height * 0.055;
    Globals.userInfoLLHeaderWidth = width * 0.27;
    Globals.userInfoLLImageHeaderBorder = (height * 0.07) / 2;

    Globals.pBottomScrollBar = height * 0.075;

    Globals.actionBarBMargin = height * 0.04;
    Globals.actionBarHeight = height * 0.045;
    Globals.actionBarWidth = height * 0.045;

    Globals.headerHeight = height * 0.08;
    Globals.googlePlacesLayoutHeight = height * 0.1;
  }
}

export default Globals;
