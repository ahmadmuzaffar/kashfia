export default class Card {
  constructor() {
    this.cardHolderName = '';
    this.cardNumber = '';
    this.cardType = '';
    this.cardExpiry = '';
    this.cardCode = '';
  }
}
