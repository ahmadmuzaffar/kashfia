export default class User {
  constructor() {
    this.id = '';
    this.fullName = '';
    this.phoneNumber = '';
    this.email = '';
    this.gender = 'none';
    this.dateOfBirth = '';
    this.dateObj = new Date();
    this.role = 'none';
    this.district = '';
    this.city = '';
    this.country = 'Iraq';
    this.biography = '';
    this.specialization = '';
    this.syndicateId = '';
    this.graduationCertificates = {
      tags: [],
      files: [],
      urls: [],
    };
    this.syndicateIdDocuments = {
      tags: [],
      files: [],
      urls: [],
    };
    this.professionalPracticeLetters = {
      tags: [],
      files: [],
      urls: [],
    };
    this.specializationCertificates = {
      tags: [],
      files: [],
      urls: [],
    };
    this.clinicalCertificate = {
      tags: [],
      files: [],
      urls: [],
    };
    this.accessToken = '';
    this.isVerified = false;
    this.isApproved = false;
    this.avatar = '';
    this.authCode = '';
  }
}
