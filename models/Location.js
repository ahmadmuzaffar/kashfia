export default class Location {
  constructor() {
    this.pinLocation = '';
    this.country = '';
    this.locationTitle = '';
    this.street = '';
    this.building = '';
    this.apartmentNumber = '';
    this.nearByLandmark = '';
    this.contactNumber = '';
    this.instructions = '';
  }
}
