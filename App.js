import React, {Component} from 'react';
import type {Node} from 'react';
import {Image, Dimensions} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import User from './models/User';
import Splash from './views/Splash';
import StartScreen from './views/StartScreen';
import SignIn from './views/Auth/SignIn';
import BasicRegistration from './views/Registration/BasicRegistration';
import DoctorRegistration from './views/Registration/DoctorRegistration';
import Otp from './views/Auth/Otp';
import Congrats from './views/Congrats';
import ProfileBuilder from './views/Registration/ProfileBuilder';
import UnderReview from './views/Registration/UnderReview';
import ReferFriend from './views/ReferFriend';

import UserHome from './views/Tabs/User/UserHome';
import UserSearch from './views/Tabs/User/UserSearch';
import UserConsultation from './views/Tabs/User/UserConsultation';
import UserNotification from './views/Tabs/User/UserNotification';
import UserMore from './views/Tabs/User/UserMore';
import Specialization from './views/Tabs/User/Specialization';
import DoctorListing from './views/Tabs/User/DoctorListing';
import RequestNurse from './views/Tabs/User/RequestNurse';
import Tutorials from './views/Tabs/User/Tutorials';
import DoctorDetails from './views/Tabs/User/DoctorDetails';
import Payment from './views/Tabs/User/Payment';
import AddLocation from './views/Tabs/User/AddLocation';
import SaveLocation from './views/Tabs/User/SaveLocation';
import BookingConfirmation from './views/Tabs/User/BookingConfirmation';
import ChatScreen from './views/Tabs/User/ChatScreen';
import UserSettings from './views/Tabs/User/UserSettings';
import AllLocations from './views/Tabs/User/AllLocations';
import AllCards from './views/Tabs/User/AllCards';
import Profile from './views/Tabs/User/Profile';

import DoctorHome from './views/Tabs/Doctor/DoctorHome';
import DoctorConsultation from './views/Tabs/Doctor/DoctorConsultation';
import DoctorRequest from './views/Tabs/Doctor/DoctorRequest';
import DoctorNotification from './views/Tabs/Doctor/DoctorNotification';
import DoctorMore from './views/Tabs/Doctor/DoctorMore';

import NurseHome from './views/Tabs/Nurse/NurseHome';
import NurseJobs from './views/Tabs/Nurse/NurseJobs';
import NurseOrders from './views/Tabs/Nurse/NurseOrders';
import NurseNotification from './views/Tabs/Nurse/NurseNotification';
import NurseMore from './views/Tabs/Nurse/NurseMore';

import SharedPreferences from 'react-native-shared-preferences';
import Colors from './utils/Colors';

import {connect} from 'react-redux';
import {updateUser} from './redux/actions/updateUser.js';
import {updateLogin} from './redux/actions/updateLogin.js';
import Globals from './utils/Globals';

class App extends Component {
  constructor(props) {
    super(props);
    console.disableYellowBox = true;
    Globals.setResolution(
      Dimensions.get('window').height,
      Dimensions.get('window').width,
    );
  }

  render() {
    const Stack = createStackNavigator();
    const Tabs = () => {
      const Tab = createBottomTabNavigator();
      if (Globals.user.role === 'user') {
        return (
          <Tab.Navigator
            initialRouteName="user_home"
            screenOptions={{
              labelStyle: {
                fontFamily: 'urbanist_regular',
              },
              tabBarStyle: {position: 'absolute'},
              tabBarActiveTintColor: Colors.secondary,
            }}>
            <Tab.Screen
              name="user_home"
              component={UserHome}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'Home',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/home.png')}
                    />
                  );
                },
              }}
            />
            <Tab.Screen
              name="user_search"
              component={UserSearch}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'Search',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/search.png')}
                    />
                  );
                },
              }}
            />
            <Tab.Screen
              name="user_consultations"
              component={UserConsultation}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'Consultations',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/consultation.png')}
                    />
                  );
                },
              }}
            />
            <Tab.Screen
              name="user_notifications"
              component={UserNotification}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'Notifications',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/notification.png')}
                    />
                  );
                },
              }}
            />
            <Tab.Screen
              name="user_more"
              component={UserMore}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'More',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/more.png')}
                    />
                  );
                },
              }}
            />
          </Tab.Navigator>
        );
      } else if (Globals.user.role === 'doctor') {
        return (
          <Tab.Navigator
            initialRouteName="doctor_home"
            screenOptions={{
              tabBarStyle: {position: 'absolute'},
            }}>
            <Tab.Screen
              name="doctor_home"
              component={DoctorHome}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'Home',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/home.png')}
                    />
                  );
                },
              }}
            />
            <Tab.Screen
              name="doctor_consultations"
              component={DoctorConsultation}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'Consultations',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/consultation.png')}
                    />
                  );
                },
              }}
            />
            <Tab.Screen
              name="doctor_requests"
              component={DoctorRequest}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'Requests',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/requests.png')}
                    />
                  );
                },
              }}
            />
            <Tab.Screen
              name="doctor_notifications"
              component={DoctorNotification}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'Notifications',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/notification.png')}
                    />
                  );
                },
              }}
            />
            <Tab.Screen
              name="doctor_more"
              component={DoctorMore}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'More',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/more.png')}
                    />
                  );
                },
              }}
            />
          </Tab.Navigator>
        );
      } else if (Globals.user.role === 'nurse') {
        return (
          <Tab.Navigator
            initialRouteName="nurse_home"
            screenOptions={{
              tabBarStyle: {position: 'absolute'},
            }}>
            <Tab.Screen
              name="nurse_home"
              component={NurseHome}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'Home',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/home.png')}
                    />
                  );
                },
              }}
            />
            <Tab.Screen
              name="nurse_job"
              component={NurseJobs}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'Jobs',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/job.png')}
                    />
                  );
                },
              }}
            />
            <Tab.Screen
              name="nurse_orders"
              component={NurseOrders}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'Orders',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/orders.png')}
                    />
                  );
                },
              }}
            />
            <Tab.Screen
              name="nurse_notifications"
              component={NurseNotification}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'Notifications',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/notification.png')}
                    />
                  );
                },
              }}
            />
            <Tab.Screen
              name="nurse_more"
              component={NurseMore}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'More',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/more.png')}
                    />
                  );
                },
              }}
            />
          </Tab.Navigator>
        );
      } else {
        return (
          <Tab.Navigator
            initialRouteName="nurse_home"
            screenOptions={{
              tabBarStyle: {position: 'absolute'},
            }}>
            <Tab.Screen
              name="nurse_home"
              component={NurseHome}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'Home',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/home.png')}
                    />
                  );
                },
              }}
            />
            <Tab.Screen
              name="nurse_job"
              component={NurseJobs}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'Jobs',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/job.png')}
                    />
                  );
                },
              }}
            />
            <Tab.Screen
              name="nurse_orders"
              component={NurseOrders}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'Messages',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/messages.png')}
                    />
                  );
                },
              }}
            />
            <Tab.Screen
              name="nurse_notifications"
              component={NurseNotification}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'Notifications',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/notification.png')}
                    />
                  );
                },
              }}
            />
            <Tab.Screen
              name="nurse_more"
              component={NurseMore}
              options={{
                title: '',
                headerShown: false,
                tabBarLabel: 'More',
                tabBarIcon: ({color}) => {
                  return (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                        tintColor: color,
                      }}
                      source={require('./assets/more.png')}
                    />
                  );
                },
              }}
            />
          </Tab.Navigator>
        );
      }
    };

    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName={'splash'}>
          <Stack.Screen
            name="splash"
            component={Splash}
            options={{
              title: '',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="start"
            component={StartScreen}
            options={{
              title: '',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="sign_in"
            component={SignIn}
            options={{
              title: 'SignIn',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="otp"
            component={Otp}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="congrats"
            component={Congrats}
            options={{
              title: 'Congrats',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="basic_registration"
            component={BasicRegistration}
            options={{
              title: 'Signup',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="doctor_registration"
            component={DoctorRegistration}
            options={{
              title: 'Signup',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="dashboard"
            component={Tabs}
            options={{
              title: 'Dashboard',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="under_review"
            component={UnderReview}
            options={{
              title: 'Under Review',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="profile_builder"
            component={ProfileBuilder}
            options={{
              title: 'Profile',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="specialization"
            component={Specialization}
            options={{
              title: 'Specialization',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="doctor_listing"
            component={DoctorListing}
            options={{
              title: 'Doctor Listing',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="request_nurse"
            component={RequestNurse}
            options={{
              title: 'Request Nurse',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="tutorials"
            component={Tutorials}
            options={{
              title: 'Tutorials',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="doctor_details"
            component={DoctorDetails}
            options={{
              title: 'Doctor Details',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="payment"
            component={Payment}
            options={{
              title: 'Payment',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="add_location"
            component={AddLocation}
            options={{
              title: 'Add Location',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="save_location"
            component={SaveLocation}
            options={{
              title: 'Save Location',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="booking_confirmation"
            component={BookingConfirmation}
            options={{
              title: 'Booking Confirmation',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="chat_screen"
            component={ChatScreen}
            options={{
              title: 'Chat Screen',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="settings"
            component={UserSettings}
            options={{
              title: 'Settings',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="all_locations"
            component={AllLocations}
            options={{
              title: 'Locations',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="all_cards"
            component={AllCards}
            options={{
              title: 'Cards',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="profile"
            component={Profile}
            options={{
              title: 'Profile',
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="refer_friend"
            component={ReferFriend}
            options={{
              title: 'Refer a Friend',
              headerShown: false,
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  isLoggedIn: state.isLoggedIn,
});

const mapDispatchToProps = dispatch => {
  return {
    updateUser: key => dispatch(updateUser(key)),
    updateLogin: key => dispatch(updateLogin(key)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

// const App: () => Node = () => {
//   const Stack = createStackNavigator();

//   return (
//     <NavigationContainer>
//       <Stack.Navigator initialRouteName="start">
//         <Stack.Screen
//           name="start"
//           component={StartScreen}
//           options={{
//             title: '',
//             headerShown: false,
//           }}
//         />
//         <Stack.Screen
//           name="sign_in"
//           component={SignIn}
//           options={{
//             title: 'SignIn',
//             headerShown: false,
//           }}
//         />
//         <Stack.Screen
//           name="otp"
//           component={Otp}
//           options={{
//             headerShown: false,
//           }}
//         />
//         <Stack.Screen
//           name="congrats"
//           component={Congrats}
//           options={{
//             title: 'Congrats',
//             headerShown: false,
//           }}
//         />
//         <Stack.Screen
//           name="basic_registration"
//           component={BasicRegistration}
//           options={{
//             title: 'Signup',
//             headerShown: false,
//           }}
//         />
//         <Stack.Screen
//           name="doctor_registration"
//           component={DoctorRegistration}
//           options={{
//             title: 'Signup',
//             headerShown: false,
//           }}
//         />
//         <Stack.Screen
//           name="dashboard"
//           component={Dashboard}
//           options={{
//             title: 'Dashboard',
//             headerShown: false,
//           }}
//         />
//       </Stack.Navigator>
//     </NavigationContainer>
//   );
// };

// export default App;

// "release_build": "react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res && cd android && gradlew assembleRelease && cd ..",
// react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res/
// headerStyle: {
//   backgroundColor: '#1167b2',
// },
// headerTintColor: '#fff',
// headerTitleStyle: {
//   fontWeight: 'bold',
// },

// doLast {
//     def moveFunc = { resSuffix ->
//         File originalDir = file("$buildDir/generated/res/react/release/${resSuffix}")
//         if (originalDir.exists()) {
//             File destDir = file("$buildDir/../src/main/res/${resSuffix}")
//             ant.move(file: originalDir, tofile: destDir)
//         }
//     }
//     moveFunc.curry('drawable-ldpi').call()
//     moveFunc.curry('drawable-mdpi').call()
//     moveFunc.curry('drawable-hdpi').call()
//     moveFunc.curry('drawable-xhdpi').call()
//     moveFunc.curry('drawable-xxhdpi').call()
//     moveFunc.curry('drawable-xxxhdpi').call()
//     moveFunc.curry('raw').call()
// }
