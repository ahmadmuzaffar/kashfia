import {createStore, combineReducers} from 'redux';
import issuesReducer from '../redux/reducers/issuesReducer';
import UserReducer from '../redux/reducers/UserReducer.js';
import loginReducer from '../redux/reducers/loginReducer';
const rootReducer = combineReducers({
  issuesReducer,
  UserReducer,
  loginReducer,
});
const configureStore = () => {
  return createStore(rootReducer);
};
export default configureStore;
