import {ISSUES_CHANGE} from '../constants';
const initialState = {
  issues: [],
};
function issuesReducer(state = initialState, action){
  switch (action.type) {
    case ISSUES_CHANGE:
      return {
        ...state,
        issues: action.payload,
      };
    default:
      return state;
  }
};
export default issuesReducer;
