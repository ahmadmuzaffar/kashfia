import {UPDATE_USER} from '../constants';
const initialState = {
  user: null,
};
function UserReducer(state = initialState, action) {
  switch (action.type) {
    case UPDATE_USER:
      // console.log('Action: ' + JSON.stringify(action));
      return {
        user: action.payload,
      };
    default:
      return state;
  }
}
export default UserReducer;
