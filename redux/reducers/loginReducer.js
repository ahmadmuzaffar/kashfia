import {IS_LOGGED_IN} from '../constants';
const initialState = {
  isLoggedIn: false,
};
function loginReducer(state = initialState, action) {
  switch (action.type) {
    case IS_LOGGED_IN:
      return {
        ...state,
        isLoggedIn: action.payload,
      };
    default:
      return state;
  }
}
export default loginReducer;
