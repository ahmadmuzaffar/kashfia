import {UPDATE_USER} from '../constants';
export const updateUser = key => ({
  type: UPDATE_USER,
  payload: key,
});
