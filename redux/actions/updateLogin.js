import {IS_LOGGED_IN} from '../constants';
export const updateLogin = key => ({
  type: IS_LOGGED_IN,
  payload: key,
});
