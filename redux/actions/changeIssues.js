import {ISSUES_CHANGE} from '../constants';
export const changeIssues = key => ({
  type: ISSUES_CHANGE,
  payload: key,
});
