export default {
  complete_health: 'Complete Health Solutions',
  early_protection: 'Early Protection for family Health',
  join_now: 'Join Now',
  sign_in: 'Sign In',
};
