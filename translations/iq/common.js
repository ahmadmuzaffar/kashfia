export default {
  complete_health: 'الصحة الكاملة حلول',
  early_protection: 'الحماية المبكرة من أجل صحة الأسرة',
  join_now: 'نضم الان',
  sign_in: 'تسجيل الدخول',
};
